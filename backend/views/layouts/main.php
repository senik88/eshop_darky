<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

$this->registerJs("
    $('select').selectpicker();
    $('.fancybox').fancybox();
", View::POS_END)
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Dashboard', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/uzivatel/default/prihlaseni']];
    } else {
        $menuItems[] = [
            'label' => 'Exporty',
            'items' => [
                [
                    'label' => 'Heureka.cz - zboží',
                    'url' => ['/export/heureka/zbozi']
                ],
                [
                    'label' => 'Heureka.cz - dostupnost',
                    'url' => ['/export/heureka/dostupnost']
                ],
                [
                    'label' => 'Zboží.cz',
                    'url' => ['/export/zbozi/index']
                ],
                [
                    'label' => 'Moje zboží',
                    'url' => ['/export/polozky/moje']
                ]
            ]
        ];
        $menuItems[] = [
            'label' => 'Importy',
            'items' => [
                [
                    'label' => 'V-Garden',
                    'url' => ['/import/vgarden/index']
                ],
                [
                    'label' => 'Cizí zboží',
                    'url' => '[/import/polozky/cizi]'
                ]
            ]
        ];
        $menuItems[] = [
            'label' => 'Eshop',
            'items' => [
                [
                    'label' => 'Číselníky',
                    'items' => [
                        [
                            'label' => 'Kategorie',
                            'url' => ['/eshop/kategorie/index']
                        ],
                        [
                            'label' => 'Výrobci',
                            'url' => ['/eshop/vyrobci/index']
                        ],
                        [
                            'label' => 'Dodavatelé',
                            'url' => ['/eshop/dodavatele/index']
                        ],
                        [
                            'label' => 'Dopravy a platby',
                            'url' => ['/eshop/dopravy/index']
                        ],
                    ]
                ],
                [
                    'label' => 'Zboží',
                    'url' => ['/eshop/polozky/index']
                ],
                [
                    'label' => 'Objednávky',
                    'url' => ['/eshop/objednavky/index']
                ],
                [
                    'label' => 'Faktury',
                    'url' => ['/eshop/faktury/index']
                ],
            ]
        ];
        $menuItems[] = [
            'label' => 'Uživatelé',
            'items' => [
                [
                    'label' => 'Zákazníci',
                    'url' => ['/uzivatel/frontend/index']
                ],
                [
                    'label' => 'Administrátoři',
                    'url' => ['/uzivatel/backend/index']
                ]
            ]
        ];
        $menuItems[] = [
            'label' => 'Odhlásit (' . Yii::$app->user->identity->email . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4"><p class="text-left"><a href="#">Zobrazit frontend</a></p></div>
            <div class="col-md-4"><p class="text-center">&copy; Senovo.cz <?= date('Y') ?></p></div>
            <div class="col-md-4"><p class="text-right"><?= Yii::powered() ?></p></div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
