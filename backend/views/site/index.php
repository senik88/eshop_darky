<?php

/**
 * @var $this yii\web\View
 * @var $mDashboard Dashboard
 */

use backend\components\Dashboard;
use backend\components\grid\GridView;
use backend\modules\eshop\models\Objednavka;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;

$this->title = Yii::$app->name . ' - Dashboard';
?>

<div class="row">
    <div class="col-md-12">
        <div class="h1-buttons">
            <h1><?= $this->title ?></h1>
            <div class="pull-right">
                <?= Html::a('Přidat položku', ['/eshop/polozky/pridat'], ['class' => 'btn btn-info']) ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>Objednávky ke zpracování</h2>

        <p class="hint">
            Objednávky které čekají na zpracování, zkontrolovat postupně od nejstarších (defaultně řazené). Bude řešeno automaticky?
        </p>

        <?= GridView::widget([
            'dataProvider' => $mDashboard->vratObjednavkyKeZpracovani(),
            'emptyText' => "Žádné objednávky ke zpracování.",
            'columns' => [
                'cislo' => [
                    'attribute' => 'cislo',
                    'label' => 'Číslo',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a(
                            $model['cislo'],
                            ['/eshop/objednavky/detail', 'id' => $model['objednavka_pk']]
                        );
                    }
                ],
                'cas_vytvoreni' => [
                    'attribute' => 'cas_vytvoreni',
                    'label' => 'Čas vytvoření',
                    'format' => 'datetime'
                ],
                'uzivatel_jmeno' => [
                    'attribute' => 'uzivatel_jmeno',
                    'label' => 'Zákazník',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a(
                            $model['uzivatel_jmeno'],
                            ['/uzivatel/frontend/detail', 'id' => $model['uzivatel_pk']]
                        );
                    }
                ],
                'stav_objednavky_id' => [
                    'attribute' => 'stav_objednavky_id',
                    'label' => 'Stav',
                    'value' => function ($model) {
                        return Objednavka::itemAlias('stav', $model['stav_objednavky_id']);
                    }
                ],
                'pocet_polozek' => [
                    'attribute' => 'pocet_polozek',
                    'label' => 'Pol.'
                ],
                'cena_polozek' => [
                    'attribute' => 'cena_polozek',
                    'label' => 'Cena položek',
                    'format' => 'currency'
                ],
                'cena_dopravy' => [
                    'attribute' => 'cena_dopravy',
                    'label' => 'Cena dopravy',
                    'format' => 'currency'
                ],
                'akce' => [
                    'header' => 'Akce',
                    'class' => ActionColumn::className(),
                    'template' => '{view} {prevzit}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a(
                                Html::icon('eye-open'),
                                ['/eshop/objednavky/detail', 'id' => $key],
                                [
                                    'title' => 'Náhled'
                                ]
                            );
                        },
                        'prevzit' => function ($url, $model, $key) {
                            return Html::a(
                                Html::icon('share-alt'),
                                ['/eshop/objednavky/prevzit-zpracovani', 'id' => $key],
                                [
                                    'title' => 'Převzít zpracování'
                                ]
                            );
                        }
                    ]
                ]
            ]
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>Faktury po splatnosti</h2>

        <p class="hint">
            Zkontrolovat platby, objednávku odeslat až po zaplacení, pokud se nejedná o osobní odběr nebo dobírku.
            <br>
            Platby se budou párovat automaticky, takže tohle by měla být kontrola jen pro objednávky, že třeba ani po 14 dnech nebyla zaplacena.
        </p>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $mDashboard->vratFakturyPoSplatnosti(),
            'emptyText' => "Žádné faktury po splatnosti."
        ]) ?>
    </div>
</div>