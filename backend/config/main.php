<?php

use \yii\web\Request;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name' => 'Administrace eshopu',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'cs-CZ',
    'modules' => [
        'redactor' => 'yii\redactor\RedactorModule',
        'uzivatel' => [
            'class' => 'backend\modules\uzivatel\UzivatelModule',
        ],
        'eshop' => [
            'class' => 'backend\modules\eshop\EshopModule',
        ],
        'import' => [
            'class' => 'backend\modules\import\ImportModule',
        ],
        'export' => [
            'class' => 'backend\modules\export\ExportModule',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\components\uzivatel\Uzivatel',
            'enableAutoLogin' => true,
            'loginUrl'=>['/uzivatel/default/prihlaseni'],
            'identityCookie' => [
                'name' => '_backendUser', // unique for backend
                'path'=>'/backend/web'  // correct path for the backend app.
            ]
        ],
        'session' => [
            'name' => '_backendSessionId', // unique for backend
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            // here is your backend URL rules
        ],
        'urlManagerFrontEnd' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => str_replace('/backend/web', '/frontend/web', (new Request)->getBaseUrl()),
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'katalog/<id:[\w-]+>/<vyrobce:[\w-]+>' => 'eshop/katalog/index',
                'katalog/<id:[\w-]+>' => 'eshop/katalog/index',
                'polozka/<id:[\w-]+>' => 'eshop/katalog/detail'
            ],
        ],
    ],
    'params' => $params,
];
