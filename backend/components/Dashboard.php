<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 30.09.2016
 * Time: 21:52
 */

namespace backend\components;
use common\components\SqlDataProvider;
use common\models\Objednavka;
use yii\data\ArrayDataProvider;

/**
 * Class Dashboard
 * @package backend\components
 */
class Dashboard
{
    /**
     * @var integer
     */
    public $uzivatel_pk;

    /**
     * @return Dashboard
     */
    public static function factory()
    {
        $instance = new self;
        $instance->uzivatel_pk = \Yii::$app->user->id;

        return $instance;
    }

    /**
     * @return SqlDataProvider
     */
    public function vratObjednavkyKeZpracovani()
    {
        $sql = "
            SELECT
                ov.*
                , (u.prijmeni || ' ' || u.jmeno) AS uzivatel_jmeno
            FROM objednavky_view ov
                JOIN uzivatel u ON ov.uzivatel_pk = u.uzivatel_pk
            WHERE stav_objednavky_id = :stav
        ";

        $params = [
            ':stav' => Objednavka::STAV_POTVRZENA
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'key' => 'objednavka_pk',
            'sort' => [
                'attributes' => ['cas_vytvoreni'],
                'defaultOrder' => [
                    'cas_vytvoreni' => SORT_ASC
                ]
            ],
            'pagination' => [
                'pageSize' => 5
            ]
        ]);
    }

    /**
     *
     */
    public function vratFakturyPoSplatnosti()
    {
        return new ArrayDataProvider([

        ]);
    }
}