<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 02.10.2016
 * Time: 17:20
 */

namespace backend\components\grid;

/**
 * Class GridView
 * @package backend\components\grid
 */
class GridView extends \yii\grid\GridView
{
    /**
     * @var array the HTML attributes for the grid table element.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $tableOptions = [
        'class' => 'table table-striped table-bordered table-condensed'
    ];
}