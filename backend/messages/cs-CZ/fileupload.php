<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.10.2015
 * Time: 17:39
 */

return [
    'Add files' => 'Přidat soubory',
    'Start upload' => 'Nahrát',
    'Cancel upload' => 'Zrušit',
    'Delete' => 'Smazat',
];