<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mVgarden Vgarden
 */
use backend\modules\import\models\Vgarden;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-12 h2-buttons">
        <h2>Import zboží od V-GARDEN</h2>
        <?= Html::a('Importovat zboží', ['/import/vgarden/import'], [
            'class' => 'btn btn-info'
        ]) ?>
    </div>
</div>

<?php
echo $mVgarden;