<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:06
 */

namespace backend\modules\import;


use Yii;
use yii\base\Module;

class ImportModule extends Module
{
    public $controllerNamespace = 'backend\modules\import\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    /**
     * Naplni konfiguraci aplikace cestou k prekladum
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['import/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'cs-CZ',
            'basePath' => '@app/modules/import/messages',
            'fileMap' => [
                'app' => 'app.php',
                'form' => 'form.php'
            ],
        ];
    }

    /**
     * @param $category
     * @param $message
     * @param array $params
     * @param null $language
     * @return string
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('import/'.$category, $message, $params, $language);
    }
}