<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:06
 */

namespace backend\modules\export;


use Yii;
use yii\base\Module;

class ExportModule extends Module
{
    public $controllerNamespace = 'backend\modules\export\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    /**
     * Naplni konfiguraci aplikace cestou k prekladum
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['export/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'cs-CZ',
            'basePath' => '@app/modules/export/messages',
            'fileMap' => [
                'app' => 'app.php',
                'form' => 'form.php'
            ],
        ];
    }

    /**
     * @param $category
     * @param $message
     * @param array $params
     * @param null $language
     * @return string
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('export/'.$category, $message, $params, $language);
    }
}