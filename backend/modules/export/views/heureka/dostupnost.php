<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mHeureka Heureka
 */
use backend\modules\export\models\Heureka;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-12 h2-buttons">
        <h2>Export dostupnosti do Heureka.cz</h2>
        <?= Html::a('Generovat feed', ['/export/heureka/exportDostupnost'], [
            'class' => 'btn btn-info'
        ]) ?>
    </div>
</div>

<?= Html::a('Specifikace XML souboru', 'http://sluzby.heureka.cz/napoveda/dostupnostni-feed/', [
    'class' => '', 'target' => '_blank'
]) ?>

<?php
echo $mHeureka;