<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mZbozi Zbozi
 */
use backend\modules\export\models\Zbozi;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-12 h2-buttons">
        <h2>Export zboží do Zboží.cz</h2>
        <?= Html::a('Generovat feed', ['/export/zbozi/export'], [
            'class' => 'btn btn-info'
        ]) ?>
    </div>
</div>

<?= Html::a('Specifikace XML souboru', 'http://napoveda.seznam.cz/cz/zbozi/specifikace-xml-pro-obchody/specifikace-xml-feedu/', [
    'class' => '', 'target' => '_blank'
]) ?>

<?php
echo $mZbozi;