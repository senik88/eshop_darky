<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:30
 */

namespace backend\modules\export\controllers;


use backend\modules\eshop\forms\PolozkaForm;
use backend\modules\export\models\Heureka;
use common\components\Application;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class HeurekaController
 * @package backend\modules\export\controllers
 */
class HeurekaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['zbozi', 'dostupnost', 'import'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionZbozi()
    {
        //$mVgarden = new Vgarden();
        $mHeureka = 'ahoj';

        return $this->render('zbozi', [
            'mHeureka' => $mHeureka
        ]);
    }

    /**
     *
     */
    public function actionDostupnost()
    {
        //$mVgarden = new Vgarden();
        $mHeureka = 'ahoj dostupnost';

        return $this->render('dostupnost', [
            'mHeureka' => $mHeureka
        ]);
    }

    /**
     *
     */
    public function actionImport()
    {
        /**
         * data z feedu:
         * kategorie, id, ean, product, description, availability, imgurl, price_akce_vat, price_vat
         */

        $feedUrl = "http://objednavky.v-garden.cz/inet/xml/zbozi.xml";
        $xml = simplexml_load_file($feedUrl) or die("Error: Cannot create object");
        foreach ($xml->children() as $zbozi) {
            if ($zbozi->KATEGORIE == "Náhradní díl") continue;
            if ($zbozi->AVAILABILITY <= 0) continue;

            // zatím - zjistit co tam všechno je
            if ($zbozi->children() == "Doplňky") continue;
            if ($zbozi->children() == "AKCE") continue;
            if ($zbozi->children() == "VÝPRODEJ") continue;

            $zbozi = json_decode(json_encode($zbozi));

            $data['PolozkaForm'] = array(
                'externi_id' => $zbozi->ID,
                'ean' => $zbozi->EAN,
                'titulek' => $zbozi->PRODUCT,
                'popis' => $zbozi->DESCRIPTION,
                'cena_aktualni' => $zbozi->PRICE_AKCE_VAT,
                'cena_doporucena' => $zbozi->PRICE_VAT,
                'dodavatel_pk' => 1,
                'vyrobce_pk' => 1,
                'platne_od' => date('Y-m-d H:i:s'),
                'viditelne_od' => date('Y-m-d H:i:s'),
                'skladem' => $zbozi->AVAILABILITY,
                'kategorie_id' => 'nezarazeno'
            );

            $this->zpracuj($data);
        }
    }

    protected function zpracuj($data)
    {

        $mPolozka = new PolozkaForm();

        if (!empty($data)) {
            $mPolozka->load($data);

            if ($mPolozka->validate()) {
                try {
                    $pk = $mPolozka->uloz();
                } catch (\Exception $e) {
                    ddd($e);
                }
            } else {
                ddd($mPolozka->getErrors());
            }
        }

        ddd($pk);
    }
}