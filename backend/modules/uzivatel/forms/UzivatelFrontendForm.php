<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\uzivatel\forms;

use backend\modules\uzivatel\UzivatelModule;
use common\components\ItemAliasTrait;
use common\components\uzivatel\Uzivatel;
use frontend\modules\uzivatel\models\UzivatelFrontend;
use yii\base\Model;

/**
 * Class UzivatelFrontendForm
 * @package backend\modules\uzivatel\forms
 */
class UzivatelFrontendForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $uzivatel_pk;

    /**
     * @var
     */
    public $email;

    /**
     * @var
     */
    public $heslo;

    /**
     * @var string kontrola hesla
     */
    public $heslo2;

    /**
     * @var
     */
    public $stav;

    /**
     * @var
     */
    public $jmeno;

    /**
     * @var
     */
    public $prijmeni;

    /**
     * @var UzivatelFrontend
     */
    protected $_uzivatel;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->_uzivatel == null) {
            $this->_uzivatel = new UzivatelFrontend(['scenario' => Uzivatel::SCENARIO_PRIDAT]);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'heslo', 'heslo2', 'stav'], 'required'],
            [['jmeno', 'prijmeni'], 'required'],
            [['email'], 'email'],
            ['heslo2', 'compare', 'compareAttribute' => 'heslo']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => UzivatelModule::t('form', 'E-mail'),
            'heslo' => UzivatelModule::t('form', 'Heslo'),
            'heslo2' => UzivatelModule::t('form', 'Heslo (kontrola)'),
            'stav' => UzivatelModule::t('form', 'Stav'),
            'jmeno' => UzivatelModule::t('form', 'Jméno'),
            'prijmeni' => UzivatelModule::t('form', 'Příjmení')
        ];
    }

    /**
     * Do instance uzivatele naloaduje data z formulare, uzivatele ulozi a vrati jeho primarni klic
     */
    public function uloz()
    {
        $this->_uzivatel->load($this->attributes, '');

        if ($this->_uzivatel->scenario == UzivatelFrontend::SCENARIO_PRIDAT) {
            $this->_uzivatel->rozhrani = UzivatelFrontend::ROZHRANI_FRONT;
            $this->_uzivatel->cas_registrace = date('Y-m-d H:i:s', time());
            $this->_uzivatel->ip_registrace = \Yii::$app->request->getUserIP();
            $this->_uzivatel->heslo = \Yii::$app->security->generatePasswordHash($this->heslo);
            $this->_uzivatel->validacni_klic = md5($this->_uzivatel->email.$this->_uzivatel->ip_registrace);
        }

        if ($this->_uzivatel->save()) {
            $this->_uzivatel->refresh();

            return $this->_uzivatel->uzivatel_pk;
        } else {
            return false;
        }
    }

    /**
     * Najde uzivatele z predaneho pk a nacte do sebe jeho atributy
     * @param $id
     * @return $this|null
     */
    public function nacti($id)
    {
        $this->_uzivatel = UzivatelFrontend::findOne($id);

        if ($this->_uzivatel == null) {
            return null;
        } else {
            $this->_uzivatel->scenario = UzivatelFrontend::SCENARIO_UPRAVIT;
            $this->_uzivatel->heslo = null; // musim vynulovat heslo, aby se nezobrazilo ve formulari
        }

        if ($this->load($this->_uzivatel->attributes, '')) {
            return $this;
        } else {
            return null;
        }
    }
}