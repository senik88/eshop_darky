<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\uzivatel\forms;

use backend\modules\uzivatel\models\UzivatelBackend;
use common\components\ItemAliasTrait;
use common\components\uzivatel\Uzivatel;
use yii\base\Model;

/**
 * Class UzivatelBackendForm
 * @package backend\modules\uzivatel\forms
 */
class UzivatelBackendForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $email;

    /**
     * @var
     */
    public $heslo;

    /**
     * @var string kontrola hesla
     */
    public $heslo2;

    /**
     * @var
     */
    public $stav;

    /**
     * @var
     */
    public $jmeno;

    /**
     * @var
     */
    public $prijmeni;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'heslo', 'heslo2', 'stav'], 'required'],
            [['jmeno', 'prijmeni'], 'safe'],
            [['email'], 'email'],
            ['heslo2', 'compare', 'compareAttribute' => 'heslo']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'heslo' => 'Heslo',
            'heslo2' => 'Heslo (kontrola)',
            'stav' => 'Stav',
            'jmeno' => 'Jméno',
            'prijmeni' => 'Příjmení'
        ];
    }

    /**
     *
     */
    public function uloz()
    {
        $mUzivatelBackend = new UzivatelBackend();
        $mUzivatelBackend->load($this->attributes, '');

        $mUzivatelBackend->rozhrani = Uzivatel::ROZHRANI_BACK;
        $mUzivatelBackend->cas_registrace = date('Y-m-d H:i:s', time());
        $mUzivatelBackend->ip_registrace = \Yii::$app->request->getUserIP();
        $mUzivatelBackend->heslo = \Yii::$app->security->generatePasswordHash($this->heslo);
        $mUzivatelBackend->validacni_klic = md5($mUzivatelBackend->email.$mUzivatelBackend->ip_registrace);

        if ($mUzivatelBackend->save()) {
            $mUzivatelBackend->refresh();

            return $mUzivatelBackend->uzivatel_pk;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    protected static function itemAliasData()
    {
        return [
            'stavy' => [
                Uzivatel::STAV_AKTIVNI => 'Aktivní',
                Uzivatel::STAV_REGISTRACE => 'Registrace',
                Uzivatel::STAV_NEAKTIVNI => 'Neaktivní',
                Uzivatel::STAV_SMAZANY => 'Smazaný'
            ]
        ];
    }
}