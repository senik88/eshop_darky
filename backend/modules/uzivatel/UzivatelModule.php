<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:06
 */

namespace backend\modules\uzivatel;


use Yii;
use yii\base\Module;

/**
 * Class UzivatelModule
 * @package backend\modules\uzivatel
 */
class UzivatelModule extends Module
{
    public $controllerNamespace = 'backend\modules\uzivatel\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    /**
     * Naplni konfiguraci aplikace cestou k prekladum
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['uzivatel/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'cs-CZ',
            'basePath' => '@app/modules/uzivatel/messages',
            'fileMap' => [
                'app' => 'app.php',
                'form' => 'form.php'
            ],
        ];
    }

    /**
     * @param $category
     * @param $message
     * @param array $params
     * @param null $language
     * @return string
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('uzivatel/'.$category, $message, $params, $language);
    }
}