<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:03
 */

namespace backend\modules\uzivatel\models;


use common\components\uzivatel\Uzivatel;
use Exception;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/**
 * Class UzivatelBackend
 * @package backend\modules\uzivatel\models
 */
class UzivatelBackend extends Uzivatel
{
    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        $this->rozhrani = self::ROZHRANI_BACK;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @param string $username
     * @return null|static
     */
    public static function findByUsername($username)
    {
        return parent::findByUsername($username, self::ROZHRANI_BACK);
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $query = self::find()->where(['rozhrani' => $this->rozhrani]);

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'id' => [
                'attribute' => 'uzivatel_pk',
                'label' => 'ID'
            ],
            'email',
            'stav',
            'cas_registrace',
            'cas_prihlaseni',
            'ip_prihlaseni',
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/uzivatel/backend/detail', 'id' => $model['uzivatel_pk']);
                            break;
                        case 'update':
                            $url = array('/uzivatel/backend/upravit', 'id' => $model['uzivatel_pk']);
                            break;
                        case 'delete':
                            $url = array('/uzivatel/backend/smazat', 'id' => $model['uzivatel_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil',
                            ]),
                            $url,
                            [
                                'title' => 'Upravit',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-trash',
                            ]),
                            $url,
                            [
                                'title' => 'Smazat',
                                'data-pjax' => 0,
                                'data-confirm' => "Opravdu chcete smazat uživatele?",
                                'data-method' => "post",
                            ]
                        );
                    }
                ),
            ]
        ];
    }
}