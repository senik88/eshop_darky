<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $this View
 * @var $mUzivatelForm UzivatelFrontendForm
 */

use backend\modules\uzivatel\forms\UzivatelFrontendForm;
use yii\web\View;
?>

<h2>Úprava zákazníka</h2>

<?= $this->render('_form', [
    'mUzivatelForm' => $mUzivatelForm
]); ?>