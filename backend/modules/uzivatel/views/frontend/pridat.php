<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mUzivatelForm UzivatelFrontendForm
 */
use backend\modules\uzivatel\forms\UzivatelFrontendForm;

?>

<h2>Nový zákazník</h2>

<?= $this->render('_form', [
    'mUzivatelForm' => $mUzivatelForm
]) ?>