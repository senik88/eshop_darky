<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mUzivatel UzivatelFrontend
 */
use frontend\modules\uzivatel\models\UzivatelFrontend;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<h2>Správa zákazníků</h2>

<?= Html::a('Nový', ['/uzivatel/frontend/pridat'], [
    'class' => 'btn btn-info'
]) ?>

<?php
echo GridView::widget([
    'dataProvider' => $mUzivatel->search(),
    'columns' => $mUzivatel->vratSloupce()
]);