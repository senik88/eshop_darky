<?php
/**
 * @var $mUzivatelForm UzivatelFrontendForm
 */
use backend\modules\uzivatel\forms\UzivatelBackendForm;

?>

    <h2>Nový uživatel backendu</h2>

<?= $this->render('_form', [
    'mUzivatelForm' => $mUzivatelForm
]) ?>