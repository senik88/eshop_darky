<?php
/**
 * @var $mUzivatel UzivatelBackend
 */
use backend\modules\uzivatel\models\UzivatelBackend;
use yii\grid\GridView;
use yii\helpers\Html;

?>

    <h2>Správa administrátorů</h2>

<?= Html::a('Nový', ['/uzivatel/backend/pridat'], [
    'class' => 'btn btn-info'
]) ?>

<?php
echo GridView::widget([
    'dataProvider' => $mUzivatel->search(),
    'columns' => $mUzivatel->vratSloupce()
]);