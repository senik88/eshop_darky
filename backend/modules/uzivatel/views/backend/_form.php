<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:22
 *
 * @var $mUzivatelForm UzivatelFrontendForm
 */

use backend\modules\uzivatel\forms\UzivatelFrontendForm;
use common\components\uzivatel\Uzivatel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'layout' => 'horizontal'
]);
?>
<div class="form-fields">
    <?= $form->field($mUzivatelForm, 'email') ?>
    <?= $form->field($mUzivatelForm, 'heslo')->passwordInput() ?>
    <?= $form->field($mUzivatelForm, 'heslo2')->passwordInput() ?>
    <?= $form->field($mUzivatelForm, 'stav')->dropDownList(Uzivatel::itemAlias('stavy')) ?>
    <?= $form->field($mUzivatelForm, 'jmeno') ?>
    <?= $form->field($mUzivatelForm, 'prijmeni') ?>
</div>

<div class="form-actions">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
    <?= Html::a('Zrušit', ['/uzivatel/frontend/index'], [
        'class' => 'btn btn-danger'
    ]) ?>
</div>
<?php
ActiveForm::end();