<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:30
 */

namespace backend\modules\uzivatel\controllers;


use backend\modules\uzivatel\forms\UzivatelBackendForm;
use common\components\Application;
use backend\modules\uzivatel\models\UzivatelBackend;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class BackendController
 * @package backend\modules\uzivatel\controllers
 */
class BackendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'detail', 'pridat', 'upravit', 'smazat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $mUzivatel = new UzivatelBackend();

        return $this->render('index', [
            'mUzivatel' => $mUzivatel
        ]);
    }

    /**
     *
     */
    public function actionPridat()
    {
        $mUzivatelForm = new UzivatelBackendForm();

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mUzivatelForm->load($post);

            if ($mUzivatelForm->validate()) {
                $uzivatel_pk = $mUzivatelForm->uloz();

                if ($uzivatel_pk) {
                    Application::setFlashSuccess('Uživatel uložen.');
                    return $this->redirect(['/uzivatel/backend/detail', 'id' => $uzivatel_pk]);
                } else {
                    Application::setFlashError('Uživatele se nepodařilo uložit.');
                }
            }
        }

        return $this->render('pridat', [
            'mUzivatelForm' => $mUzivatelForm
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        $mUzivatel = UzivatelBackend::findOne($id);

        return $this->render('detail', [
            'mUzivatel' => $mUzivatel
        ]);
    }

    /**
     *
     */
    public function actionUpravit()
    {
        return $this->render('upravit');
    }

    /**
     *
     */
    public function actionSmazat()
    {
        Application::setFlashInfo('Není implementováno :)');
        return $this->redirect(['/uzivatel/backend/index']);
    }
}