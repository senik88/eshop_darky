<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:30
 */

namespace backend\modules\uzivatel\controllers;


use backend\modules\uzivatel\forms\UzivatelFrontendForm;
use common\components\Application;
use frontend\modules\uzivatel\models\UzivatelFrontend;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class FrontendController
 * @package backend\modules\uzivatel\controllers
 */
class FrontendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'detail', 'pridat', 'upravit', 'smazat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $mUzivatel = new UzivatelFrontend();

        return $this->render('index', [
            'mUzivatel' => $mUzivatel
        ]);
    }

    /**
     *
     */
    public function actionPridat()
    {
        $mUzivatelForm = new UzivatelFrontendForm();

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mUzivatelForm->load($post);

            if ($mUzivatelForm->validate()) {
                $uzivatel_pk = $mUzivatelForm->uloz();

                if ($uzivatel_pk) {
                    Application::setFlashSuccess('Uživatel uložen.');
                    return $this->redirect(['/uzivatel/frontend/detail', 'id' => $uzivatel_pk]);
                } else {
                    Application::setFlashError('Uživatele se nepodařilo uložit.');
                }
            }
        }

        return $this->render('pridat', [
            'mUzivatelForm' => $mUzivatelForm
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        $mUzivatel = UzivatelFrontend::findOne($id);

        return $this->render('detail', [
            'mUzivatel' => $mUzivatel
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpravit($id)
    {
        /** @var UzivatelFrontendForm $mUzivatelForm */
        $mUzivatelForm = (new UzivatelFrontendForm())->nacti($id);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mUzivatelForm->load($post);

            if ($mUzivatelForm->validate()) {
                $uzivatel_pk = $mUzivatelForm->uloz();

                if ($uzivatel_pk) {
                    Application::setFlashSuccess('Uživatel uložen.');
                    return $this->redirect(['/uzivatel/frontend/detail', 'id' => $uzivatel_pk]);
                } else {
                    Application::setFlashError('Uživatele se nepodařilo uložit.');
                }
            }
        }

        return $this->render('upravit', [
            'mUzivatelForm' => $mUzivatelForm
        ]);
    }

    /**
     *
     */
    public function actionSmazat()
    {
        Application::setFlashInfo('Není implementováno :)');
        return $this->redirect(['/uzivatel/frontend/index']);
    }
}