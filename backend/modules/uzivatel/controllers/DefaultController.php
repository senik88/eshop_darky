<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:02
 */

namespace backend\modules\uzivatel\controllers;


use common\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package backend\modules\uzivatel\controllers
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['prihlaseni', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['odhlaseni', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionPrihlaseni()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $mLoginForm = new LoginForm();

        if ($mLoginForm->load(Yii::$app->request->post()) && $mLoginForm->login()) {
            return $this->goBack();
        } else {
            return $this->render('prihlaseni', [
                'model' => $mLoginForm
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionOdhlaseni()
    {
        return $this->redirect(['/uzivatel/default/prihlaseni']);
    }

}