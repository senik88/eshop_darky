<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:30
 */

namespace backend\modules\eshop\controllers;


use backend\modules\eshop\forms\DodavatelForm;
use backend\modules\eshop\models\Dodavatel;
use common\components\Application;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class DodavateleController
 * @package backend\modules\eshop\controllers
 */
class DodavateleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'detail', 'pridat', 'upravit', 'smazat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $mDodavatel = new Dodavatel();

        return $this->render('index', [
            'mDodavatel' => $mDodavatel
        ]);
    }

    /**
     *
     */
    public function actionPridat()
    {
        $mDodavatelForm = new DodavatelForm();

        $post = \Yii::$app->request->post();
        $isAjax = \Yii::$app->request->isAjax;

        if (!empty($post)) {
            $mDodavatelForm->load($post);

            if ($mDodavatelForm->validate()) {

                $dodavatel_pk = $mDodavatelForm->uloz();

                if ($dodavatel_pk) {
                    if (!$isAjax)
                        Application::setFlashSuccess('Dodavatel uložen.');
                    $success = true;
                } else {
                    if (!$isAjax)
                        Application::setFlashError('Dodavatele se nepodařilo uložit.');
                    $success = false;
                }

                if ($success) {
                    if ($isAjax) {
                        $response = [
                            'success' => $dodavatel_pk,
                            'dodavatele' => ArrayHelper::map(Dodavatel::find()->all(), 'dodavatel_pk', 'nazev')
                        ];

                        \Yii::$app->response->format = Response::FORMAT_JSON;
                        return $response;
                    } else {
                        return $this->redirect(['/eshop/dodavatele/detail', 'id' => $dodavatel_pk]);
                    }
                }
            }
        }

        // pri renderovani ajaxem nechci cely layout
        if ($isAjax) {
            $view = $this->renderAjax('pridat', [
                'mDodavatelForm' => $mDodavatelForm
            ]);
        } else {
            $view = $this->render('pridat', [
                'mDodavatelForm' => $mDodavatelForm
            ]);
        }

        return $view;
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        $mDodavatel = Dodavatel::findOne($id);

        return $this->render('detail', [
            'mDodavatel' => $mDodavatel
        ]);
    }

    /**
     *
     */
    public function actionUpravit($id)
    {
        /** @var DodavatelForm $mDodavatelForm */
        $mDodavatelForm = (new DodavatelForm())->nacti($id);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mDodavatelForm->load($post);

            if ($mDodavatelForm->validate()) {
                $dodavatel_pk = $mDodavatelForm->uloz();

                if ($dodavatel_pk) {
                    Application::setFlashSuccess('Dodavatel uložen.');
                    return $this->redirect(['/eshop/dodavatele/detail', 'id' => $dodavatel_pk]);
                } else {
                    Application::setFlashError('Dodavatele se nepodařilo uložit.');
                }
            }
        }

        return $this->render('upravit', [
            'mDodavatelForm' => $mDodavatelForm
        ]);
    }

    /**
     *
     */
    public function actionSmazat()
    {
        Application::setFlashInfo('Není implementováno :)');
        return $this->redirect(['/eshop/dodavatele/index']);
    }
}