<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 09.10.2015
 * Time: 20:00
 */

namespace backend\modules\eshop\controllers;


use backend\modules\eshop\forms\ParametrForm;
use backend\modules\eshop\forms\ParametryPolozkyForm;
use backend\modules\eshop\forms\PolozkaForm;
use backend\modules\eshop\models\EshopCena;
use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use common\components\Application;
use common\forms\GalerieForm;
use common\models\ParametrPolozky;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class PolozkyController
 * @package backend\modules\eshop\controllers
 */
class PolozkyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'detail', 'pridat', 'upravit', 'smazat', 'obrazky', 'parametry'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|void
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->id == 'obrazky') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $mPolozka = new Polozka();

        $get = Yii::$app->request->get();
        $rozsireny = false;
        if (!empty($get)) {
            $mPolozka->load($get);

            // podle vyplnenych parametru rozhodnu zobrazeni filtru... casem dat do cookie?
            if ($mPolozka->kategorie_id != null
                || $mPolozka->zobrazit == 'AKTUALNI'
            ) {
                $rozsireny = true;
            }
        }

        return $this->render('index', [
            'mPolozka' => $mPolozka,
            'rozsireny' => $rozsireny
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionDetail($id)
    {
        /** @var Polozka $mPolozka */
        $mPolozka = (new Polozka())->nactiPodlePk($id);
        if ($mPolozka == null) {
            throw new HttpException(404, 'Položka neexistuje');
        }

        $mEshopCena = new EshopCena();
        /** @var Kategorie $mKategorie */
        $mKategorie = Kategorie::findOne(['kategorie_id' => $mPolozka->kategorie_id]);

        $fParametr = new ParametrForm(['polozka_pk' => $id]);
        $fGalerie = new GalerieForm();
        $fGalerie->polozka_pk = $mPolozka->polozka_pk;

        $post = Yii::$app->request->post();

        if (isset($post['akce'])) {
            switch ($post['akce']) {
                case 'parametr':
                    ddd($post);
                    break;
                default:
                    throw new HttpException(400, 'Neznámá akce');
            }
        }

        return $this->render('detail', [
            'mPolozka' => $mPolozka,
            'mEshopCena' => $mEshopCena,
            'mKategorie' => $mKategorie,
            'fGalerie' => $fGalerie,
            'fParametr' => $fParametr,
            'aParametry' => $mPolozka->vratParametry()
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionPridat()
    {
        $mPolozka = new PolozkaForm();

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mPolozka->load($post);

            if ($mPolozka->validate()) {
                try {
                    $pk = $mPolozka->uloz();

                    Application::setFlashSuccess('Položka eshopu byla vytvořena.');
                    return $this->redirect(['/eshop/polozky/detail', 'id' => $pk]);
                } catch (\Exception $e) {
                    ddd($e);
                }
            } else {
                ddd($mPolozka->getErrors());
            }
        }

        return $this->render('pridat', [
            'mPolozka' => $mPolozka
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function actionUpravit($id)
    {
        $mPolozka = (new PolozkaForm())->nactiPodlePk($id);

        $post = Yii::$app->request->post();

        if (!empty($post)) {
            $mPolozka->load($post);

            if ($mPolozka->validate()) {
                try {
                    $pk = $mPolozka->uloz();

                    Application::setFlashSuccess('Položka eshopu byla upravena.');
                    return $this->redirect(['/eshop/polozky/detail', 'id' => $pk]);
                } catch (\Exception $e) {
                    ddd($e);
                }
            } else {
                ddd($mPolozka->getErrors());
            }
        }

        return $this->render('upravit', [
            'mPolozka' => $mPolozka
        ]);
    }

    /**
     * @param $polozka
     * @return array
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionObrazky($polozka)
    {
        Yii::$app->response->getHeaders()->set('Vary', 'Accept');
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [];
        $mPolozka = (new Polozka())->nactiPodlePk($polozka);
        $fGalerie = new GalerieForm();

        if ($mPolozka == null) {
            throw new NotFoundHttpException('Položka eshopu nenalezena');
        } else {
            $fGalerie->polozka_pk = $mPolozka->polozka_pk;
        }

        $fGalerie->soubor = UploadedFile::getInstance($fGalerie, 'soubor');

        if ($fGalerie->soubor != null) {
            $nazev = $fGalerie->uloz();
            if ($nazev) {
                $response['files'][] = [
                    'name' => $fGalerie->soubor->name,
                    'type' => $fGalerie->soubor->type,
                    'size' => $fGalerie->soubor->size,
                    'url' => $fGalerie->getImageUrl() . DIRECTORY_SEPARATOR . $nazev,
                    'thumbnailUrl' => $fGalerie->getThumbnailUrl() . DIRECTORY_SEPARATOR . $nazev,
                    'deleteUrl' => Url::to(['delete'/*, 'id' => $fGalerie->id*/]),
                    'deleteType' => 'POST'
                ];
            } else {
                $response[] = ['error' => 'Nepodařilo se uložit obrázek'];
            }
        } else {
            if ($fGalerie->hasErrors(['soubor'])) {
                $response[] = ['error' => implode('; ', $fGalerie->getErrors('soubor'))];
            } else {
                throw new HttpException(500, Yii::t('app', 'Could not upload file.'));
            }
        }

        return $response;
    }

    /**
     * Nastaveni jednotlivych parametru produktu
     */
    public function actionParametry()
    {
        $post = Yii::$app->request->post();

        if (!empty($post)) {
            if (!isset($post['ParametrForm']) && !isset($post['ParametrForm']['polozka_pk'])) {
                Yii::error('nelze ulozit parametry, neznam polozka_pk');
                Application::setFlashError("Neočekávaná chyba, opakujte akci později.");

                return $this->redirect(['/eshop/polozky/index']);
            } else {
                $polozka_pk = $post['ParametrForm']['polozka_pk'];
            }

            $form = new ParametryPolozkyForm(['polozka_pk' => $polozka_pk]);
            if ($form->load($post)) {
                $mParametry = [];
                $valid = $form->validate();

                foreach ($form->parametry as $data) {
                    $mParametrPolozky = new ParametrPolozky(['polozka_pk' => $form->polozka_pk]);
                    $mParametrPolozky->load((array) $data);

                    $valid = $mParametrPolozky->validate() && $valid;

                    if ($valid) {
                        $mParametry[] = $mParametrPolozky;
                    }
                }

                if ($valid) {
                    try {
                        $form->parametry = $mParametry;
                        $form->preulozVse();

                        Application::setFlashSuccess('Parametry položky byly uloženy.');
                    } catch (\Exception $e) {
                        Yii::error("chyba pri ukladani parametru k polozce {$form->polozka_pk}: {$e->getMessage()}");
                        Application::setFlashError("Neočekávaná chyba, opakujte akci později");
                    }
                } else {
                    Application::setFlashError("Chyba při validaci parametrů, zkontrolujte zadávaná data.");
                }

                return $this->redirect(['/eshop/polozky/detail', 'id' => $form->polozka_pk, '#' => 'parametry']);
            }
        }

        // todo opravdu to tu nechat?...
        Application::setFlashInfo("Chyba v datech, ruční dotaz na stránku?");
        return $this->redirect(['/eshop/polozky/index']);
    }
}