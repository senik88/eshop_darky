<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 09.10.2015
 * Time: 20:00
 */

namespace backend\modules\eshop\controllers;

use backend\modules\eshop\forms\ObjednavkaSearchForm;
use backend\modules\eshop\models\Objednavka;
use common\components\Application;
use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\PlatbaDopravy;
use common\models\ZpusobDopravy;
use common\models\ZpusobPlatby;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;


/**
 * Class ObjednavkyController
 * @package backend\modules\eshop\controllers
 */
class ObjednavkyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index', 'detail', 'stornovat', 'prevzit-zpracovani',
                            'ajax-zkontroluj-pred-odeslanim'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $mObjednavka = new Objednavka();
        $fSearch = new ObjednavkaSearchForm();

        $post = Yii::$app->request->get();
        if (!empty($post)) {
            $fSearch->load($post);
        }

        return $this->render('index', [
            'mObjednavka' => $mObjednavka,
            'fSearch' => $fSearch
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionDetail($id)
    {
        $mObjednavka = (new Objednavka())->nactiPodlePk($id);

        $mDodaciUdaje = (new DodaciUdaje())->nactiPodlePk($mObjednavka->dodaci_udaje_pk);
        $mFakturacniUdaje = (new FakturacniUdaje())->nactiPodlePk($mObjednavka->fakturacni_udaje_pk);
        /** @var PlatbaDopravy $mPlatbaDopravy */
        $mPlatbaDopravy = (new PlatbaDopravy())->findOne(['platba_dopravy_id' => $mObjednavka->platba_dopravy_id]);

        $mZpusobDopravy = (new ZpusobDopravy())->findOne($mPlatbaDopravy->zpusob_dopravy_pk);
        $mZpusobPlatby = (new ZpusobPlatby())->findOne($mPlatbaDopravy->zpusob_platby_pk);

        return $this->render('detail', [
            'mObjednavka' => $mObjednavka,
            'mDodaciUdaje' => $mDodaciUdaje,
            'mFakturacniUdaje' => $mFakturacniUdaje,
            'mPlatbaDopravy' => $mPlatbaDopravy,
            'mZpusobDopravy' => $mZpusobDopravy,
            'mZpusobPlatby' => $mZpusobPlatby,
        ]);
    }

    /**
     * todo doplnit do DB sloupec zpracovava_uzivatel_pk
     * @param $id
     * @return \yii\web\Response
     */
    public function actionPrevzitZpracovani($id)
    {
        /** @var Objednavka $mObjednavka */
        $mObjednavka = (new Objednavka())->nactiPodlePk($id);

        if (false) {
            Application::setFlashWarning("Tuto objednávku již zpracovává jiný uživatel.");
            return $this->redirect(Yii::$app->request->referrer);
        }

        // ok, prejdu na detail
        if ($mObjednavka->priradZpracovani(Yii::$app->user->id)) {
            Application::setFlashSuccess("Objednávka byla přiřazena.");
            return $this->redirect(['/eshop/objednavky/detail', 'id' => $mObjednavka->objednavka_pk]);
        }
        // chyba, zustan kde jsi
        else {
            Application::setFlashError("Chyba při přiřazování objednávky, opakujte akci později.");
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param $id int pk objednavky
     */
    public function actionStornovat($id)
    {
        ddd('@implement me... :)');
    }

    /**
     * zkontroluje stav objednavky, pokud bude neco chybet, tak vyvola modal pro doplneni
     *      - napr. nemuzu odeslat objednavku, dokud nema vygenerovanou fakturu
     *      - pokud ma doprava tracking, dopta se na trackovaci cislo
     */
    public function actionAjaxZkontrolujPredOdeslanim()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $pk = Yii::$app->request->post('pk');

        $mObjednavka = (new Objednavka())->nactiPodlePk($pk);
        if ($mObjednavka == null) {
            return ['error' => 'Objednávka neexistuje'];
        }

        return ['success' => $pk];
    }
}