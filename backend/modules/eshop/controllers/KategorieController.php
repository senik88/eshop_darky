<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:30
 */

namespace backend\modules\eshop\controllers;


use backend\modules\eshop\forms\KategorieForm;
use backend\modules\eshop\models\Kategorie;
use common\components\Application;
use common\models\Parametr;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\Controller;

/**
 * Class KategorieController
 * @package backend\modules\eshop\controllers
 */
class KategorieController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'detail', 'pridat', 'upravit', 'smazat', 'ajax-pridat-parametr'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $mKategorie = new Kategorie();

        return $this->render('index', [
            'mKategorie' => $mKategorie
        ]);
    }

    /**
     *
     */
    public function actionPridat()
    {
        $mKategorieForm = new KategorieForm();

        $post = \Yii::$app->request->post();
        $isAjax = \Yii::$app->request->isAjax;

        $aParametry = $mKategorieForm->nactiParametry();

        if (!empty($post)) {
            $mKategorieForm->load($post);

            if ($mKategorieForm->validate()) {

                $kategorie_pk = $mKategorieForm->uloz();
                /** @var Kategorie $mKategorie */
                $mKategorie = Kategorie::findOne($kategorie_pk);

                if ($kategorie_pk) {
                    if (!$isAjax)
                        Application::setFlashSuccess('Výrobce uložen.');

                    $success = true;
                } else {
                    if (!$isAjax)
                        Application::setFlashError('Výrobce se nepodařilo uložit.');

                    $success = false;
                }

                if ($success) {
                    if ($isAjax) {
                        $response = [
                            'success' => $mKategorie->kategorie_id,
                            'kategorie' => ArrayHelper::map(Kategorie::find()->all(), 'kategorie_id', 'nazev')
                        ];

                        \Yii::$app->response->format = Response::FORMAT_JSON;
                        return $response;
                    } else {
                        return $this->redirect(['/eshop/kategorie/detail', 'id' => $kategorie_pk]);
                    }
                }
            }
        }

        // pri renderovani ajaxem nechci cely layout
        if ($isAjax) {
            $view = $this->renderAjax('pridat', [
                'mKategorieForm' => $mKategorieForm,
                'aParametry' => $aParametry
            ]);
        } else {
            $view = $this->render('pridat', [
                'mKategorieForm' => $mKategorieForm,
                'aParametry' => $aParametry
            ]);
        }

        return $view;
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        /** @var Kategorie $mKategorie */
        $mKategorie = Kategorie::findOne($id);
        $aParametry = $mKategorie->vratParametry();

        return $this->render('detail', [
            'mKategorie' => $mKategorie,
            'aParametry' => $aParametry
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpravit($id)
    {
        /** @var KategorieForm $mKategorieForm */
        $mKategorieForm = (new KategorieForm())->nacti($id);

        $post = \Yii::$app->request->post();

        $aParametry = $mKategorieForm->nactiParametry();

        if (!empty($post)) {

            $parametry = [];
            $mKategorieForm->load($post);
            $valid = $mKategorieForm->validate();

            if (isset($post['KategorieForm']['parametry'])) {
                $postParametry = $post['KategorieForm']['parametry'];

                if (is_array($postParametry)) {
                    foreach($postParametry as $parametr) {
                        $mParametr = new Parametr();
                        $mParametr->load($parametr);

                        $parametry[] = $mParametr;

                        $valid = $mParametr->validate() && $valid;
                    }

                    $mKategorieForm->parametry = $parametry;
                }
            }

            if ($valid) {
                $kategorie_pk = $mKategorieForm->uloz();

                if ($kategorie_pk) {
                    Application::setFlashSuccess('Kategorie uložena.');
                    return $this->redirect(['/eshop/kategorie/detail', 'id' => $kategorie_pk]);
                } else {
                    Application::setFlashError('Kategorii se nepodařilo uložit.');
                }
            }
        }

        return $this->render('upravit', [
            'mKategorieForm' => $mKategorieForm,
            'aParametry' => $aParametry
        ]);
    }

    /**
     *
     */
    public function actionSmazat()
    {
        Application::setFlashInfo('Není implementováno :)');
        return $this->redirect(['/uzivatel/frontend/index']);
    }

    /**
     * @return string
     */
    public function actionAjaxPridatParametr()
    {
        $mParametr = new Parametr();

        $klic = \Yii::$app->request->post('klic', 0);
        $pk = \Yii::$app->request->post('parametr_pk');

        if ($pk != null) {
            $mParametr->nactiPodlePk($pk);
        }

        return $this->renderAjax('_parametr', [
            'mParametr' => $mParametr,
            'klic' => $klic
        ]);
    }
}