<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:30
 */

namespace backend\modules\eshop\controllers;


use backend\modules\eshop\forms\VyrobceForm;
use backend\modules\eshop\models\Vyrobce;
use common\components\Application;
use common\models\Logo;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class VyrobciController
 * @package backend\modules\eshop\controllers
 */
class VyrobciController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'detail', 'pridat', 'upravit', 'smazat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $mVyrobce = new Vyrobce();

        return $this->render('index', [
            'mVyrobce' => $mVyrobce
        ]);
    }

    /**
     *
     */
    public function actionPridat()
    {
        $mVyrobceForm = new VyrobceForm();

        $post = \Yii::$app->request->post();
        $isAjax = \Yii::$app->request->isAjax;

        if (!empty($post)) {
            $mVyrobceForm->load($post);
            $mVyrobceForm->logo = UploadedFile::getInstance($mVyrobceForm, 'logo');

            if ($mVyrobceForm->validate()) {
                $vyrobce_pk = $mVyrobceForm->uloz();

                if ($vyrobce_pk) {
                    if (!$isAjax)
                        Application::setFlashSuccess('Výrobce uložen.');

                    $success = true;
                } else {
                    if (!$isAjax)
                        Application::setFlashError('Výrobce se nepodařilo uložit.');

                    $success = false;
                }

                if ($success) {
                    if ($isAjax) {
                        $response = [
                            'success' => $vyrobce_pk,
                            'vyrobci' => ArrayHelper::map(Vyrobce::find()->all(), 'vyrobce_pk', 'nazev')
                        ];

                        \Yii::$app->response->format = Response::FORMAT_JSON;
                        return $response;
                    } else {
                        return $this->redirect(['/eshop/vyrobci/detail', 'id' => $vyrobce_pk]);
                    }
                }
            }
        }

        // pri renderovani ajaxem nechci cely layout
        if ($isAjax) {
            $view = $this->renderAjax('pridat', [
                'mVyrobceForm' => $mVyrobceForm
            ]);
        } else {
            $view = $this->render('pridat', [
                'mVyrobceForm' => $mVyrobceForm
            ]);
        }

        return $view;
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        /** @var Vyrobce $mVyrobce */
        $mVyrobce = Vyrobce::findOne($id);

        $mLogo = (new Logo())->nactiPodlePk($mVyrobce->logo_pk);

        return $this->render('detail', [
            'mVyrobce' => $mVyrobce,
            'mLogo' => $mLogo
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpravit($id)
    {
        /** @var VyrobceForm $mVyrobceForm */
        $mVyrobceForm = (new VyrobceForm())->nacti($id);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mVyrobceForm->load($post);
            $mVyrobceForm->logo = UploadedFile::getInstance($mVyrobceForm, 'logo');

            if ($mVyrobceForm->validate()) {
                $vyrobce_pk = $mVyrobceForm->uloz();

                if ($vyrobce_pk) {
                    Application::setFlashSuccess('Výrobce uložen.');
                    return $this->redirect(['/eshop/vyrobci/detail', 'id' => $vyrobce_pk]);
                } else {
                    Application::setFlashError('Výrobce se nepodařilo uložit.');
                }
            }
        }

        return $this->render('upravit', [
            'mVyrobceForm' => $mVyrobceForm
        ]);
    }

    /**
     * Hromadne prevedeni zbozi pod jineho vyrobce, abych mohl toho stareho smazat.
     */
    public function actionPrevestZbozi()
    {
        $post = \Yii::$app->request->post();
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSmazat($id)
    {
        /** @var Vyrobce $mVyrobce */
        $mVyrobce = Vyrobce::findOne($id);
        if ($mVyrobce == null) {
            Application::setFlashError("Neexistující výrobce");
            return $this->redirect(['/eshop/vyrobci/index']);
        }

        if ($mVyrobce->maProdukty()) {
            Application::setFlashWarning("Výrobce \"{$mVyrobce->nazev}\" nelze smazat, má navázané zboží.");
        } else {
            try {
                $mVyrobce->delete();
                Application::setFlashSuccess("Výrobce \"{$mVyrobce->nazev}\" smazán.");
            } catch (\Exception $e) {
                \Yii::error("chyba pri mazani vyrobce {$mVyrobce->vyrobce_pk} : {$e->getMessage()}");
                Application::setFlashError("Chyba při mazání výrobce, opakujte akci později.");
            }
        }

        return $this->redirect(['/eshop/vyrobci/index']);
    }
}