<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:30
 */

namespace backend\modules\eshop\controllers;


use backend\modules\eshop\forms\DopravaForm;
use backend\modules\eshop\forms\PlatbaForm;
use backend\modules\eshop\forms\SpojeniForm;
use common\components\Application;
use common\models\PlatbaDopravy;
use common\models\ZpusobDopravy;
use common\models\ZpusobPlatby;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Class DopravyController
 * @package backend\modules\eshop\controllers
 */
class DopravyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'detail-dopravy', 'pridat-dopravu', 'pridat-platbu', 'pridat-spojeni', 'upravit-dopravu', 'smazat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $mZpusobDopravy = new ZpusobDopravy();
        $mZpusobPlatby = new ZpusobPlatby();
        $mPlatbaDopravy = new PlatbaDopravy();

        $fDopravaForm = new DopravaForm();

        return $this->render('index', [
            'mZpusobDopravy' => $mZpusobDopravy,
            'mZpusobPlatby' => $mZpusobPlatby,
            'mPlatbaDopravy' => $mPlatbaDopravy,
            'fDopravaForm' => $fDopravaForm
        ]);
    }

    /**
     *
     */
    public function actionPridatDopravu()
    {
        $mDopravaForm = new DopravaForm();

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mDopravaForm->load($post);

            if ($mDopravaForm->validate()) {

                $zpusob_dopravy_pk = $mDopravaForm->uloz();

                if ($zpusob_dopravy_pk) {
                    Application::setFlashSuccess('Způsob dopravy uložen.');
                    return $this->redirect(['/eshop/dopravy/index', 'id' => $zpusob_dopravy_pk]);
                } else {
                    Application::setFlashError('Způsob dopravy se nepodařilo uložit.');
                }
            }
        }

        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('pridatDopravu', [
                'mDopravaForm' => $mDopravaForm
            ]);
        } else {
            return $this->render('pridatDopravu', [
                'mDopravaForm' => $mDopravaForm
            ]);
        }
    }

    /**
     *
     */
    public function actionPridatPlatbu()
    {
        $mPlatbaForm = new PlatbaForm();

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mPlatbaForm->load($post);

            if ($mPlatbaForm->validate()) {

                $zpusob_platby_pk = $mPlatbaForm->uloz();

                if ($zpusob_platby_pk) {
                    Application::setFlashSuccess('Způsob platby uložen.');
                    return $this->redirect(['/eshop/dopravy/index', 'id' => $zpusob_platby_pk]);
                } else {
                    Application::setFlashError('Způsob platby se nepodařilo uložit.');
                }
            }
        }

        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('pridatPlatbu', [
                'mPlatbaForm' => $mPlatbaForm
            ]);
        } else {
            return $this->render('pridatPlatbu', [
                'mPlatbaForm' => $mPlatbaForm
            ]);
        }
    }

    /**
     *
     */
    public function actionPridatSpojeni()
    {
        $mSpojeniForm = new SpojeniForm();

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mSpojeniForm->load($post);

            if ($mSpojeniForm->validate()) {

                $platba_dopravy_pk = $mSpojeniForm->uloz();

                if ($platba_dopravy_pk) {
                    Application::setFlashSuccess('Spojení platby a dopravy uloženo.');
                    return $this->redirect(['/eshop/dopravy/index', 'id' => $platba_dopravy_pk]);
                } else {
                    Application::setFlashError('Spojení platby a dopravy se nepodařilo uložit.');
                }
            }
        }

        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('pridatSpojeni', [
                'mSpojeniForm' => $mSpojeniForm,
            ]);
        } else {
            return $this->render('pridatSpojeni', [
                'mSpojeniForm' => $mSpojeniForm,
            ]);
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetailDopravy($id)
    {
        $mZpusobDopravy = ZpusobDopravy::findOne($id);

        return $this->render('detailDopravy', [
            'mZpusobDopravy' => $mZpusobDopravy
        ]);
    }

    /**
     *
     */
    public function actionUpravitDopravu($id)
    {
        /** @var DopravaForm $mDopravaForm */
        $mDopravaForm = (new DopravaForm())->nacti($id);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mDopravaForm->load($post);
            $mDopravaForm->logo = UploadedFile::getInstance($mDopravaForm, 'logo');

            if ($mDopravaForm->validate()) {
                $zpusob_dopravy_pk = $mDopravaForm->uloz();

                if ($zpusob_dopravy_pk) {
                    Application::setFlashSuccess('Způsob dopravy uložen.');
                    return $this->redirect(['/eshop/dopravy/detail-dopravy', 'id' => $zpusob_dopravy_pk]);
                } else {
                    Application::setFlashError('Způsob dopravy se nepodařilo uložit.');
                }
            }
        }

        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('upravitDopravu', [
                'mDopravaForm' => $mDopravaForm
            ]);
        } else {
            return $this->render('upravitDopravu', [
                'mDopravaForm' => $mDopravaForm
            ]);
        }
    }

    /**
     *
     */
    public function actionSmazat()
    {
        Application::setFlashInfo('Není implementováno :)');
        return $this->redirect(['/eshop/dodavatele/index']);
    }
}