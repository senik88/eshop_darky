<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 09.10.2015
 * Time: 19:52
 */

namespace backend\modules\eshop\forms;


use yii\base\Model;

/**
 * Class PolozkaForm
 * @package backend\modules\eshop\forms
 */
class PolozkaForm extends Model
{
    const SCENARIO_PRIDAT = 'pridat';
    const SCENARIO_UPRAVIT = 'upravit';

    public $polozka_pk;

    public $polozka_id;

    public $titulek;

    public $subtitulek;

    public $popis;

    public $dodavatel_pk;

    public $vyrobce_pk;

    public $kategorie_id;

    public $viditelne_od;

    public $viditelne_do;

    public $skladem;

    public $blokace;

    public $cena_doporucena;

    public $cena_aktualni;

    public $platne_od;

    public $platne_do;

    public $externi_id;

    public $ean;

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_UPRAVIT] = ['polozka_pk', 'titulek', 'subtitulek', 'popis', 'vyrobce_pk', 'dodavatel_pk', 'kategorie_id', 'viditelne_od', 'skladem'];

        return $scenarios;
    }

    /**
     * @return array
     */
    public function rules()
    {
        if ($this->scenario == self::SCENARIO_UPRAVIT) {
            return [
                [['titulek', 'popis', 'vyrobce_pk', 'dodavatel_pk', 'kategorie_id', 'viditelne_od', 'skladem'], 'required'],
                [['polozka_pk', 'subtitulek'], 'safe'],
            ];
        } else {
            return [
                [['titulek', 'popis', 'vyrobce_pk', 'dodavatel_pk', 'kategorie_id'], 'required'],
                [['viditelne_od', 'skladem', 'cena_doporucena', 'cena_aktualni', 'platne_od'], 'required'],
                [$this->attributes(), 'safe']
            ];
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'titulek' => 'Titulek',
            'subtitulek' => 'Subtitulek',
            'vyrobce_pk' => 'Výrobce',
            'dodavatel_pk' => 'Dodavatel',
            'kategorie_id' => 'Kategorie',
            'popis' => 'Popis položky',
            'viditelne_od' => 'Viditelná od',
            'viditelne_do' => 'Viditelná do',
            'skladem' => 'Skladem kusů',
            'blokace' => 'Blokováno kusů',
            'cena_doporucena' => 'Doporučená cena',
            'cena_aktualni' => 'Aktuální cena',
            'platne_od' => 'Cena platí od',
            'platne_do' => 'Cena platí do',
        ];
    }

    /**
     * @param $pk
     * @return PolozkaForm
     * @throws \Exception
     */
    public function nactiPodlePk($pk)
    {
        $db = \Yii::$app->db;
        $sql = "select * from eshop_aktualni where polozka_pk = :pk";

        $result = $db->createCommand($sql, [
            'pk' => $pk
        ])->queryAll();

        if (count($result) > 1) {
            throw new \Exception("View eshop_aktualni vraci vice zaznamu pro polozku s pk = $pk");
        } else if (count($result) == 0) {
            throw new \Exception("View eshop_aktualni nevraci zadny zaznam pro polozku s pk = $pk, pokus o editaci neexistujici?");
        } else {
            $result = $result[0];
        }

        if (!$this->load($result, '')) {
            throw new \Exception("Nepodarilo se nahrat data do modelu PolozkaForm, data: " . var_export($result, true));
        }

        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function uloz()
    {
        $db = \Yii::$app->db;

        // pokud mam pk, tak se jedna o editaci (no, snad to bude bezpecne)
        if ($this->polozka_pk != null) {
            $sql = "SELECT * FROM eshop_polozka_zmena(:pk, :ti, :st, :pp, :vy, :dd, :ka, :vo, :vd, :sk)";
            $params = [
                ':pk' => $this->polozka_pk,
                ':ti' => $this->titulek,
                ':st' => $this->subtitulek,
                ':pp' => $this->popis,
                ':vy' => $this->vyrobce_pk,
                ':dd' => $this->dodavatel_pk,
                ':ka' => $this->kategorie_id,
                ':vo' => $this->viditelne_od == '' ? null : $this->viditelne_od,
                ':vd' => $this->viditelne_do == '' ? null : $this->viditelne_do,
                ':sk' => $this->skladem,
            ];
        }
        // pokud pk nemam, tak se jedna o insert...
        else {
            $sql = "SELECT * FROM eshop_polozka_zaloz(:ti, :st, :pp, :dd, :vy, :ei, :ea, :ka, :vo, :vd, :sk, :bl, :cd::NUMERIC, :ca::NUMERIC, :po, :pd)";
            $params = [
                ':ti' => $this->titulek,
                ':st' => $this->subtitulek,
                ':pp' => $this->popis,
                ':dd' => $this->dodavatel_pk,
                ':vy' => $this->vyrobce_pk,
                ':ei' => $this->externi_id,
                ':ea' => $this->ean,
                ':ka' => $this->kategorie_id,
                ':vo' => $this->viditelne_od == '' ? null : $this->viditelne_od,
                ':vd' => $this->viditelne_do == '' ? null : $this->viditelne_do,
                ':sk' => $this->skladem,
                ':bl' => $this->blokace,
                ':cd' => $this->cena_doporucena,
                ':ca' => $this->cena_aktualni,
                ':po' => $this->platne_od == '' ? null : $this->platne_od,
                ':pd' => $this->platne_do == '' ? null : $this->platne_do
            ];
        }

        $result = $db->createCommand($sql)->bindValues($params)->queryOne();

        if ($result['r_polozka_pk'] > 0) {
            return $this->polozka_pk = $result['r_polozka_pk'];
        } else {
            throw new \Exception($result['r_vysledek']);
        }
    }
}