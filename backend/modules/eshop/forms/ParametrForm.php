<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 28.02.2016
 * Time: 10:27
 */

namespace backend\modules\eshop\forms;


use yii\base\Model;

/**
 * Class ParametrForm
 * @package backend\modules\eshop\forms
 */
class ParametrForm extends Model
{
    /**
     * @var
     */
    public $parametr_pk;

    /**
     * @var
     */
    public $parametr_id;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $hodnota;

    /**
     * @var
     */
    public $hodnota_typ;

    /**
     * @var
     */
    public $skupina_pk;

    /**
     * @var
     */
    public $jednotka;

    /**
     * @var
     */
    public $polozka_pk;

    /**
     *
     */
    public function ulozProduktu()
    {
        $sql = "SELECT * FROM parametr_produktu_zmena()";

        $params = [];
    }

}