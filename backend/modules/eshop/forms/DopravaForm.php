<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\eshop\forms;

use common\components\Application;
use common\models\Logo;
use common\models\ZpusobDopravy;
use common\components\ItemAliasTrait;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class DopravaForm
 * @package backend\modules\doprava\forms
 */
class DopravaForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $zpusob_dopravy_pk;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $logo;

    /**
     * @var
     */
    public $ma_tracking;

    /**
     * @var ZpusobDopravy
     */
    protected $_doprava;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->_doprava == null) {
            $this->_doprava = new ZpusobDopravy();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev'], 'required'],
            [['logo', 'ma_tracking'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'nazev' => 'Název'
        ];
    }

    /**
     *
     */
    public function uloz()
    {
        $this->_doprava->load($this->attributes, '');

        $mLogo = new Logo();
        $mLogo->soubor = $this->logo;

        try {
            $this->_doprava->logo_pk = $mLogo->ulozZeSouboru();
        } catch (\Exception $e) {
            \Yii::error("[dopravaForm] chyba pri ukladani loga k doprave: {$e->getMessage()}");
            Application::setFlashError("Nepodařilo se uložit logo k dopravě");
        }

        if ($this->_doprava->save()) {
            $this->_doprava->refresh();

            return $this->_doprava->zpusob_dopravy_pk;
        } else {
            return false;
        }
    }

    /**
     * Najde zpusob dopravy z predaneho pk a nacte do sebe jeji atributy
     * @param $id
     * @return $this|null
     */
    public function nacti($id)
    {
        $this->_doprava = ZpusobDopravy::findOne($id);

        if ($this->_doprava == null) {
            return null;
        }

        if ($this->load($this->_doprava->attributes, '')) {
            return $this;
        } else {
            return null;
        }
    }
}