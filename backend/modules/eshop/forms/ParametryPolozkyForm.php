<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 20.06.2016
 * Time: 18:58
 */

namespace backend\modules\eshop\forms;


use common\models\ParametrPolozky;
use yii\base\Model;

/**
 * Class ParametryPolozkyForm
 * @package backend\modules\eshop\forms
 */
class ParametryPolozkyForm extends Model
{
//    /**
//     * @var
//     */
//    public $parametr_pk;
//
    /**
     * @var
     */
    public $polozka_pk;
//
//    /**
//     * @var
//     */
//    public $hodnota;

    /**
     * @var ParametrPolozky[]
     */
    public $parametry;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     *
     */
    public function preulozVse()
    {
        $db = \Yii::$app->db;
        $trans = $db->beginTransaction();

        try {

            $sql = "DELETE FROM parametr_produktu WHERE polozka_pk = :ppk";
            $params = [
                ':ppk' => $this->polozka_pk
            ];

            $db->createCommand($sql)->bindValues($params)->execute();

            foreach ($this->parametry as $mParametr) {
                $mParametr->ulozHodnotu();
            }

            $trans->commit();
            return true;
        } catch (\Exception $e) {
            $trans->rollBack();

            throw $e;
        }
    }
}