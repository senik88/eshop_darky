<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\eshop\forms;

use backend\modules\eshop\models\Vyrobce;
use common\components\Application;
use common\components\helpers\String;
use common\components\ItemAliasTrait;
use common\models\Logo;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * Class VyrobceForm
 * @package backend\modules\vyrobce\forms
 */
class VyrobceForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $vyrobce_pk;

    /**
     * @var
     */
    public $vyrobce_id;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $web;

    /**
     * @var string
     */
    public $logo;

    /**
     * @var Vyrobce
     */
    protected $_vyrobce;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->_vyrobce == null) {
            $this->_vyrobce = new Vyrobce();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev'], 'required'],
            [['web'], 'safe'],
            ['nazev', 'existujeVyrobceValidator']
        ];
    }

    public function attributeLabels()
    {
        return [
            'nazev' => 'Název'
        ];
    }

    /**
     * @return integer|false
     */
    public function uloz()
    {
        $this->_vyrobce->nazev = $this->nazev;

        // doplnit http://, pokud nebude obsahovat...
        $this->_vyrobce->web = $this->web;

        if ($this->_vyrobce->isNewRecord) {
            $this->_vyrobce->datum_vlozeni = date('Y-m-d H:i:s');
        }

        if ($this->vyrobce_pk != null) {
            $this->_vyrobce->vyrobce_pk = $this->vyrobce_pk;
        }

        // id pregeneruju pouze pokud se mi zmenil nazev (a u noveho vyrobce)
        if ($this->nazev != $this->_vyrobce->nazev || $this->_vyrobce->vyrobce_id == NULL) {
            $this->_vyrobce->vyrobce_id = $this->_vyrobce->generujId();
        }

        if ($this->logo instanceof UploadedFile) {
            try {
                $mLogo = new Logo();
                $mLogo->soubor = $this->logo;

                $this->_vyrobce->logo_pk = $mLogo->ulozZeSouboru();
            } catch (\Exception $e) {
                \Yii::error("neporadilo se ulozit logo k vyrobci, chyba: {$e->getMessage()}");
                Application::setFlashWarning("Nepodařilo se uložit logo k výrobci.");
            }
        }

        if ($this->_vyrobce->save()) {
            $this->_vyrobce->refresh();

            return $this->_vyrobce->vyrobce_pk;
        } else {
            return false;
        }
    }

    /**
     * Najde vyrobce z predaneho pk a nacte do sebe jeho atributy
     * @param $id
     * @return $this|null
     */
    public function nacti($id)
    {
        $this->_vyrobce = Vyrobce::findOne($id);

        if ($this->_vyrobce == null) {
            return null;
        }

        if ($this->load($this->_vyrobce->attributes, '')) {
            return $this;
        } else {
            return null;
        }
    }



    /**
     * @param $attribute
     * @param $params
     */
    public function existujeVyrobceValidator($attribute, $params)
    {
        if ($this->_vyrobce->isNewRecord) {

            $sql = "SELECT vyrobce_pk, nazev FROM vyrobce WHERE lower(nazev) = lower(:nazev)";
            $params = [
                ':nazev' => $this->$attribute
            ];

            $res = \Yii::$app->db->createCommand($sql, $params)->queryOne();

            if ($res) {
                $this->addError($attribute, sprintf(
                    "Výrobce s názvem '%s' již existuje, chcete jej %s?",
                    $this->$attribute,
                    Html::a('editovat', ['/eshop/vyrobci/upravit', 'id' => $res['vyrobce_pk']])
                ));
            }
        }
    }
}