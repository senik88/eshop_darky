<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\eshop\forms;

use backend\modules\eshop\models\Dodavatel;
use common\components\ItemAliasTrait;
use yii\base\Model;

/**
 * Class DodavatelForm
 * @package backend\modules\dodavatel\forms
 */
class DodavatelForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $dodavatel_pk;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var Dodavatel
     */
    protected $_dodavatel;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->_dodavatel == null) {
            $this->_dodavatel = new Dodavatel();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'nazev' => 'Název'
        ];
    }

    /**
     *
     */
    public function uloz()
    {
        $this->_dodavatel->load($this->attributes, '');

        if ($this->_dodavatel->save()) {
            $this->_dodavatel->refresh();

            return $this->_dodavatel->dodavatel_pk;
        } else {
            return false;
        }
    }

    /**
     * Najde dodavatele z predaneho pk a nacte do sebe jeho atributy
     * @param $id
     * @return $this|null
     */
    public function nacti($id)
    {
        $this->_dodavatel = Dodavatel::findOne($id);

        if ($this->_dodavatel == null) {
            return null;
        }

        if ($this->load($this->_dodavatel->attributes, '')) {
            return $this;
        } else {
            return null;
        }
    }
}