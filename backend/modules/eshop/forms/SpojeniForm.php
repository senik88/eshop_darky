<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\eshop\forms;

use common\components\ItemAliasTrait;
use common\models\PlatbaDopravy;
use yii\base\Model;

/**
 * Class SpojeniForm
 * @package backend\modules\doprava\forms
 */
class SpojeniForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $platba_dopravy_pk;

    /**
     * @var
     */
    public $platba_dopravy_id;

    /**
     * @var
     */
    public $zpusob_dopravy_pk;

    /**
     * @var
     */
    public $zpusob_platby_pk;

    /**
     * @var
     */
    public $viditelne_od;

    /**
     * @var
     */
    public $viditelne_do;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $cena;

    /**
     * @var PlatbaDopravy
     */
    protected $_spojeni;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->_spojeni == null) {
            $this->_spojeni = new PlatbaDopravy();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'nazev' => 'Název'
        ];
    }

    /**
     *
     */
    public function uloz()
    {
        $data = $this->attributes;
        if ($this->_spojeni->isNewRecord) {
            unset($data['platba_dopravy_pk']);
        }

        $this->_spojeni->load($data, '');

        if ($this->_spojeni->save()) {
            $this->_spojeni->refresh();

            return $this->_spojeni->platba_dopravy_pk;
        } else {
            return false;
        }
    }

    /**
     * Najde spojeni platby a dopravy z predaneho pk a nacte do sebe jejich atributy
     * @param $id
     * @return $this|null
     */
    public function nacti($id)
    {
        $this->_spojeni = PlatbaDopravy::findOne($id);

        if ($this->_spojeni == null) {
            return null;
        }

        if ($this->load($this->_spojeni->attributes, '')) {
            return $this;
        } else {
            return null;
        }
    }
}