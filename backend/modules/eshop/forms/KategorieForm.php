<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\eshop\forms;

use backend\modules\eshop\EshopModule;
use backend\modules\eshop\KategorieModule;
use backend\modules\eshop\models\Kategorie;
use common\components\helpers\String;
use common\components\ItemAliasTrait;
use common\models\Parametr;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class KategorieForm
 * @package backend\modules\kategorie\forms
 */
class KategorieForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $kategorie_pk;

    /**
     * @var
     */
    public $kategorie_id;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $nadrazena;

    /**
     * @var
     */
    public $menu_zobrazit;

    /**
     * @var Parametr[]
     */
    public $parametry;

    /**
     * @var Kategorie
     */
    protected $_kategorie;

    public function init()
    {
        parent::init();

        if ($this->_kategorie == null) {
            $this->_kategorie = new Kategorie();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev'], 'required'],
            [['nadrazena', 'menu_zobrazit'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nazev' => EshopModule::t('kategorie', 'Název'),
            'nadrazena' => EshopModule::t('kategorie', 'Nadřazená kategorie')
        ];
    }

    /**
     * todo ucesat, doladit
     */
    public function uloz()
    {
        $db = \Yii::$app->db;
        $trans = $db->beginTransaction();

        try {
            $this->_kategorie->load($this->attributes, '');

            if ($this->_kategorie->kategorie_pk != null) {
                // smazat parametry z kategorie
                $rows = $db->createCommand("DELETE FROM parametr_kategorie WHERE kategorie_pk = :pk")
                    ->bindValue(':pk', $this->_kategorie->kategorie_pk)
                    ->execute();

                \Yii::info("smazano $rows parametru z kategorie {$this->_kategorie->nazev}");
            }

            // normalni prace s kategorii
            $this->_kategorie->kategorie_id = String::webalize($this->nazev);

            if ($this->_kategorie->save()) {
                $this->_kategorie->refresh();

                if ($this->parametry != null) {
                    foreach ($this->parametry as $mParametr) {
                        $mParametr->parametr_id = String::webalize($mParametr->nazev);
                        $pk = $mParametr->najdiPakUloz();


                        $db->createCommand("INSERT INTO parametr_kategorie (kategorie_pk, parametr_pk) VALUES (:kpk, :ppk)")
                            ->bindValues([
                                ':kpk' => $this->_kategorie->kategorie_pk,
                                ':ppk' => $pk
                            ])
                            ->execute();
                    }
                }

                $trans->commit();

                return $this->_kategorie->kategorie_pk;
            } else {
                throw new \Exception('nepodarilo se ulozit kategorii, neznama chyba');
            }
        } catch (\Exception $e) {
            $trans->rollBack();
            \Yii::error("chyba pri ukladani kategorie" . $e->getMessage());

            return false;
        }
    }

    /**
     * Najde kategorii z predaneho pk a nacte do sebe její atributy
     * @param $id
     * @return $this|null
     */
    public function nacti($id)
    {
        $this->_kategorie = Kategorie::findOne($id);

        if ($this->_kategorie == null) {
            return null;
        }

        if ($this->load($this->_kategorie->attributes, '')) {
            return $this;
        } else {
            return null;
        }
    }

    /**
     * @return Parametr[]
     */
    public function nactiParametry()
    {
        if ($this->_kategorie == null) {
            return [];
        } else {
            return $this->_kategorie->vratParametry();
        }
    }
}