<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:19
 */

namespace backend\modules\eshop\forms;

use common\models\ZpusobPlatby;
use common\components\ItemAliasTrait;
use yii\base\Model;

/**
 * Class PlatbaForm
 * @package backend\modules\doprava\forms
 */
class PlatbaForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $zpusob_platby_pk;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var ZpusobPlatby
     */
    protected $_platba;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->_platba == null) {
            $this->_platba = new ZpusobPlatby();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'nazev' => 'Název'
        ];
    }

    /**
     *
     */
    public function uloz()
    {
        $this->_platba->load($this->attributes, '');

        if ($this->_platba->save()) {
            $this->_platba->refresh();

            return $this->_platba->zpusob_platby_pk;
        } else {
            return false;
        }
    }

    /**
     * Najde zpusob dopravy z predaneho pk a nacte do sebe jeji atributy
     * @param $id
     * @return $this|null
     */
    public function nacti($id)
    {
        $this->_platba = ZpusobPlatby::findOne($id);

        if ($this->_platba == null) {
            return null;
        }

        if ($this->load($this->_platba->attributes, '')) {
            return $this;
        } else {
            return null;
        }
    }
}