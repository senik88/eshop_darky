<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 30.09.2016
 * Time: 20:21
 */

namespace backend\modules\eshop\forms;


use backend\modules\eshop\models\Objednavka;
use common\components\SqlDataProvider;
use yii\base\Model;

/**
 * Class ObjednavkaSearchForm
 * @package backend\modules\eshop\forms
 */
class ObjednavkaSearchForm extends Model
{
    /**
     * @var
     */
    public $stav;

    /**
     * @var
     */
    public $zpracovava_pk;

    /**
     * @var
     */
    public $uzivatel_pk;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['stav', 'zpracovava_pk', 'uzivatel_pk'], 'safe'],
        ];
    }

    /**
     * @return SqlDataProvider
     */
    public function search()
    {

        $sql = "
            SELECT
                ov.*
                , ov.cena_dopravy + ov.cena_polozek AS cena_celkova
            FROM objednavky_view ov
                JOIN objednavka o ON ov.objednavka_pk = o.objednavka_pk
        ";

        $params = $where = [];

        if ($this->zpracovava_pk != null) {
            if ($this->zpracovava_pk == '#') {
                $where[] = "o.zpracovava_uzivatel_pk IS NULL";
            } else {
                $params[':zpracovava'] = $this->zpracovava_pk;
                $where[] = "o.zpracovava_uzivatel_pk = :zpracovava";
            }
        }

        if ($this->stav != null) {
            $params[':stav'] = $this->stav;
            $where[] = "o.stav_objednavky_id = :stav";
        }


        if (!empty($where)) {
            $sql .= " WHERE " . implode("\n AND ", $where);
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params
        ]);

        /*$model = new Objednavka();
        return $model->search();*/
    }

}