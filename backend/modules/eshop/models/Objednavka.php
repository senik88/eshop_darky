<?php
/**
 * Created by PhpStorm.
 * User: Doma
 * Date: 25.11.2015
 * Time: 20:07
 */

namespace backend\modules\eshop\models;


use common\components\SqlDataProvider;
use common\components\uzivatel\Uzivatel;
use frontend\modules\uzivatel\models\UzivatelFrontend;
use Yii;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;

/**
 * Class Objednavka
 * @package backend\modules\eshop\models
 */
class Objednavka extends \common\models\Objednavka {

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * todo zatim jen priradi zpracovatele, vymyslet co vic s tim
     * @param $uzivatel_pk
     * @return bool
     */
    public function priradZpracovani($uzivatel_pk)
    {
        $sql = "SELECT objednavka_prevzit_zpracovani(:opk, :upk)";

        $res = Yii::$app->db->createCommand($sql, [
            ':opk' => $this->objednavka_pk,
            ':upk' => $uzivatel_pk
        ])->queryScalar();

        if ($res === 0) {
            Yii::info("objednavka s pk = {$this->objednavka_pk} prirazena ke zpracovani uzivateli s pk = {$uzivatel_pk}");
            return true;
        } else {
            Yii::error("objednavku s pk = {$this->objednavka_pk} se nepodarilo priradit ke zpracovani uzivateli s pk = {$uzivatel_pk}");
            return false;
        }
    }

    /**
     * @return string
     */
    public function vykresliButtonyProZpracovani()
    {
        $buttony = '';

        if ($this->stav_objednavky_id == Objednavka::STAV_POTVRZENA) {
            $buttony .= Html::a('Převzít ke zpracování', ['/eshop/objednavky/prevzit-zpracovani', 'id' => $this->objednavka_pk], [
                'class' => 'btn btn-success',
                'data-objednavka-pk' => $this->objednavka_pk
            ]);
        }

        if ($this->stav_objednavky_id == Objednavka::STAV_ZPRACOVAVA) {
            $buttony .= Html::a('Odeslaná', '#', [
                'class' => 'btn btn-success',
                'id' => 'button-objednavka-odeslana',
                'data-objednavka-pk' => $this->objednavka_pk
            ]);
        }

        $buttony .= Html::a('Stornovat', ['/eshop/objednavky/stornovat', 'id' => $this->objednavka_pk], [
            'class' => 'btn btn-danger',
            'data-objednavka-pk' => $this->objednavka_pk
        ]);

        return $buttony;
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $query = "
            select
                o.*
                , o.cena_dopravy + o.cena_polozek AS cena_celkova
            from objednavky_view o
            where o.stav_objednavky_id <> :stav
        ";

        return new SqlDataProvider([
            'sql' => $query,
            'params' => [
                ':stav' => Objednavka::STAV_KOSIK
            ]
        ]);
    }

    protected function celkovaCenaObj($objednavka_pk)
    {
        $query = "select sum(cena_aktualni) from objednavka_polozka_mn opm left join eshop_cena ec on ec.eshop_cena_pk = opm.eshop_cena_pk where opm.objednavka_pk = :pk";
        $params = [
            ':pk' => $objednavka_pk,
        ];
        return \Yii::$app->db->createCommand($query, $params)->queryScalar();
    }

    protected function celkovyPocetZboziVObj($objednavka_pk)
    {
        $query = "select sum(mnozstvi) from objednavka_polozka_mn where objednavka_pk = :pk";
        $params = [
            ':pk' => $objednavka_pk,
        ];
        return \Yii::$app->db->createCommand($query, $params)->queryScalar();
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'cislo' => [
                'attribute' => 'cislo',
                'label' => 'Číslo',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model['cislo'], ['/eshop/objednavky/detail', 'id' => $model['objednavka_pk']]);
                }
            ],
            'uzivatel_pk' => [
                'value' => function ($model) {
                    /** @var Uzivatel $mUser */
                    $mUser = Uzivatel::findOne($model['uzivatel_pk']);
                    return $mUser->jmeno .' '. $mUser->prijmeni;
                },
                'label' => 'Zákazník'
            ],
            'cas_vytvoreni' => [
                'value' =>  function ($model) {
                    return Yii::$app->formatter->asDatetime($model['cas_vytvoreni']);
                },
                'label' => 'Vytvořena'
            ],
            'stav_objednavky_id' => [
                'attribute' => 'stav_objednavky_id',
                'label' => 'Stav',
                'value' => function ($model) {
                    return Objednavka::itemAlias('stav', $model['stav_objednavky_id']);
                }
            ],
            'polozek' => [
                'value' => function ($model) {
                    return $model['pocet_polozek'];
                },
                'label' => 'Položek'
            ],
            'cena' => [
                'value' => function ($model) {
                    return Yii::$app->formatter->asCurrency($model['cena_celkova'], 'CZK');
                },
                'label' => 'Cena'
            ],
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/eshop/objednavky/detail', 'id' => $model['objednavka_pk']);
                            break;
                        case 'update':
                            $url = array('/eshop/objednavky/upravit', 'id' => $model['objednavka_pk']);
                            break;
                        case 'delete':
                            $url = array('/eshop/objednavky/smazat', 'id' => $model['objednavka_pk']);
                            break;
                        default:
                            throw new \Exception('Undefined action for model');
                    }
                    return $url;
                },
            ]
        ];
    }

    /**
     * todo nebrat vsechny backend uzivatele, ale jen ty, kteri na to maji opravneni
     * @return array pole vsech moznych zpracovatelu objednavek
     */
    public function vratVsechnyZpracovatele()
    {
        $sql = "
            SELECT
                uzivatel_pk
                , prijmeni || ' ' || jmeno AS cele_jmeno
            FROM uzivatel WHERE rozhrani = :rozhrani AND stav = :stav
            ORDER BY prijmeni ASC, jmeno ASC
        ";
        $params = [
            ':rozhrani' => Uzivatel::ROZHRANI_BACK,
            ':stav' => Uzivatel::STAV_AKTIVNI
        ];

        return ArrayHelper::map(
            Yii::$app->db->createCommand($sql, $params)->queryAll(),
            'uzivatel_pk', 'cele_jmeno'
        );
    }

}