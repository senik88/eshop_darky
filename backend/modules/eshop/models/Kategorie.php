<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:03
 */

namespace backend\modules\eshop\models;

use common\components\SqlDataProvider;
use common\models\Parametr;
use Exception;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\grid\ActionColumn;
use yii\helpers\Html;


/**
 * Class Kategorie
 * @package backend\modules\eshop\models
 *
 * @property integer kategorie_pk
 * @property string nazev
 * @property string kategorie_id
 * @property boolean menu_zobrazit
 * @property integer nadrazena
 */
class Kategorie extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array('nazev', 'kategorie_id', 'nadrazena', 'menu_zobrazit'), 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        /*$scenarios[self::SCENARIO_PRIDAT] = [
            'nazev'
        ];
        $scenarios[self::SCENARIO_UPRAVIT] = [
            'nazev'
        ];*/
        return $scenarios;
    }

    public function getKategorie()
    {
        return $this->hasOne(Kategorie::className(), ['nadrazena' => 'kategorie_pk']);
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    public function vratPodkategorie($pk = null)
    {
        if ($pk == null) {
            $pk = $this->kategorie_pk;
        }

        $ret = [];
        $pod = $this->find()->where(['nadrazena' => $pk])->orderBy('nazev ASC')->all();

        /** @var Kategorie $kat */
        foreach ($pod as $kat) {
            $ret[] = [
                'pk' => $kat->kategorie_pk,
                'id' => $kat->kategorie_id,
                'na' => $kat->nazev
            ];
        }

        return $ret;
    }

    /**
     * @return null|string
     */
    public function vratNazevNadrazene()
    {
        $nadrazena = Kategorie::findOne(['kategorie_pk' => $this->nadrazena]);

        if ($nadrazena == null) {
            return null;
        } else {
            return $nadrazena->nazev;
        }
    }

    /**
     * todo doresit, jestli se bude u kategorii menit obrazek v banneru
     * @return string
     */
    public function vratObrazek()
    {
        return 'back.jpg';
    }

    /**
     * @return string
     */
    public function vratRandomObrazekProduktu()
    {
        $sql = "
            SELECT polozka_obrazek.nahled
            FROM polozka
                JOIN polozka_obrazek ON polozka.polozka_pk = polozka_obrazek.polozka_pk
            WHERE polozka.kategorie_id = :id
            LIMIT 1
        ";
        $params = [':id' => $this->kategorie_id];

        return \Yii::$app->db->createCommand($sql)->bindValues($params)->queryScalar();
    }

    /**
     * @return Parametr[]
     */
    public function vratParametry()
    {
        $sql = "
            SELECT p.*
            FROM parametr_kategorie pk
                JOIN parametr p ON pk.parametr_pk = p.parametr_pk
                JOIN skupina_parametru sp ON p.skupina_pk = sp.skupina_pk
            WHERE pk.kategorie_pk = :kpk
            ORDER BY sp.nazev, p.nazev
        ";

        $data = \Yii::$app->db->createCommand($sql)->bindValue(':kpk', $this->kategorie_pk)->queryAll();

        $parametry = [];
        foreach ($data as $radek) {
            $mParametr = new Parametr();
            $mParametr->load($radek, '');

            $parametry[] = $mParametr;
        }

        return $parametry;
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $sql = "
            SELECT * FROM (
                SELECT
                      k.kategorie_pk
                    , k.kategorie_id
                    , k.nazev
                    , n.nazev AS nadrazena
                    , count(p.polozka_pk) AS polozek
                FROM kategorie k
                    LEFT JOIN kategorie n ON k.nadrazena = n.kategorie_pk
                    LEFT JOIN polozka p ON p.kategorie_id = k.kategorie_id
                GROUP BY k.kategorie_pk, k.kategorie_id, k.nazev, n.nazev
            ) AS sub

          ";

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => [],
            'pagination' => $pagination ? [
                'pageSize' => $pagination
            ] : false,
            'sort' => [
                'attributes' => ['kategorie_pk', 'kategorie_id', 'nadrazena', 'nazev', 'polozek'],
                'defaultOrder' => ['nazev' => SORT_ASC]
            ]
        ]);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'id' => [
                'attribute' => 'kategorie_pk',
                'label' => 'ID'
            ],
            'nazev' => [
                'attribute' => 'nazev'
            ],
            'nadrazena' => [
                'attribute' => 'nadrazena',
                'value' => function ($model) {
                    return $model['nadrazena'];
                },
                'label' => 'Nadřazená'
            ],
            'polozek' => [
                'attribute' => 'polozek',
                'label' => 'Položek'
            ],
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/eshop/kategorie/detail', 'id' => $model['kategorie_pk']);
                            break;
                        case 'update':
                            $url = array('/eshop/kategorie/upravit', 'id' => $model['kategorie_pk']);
                            break;
                        case 'delete':
                            $url = array('/eshop/kategorie/smazat', 'id' => $model['kategorie_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil',
                            ]),
                            $url,
                            [
                                'title' => 'Upravit',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-trash',
                            ]),
                            $url,
                            [
                                'title' => 'Smazat',
                                'data-pjax' => 0,
                                'data-confirm' => "Opravdu chcete smazat výrobce?",
                                'data-method' => "post",
                            ]
                        );
                    }
                ),
            ]
        ];
    }
}