<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.11.2015
 * Time: 19:43
 */

namespace backend\modules\eshop\models;


use yii\db\ActiveRecord;

/**
 * Class Varianta
 *
 * aplikacni trida pro tabulku eshop
 *
 * @package backend\modules\eshop\models
 *
 * @property integer eshop_pk
 * @property integer polozka_pk
 * @property string viditelne_od
 * @property string viditelne_do
 * @property integer skladem
 * @property integer blokace
 */
class Varianta extends ActiveRecord
{
    /**
     * @var EshopCena[]
     */
    public $ceny;

    /**
     * @var EshopCena
     */
    public $mCena;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eshop';
    }

}