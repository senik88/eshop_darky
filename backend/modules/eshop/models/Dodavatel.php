<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:03
 */

namespace backend\modules\eshop\models;

use Exception;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\grid\ActionColumn;
use yii\helpers\Html;


/**
 * Class Dodavatel
 * @package backend\modules\eshop\models
 * @property integer dodavatel_pk
 * @property string nazev
 */
class Dodavatel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['nazev'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        /*$scenarios[self::SCENARIO_PRIDAT] = [
            'nazev'
        ];
        $scenarios[self::SCENARIO_UPRAVIT] = [
            'nazev'
        ];*/
        return $scenarios;
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $query = self::find();

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'id' => [
                'attribute' => 'dodavatel_pk',
                'label' => 'ID'
            ],
            'nazev',
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/eshop/dodavatele/detail', 'id' => $model['dodavatel_pk']);
                            break;
                        case 'update':
                            $url = array('/eshop/dodavatele/upravit', 'id' => $model['dodavatel_pk']);
                            break;
                        case 'delete':
                            $url = array('/eshop/dodavatele/smazat', 'id' => $model['dodavatel_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil',
                            ]),
                            $url,
                            [
                                'title' => 'Upravit',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-trash',
                            ]),
                            $url,
                            [
                                'title' => 'Smazat',
                                'data-pjax' => 0,
                                'data-confirm' => "Opravdu chcete smazat dodavatele?",
                                'data-method' => "post",
                            ]
                        );
                    }
                ),
            ]
        ];
    }
}