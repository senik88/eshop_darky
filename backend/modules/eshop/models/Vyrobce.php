<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:03
 */

namespace backend\modules\eshop\models;

use common\components\SqlDataProvider;
use Exception;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\grid\ActionColumn;
use yii\helpers\Html;


/**
 * Class Vyrobce
 * @package backend\modules\eshop\models
 * @property integer vyrobce_pk
 * @property string vyrobce_id
 * @property string nazev
 * @property string web
 * @property string datum_vlozeni
 * @property integer logo_pk
 */
class Vyrobce extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['nazev', 'logo_pk', 'vyrobce_pk', 'vyrobce_id', 'web', 'datum_vlozeni'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        /*$scenarios[self::SCENARIO_PRIDAT] = [
            'nazev'
        ];
        $scenarios[self::SCENARIO_UPRAVIT] = [
            'nazev'
        ];*/
        return $scenarios;
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $sql = "
            SELECT
                v.*, min(l.zdroj) AS logo, count(p.polozka_pk) AS produktu
            FROM vyrobce v
                LEFT JOIN polozka p ON v.vyrobce_pk = p.vyrobce_pk
                LEFT JOIN logo l ON v.logo_pk = l.logo_pk
            GROUP BY v.vyrobce_pk
        ";

        return new SqlDataProvider([
            'sql' => $sql,
            'pagination' => $pagination ? [
                'pageSize' => $pagination
            ] : false
        ]);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function maProdukty()
    {
        if ($this->vyrobce_pk == null) {
            throw new \yii\base\Exception('nelze zjistit pocet produktu, neni nastaveno vyrobce_pk');
        }

        $sql = "SELECT count(*) FROM polozka WHERE vyrobce_pk = :pk";
        $produktu = \Yii::$app->db->createCommand($sql, [':pk' => $this->vyrobce_pk])->queryScalar();

        return $produktu > 0;
    }

    /**
     * @return array
     */
    public function vratPrehledProduktuKategorii()
    {
        $sql = "
            SELECT v.vyrobce_pk, k.kategorie_pk, v.nazev AS vyrobce, k.nazev AS kategorie, json_agg(row_to_json(t)) as \"data\"
            FROM (
                SELECT
                      p.titulek
                    , p.subtitulek
                    , p.polozka_id
                    , po.nahled
                    , p.vyrobce_pk
                    , p.kategorie_id
                    , p.cena_aktualni AS cena
                FROM eshop_aktualni p
                    JOIN polozka_obrazek po ON po.polozka_obrazek_pk = polozka_obrazek_hlavni_vrat_pk(p.polozka_pk)
                    JOIN vyrobce v2 ON v2.vyrobce_pk = p.vyrobce_pk
                WHERE v2.vyrobce_id = :vid
                LIMIT 4
            ) t
                JOIN vyrobce v ON v.vyrobce_pk = t.vyrobce_pk
                JOIN kategorie k ON k.kategorie_id = t.kategorie_id
            GROUP BY v.vyrobce_pk, k.kategorie_pk, k.nazev
            LIMIT 3
        ";

        $data = \Yii::$app->db->createCommand($sql, [
            ':vid' => $this->vyrobce_id
        ])->queryAll();

        return $data;
    }

    /**
     * Prevede produkty pod noveho vyrobce. Vola db proceduru a vraci boolean podle vysledku.
     *
     * @param integer $novy vyrobce_pk
     * @param integer|null $stary vyrobce_pk
     * @return bool
     * @throws Exception
     */
    public function prevedProduktyPodNoveho($novy, $stary = null)
    {
        try {
            if ($stary == null) {
                $stary = $this->vyrobce_pk;
            }

            if ($stary == null || $novy == null) {
                throw new Exception("nelze prevest produkty, neznam vyrobce: novy ($novy), stary ($stary)");
            }

            $sql = "SELECT preved_produkty_vyrobce(:novy, :stary)";
            $result = \Yii::$app->db->createCommand($sql, [
                ':novy' => $novy,
                ':stary' => $stary
            ])->queryScalar();

            if ($result < 0) {
                throw new Exception("vysledek preved_produkty_vyrobce($novy, $stary) = $result");
            }

            \Yii::info("pod vyrobce $novy prevedeno $result produktu vyrobce $stary");
            return true;
        } catch (Exception $e) {
            \Yii::error("chyba pri prevodu produktu pod noveho vyrobce: {$e->getMessage()}");
            return false;
        }
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'pk' => [
                'attribute' => 'vyrobce_pk',
                'label' => 'PK'
            ],
            'id' => [
                'attribute' => 'vyrobce_id',
                'label' => 'ID výrobce'
            ],
            'nazev',
            'logo',
            'datum_vlozeni' => [
                'label' => 'Datum vložení',
                'value' => function ($model) {
                    return \Yii::$app->formatter->asDatetime($model['datum_vlozeni']);
                }
            ],
            'produktu' => [
                'attribute' => 'produktu',
                'label' => 'Produktů'
            ],
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/eshop/vyrobci/detail', 'id' => $model['vyrobce_pk']);
                            break;
                        case 'update':
                            $url = array('/eshop/vyrobci/upravit', 'id' => $model['vyrobce_pk']);
                            break;
                        case 'delete':
                            $url = array('/eshop/vyrobci/smazat', 'id' => $model['vyrobce_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil',
                            ]),
                            $url,
                            [
                                'title' => 'Upravit',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        if ($model['produktu'] === 0) {
                            return Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-trash',
                                ]),
                                $url,
                                [
                                    'title' => 'Smazat',
                                    'data-pjax' => 0,
                                    'data-confirm' => "Opravdu chcete smazat výrobce?",
                                    'data-method' => "post",
                                ]
                            );
                        } else {
                            return null;
                        }
                    }
                ),
            ]
        ];
    }

    /**
     * @return null|string
     */
    public function generujId()
    {
        if ($this->nazev == null) {
            return null;
        }

        $id = \Yii::$app->db->createCommand("SELECT vyrobce_generuj_id(:nazev)", [
            ':nazev' => $this->nazev
        ])->queryScalar();

        return $id;
    }

    /**
     * @param int $pocet
     * @return SqlDataProvider
     */
    public static function vratVybrane($pocet = 4)
    {
        $sql = "
            SELECT *
            FROM vyrobce
                JOIN logo ON vyrobce.logo_pk = logo.logo_pk
            ORDER BY random() LIMIT :limit
        ";
        $params = [
            ':limit' => $pocet
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => false
        ]);
    }
}