<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 10.10.2015
 * Time: 16:14
 */

namespace backend\modules\eshop\models;


use backend\modules\eshop\EshopModule;
use common\components\SqlDataProvider;
use common\models\ParametrPolozky;
use common\models\PolozkaObrazek;
use yii\base\Model;
use yii\grid\ActionColumn;

/**
 * Class Polozka
 * @package backend\modules\eshop\models
 *
 * @see https://github.com/yiidoc/yii2-redactor
 * @see http://code.tutsplus.com/tutorials/programming-with-yii2-rich-text-input-with-redactor--cms-23174
 */
class Polozka extends Model
{
    /**
     * @var
     */
    public $polozka_pk;

    /**
     * @var
     */
    public $polozka_id;

    /**
     * @var
     */
    public $titulek;

    /**
     * @var
     */
    public $subtitulek;

    /**
     * @var
     */
    public $popis;

    /**
     * @var
     */
    public $dodavatel_pk;

    /**
     * @var Vyrobce
     */
    public $vyrobce_pk;

    /**
     * @var Kategorie
     */
    public $kategorie_id;

    /**
     * @var Dodavatel
     */
    public $mDodavatel;

    /**
     * @var Vyrobce
     */
    public $mVyrobce;

    /**
     * @var Kategorie
     */
    public $mKategorie;

    /**
     * @var Varianta
     */
    public $mVarianta;

    /**
     * @var EshopCena
     */
    public $mEshopCena;

    /**
     * @var Varianta[]
     */
    public $varianty;

    /**
     * @var array
     */
    public $eshop_cena;

    /**
     * @var PolozkaObrazek[]
     */
    public $obrazky;

    /**
     * @var
     */
    public $zobrazit;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'polozka_id' => EshopModule::t('polozka', 'ID položky'),
            'titulek' => EshopModule::t('polozka', 'Titulek'),
            'subtitulek' => EshopModule::t('polozka', 'Subtitulek'),
            'popis' => EshopModule::t('polozka', 'Popis produktu'),
            'kategorie_id' => EshopModule::t('polozka', 'Kategorie'),
            'dodavatel_pk' => EshopModule::t('polozka', 'Dodavatel'),
            'vyrobce_pk' => EshopModule::t('polozka', 'Výrobce'),
        ];
    }

    /**
     * @return SqlDataProvider
     */
    public function vypis()
    {
        return $this->_vratDataprovider();
    }

    /**
     * @return SqlDataProvider
     */
    public function export()
    {
        return $this->_vratDataprovider(false);
    }

    /**
     * @param $sql
     * @param $params
     * @return $this
     * @internal param $id
     */
    public function nacti($sql, $params)
    {
        $data['Polozka'] = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryOne();

        if ($data['Polozka'] === false) {
            return null;
        }

        $this->load($data);
        $this->mKategorie = Kategorie::findOne(['kategorie_id' => $this->kategorie_id]);
        $this->mVyrobce = Vyrobce::findOne(['vyrobce_pk' => $this->vyrobce_pk]);
        $this->mDodavatel = Dodavatel::findOne(['dodavatel_pk' => $this->dodavatel_pk]);

        // nactu si obrazky a pokud zadne nejsou, ulozim si do obrazku "no-photo"
        $obrazky = PolozkaObrazek::findAll(['polozka_pk' => $this->polozka_pk]);
        if (empty($obrazky)) {
            $obrazky[] = PolozkaObrazek::findBySql("select * from polozka_obrazek where polozka_pk is null order by polozka_obrazek_pk asc limit 1")->one();
        }
        $this->obrazky = $obrazky;

        // tady to budu pozdeji resit jinak kvuli ruznym variantam - ty ale budu asi beztak zobrazovat oddelene, protoze budu mit id varianty
        if (isset($data['Polozka']['eshop_pk'])) {
            $this->mVarianta = Varianta::findOne(['eshop_pk' => $data['Polozka']['eshop_pk']]);
        }

        if (isset($data['Polozka']['eshop_cena_pk'])) {
            $this->mEshopCena = EshopCena::findOne(['eshop_cena_pk' => $data['Polozka']['eshop_cena_pk']]);
        }

        return $this;
    }

    public function nactiPodlePk($pk)
    {
        $sql = "select * from polozka where polozka_pk = :pk";
        $params = [':pk' => $pk];

        return $this->nacti($sql, $params);
    }

    /**
     * @param $id
     * @return Polozka|null
     */
    public function nactiPodleId($id)
    {
        $sql = "select * from polozka where polozka_id = :id";
        $params = [':id' => $id];

        return $this->nacti($sql, $params);
    }

    /**
     * @param $id
     * @return Polozka
     */
    public function nactiEshop($id)
    {
        $sql = "select * from eshop_aktualni where polozka_id = :id";
        $params = [':id' => $id];

        return $this->nacti($sql, $params);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'polozka_id',
            'titulek',
            'subtitulek',
            'dodavatel' => [
                'attribute' => 'dodavatel'
            ],
            'vyrobce' => [
                'attribute' => 'vyrobce'
            ],
            'kategorie' => [
                'attribute' => 'kategorie'
            ],
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/eshop/polozky/detail', 'id' => $model['polozka_pk']);
                            break;
                        case 'update':
                            $url = array('/eshop/polozky/upravit', 'id' => $model['polozka_pk']);
                            break;
                        case 'delete':
                            $url = array('/eshop/polozky/smazat', 'id' => $model['polozka_pk']);
                            break;
                        default:
                            throw new \Exception('Undefined action for model');
                    }
                    return $url;
                },
            ]
        ];
    }

    /**
     * todo tohle bude chtit asi udelat jinak, jen jsem potreboval rychle data...
     * @return SqlDataProvider
     */
    public function vratTopProdukty()
    {
        return $this->_vratDataprovider(8);
    }

    /**
     * @return ParametrPolozky[]
     */
    public function vratParametry()
    {
        $sql = "
            SELECT * FROM (
                SELECT
                    pp.parametr_pk
                    , pp.polozka_pk
                    , pp.hodnota
                FROM parametr_produktu pp
                WHERE pp.polozka_pk = :ppk

                UNION

                SELECT
                    pk.parametr_pk
                    , :ppk AS polozka_pk
                    , NULL AS hodnota
                FROM parametr_kategorie pk
                    JOIN kategorie k ON pk.kategorie_pk = k.kategorie_pk
                WHERE NOT EXISTS(SELECT 1 FROM parametr_produktu WHERE parametr_produktu.parametr_pk = pk.parametr_pk AND parametr_produktu.polozka_pk = :ppk)
                    AND k.kategorie_id = :kid
            ) AS s
                JOIN parametr p ON s.parametr_pk = p.parametr_pk
        ";

        $params = [
            ':ppk' => $this->polozka_pk,
            ':kid' => $this->kategorie_id
        ];

        $parametry = [];
        $data = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryAll();

        foreach ($data as $row) {
            $mParametr = new ParametrPolozky();
            $mParametr->load($row, '');

            $parametry[] = $mParametr;
        }

        return $parametry;
    }

    /**
     *
     */
    public function vratParametryPoSkupinach()
    {
        $sql = "
            SELECT sp.skupina_pk, sp.nazev
            FROM skupina_parametru sp
                JOIN parametr p ON sp.skupina_pk = p.skupina_pk
                JOIN parametr_produktu pp ON p.parametr_pk = pp.parametr_pk
            WHERE pp.polozka_pk = :polozka
            GROUP BY sp.skupina_pk, sp.nazev
        ";
        $params = [
            ':polozka' => $this->polozka_pk
        ];

        $data = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryAll();

        $parametry = [];
        foreach ($data as $skupina) {
            $parametry[$skupina['skupina_pk']] = [
                'label' => $skupina['nazev'],
                'items' => []
            ];
        }

        $vsechnyParametry = $this->vratParametry();
        foreach ($vsechnyParametry as $mParametr) {
            if (isset($parametry[$mParametr->skupina_pk])) {
                $parametry[$mParametr->skupina_pk]['items'][] = $mParametr;
            }
        }

        return $parametry;
    }

    /**
     * @param int $pagination
     * @return SqlDataProvider
     */
    protected function _vratDataprovider($pagination = 10)
    {
        $sql = "
          select
              p.*
            , d.nazev as dodavatel
            , k.nazev as kategorie
            , v.nazev as vyrobce
            , coalesce(po.nahled, pp.nahled) as nahled
            , coalesce(po.zdroj, pp.zdroj) as obrazek
            , e.skladem
            , ec.cena_doporucena
            , ec.cena_aktualni
            , coalesce(h.pocet_hodnoceni, 0) AS pocet_hodnoceni
            , coalesce(h.suma_hodnoceni, 0) / coalesce(h.pocet_hodnoceni, 1) AS hodnoceni
            , round((coalesce(h.suma_hodnoceni, 0) / coalesce(h.pocet_hodnoceni, 1))::NUMERIC / 100, 2) AS hodnoceni_float
          from polozka p
            join eshop e on p.polozka_pk = e.polozka_pk and e.viditelne_od <= now() and (e.viditelne_do > now() or e.viditelne_do is null)
            join eshop_cena ec on e.eshop_pk = ec.eshop_pk and ec.platne_od <= now() and (ec.platne_od > now() or ec.platne_do is null)
            join dodavatel d on p.dodavatel_pk = d.dodavatel_pk
            join kategorie k on (p.kategorie_id = k.kategorie_id)
            join vyrobce v on p.vyrobce_pk = v.vyrobce_pk
            left join polozka_obrazek po on po.polozka_obrazek_pk = polozka_obrazek_hlavni_vrat_pk(p.polozka_pk)
            left join polozka_obrazek pp on pp.polozka_obrazek_pk = (select min(polozka_obrazek_pk) from polozka_obrazek)
            left join kategorie n on k.nadrazena = n.kategorie_pk
            left join (
                SELECT
                      polozka_pk
                    , count(*) AS pocet_hodnoceni
                    , sum(procenta) AS suma_hodnoceni
                FROM hodnoceni_produktu
                GROUP BY polozka_pk
            ) AS h ON h.polozka_pk = p.polozka_pk
        ";
        $params = $where = [];

        if ($this->vyrobce_pk != null) {
            $where[] = 'p.vyrobce_pk = :vyrobce';
            $params[':vyrobce'] = $this->vyrobce_pk;
        }

        if ($this->kategorie_id != null) {
            // tohle reseni nebude fungovat pro vetsi zanoreni, ktere se nas stejne ale netyka momentalne
            $where[] = '(p.kategorie_id = :kategorie or n.kategorie_id = :kategorie)';
            $params[':kategorie'] = $this->kategorie_id;
        }

        if (!empty($where)) {
            $sql .= "\n WHERE " . implode(' AND ', $where);
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => !$pagination ? false : [
                'pageSize' => $pagination
            ]
        ]);
    }
}