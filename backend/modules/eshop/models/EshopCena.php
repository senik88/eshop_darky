<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17. 10. 2015
 * Time: 17:24
 */

namespace backend\modules\eshop\models;

use common\components\SqlDataProvider;
use yii\db\ActiveRecord;

/**
 * Class EshopCena
 * @package backend\modules\eshop\models
 *
 * @property integer eshop_cena_pk
 * @property integer eshop_pk
 * @property string platne_od
 * @property string platne_do
 * @property float cena_doporucena
 * @property float cena_aktualni
 */
class EshopCena extends ActiveRecord
{
    /**
     * todo pak upravit stav objednavky - nezajima me kosik, jen zaplacene objednavky
     * @param $pk
     * @return SqlDataProvider
     */
    public function nactiProPolozku($pk)
    {
        $sql = "
            SELECT ec.eshop_cena_pk, ec.cena_aktualni, ec.cena_doporucena, ec.platne_od, ec.platne_do FROM eshop_cena ec
                JOIN eshop e ON ec.eshop_pk = e.eshop_pk
                JOIN polozka p ON p.polozka_pk = e.polozka_pk
            WHERE p.polozka_pk = :pk
            ORDER BY ec.platne_od DESC
        ";

        $params = [
            ':pk' => $pk
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => false
        ]);
    }
}