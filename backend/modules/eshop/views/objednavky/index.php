<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 09.10.2015
 * Time: 20:01
 *
 * @var $mObjednavka Objednavka
 * @var $fSearch ObjednavkaSearchForm
 */

use backend\modules\eshop\forms\ObjednavkaSearchForm;
use backend\modules\eshop\models\Objednavka;
use yii\bootstrap\ActiveForm;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

?>

<div class="row">
    <div class="col-md-12">
        <div class="h2-buttons">
            <h2>Objednávky</h2>
            <?php
            echo Html::a(
                'Které zpracovávám',
                [
                    '/eshop/objednavky/index',
                    Html::getInputName($fSearch, 'stav') => Objednavka::STAV_ZPRACOVAVA,
                    Html::getInputName($fSearch, 'zpracovava_pk') => Yii::$app->user->id
                ],
                [
                    'class' => 'btn btn-info'
                ]
            );

            echo Html::a(
                'Ke zpracování',
                [
                    '/eshop/objednavky/index',
                    Html::getInputName($fSearch, 'stav') => Objednavka::STAV_POTVRZENA,
                    Html::getInputName($fSearch, 'zpracovava_pk') => '#'
                ],
                [
                    'class' => 'btn btn-info'
                ]
            );
            ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="well">
            <?php
            /** @var ActiveForm $form */
            $form = ActiveForm::begin([
                'id' => 'objednavka-search-form',
                'method' => 'GET',
                'action' => ['/eshop/objednavky/index'],
                'options' => []
            ]);
            ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($fSearch, 'stav')->dropDownList(['' => 'Vše'] + Objednavka::itemAlias('stav'))->label('Stav objednávky') ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($fSearch, 'zpracovava_pk')->dropDownList(['' => 'Vše', '#' => 'Nikdo'] + $mObjednavka->vratVsechnyZpracovatele())->label('Objednávku zpracovává') ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 pull-right">
                    <?= Html::submitButton('Filtrovat', ['class' => 'btn btn-success']) ?>
                </div>
            </div>

            <?php
            $form->end();
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= GridView::widget([
            'dataProvider' => $fSearch->search(),
            'columns' => $mObjednavka->vratSloupce()
        ]) ?>
    </div>
</div>