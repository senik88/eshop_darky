<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 30.09.2016
 * Time: 20:47
 *
 * @var View $this
 * @var Objednavka $mObjednavka
 * @var DodaciUdaje $mDodaciUdaje
 * @var FakturacniUdaje $mFakturacniUdaje
 * @var PlatbaDopravy $mPlatbaDopravy
 * @var ZpusobDopravy $mZpusobDopravy
 * @var ZpusobPlatby $mZpusobPlatby
 */

use backend\components\grid\GridView;
use backend\modules\eshop\models\Objednavka;
use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\PlatbaDopravy;
use common\models\ZpusobDopravy;
use common\models\ZpusobPlatby;
use yii\bootstrap\Html;
use yii\web\View;

?>

<div class="row">
    <div class="col-md-12">
        <div class="h2-buttons">
            <h2>Objednávka <?= $mObjednavka->cislo ?></h2>
            <?= $mObjednavka->vykresliButtonyProZpracovani() ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Úkoly</h3>
        <p>
            Něco jako doplnit tracking a tak podobně? I když tracking by se dal přidat do modalu po odkliknutí
            "odeslaná". V případě, že má dopravce příznak trackingu, vyskočí modal s doplněním...
        </p>
    </div>
    <div class="col-md-12">
        <?= Html::a('Vygenerovat fakturu', ['/eshop/faktury/generuj', 'objednavka' => $mObjednavka->objednavka_pk], ['class' => 'btn btn-warning']) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <h3>Dodací údaje</h3>
        <?= $mDodaciUdaje->vypisBlokove() ?>
    </div>

    <div class="col-md-4">
        <h3>Fakturační údaje</h3>
        <?= $mFakturacniUdaje->vypisBlokove() ?>
    </div>

    <div class="col-md-4">
        <h3>Detail objednávky</h3>
        <ul>
            <li>Platba: <?= $mZpusobPlatby->nazev ?></li>
            <li>Doprava: <?= $mZpusobDopravy->nazev ?></li>
            <li>Cena: <?= Yii::$app->formatter->asCurrency($mPlatbaDopravy->cena) ?></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Položky objednávky</h3>

        <?php
        echo GridView::widget([
            // 'id' => 'objednavka-detail-grid',
            'layout' => "{items}",
            //'tableOptions' => ['class' => 'table'],
            'dataProvider' => $mObjednavka->nactiPolozky(),
            'columns' => [
                'kod' => [
                    'value' => function ($model) {
                        return sprintf('PK %s', $model['polozka_pk']);
                    },
                ],
                'nazev' => [
                    'label' => 'Položka',
                    'value' => function ($model) {
                        return Html::a(
                            sprintf('%s %s', $model['titulek'], $model['subtitulek']),
                            ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                        );
                    },
                    'format' => 'raw'
                ],
                'kusu' => [
                    'label' => 'Kusů',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if ($model['stav'] == Objednavka::STAV_ZALOZENA) {
                            $template = '<span class="polozka-kusu">{ks} ks<span class="tlacitka">{plus}&nbsp;{minus}</span></span>';
                        } else {
                            $template = '<span class="polozka-kusu">{ks} ks</span>';
                        }

                        return strtr($template, [
                            '{ks}' => $model['mnozstvi'],
                            '{plus}' => Html::a(Html::icon('plus-sign'), ['/eshop/kosik/pridat', 'id' => $model['polozka_id']], ['data-polozka' => $model['polozka_id'], 'data-func' => 'plus']),
                            '{minus}' => Html::a(Html::icon('minus-sign'), ['/eshop/kosik/odebrat', 'id' => $model['polozka_id']], ['data-polozka' => $model['polozka_id'], 'data-func' => 'minus'])
                        ]);
                    }
                ],
                'skladem' => [
                    'label' => 'Skladem',
                    'value' => function ($model) {
                        return sprintf('%s ks', $model['skladem']);
                    }
                ],
                'blokace' => [
                    'label' => 'Blokace',
                    'value' => function ($model) {
                        return sprintf('%s ks', $model['mnozstvi']);
                    }
                ],
                'cena_ks' => [
                    'attribute' => 'cena_aktualni',
                    'label' => 'Cena / ks',
                    'format' => 'currency',
                ],
                'cena_celkem' => [
                    'attribute' => 'cena_celkem',
                    'label' => 'Cena celkem',
                    'value' => function ($model) {
                        return Yii::$app->getFormatter()->asCurrency($model['cena_celkem'], 'CZK');
                    },
                ],
                /*'akce' => [
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a(
                            Html::icon('remove'),
                            array('/eshop/kosik/odebrat', 'id' => $model['polozka_id'], 'kusu' => 'vse'),
                            [
                                'title' => 'Odebrat z košíku',
                            ]
                        );
                    },
                ]*/
            ]
        ]);
        ?>
    </div>
</div>

<?php
$this->registerJs("
$('#button-objednavka-odeslana').on('click', function() {
    var id = $(this).attr('data-objednavka-pk');
    Senovo.zkontrolujObjednavkuPredOdeslanim(id)
});
");