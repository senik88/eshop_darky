<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 28.02.2016
 * Time: 10:20
 *
 * @var $this View
 * @var ParametrForm $fParametr
 * @var $mKategorie Kategorie
 * @var $aParametry Parametr[]
 */

use backend\modules\eshop\forms\ParametrForm;
use backend\modules\eshop\models\Kategorie;
use common\models\Parametr;
use common\models\SkupinaParametru;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;

?>

<div class="row">
    <?php
    $form = ActiveForm::begin([
        'id' => 'parametry-polozky-form',
        'method' => 'POST',
        'action' => ['/eshop/polozky/parametry']
    ]);
    ?>

        <div class="form-fields">
            <?= $form->field($fParametr, 'polozka_pk')->hiddenInput()->label(false) ?>
            <div class="h3-buttons">
                <h3>Parametry z kategorie</h3>
                <div class="clearfix"></div>
            </div>
            <?php
            if (!empty($aParametry)) {
                foreach ($aParametry as $parametr) {
                    echo $this->render('_parametr', ['mParametr' => $parametr, 'klic' => $parametr->parametr_pk]);
                }
            } else {
                echo Html::tag('p', "Kategorie neobsahuje žádné parametry, přidat je můžete v " . Html::a("editaci kategorie", ['/eshop/kategorie/upravit', 'id' => $mKategorie->kategorie_pk]));
            }
            ?>
            <div class="clearfix"></div>

            <div class="h3-buttons">
                <h3>Parametry produktu</h3>
                <?= Html::a('Přidat', '#', [
                    'class' => 'btn btn-info btn-sm',
                    'data-toggle' => 'modal',
                    'data-target' => '#parametr-polozky-modal'
                ]) ?>
                <div class="clearfix"></div>
            </div>
            <p>todo...</p>
            <div class="clearfix"></div>
        </div>

        <div class="form-actions">
            <?= Html::submitButton('Uložit', ['class' => 'btn btn-success']) ?>
        </div>

    <?php
    ActiveForm::end();
    ?>
</div>

<div class="row">
    <div class="col-sm-6">

        <?php
        /*$form = ActiveForm::begin();

        echo $form->field($fParametr, 'polozka_pk')->hiddenInput()->label(false);
        echo $form->field($fParametr, 'nazev');
        echo $form->field($fParametr, 'hodnota');
        echo $form->field($fParametr, 'hodnota_typ')->dropDownList(\common\models\Parametr::itemAlias('typy'));
        echo $form->field($fParametr, 'jednotka');
        echo $form->field($fParametr, 'skupina_pk')->dropDownList((new SkupinaParametru())->vratProDropdown());

        echo Html::submitButton('Uložit', ['name' => 'akce', 'value' => 'parametr']);

        ActiveForm::end();*/
        ?>
    </div>
</div>

<?php

\yii\bootstrap\Modal::begin([
    'id' => 'parametr-polozky-modal',
    'header' => 'Přidat parametr k položce'
]);
{
    echo Html::beginTag('div', ['class' => 'modal-body']);
    {

    }
    echo Html::endTag('div');
}
\yii\bootstrap\Modal::end();