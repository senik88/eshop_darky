<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 09.10.2015
 * Time: 20:01
 *
 * @var $mPolozka PolozkaForm
 */

use backend\modules\eshop\forms\PolozkaForm;
?>

<div class="row">
    <div class="col-sm-12">
        <h2>Nová položka eshopu</h2>
        <span class="required-info">Položky označené <b>*</b> jsou povinné</span>
    </div>
</div>

<?= $this->render('_form', ['mPolozka' => $mPolozka]) ?>