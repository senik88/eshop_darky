<?php
use backend\modules\eshop\models\Polozka;
use common\forms\GalerieForm;
use common\models\PolozkaObrazek;
use dosamigos\fileupload\FileUploadUI;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;

/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 27.11.2015
 * Time: 19:57
 *
 * @var $mPolozka Polozka
 * @var $mPolozkaObrazek PolozkaObrazek
 * @var $fGalerie GalerieForm
 */
?>

<div class="row" style="margin-top: 20px;">
    <div class="col-sm-8">
        <?= $mPolozka->popis ?>
    </div>
    <div class="col-sm-4">
        <?= DetailView::widget([
            'model' => $mPolozka,
            'attributes' => [
                'polozka_id',
                'vyrobce' => [
                    'value' => $mPolozka->mVyrobce->nazev,
                    'attribute' => 'vyrobce_pk'
                ],
                'dodavatel' => [
                    'value' => $mPolozka->mDodavatel->nazev,
                    'attribute' => 'dodavatel_pk'
                ],
                'kategorie' => [
                    'value' => $mPolozka->mKategorie->nazev,
                    'attribute' => 'kategorie_id'
                ],
            ],
            'options' => [
                'class' => 'table table-striped table-condensed detail-view'
            ]
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div>
            <h3>Galerie obrázků</h3>
            <?= Html::button('Nahrát', [
                'class' => 'btn btn-xs btn-info',
                'data-toggle' => 'modal',
                'data-target' => '#galerie-modal'
            ]) ?>
        </div>

        <?php
        foreach ($mPolozka->obrazky as $mPolozkaObrazek) {
            echo Html::a(
                Html::img($mPolozkaObrazek->nahled, ['class' => 'img-thumbnail']),
                $mPolozkaObrazek->zdroj,
                [
                    'class' => 'fancybox',
                    'rel' => 'group',
                    'title' => $mPolozkaObrazek->popisek
                ]
            );
        }
        ?>
    </div>
</div>

<?php
Modal::begin([
    'header' => '<h4>Upload obrázků</h4>',
    'id' => 'galerie-modal'
]);

echo FileUploadUI::widget([
    'model' => $fGalerie,
    'attribute' => 'soubor',
    'url' => ['/eshop/polozky/obrazky', 'polozka' => $mPolozka->polozka_pk],
    'gallery' => false,
    'fieldOptions' => [
        'accept' => 'image/*'
    ],
    'clientOptions' => [
        'maxFileSize' => 2000000
    ],
    'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {}',
        'fileuploadfail' => 'function(e, data) {}',
        'fileuploadfinished' => "function(e, data) {
            //$('#galerie-modal').modal('hide');
            //window.location.reload();
        }",
    ],
]);

Modal::end();