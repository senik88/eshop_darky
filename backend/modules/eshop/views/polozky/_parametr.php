<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 20.06.2016
 * Time: 18:54
 * @var $this View
 * @var $mParametr ParametrPolozky
 * @var $klic integer
 */

use common\models\ParametrPolozky;
use yii\bootstrap\Html;
use yii\web\View;
?>
<div class="parametr-wrapper parametr-wrapper-<?= $klic ?> col-md-3">

    <div class="form-group field-parametrform-hodnota">
        <label class="control-label" for="parametrform-hodnota-<?= $klic ?>"><?= $mParametr->formatujNazev() ?></label>
        <?= Html::activeHiddenInput($mParametr, 'parametr_pk', [
            'name' => "ParametryPolozkyForm[parametry][{$klic}][ParametrPolozky][parametr_pk]",
        ]) ?>
        <?= Html::activeTextInput($mParametr, 'hodnota', [
            'name' => "ParametryPolozkyForm[parametry][{$klic}][ParametrPolozky][hodnota]",
            'id' => "parametrform-hodnota-{$klic}",
            'class' => "form-control"
        ]) ?>
        <p class="help-block help-block-error"></p>
    </div>

    <span class="parametr-data">

    </span>
</div>