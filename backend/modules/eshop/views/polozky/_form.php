<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.11.2015
 * Time: 21:15
 *
 * @var $mPolozka PolozkaForm
 */

use backend\modules\eshop\forms\PolozkaForm;
use backend\modules\eshop\models\Dodavatel;
use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Vyrobce;
use dosamigos\datetimepicker\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\redactor\widgets\Redactor;
use yii\widgets\Pjax;

$form = ActiveForm::begin([
    'layout' => 'default'
]);
?>

    <div class="form-fields">
        <div class="row">
            <div class="col-md-12">
                <h3>Detaily položky</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($mPolozka, 'titulek') ?>
                <?= $form->field($mPolozka, 'subtitulek') ?>
            </div>
            <div class="col-md-6">
                <?php
                echo Html::beginTag('div', ['class' => 'select-btn-wrapper']);
                {
                    echo $form->field($mPolozka, 'vyrobce_pk')->dropDownList(ArrayHelper::map(Vyrobce::find()->all(), 'vyrobce_pk', 'nazev'));

                    echo Html::beginTag('div', ['class' => 'btn-wrapper']);
                    {
                        echo Html::button('+', [
                            'class' => 'btn btn-info',
                            'id' => 'modalButtonVyrobce',
                            'value' => Url::to(['/eshop/vyrobci/pridat']),
                            // other options
                            'data' => [
                                //'toggle' => 'modal',
                                'target' => '#modalVyrobce',
                            ],
                        ]);
                    }
                    echo Html::endTag('div');
                }
                echo Html::endTag('div');
                ?>

                <?php
                echo Html::beginTag('div', ['class' => 'select-btn-wrapper']);
                {
                    echo $form->field($mPolozka, 'dodavatel_pk')->dropDownList(ArrayHelper::map(Dodavatel::find()->all(), 'dodavatel_pk', 'nazev'));

                    echo Html::beginTag('div', ['class' => 'btn-wrapper']);
                    {
                        echo Html::button('+', [
                            'class' => 'btn btn-info',
                            'id' => 'modalButtonDodavatel',
                            'value' => Url::to(['/eshop/dodavatele/pridat']),
                            // other options
                            'data' => [
                                //'toggle' => 'modal',
                                'target' => '#modalDodavatel',
                            ],
                        ]);
                    }
                    echo Html::endTag('div');
                }
                echo Html::endTag('div');
                ?>

                <?php
                echo Html::beginTag('div', ['class' => 'select-btn-wrapper']);
                {
                    echo $form->field($mPolozka, 'kategorie_id')->dropDownList(ArrayHelper::map(Kategorie::find()->orderBy('nazev asc')->all(), 'kategorie_id', 'nazev'));

                    echo Html::beginTag('div', ['class' => 'btn-wrapper']);
                    {
                        echo Html::button('+', [
                            'class' => 'btn btn-info',
                            'id' => 'modalButtonKategorie',
                            'value' => Url::to(['/eshop/kategorie/pridat']),
                            // other options
                            'data' => [
                                //'toggle' => 'modal',
                                'target' => '#modalKategorie',
                            ],
                        ]);
                    }
                    echo Html::endTag('div');
                }
                echo Html::endTag('div');
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($mPolozka, 'popis')->widget(Redactor::className(), [
                    'clientOptions' => [
                        'lang' => 'cs',
                        'minHeight' => 300
                    ]
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h3>Platnost</h3>
                <?= $form->field($mPolozka, 'viditelne_od')->widget(DateTimePicker::className()) ?>
                <?= $form->field($mPolozka, 'viditelne_do')->widget(DateTimePicker::className()) ?>
                <?= $form->field($mPolozka, 'skladem') ?>
                <?= $form->field($mPolozka, 'blokace')->textInput(['disabled' => true]) ?>
            </div>
            <div class="col-md-6">
                <h3>Cena položky</h3>
                <?php if (Yii::$app->controller->action->id == 'pridat') { ?>
                    <?= $form->field($mPolozka, 'cena_doporucena') ?>
                    <?= $form->field($mPolozka, 'cena_aktualni') ?>
                    <?= $form->field($mPolozka, 'platne_od')->widget(DateTimePicker::className()) ?>
                    <?= $form->field($mPolozka, 'platne_do')->widget(DateTimePicker::className()) ?>
                <?php } else { ?>
                    Cenu je možné editovat pouze v detailu položky.
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="form-actions well">
        <?= Html::submitButton('Uložit', [
            'class' => 'btn btn-success'
        ]) ?>
        <?= Html::a('Zrušit', ['/eshop/polozky/index'], [
            'class' => 'btn btn-danger'
        ]) ?>
    </div>

<?php
ActiveForm::end();

Modal::begin([
    'header' => '<h4>Přidat výrobce</h4>',
    'id' => 'modalVyrobce',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentVyrobce'></div>";

Modal::end();

Modal::begin([
    'header' => '<h4>Přidat dodavatele</h4>',
    'id' => 'modalDodavatel',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentDodavatel'></div>";

Modal::end();

Modal::begin([
    'header' => '<h4>Přidat kategorii</h4>',
    'id' => 'modalKategorie',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentKategorie'></div>";

Modal::end();
