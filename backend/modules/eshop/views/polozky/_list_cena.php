<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 27.11.2015
 * Time: 20:13
 *
 * @var $model array
 * @var $key
 * @var $index
 * @var $widget
 */


use yii\bootstrap\Html;

//d($model, $key, $index, $widget);
?>

<table class="table-cena">
    <tr>
        <td>Aktuální:</td>
        <td><?= Yii::$app->formatter->asCurrency($model['cena_aktualni']) ?></td>
        <td>Doporučená:</td>
        <td><?= Yii::$app->formatter->asCurrency($model['cena_doporucena']) ?></td>
        <td rowspan="2">
            <?php
            if ($index == 0) {
                echo Html::a(
                    Html::icon('pencil'),
                    '#',
                    [
                        'title' => 'Upravit'
                    ]
                );
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>
            Platné:
        </td>
        <td colspan="3">
            <?php
            $datumy = [
                Yii::$app->formatter->asDatetime($model['platne_od']),
                Yii::$app->formatter->asDatetime($model['platne_do'])
            ]
            ?>

            od <?= implode(' do ', $datumy) ?>
        </td>
    </tr>
</table>