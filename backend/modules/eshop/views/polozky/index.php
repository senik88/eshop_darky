<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 09.10.2015
 * Time: 20:01
 *
 * @var $mPolozka Polozka
 * @var $this View
 * @var $rozsireny boolean
 */
use backend\modules\eshop\models\Polozka;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

?>

<div class="row">
    <div class="col-md-12">
        <div class="h2-buttons">
            <h2>Položky eshopu</h2>
            <?= Html::a('Nová', ['/eshop/polozky/pridat'], [
                'class' => 'btn btn-info'
            ]) ?>
        </div>
    </div>
</div>

<?= $this->render('_search', [
    'mPolozka' => $mPolozka,
    'rozsireny' => $rozsireny
]) ?>

<?= GridView::widget([
    'dataProvider' => $mPolozka->vypis(),
    'columns' => $mPolozka->vratSloupce()
]) ?>