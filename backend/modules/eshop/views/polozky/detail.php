<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 10.10.2015
 * Time: 16:12
 *
 * @var $mPolozka Polozka
 * @var $mPolozkaObrazek PolozkaObrazek
 * @var $fGalerie GalerieForm
 * @var $mEshopCena EshopCena
 * @var $fParametr ParametrForm
 * @var $mKategorie Kategorie
 * @var $aParametry Parametr[]
 */
use backend\modules\eshop\forms\ParametrForm;
use backend\modules\eshop\models\EshopCena;
use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use common\forms\GalerieForm;
use common\models\Parametr;
use common\models\PolozkaObrazek;
use dosamigos\fileupload\FileUploadUI;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\widgets\DetailView;

?>

<div class="row">
    <div class="h2-buttons">
        <h2><?= $mPolozka->titulek ?> <small><?= $mPolozka->subtitulek ?></small></h2>
        <?= Html::a('Zobrazit na frontu', Yii::$app->urlManagerFrontEnd->createUrl(['/eshop/katalog/detail', 'id' => $mPolozka->polozka_id]),['class' => 'btn btn-info', 'target' => '_blank']) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        echo Tabs::widget([
            'items' => [
                [
                    'label' => 'Detaily',
                    'content' => $this->render('_tab_detaily', ['mPolozka' => $mPolozka, 'fGalerie' => $fGalerie]),
                    'options' => [
                        'id' => 'info'
                    ]
                ],
                [
                    'label' => 'Parametry',
                    'content' => $this->render('_tab_parametry', [
                        'mPolozka' => $mPolozka,
                        'mKategorie' => $mKategorie,
                        'fParametr' => $fParametr,
                        'aParametry' => $aParametry
                    ]),
                    'options' => [
                        'id' => 'parametry'
                    ]
                ],
                [
                    'label' => 'Statistiky',
                    'content' => $this->render('_tab_stats', ['mPolozka' => $mPolozka, 'mEshopCena' => $mEshopCena]),
                    'options' => [
                        'id' => 'statistiky'
                    ]
                ]
            ]
        ])
        ?>
    </div>
</div>