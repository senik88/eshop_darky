<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 27.11.2015
 * Time: 19:58
 *
 * @var $mPolozka Polozka
 * @var $mEshopCena EshopCena
 */
use backend\modules\eshop\models\EshopCena;
use backend\modules\eshop\models\Polozka;

?>

<div class="row">
    <div class="col-sm-6">
        <h3>Ceny</h3>
        <?= \yii\widgets\ListView::widget([
            'id' => 'polozka-seznam-cen',
            'dataProvider' => $mEshopCena->nactiProPolozku($mPolozka->polozka_pk),
            'itemView' => function($model, $key, $index, $widget) {
                return $this->render('_list_cena', [
                    'model' => $model,
                    'key' => $key,
                    'index' =>$index,
                    'widget' => $widget
                ]);
            }
        ]) ?>
    </div>
    <div class="col-sm-6">
        <h3>Vývoj cen</h3>
        <p>
            tady bude grafík
        </p>
    </div>
</div>