<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.10.2015
 * Time: 18:09
 *
 * @var $mPolozka Polozka
 * @var $this View
 * @var $rozsireny
 */

use backend\modules\eshop\models\Dodavatel;
use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use backend\modules\eshop\models\Vyrobce;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

$form = ActiveForm::begin([
    'id' => 'polozky-search-form',
    'method' => 'GET',
    'options' => [
        'class' => 'well'
    ]
]);
?>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($mPolozka, 'titulek') ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($mPolozka, 'vyrobce_pk')->dropDownList(ArrayHelper::map(Vyrobce::find()->all(), 'vyrobce_pk', 'nazev'), [
            'prompt' => 'Všichni výrobci'
        ]) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($mPolozka, 'dodavatel_pk')->dropDownList(ArrayHelper::map(Dodavatel::find()->all(), 'dodavatel_pk', 'nazev'), [
            'prompt' => 'Všichni dodavatelé'
        ]) ?>
    </div>
</div>

<div id="rozsireny-filtr" class="row collapse <?= $rozsireny ? 'in' : '' ?>">
    <div class="col-md-4">
        <?= $form->field($mPolozka, 'zobrazit')->dropDownList(['VSE' => 'Všechny', 'AKTUALNI' => 'Pouze aktuální']) ?>
    </div>
    <div class="col-md-4">

    </div>
    <div class="col-md-4">
        <?= $form->field($mPolozka, 'kategorie_id')->dropDownList(ArrayHelper::map(Kategorie::find()->all(), 'kategorie_id', 'nazev'), [
            'prompt' => 'Všechny kategorie'
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <?= Html::button('Rozšířený filtr', ['class' => 'btn btn-info', 'id' => 'filter-collapse']) ?>
        </div>
        <div class="pull-right">
            <?= Html::submitButton('Filtrovat', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

<?php
ActiveForm::end();

$this->registerJs("
$('#filter-collapse').on('click', function() {
    $('#rozsireny-filtr').collapse('toggle');
});
");