<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.11.2015
 * Time: 21:15
 *
 * @var $mPolozka Polozka
 */
use backend\modules\eshop\models\Polozka;
use yii\bootstrap\Html;

?>

<div class="row">
    <div class="h2-buttons">
        <h2>Nová položka eshopu</h2>
        <?= Html::a('Zobrazit na frontu', Yii::$app->urlManagerFrontEnd->createUrl(['/eshop/katalog/detail', 'id' => $mPolozka->polozka_id]),['class' => 'btn btn-info', 'target' => '_blank']) ?>
    </div>
    <span class="required-info">Položky označené <b>*</b> jsou povinné</span>
</div>

<?= $this->render('_form', ['mPolozka' => $mPolozka]) ?>