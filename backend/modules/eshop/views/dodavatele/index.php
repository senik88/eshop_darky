<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mDodavatel Dodavatel
 */
use backend\modules\eshop\models\Dodavatel;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-12 h2-buttons">
        <h2>Správa dodavatelů</h2>
        <?= Html::a('Nový', ['/eshop/dodavatele/pridat'], [
            'class' => 'btn btn-info'
        ]) ?>
    </div>
</div>

<?php
echo GridView::widget([
    'dataProvider' => $mDodavatel->search(),
    'columns' => $mDodavatel->vratSloupce()
]);