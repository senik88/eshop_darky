<?php

use backend\modules\eshop\forms\DodavatelForm;
use yii\web\View;
?>

    <h2>Úprava dodavatele</h2>

<?= $this->render('_form', [
    'mDodavatelForm' => $mDodavatelForm
]); ?>