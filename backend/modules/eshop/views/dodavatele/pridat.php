<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mDodavatelForm DodavatelForm
 */
use backend\modules\eshop\forms\Dodavatel;

?>

<h2>Nový dodavatel</h2>

<?= $this->render('_form', [
    'mDodavatelForm' => $mDodavatelForm
]) ?>