<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:22
 *
 * @var $mDodavatelForm DodavatelForm
 */

use backend\modules\eshop\forms\DodavatelForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'dodavatel-form',
    'layout' => 'horizontal'
]);
?>
<div class="form-fields">
    <?= $form->field($mDodavatelForm, 'nazev') ?>
</div>

<div class="form-actions">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
    <?= Html::a('Zrušit', ['/eshop/dodavatele/index'], [
        'class' => 'btn btn-danger'
    ]) ?>
</div>
<?php
ActiveForm::end();