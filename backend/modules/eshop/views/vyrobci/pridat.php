<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mUzivatelForm UzivatelFrontendForm
 */
use backend\modules\eshop\forms\Vyrobce;

?>

<h2>Nový výrobce</h2>

<?= $this->render('_form', [
    'mVyrobceForm' => $mVyrobceForm
]) ?>