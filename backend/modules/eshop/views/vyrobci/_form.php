<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:22
 *
 * @var $mUzivatelForm UzivatelFrontendForm
 * @var $mVyrobceForm VyrobceForm
 */

use backend\modules\eshop\forms\VyrobceForm;
use backend\modules\uzivatel\forms\UzivatelFrontendForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'vyrobce-form',
    'layout' => 'horizontal',
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]);
?>
<div class="form-fields">
    <?= $form->field($mVyrobceForm, 'nazev', ['errorOptions' => ['encode' => false]]) ?>

    <?= $form->field($mVyrobceForm, 'web') ?>

    <?= $form->field($mVyrobceForm, 'logo')->fileInput() ?>
</div>

<div class="form-actions">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
    <?= Html::a('Zrušit', ['/eshop/vyrobci/index'], [
        'class' => 'btn btn-danger'
    ]) ?>
</div>
<?php
ActiveForm::end();