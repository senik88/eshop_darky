<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.09.2015
 * Time: 21:36
 *
 * @var $mVyrobce Vyrobce
 * @var $mLogo Logo
 */

use backend\modules\eshop\models\Vyrobce;
use common\models\Logo;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>

<div class="row">
    <div class="col-sm-12 h2-buttons">
        <h2><?= $mVyrobce->nazev ?></h2>
        <?= Html::a(
            'Upravit',
            ['/eshop/vyrobci/upravit', 'id' => $mVyrobce->vyrobce_pk],
            [
                'class' => 'btn btn-warning'
            ]
        ) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Detaily</h3>
    </div>

    <div class="col-md-4">
        <div class="vyrobce-logo">
            <?php
            if ($mLogo != null) {
                $img = Html::img(
                    Url::to(['/files/image', 'hash' => $mLogo->hash]),
                    [
                        'alt' => $mLogo->zdroj
                    ]
                );
            } else {
                $img = null;
            }

            if ($mVyrobce->web != null) {
                $link = Html::a($img == null ? $mVyrobce->web : $img, $mVyrobce->web, [
                    'target' => '_blank'
                ]);
            } else {
                $link = null;
            }

            echo $link;
            ?>
        </div>
    </div>

    <div class="col-md-4">
        <?= \yii\widgets\DetailView::widget([
            'model' => $mVyrobce,
            'attributes' => [
                'vyrobce_pk',
                'vyrobce_id',
                'nazev',
                'datum_vlozeni',
            ]
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Seznam zboží</h3>
    </div>
</div>