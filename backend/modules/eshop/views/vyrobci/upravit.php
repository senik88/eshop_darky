<?php

use backend\modules\eshop\forms\VyrobceForm;
use yii\web\View;
?>

    <h2>Úprava výrobce</h2>

<?= $this->render('_form', [
    'mVyrobceForm' => $mVyrobceForm
]); ?>