<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mVyrobce Vyrobce
 */
use backend\modules\eshop\models\Vyrobce;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-12 h2-buttons">
        <h2>Správa výrobců</h2>
        <?= Html::a('Nový', ['/eshop/vyrobci/pridat'], [
            'class' => 'btn btn-info'
        ]) ?>
    </div>
</div>

<?php
echo GridView::widget([
    'dataProvider' => $mVyrobce->search(),
    'columns' => $mVyrobce->vratSloupce()
]);