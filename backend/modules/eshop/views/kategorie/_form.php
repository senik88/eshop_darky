<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:22
 *
 * @var $this View
 * @var $mKategorieForm KategorieForm
 * @var $aParametry array
 */

use backend\modules\eshop\forms\KategorieForm;
use backend\modules\eshop\models\Kategorie;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$form = ActiveForm::begin([
    'id' => 'kategorie-form',
    'layout' => 'horizontal',
    'enableClientValidation' => false
]);
?>
<div class="form-fields">
    <?= $form->field($mKategorieForm, 'nazev') ?>
    <?= $form->field($mKategorieForm, 'nadrazena')->dropdownList(
        Kategorie::find()->where(['nadrazena' => null])->select(['nazev', 'kategorie_pk'])->indexBy('kategorie_pk')->column(),
        ['prompt'=>'Vyberte kategorii'])->hint('Je možné vybrat pouze kategorie, které nemají žádnou nadřazenou')
    ?>
    <?= $form->field($mKategorieForm, 'menu_zobrazit')->checkbox()->label("Zobrazit v hlavním menu") ?>
</div>

<div class="form-fields" id="kategorie-parametry">
    <div class="h3-buttons">
        <h3>Parametry produktů kategorie</h3>
        <?= Html::a('Přidat', '', ['id' => 'kategorie-pridat-parametr', 'class' => 'btn btn-primary btn-sm']) ?>
        <div class="clearfix"></div>
    </div>

    <?php
    foreach ($aParametry as $mParametr) {
        echo $this->render('_parametr', ['mParametr' => $mParametr, 'klic' => $mParametr->parametr_pk]);
    }
    ?>
</div>

<div class="form-actions">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
    <?= Html::a('Zrušit', ['/eshop/kategorie/index'], [
        'class' => 'btn btn-danger'
    ]) ?>
</div>
<?php
ActiveForm::end();

$this->registerJs("
$('a#kategorie-pridat-parametr').on('click', function() {

    var wrapper = $('#kategorie-parametry');
    var klicPolozky = $('.parametr-wrapper').length;

    while ($('.parametr-wrapper-'+klicPolozky).length != 0) {
        klicPolozky++;
    }

    var request = $.ajax({
        url: 'ajax-pridat-parametr',
        data: {klic: klicPolozky},
        type: 'POST'
    });

    request.done(function(response) {
        wrapper.append(response);
        $('select').selectpicker();
    });

    // todo dodelat

    return false;
});
");