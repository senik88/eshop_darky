<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.09.2015
 * Time: 21:36
 *
 * @var $mKategorie Kategorie
 * @var $aParametry Parametr[]
 */

use backend\modules\eshop\models\Kategorie;
use common\models\Parametr;
use yii\bootstrap\Html;

?>

<div class="row">
    <div class="col-sm-12 h2-buttons">
        <h2><?= $mKategorie->nazev ?></h2>
        <?= Html::a(
            'Upravit',
            ['/eshop/kategorie/upravit', 'id' => $mKategorie->kategorie_pk],
            [
                'class' => 'btn btn-warning'
            ]
        ) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <h3>Detaily</h3>
        <?= \yii\widgets\DetailView::widget([
            'model' => $mKategorie,
            'attributes' => [
                'kategorie_pk',
                'nazev',
                'nadrazena' => [
                    'label' => 'nadřazena',
                    'value' => $mKategorie->vratNazevNadrazene()
                ]
            ]
        ]) ?>
    </div>

    <div class="col-md-4">
        <h3>Parametry produktů v kategorii</h3>
        <ul>
        <?php
        foreach ($aParametry as $mParametr) {
            $content = $mParametr->nazev;
            if ($mParametr->jednotka != null) {
                $content .= " [" . $mParametr->jednotka . "]";
            }

            echo Html::tag('li', $content);
        }
        ?>
        </ul>
    </div>
</div>
<?= Html::a(
    'Zpět na seznam kategorií',
    ['/eshop/kategorie/index'],
    [
        'class' => 'btn btn-warning'
    ]
) ?>