<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mKategorieForm KategorieForm
 * @var $aParametry Parametr[]
 */

use backend\modules\eshop\forms\KategorieForm;
use common\models\Parametr;

?>

<h2>Nová kategorie</h2>

<?= $this->render('_form', [
    'mKategorieForm' => $mKategorieForm,
    'aParametry' => $aParametry
]) ?>