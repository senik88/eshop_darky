<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 18.06.2016
 * Time: 13:04
 *
 * @var $this View
 * @var $mParametr Parametr
 * @var $klic integer
 */
use common\models\Parametr;
use common\models\SkupinaParametru;
use yii\bootstrap\Html;
use yii\web\View;
?>

<div class="parametr-wrapper parametr-wrapper-<?= $klic ?>">
    <?= Html::activeDropDownList($mParametr, 'parametr_pk', ['' => 'Nový parametr'] + $mParametr->vratProDropdown(), [
        'name' => "KategorieForm[parametry][$klic][Parametr][parametr_pk]",
        'id' => Html::getInputId($mParametr, 'parametr_pk') . '-' . $klic,
        'data-live-search' => "true"
    ]) ?>
    <span class="parametr-data">
        <?= Html::activeTextInput($mParametr, 'nazev', [
            'placeholder' => 'Název',
            'name' => "KategorieForm[parametry][$klic][Parametr][nazev]",
            'id' => Html::getInputId($mParametr, 'nazev') . '-' . $klic,
            'class' => 'form-control'
        ]) ?>

        <?= Html::activeDropDownList($mParametr, 'hodnota_typ', Parametr::itemAlias('typy'), [
            'name' => "KategorieForm[parametry][$klic][Parametr][hodnota_typ]",
            'id' => Html::getInputId($mParametr, 'hodnota_typ') . '-' . $klic
        ]) ?>

        <?= Html::activeTextInput($mParametr, 'jednotka', [
            'placeholder' => 'Jednotka',
            'name' => "KategorieForm[parametry][$klic][Parametr][jednotka]",
            'id' => Html::getInputId($mParametr, 'jednotka') . '-' . $klic,
            'class' => 'form-control'
        ]) ?>

        <?= Html::activeDropDownList($mParametr, 'skupina_pk', (new SkupinaParametru())->vratProDropdown(), [
            'name' => "KategorieForm[parametry][$klic][Parametr][skupina_pk]",
            'id' => Html::getInputId($mParametr, 'skupina_pk') . '-' . $klic
        ]) ?>
    </span>
</div>