<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mKategorie Kategorie
 */
use backend\modules\eshop\models\Kategorie;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-12 h2-buttons">
        <h2>Správa kategorií</h2>
        <?= Html::a('Nová', ['/eshop/kategorie/pridat'], [
            'class' => 'btn btn-info'
        ]) ?>
    </div>
</div>

<?php
echo GridView::widget([
    'dataProvider' => $mKategorie->search(),
    'columns' => $mKategorie->vratSloupce()
]);