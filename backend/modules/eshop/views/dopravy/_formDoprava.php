<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:22
 *
 * @var $mDopravaForm DopravaForm
 */

use backend\modules\eshop\forms\DopravaForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'method' => 'POST',
    'options' => [
        'enctype' => 'multipart/form-data'
    ]

]);
?>
<div class="form-fields">
    <?= $form->field($mDopravaForm, 'nazev') ?>
    <?= $form->field($mDopravaForm, 'logo')->fileInput() ?>
    <?= $form->field($mDopravaForm, 'ma_tracking')->checkbox() ?>
</div>

<div class="form-actions">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
    <?= Html::a('Zrušit', ['/eshop/dopravy/index'], [
        'class' => 'btn btn-danger'
    ]) ?>
</div>
<?php
ActiveForm::end();