<?php

use backend\modules\eshop\forms\DopravaForm;
use yii\web\View;
?>

    <h2>Úprava způsobu dopravy</h2>

<?= $this->render('_formDoprava', [
    'mDopravaForm' => $mDopravaForm
]); ?>