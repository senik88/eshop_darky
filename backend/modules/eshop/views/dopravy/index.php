<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $this View
 * @var $mZpusobDopravy ZpusobDopravy
 * @var $mPlatbaDopravy PlatbaDopravy
 * @var $mZpusobPlatby ZpusobPlatby
 */

use common\models\ZpusobDopravy;
use common\models\ZpusobPlatby;
use common\models\PlatbaDopravy;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\web\View;

?>

    <div class="row">
        <div class="col-md-12 h2-buttons">
            <h2>Správa doprav a plateb</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6" id="doprava">
            <h3>Přehled doprav</h3>

            <?= Html::button('Nová doprava', [
                'value' => Url::to('pridat-dopravu'),
                'class' => 'btn btn-info',
                'id' => 'modalButtonDoprava',
                'data-toggle' => 'modal',
                'data-target' => '#modalDoprava'
            ]) ?>

            <?php Modal::begin([
                    'header' => '<h3>Nová doprava</h3>',
                    'id' => 'modalDoprava',
                    'size' => 'modal-lg',
                ]);

                echo $this->render('_formDoprava', [
                    'mZpusobDopravy' => $mZpusobDopravy,
                    'mDopravaForm' => $fDopravaForm
                ]);

                Modal::end();
            ?>

            <?= GridView::widget([
                'dataProvider' => $mZpusobDopravy->search(),
                'columns' => $mZpusobDopravy->vratSloupce()
            ]) ?>
        </div>


        <div class="col-md-6" id="platba">
            <h3>Přehled plateb</h3>

            <?= Html::button('Nová platba', [
                'value' => Url::to('pridat-platbu'),
                'class' => 'btn btn-info',
                'id' => 'modalButtonPlatba',
            ]) ?>

            <?php Modal::begin([
                'header' => '<h4>Test</h4>',
                'id' => 'modalPlatba',
                'size' => 'modal-lg',
                ]);

                echo "<div id='modalContentPlatba'></div>";

                Modal::end();
            ?>

            <?= GridView::widget([
                'dataProvider' => $mZpusobPlatby->search(),
                'columns' => $mZpusobPlatby->vratSloupce()
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" id="platbaDopravy">
            <h3>Přehled spojení dopravy a platby</h3>

            <?= /*Html::button('Nové spojení', [
                'value' => Url::to('pridat-spojeni'),
                'class' => 'btn btn-info',
                'id' => 'modalButtonSpojeni',
            ])*/
            Html::a(
                'Nové spojení',
                ['pridat-spojeni'],
                ['class' => 'btn btn-info']
            );
            ?>

            <?php /*Modal::begin([
                'header' => '<h4>Test</h4>',
                'id' => 'modalSpojeni',
                'size' => 'modal-lg',
                ]);

                echo "<div id='modalContentSpojeni'></div>";

                Modal::end();*/
            ?>

            <?= GridView::widget([
                'dataProvider' => $mPlatbaDopravy->search(),
                'columns' => $mPlatbaDopravy->vratSloupce()
            ]) ?>
        </div>
    </div>

<?php



