<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mDopravaForm DopravaForm
 */
use backend\modules\eshop\forms\Doprava;

?>

<h2>Nová doprava</h2>

<?= $this->render('_formDoprava', [
    'mDopravaForm' => $mDopravaForm
]) ?>