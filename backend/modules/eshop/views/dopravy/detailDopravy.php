<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.09.2015
 * Time: 21:36
 *
 * @var $mZpusobDopravy ZpusobDopravy
 */

use common\models\ZpusobDopravy;
use yii\bootstrap\Html;

?>

<div class="row">
    <div class="col-sm-12 h2-buttons">
        <h2><?= $mZpusobDopravy->nazev ?></h2>
        <?= Html::a(
            'Upravit',
            ['/eshop/dopravy/upravit-dopravu', 'id' => $mZpusobDopravy->zpusob_dopravy_pk],
            [
                'class' => 'btn btn-warning'
            ]
        ) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <h3>Detaily</h3>
        <?= \yii\widgets\DetailView::widget([
            'model' => $mZpusobDopravy,
            'attributes' => [
                'zpusob_dopravy_pk',
                'nazev'
            ]
        ]) ?>
    </div>
    <div class="col-md-4">
        <h3>Fakturační údaje</h3>
    </div>
    <div class="col-md-4">
        <h3>Statistiky</h3>
    </div>
</div>
