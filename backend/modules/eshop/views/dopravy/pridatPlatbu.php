<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mPlatbaForm PlatbaForm
 */
use backend\modules\eshop\forms\Platba;

?>

<h2>Nová platba</h2>

<?= $this->render('_formPlatba', [
    'mPlatbaForm' => $mPlatbaForm
]) ?>