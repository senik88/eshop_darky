<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:32
 *
 * @var $mSpojeniForm SpojeniForm
 */
use backend\modules\eshop\forms\Doprava;

?>

<h2>Nové spojení dopravy a platby</h2>

<?= $this->render('_formSpojeni', [
    'mSpojeniForm' => $mSpojeniForm,
]) ?>