<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:22
 *
 * @var $mSpojeniForm SpojeniForm
 */

use backend\modules\eshop\forms\SpojeniForm;
use common\models\Logo;
use common\models\ZpusobDopravy;
use common\models\ZpusobPlatby;
use dosamigos\datetimepicker\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'layout' => 'horizontal'
]);
?>
<div class="form-fields">
    <?php
    $spojeniData = [];
    $spojeniOptions = [];

    /**
     * todo tohle je dobra prasecina, refactorovat, ideálně z toho udělat regulérní widget
     * @var ZpusobDopravy $mZpusobDopravy
     */
    foreach (ZpusobDopravy::nactiProDropdownSlogy() as $mZpusobDopravy) {
        $spojeniData[$mZpusobDopravy->zpusob_dopravy_pk] = $mZpusobDopravy->nazev;

        $logo = Html::beginTag('div', ['style' => 'width: 20px; height: 20px; float: left; margin-right: 5px;']);
        if ($mZpusobDopravy->logo_pk != null) {
            /** @var Logo $mLogo */
            $mLogo = Logo::findOne($mZpusobDopravy->logo_pk);
            $logo .= Html::img(['/files/image', 'hash' => $mLogo->hash], ['style' => 'width: 20px; height: 20px;']);
        }
        $logo .= Html::endTag('div');

        $spojeniOptions[$mZpusobDopravy->zpusob_dopravy_pk] = [
            'data-content' => "{$logo} {$mZpusobDopravy->nazev}"
        ];
    }
    ?>

    <?= $form->field($mSpojeniForm, 'zpusob_dopravy_pk')->dropDownList($spojeniData,
        ['prompt'=>'Vyberte', 'options' => $spojeniOptions])->label('Způsob dopravy') ?>

    <?= $form->field($mSpojeniForm, 'zpusob_platby_pk')->dropDownList(ArrayHelper::map(ZpusobPlatby::find()->all(), 'zpusob_platby_pk', 'nazev'),
        ['prompt'=>'Vyberte'])->label('Způsob platby') ?>

    <?= $form->field($mSpojeniForm, 'platba_dopravy_id') ?>

    <?= $form->field($mSpojeniForm, 'cena') ?>

    <?= $form->field($mSpojeniForm, 'viditelne_od')->widget(DateTimePicker::className()) ?>

    <?= $form->field($mSpojeniForm, 'viditelne_do')->widget(DateTimePicker::className()) ?>

</div>

<div class="form-actions">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
    <?= Html::a('Zrušit', ['/eshop/dopravy/index'], [
        'class' => 'btn btn-danger'
    ]) ?>
</div>
<?php
ActiveForm::end();