<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 21:22
 *
 * @var $mPlatbaForm PlatbaForm
 */

use backend\modules\eshop\forms\PlatbaForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'layout' => 'horizontal'
]);
?>
<div class="form-fields">
    <?= $form->field($mPlatbaForm, 'nazev') ?>
</div>

<div class="form-actions">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
    <?= Html::a('Zrušit', ['/eshop/dopravy/index'], [
        'class' => 'btn btn-danger'
    ]) ?>
</div>
<?php
ActiveForm::end();