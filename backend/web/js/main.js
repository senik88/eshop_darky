(function (Senovo, $, undefined) {
    Senovo.zkontrolujObjednavkuPredOdeslanim = function (id) {
        var request = $.ajax({
            url: 'ajax-zkontroluj-pred-odeslanim',
            type: 'POST',
            data: {pk: id}
        });

        request.success(function(response) {
            window.console.log(response);
        });
    }
}(window.Senovo = window.Senovo || {}, jQuery));

$(function(){
   /*$('#modalButtonDoprava').click(function() {
       $('#modalDoprava').modal('show')
           .find('#modalContentDoprava')
           .load($(this).attr('value'));
       $('select').selectpicker();
   });*/
});

$(function(){
    $('#modalButtonPlatba').click(function() {
        $('#modalPlatba').modal('show')
            .find('#modalContentPlatba')
            .load($(this).attr('value'));
        $('select').selectpicker();
    });
});

$(function(){
    $('#modalButtonSpojeni').click(function() {
        $('#modalSpojeni').modal('show')
            .find('#modalContentSpojeni')
            .load($(this).attr('value'));
        $('select').selectpicker();
    });
});

$(function(){
    $('#modalButtonVyrobce').click(function() {
        $('#modalVyrobce').modal('show')
            .find('#modalContentVyrobce')
            .load($(this).attr('value'));
    });
});

$(function(){
    $('#modalButtonDodavatel').click(function() {
        $('#modalDodavatel').modal('show')
            .find('#modalContentDodavatel')
            .load($(this).attr('value'));
    });
});

$(function(){
    $('#modalButtonKategorie').click(function() {
        $('#modalKategorie').modal('show')
            .find('#modalContentKategorie')
            .load($(this).attr('value'));
    });
});

// Modal okna pro formular polozky
$(document).ready(function () {
    var $body = $('body');

    $body.on('beforeSubmit', '.modal-body form#vyrobce-form', function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length)
        {
            return false;
        }
        // submit form
        $.ajax({
            url    : form.attr('action'),
            type   : 'POST',
            data   : form.serialize(),
            success: function (response)
            {
                var vyrobciSelect = $('#polozkaform-vyrobce_pk'),
                    vyrobci = response.vyrobci;

                vyrobciSelect.find('option').remove();

                $.each(vyrobci, function(pk, nazev) {
                    var option = $('<option/>', {
                        value: pk
                    }).text(nazev).prop('selected', pk == response.success);

                    vyrobciSelect.append(option);
                });

                vyrobciSelect.selectpicker('refresh');
                $('#modalVyrobce').modal('hide');
            },
            error  : function ()
            {
                console.log('internal server error');
            }
        });
        return false;
    });

    $body.on('beforeSubmit', '.modal-body form#dodavatel-form', function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length)
        {
            return false;
        }
        // submit form
        $.ajax({
            url    : form.attr('action'),
            type   : 'POST',
            data   : form.serialize(),
            success: function (response)
            {
                var dodavateleSelect = $('#polozkaform-dodavatel_pk'),
                    dodavatele = response.dodavatele;

                dodavateleSelect.find('option').remove();

                $.each(dodavatele, function(pk, nazev) {
                    var option = $('<option/>', {
                        value: pk
                    }).text(nazev).prop('selected', pk == response.success);

                    dodavateleSelect.append(option);
                });

                dodavateleSelect.selectpicker('refresh');
                $('#modalDodavatel').modal('hide');
            },
            error  : function ()
            {
                console.log('internal server error');
            }
        });
        return false;
    });

    $body.on('beforeSubmit', '.modal-body form#kategorie-form', function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length)
        {
            return false;
        }
        // submit form
        $.ajax({
            url    : form.attr('action'),
            type   : 'POST',
            data   : form.serialize(),
            success: function (response)
            {
                var kategorieSelect = $('#polozkaform-kategorie_id'),
                    kategorie = response.kategorie;

                kategorieSelect.find('option').remove();

                $.each(kategorie, function(pk, nazev) {
                    var option = $('<option/>', {
                        value: pk
                    }).text(nazev).prop('selected', pk == response.success);

                    kategorieSelect.append(option);
                });

                kategorieSelect.selectpicker('refresh');
                $('#modalKategorie').modal('hide');
            },
            error  : function ()
            {
                console.log('internal server error');
            }
        });
        return false;
    });

    $(document).on('change', '#kategorie-form select[id^=parametr-parametr_pk]', function() {
        var value = $(this).val(),
            wrapper = $(this).closest('.parametr-wrapper'),
            klicPolozky = $(this).closest('[class*=parametr-wrapper-]').attr("class").match(/\d+/)[0],
            select
        ;

        if (value != '') {
            var request = $.ajax({
                url: 'ajax-pridat-parametr',
                data: {klic: klicPolozky, parametr_pk: value},
                type: 'POST'
            });

            request.done(function(html) {
                wrapper.replaceWith(html);
                $('select').selectpicker();
            });
        } else {
            // vynuluju formular
            $('#parametr-nazev-'+klicPolozky).val('');
            $('#parametr-jednotka-'+klicPolozky).val('');

            select = $('#parametr-hodnota_typ-'+klicPolozky);

            select.find('option').prop('selected', false);
            select.first('option').prop('selected', true).selectpicker('render');

            select = $('#parametr-skupina_pk-'+klicPolozky);

            select.find('option').prop('selected', false);
            select.first('option').prop('selected', true).selectpicker('render');
        }
    });
});

$(document).ready(function() {
    // show active tab on reload
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

    // remember the hash in the URL without jumping
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if(history.pushState) {
            history.pushState(null, null, '#'+$(e.target).attr('href').substr(1));
        } else {
            location.hash = '#'+$(e.target).attr('href').substr(1);
        }
    });
});