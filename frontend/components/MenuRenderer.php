<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.06.2016
 * Time: 18:36
 */

namespace frontend\components;


class MenuRenderer {


    public static function factory()
    {
        $theme = \Yii::$app->view->theme;

        $class = 'MenuRenderer';

        $path = $theme->pathMap['@app/views'] . '/components/' . $class;

        return new self;
    }

    public function renderMenu() {

    }
}