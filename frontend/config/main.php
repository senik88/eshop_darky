<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'cs-CZ',
    'name' => 'Shopin',
    'components' => [
        'user' => [
            'identityClass' => 'common\components\uzivatel\Uzivatel',
            'enableAutoLogin' => true,
            'loginUrl' => ['/uzivatel/default/prihlaseni'],
            'identityCookie' => [
                'name' => '_frontendUser', // unique for frontend
                'path' => '/frontend/web'  // correct path for the frontend app.
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'enableStrictParsing' => false,
            'rules' => [
                'katalog/<id:[\w-]+>/<vyrobce:[\w-]+>' => 'eshop/katalog/index',
                'katalog/<id:[\w-]+>' => 'eshop/katalog/index',
                'polozka/<id:[\w-]+>' => 'eshop/katalog/detail'
            ],
        ],
        'session' => [
            'name' => '_frontendSessionId', // unique for frontend
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on frontend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/shopin',
                'baseUrl' => '@web/themes/shopin',
                'pathMap' => [
                    '@app/views' => '@app/themes/shopin',
                    '@app/modules' => '@app/themes/shopin/modules'
                ],
            ],
        ],
    ],
    'modules' => [
        'uzivatel' => [
            'class' => 'frontend\modules\uzivatel\UzivatelModule',
        ],
        'eshop' => [
            'class' => 'frontend\modules\eshop\EshopModule',
        ],
    ],
    'params' => $params,
];
