<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 02.10.2016
 * Time: 19:12
 */

namespace frontend\modules\eshop\models;


use yii\db\ActiveRecord;

/**
 * Class HodnoceniProduktu
 * @package frontend\modules\eshop\models
 */
class HodnoceniProduktu extends ActiveRecord
{
    /** @var integer */
    public $hodnoceni_produktu_pk;

    /** @var integer */
    public $procenta;

    /** @var string */
    public $popis;

    /** @var string */
    public $klady;

    /** @var string */
    public $zapory;

    /** @var integer */
    public $polozka_pk;

    /** @var integer */
    public $uzivatel_pk;
}