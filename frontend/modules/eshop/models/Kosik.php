<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 18. 10. 2015
 * Time: 10:43
 */

namespace frontend\modules\eshop\models;
use common\components\SqlDataProvider;
use yii\base\Model;

/**
 * Class Kosik
 * @package frontend\modules\eshop\models
 */
class Kosik extends Model
{

    public $polozky;

    public $uzivatel_pk;

    protected $_souhrn = array();

    public function init()
    {
        parent::init();

        $this->uzivatel_pk = \Yii::$app->user->id;

        $this->_souhrn = $this->vratSouhrn();
    }

    /**
     * @param null $krok
     * @return SqlDataProvider
     */
    public function nacti($krok = null)
    {
        $sql = "
            select
                  k.*
                , o.nahled
                , p.subtitulek
                , 1 AS typ
            from kosik_uzivatele k
                join polozka p on p.polozka_pk = k.polozka_pk
                left join polozka_obrazek o on o.polozka_obrazek_pk = polozka_obrazek_hlavni_vrat_pk(k.polozka_pk)
            where k.uzivatel_pk = :uid
        ";

        if ($krok == 3) {
            $sql .= "\nUNION ALL\n";

            $sql .= "
            SELECT
                NULL as polozka_pk
                , p.platba_dopravy_id AS polozka_id
                , 'Doprava a platba:' AS titulek
                , p.cena AS cena_aktualni
                , p.cena AS cena_celkem
                , 1 AS skladem
                , 1 AS mnozstvi
                , o.objednavka_pk
                , o.uzivatel_pk
                , NULL AS nahled
                , coalesce(zd.nazev || ' - ' || zp.nazev, 'doprava a platba') AS subtitulek
                , 2 AS typ
            FROM objednavka o
                JOIN platba_dopravy p ON o.platba_dopravy_id = p.platba_dopravy_id
                JOIN zpusob_platby zp ON p.zpusob_platby_pk = zp.zpusob_platby_pk
                JOIN zpusob_dopravy zd ON p.zpusob_dopravy_pk = zd.zpusob_dopravy_pk
            WHERE o.stav_objednavky_id = 'KOSIK'
                AND o.uzivatel_pk = :uid
            ";
        }

        $params = [
            ':uid' => $this->uzivatel_pk
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => false,
            'sort' => [
                'attributes' => ['typ', 'polozka_id'],
                'defaultOrder' => ['typ' => SORT_ASC, 'polozka_id' => SORT_ASC]
            ]
        ]);
    }

    /**
     * @return array|bool pole [polozek, suma]
     */
    protected function vratSouhrn()
    {
        $celkem_sql = "
            SELECT
                sum(ku.mnozstvi) AS polozek
                , sum(ku.cena_aktualni * mnozstvi) AS castka
                , coalesce(pd.cena, 0.00) as doprava
            FROM kosik_uzivatele ku
                JOIN objednavka o ON o.objednavka_pk = ku.objednavka_pk
                LEFT JOIN platba_dopravy pd ON o.platba_dopravy_id = pd.platba_dopravy_id
            WHERE ku.uzivatel_pk = :uid
            GROUP BY pd.cena
        ";

        $params = [':uid' => \Yii::$app->user->id];

        $celkem = \Yii::$app->db->createCommand($celkem_sql)->bindValues($params)->queryOne();

        return $celkem;
    }

    public function vratPocetPolozek()
    {
        if (isset($this->_souhrn['polozek'])) {
            return $this->_souhrn['polozek'];
        } else {
            return null;
        }
    }

    public function vratCastkuObjednavky()
    {
        if (isset($this->_souhrn['castka'])) {
            return $this->_souhrn['castka'];
        } else {
            return null;
        }
    }

    public function vratCastkuDopravy()
    {
        if (isset($this->_souhrn['doprava'])) {
            return $this->_souhrn['doprava'];
        } else {
            return null;
        }
    }

    public function vratCastkuCelkem()
    {
        return $this->vratCastkuDopravy() + $this->vratCastkuObjednavky();
    }

    /**
     * Vola proceduru pro pridani polozky do kosiku, vse si osetrim na strane DB
     * @param $id
     * @param $kusu
     * @return bool
     */
    public static function pridejPolozku($id, $kusu)
    {
        $sql = "select * from objednavka_pridej_polozku(:uid, :pid, :ks)";

        // result['r_objednavka_pk'], result['r_vysledek']
        $result = \Yii::$app->db->createCommand($sql)->bindValues([
            ':uid' => \Yii::$app->user->id,
            ':pid' => $id,
            ':ks' => $kusu
        ])->queryOne();

        if ($result['r_objednavka_pk'] < 0) {
            \Yii::error("Nepodarilo se vlozit polozku do kosiku ({$result['r_objednavka_pk']}), chyba: {$result['r_vysledek']}");
            return false;
        } else {
            // auditlog? mohl by byt
            return true;
        }
    }

    /**
     * Stejne jako v pripade pridani, vse si bude resit DB
     * @param $id
     * @param bool $vse
     * @return bool
     */
    public static function odeberPolozku($id, $vse = false)
    {
        $sql = "select * from objednavka_odeber_polozku(:uid, :pid, :vse = 1)";
        //return true;

        // result['r_objednavka_pk'], result['r_vysledek']
        $result = \Yii::$app->db->createCommand($sql)->bindValues([
            ':uid' => \Yii::$app->user->id,
            ':pid' => $id,
            ':vse' => (int) $vse
        ])->queryOne();

        if ($result['r_objednavka_pk'] < 0) {
            \Yii::error("Nepodarilo se odebrat polozku z kosiku ({$result['r_objednavka_pk']}), chyba: {$result['r_vysledek']}");
            return false;
        } else {
            // auditlog? mohl by byt
            return true;
        }
    }

    /**
     * @return bool
     */
    public static function vysypat()
    {
        return false;
    }
}