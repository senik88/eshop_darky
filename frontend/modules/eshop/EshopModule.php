<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 07.09.2015
 * Time: 21:06
 */

namespace frontend\modules\eshop;


use Yii;
use yii\base\Module;

class EshopModule extends Module
{
    public $controllerNamespace = 'frontend\modules\eshop\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    /**
     * Naplni konfiguraci aplikace cestou k prekladum
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['eshop/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'cs-CZ',
            'basePath' => '@app/modules/eshop/messages',
            'fileMap' => [
                'app' => 'app.php',
                'form' => 'form.php',
                'kategorie' => 'kategorie.php',
                'polozka' => 'polozka.php'
            ],
        ];
    }

    /**
     * @param $category
     * @param $message
     * @param array $params
     * @param null $language
     * @return string
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('eshop/'.$category, $message, $params, $language);
    }
}