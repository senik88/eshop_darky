<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.11.2015
 * Time: 19:21
 */

namespace frontend\modules\eshop\forms;


use common\models\Objednavka;
use common\models\PlatbaDopravy;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class DopravaForm
 * @package frontend\modules\eshop\forms
 */
class DopravaForm extends Model {

    public $objednavka_pk;

    public $zpusob_dopravy_pk;

    public $zpusob_platby_pk;

    /**
     * @var Objednavka
     */
    public $mObjednavka;

    public $mZpusobDopravy;

    public $mZpusobPlatby;

    /**
     * @var PlatbaDopravy
     */
    public $mPlatbaDopravy;

    public function rules()
    {
        return [
            [['zpusob_dopravy_pk', 'zpusob_platby_pk'], 'required'],
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @param Objednavka $objednavka
     * @return DopravaForm
     */
    public function objednavkaNacti($objednavka)
    {
        $this->mObjednavka = $objednavka;

        $this->mPlatbaDopravy = new PlatbaDopravy();
        if ($this->mObjednavka->platba_dopravy_id != null) {
            $this->mPlatbaDopravy = $this->mPlatbaDopravy->findOne(['platba_dopravy_id' => $this->mObjednavka->platba_dopravy_id]);

            $this->zpusob_dopravy_pk = $this->mPlatbaDopravy->zpusob_dopravy_pk;
            $this->zpusob_platby_pk = $this->mPlatbaDopravy->zpusob_platby_pk;
        }

        return $this;
    }

    /**
     *
     */
    public function vratZpusobyDopravyProForm()
    {
        $sql = "
            select zp.zpusob_dopravy_pk as pk, zp.nazev
            from zpusob_dopravy zp
                join platba_dopravy pd on zp.zpusob_dopravy_pk = pd.zpusob_dopravy_pk
            where pd.viditelne_od <= now()
                and (pd.viditelne_do is null or pd.viditelne_do > now())
            group by zp.zpusob_dopravy_pk
        ";

        $dopravy = \Yii::$app->db->createCommand($sql)->queryAll();

        return ArrayHelper::map($dopravy, 'pk', 'nazev');
    }

    public function vratZpusobyPlatbyProForm()
    {
        $sql = "
            select zp.zpusob_platby_pk as pk, zp.nazev
            from zpusob_platby zp
                join platba_dopravy pd on zp.zpusob_platby_pk = pd.zpusob_platby_pk
            where pd.viditelne_od <= now()
                and (pd.viditelne_do is null or pd.viditelne_do > now())
                and pd.zpusob_dopravy_pk = :doprava
            group by zp.zpusob_platby_pk
        ";

        $platby = \Yii::$app->db->createCommand($sql, [
            'doprava' => $this->zpusob_dopravy_pk
        ])->queryAll();

        return ArrayHelper::map($platby, 'pk', 'nazev');
    }
}