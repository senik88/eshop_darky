<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.10.2015
 * Time: 20:27
 *
 * @var $model array
 */
use yii\bootstrap\Html;

?>
<article data-id="<?= $model['polozka_id'] ?>" class="polozka">
<div class="polozka-wrapper">

    <div class="polozka-obrazek">
        <?= Html::a(
            Html::img($model['nahled'], ['class' => 'img-thumbnail center-block']),
            ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
        ) ?>
    </div>

    <div class="polozka-popis">
        <span>
            <?= Html::a($model['titulek'], ['/eshop/katalog/detail', 'id' => $model['polozka_id']]) ?>
        </span>
        <br>
        <span>
            <?= Yii::$app->formatter->asCurrency($model['cena_aktualni'], 'CZK') ?>
        </span>
    </div>

    <div class="polozka-akce">
        <!--< ?= Html::button('Do košíku', [
            'data-polozka' => $model['polozka_id'],
            'class' => 'btn btn-success btn-xs'
        ]) ?>-->
        <?= Html::a(
            Html::icon("shopping-cart") . " Přidat do košíku",
            ['/eshop/kosik/pridat', 'id' => $model['polozka_id']],
            [
                'data-polozka' => $model['polozka_id'],
                'class' => 'btn btn-primary btn-xs'
            ]
        ) ?>
    </div>
</div>
</article>