<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.10.2015
 * Time: 20:10
 *
 * @var $mKategorie Kategorie
 * @var $mPolozka Polozka
 * @var $mNadrazena Kategorie
 */

use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;

$this->title = Yii::$app->name . ' | ' . $mKategorie->nazev;
if ($mNadrazena != null) {
    $this->params['breadcrumbs'][] = [
        'label' => $mNadrazena->nazev,
        'url' => ['/eshop/katalog/index', 'id' => $mNadrazena->kategorie_id]
    ];
}
$this->params['breadcrumbs'][] = $mKategorie->nazev;

$dataprovider = $mPolozka->vypis();

?>

<div class="row">
    <?php if ($dataprovider->getCount() > 0) { ?>
        <div id="katalog-filtr" class="col-md-12">
            <div>
                <h4>Jednoduchý filtr na značku, cenu + řazení</h4>
            </div>
        </div>
    <?php } ?>

    <div class="col-md-12">
    <?php
    echo \yii\widgets\ListView::widget([
        'id' => 'katalog-polozky',
        'itemView' => '_polozka',
        'dataProvider' => $dataprovider,
        'layout' => "{pager}\n{items}",
        'emptyText' => "<div class='col-md-12'><h4>Kategorie zatím neobsahuje žádné zboží.</h4></div>"
    ]);
    ?>
    </div>
</div>