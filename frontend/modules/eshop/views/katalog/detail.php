<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.10.2015
 * Time: 20:30
 *
 * @var $this View
 * @var $mPolozka Polozka
 * @var $mVyrobce Vyrobce
 * @var $mLogo Logo
 */

use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use backend\modules\eshop\models\Vyrobce;
use common\models\Logo;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::$app->name . ' | ' . $mPolozka->titulek;
if ($mPolozka->mKategorie->nadrazena) {
    /** @var Kategorie $mNadrazena */
    $mNadrazena = Kategorie::findOne($mPolozka->mKategorie->nadrazena);

    $this->params['breadcrumbs'][] = [
        'url' => ['/eshop/katalog/index', 'id' => $mNadrazena->kategorie_id],
        'label' => $mNadrazena->nazev
    ];
}
$this->params['breadcrumbs'][] = [
    'url' => ['/eshop/katalog/index', 'id' => $mPolozka->kategorie_id],
    'label' => $mPolozka->mKategorie->nazev
];
$this->params['breadcrumbs'][] = $mPolozka->titulek;

?>

<div id="detail-polozky">
    <div class="row">
        <div class="col-md-12">
            <?= Html::a(
                Html::icon('chevron-left') . ' Zpět na výpis produktů',
                ['/eshop/katalog/index', 'id' => $mPolozka->kategorie_id],
                [
                    'class' => 'btn btn-info'
                ]
            ) ?>
        </div>
    </div>

    <div class="row">
        <div id="detail-informace">
            <div class="col-md-3">
                <?= Html::a(
                    Html::img($mPolozka->obrazky[0]->nahled, ['class' => 'img-thumbnail']),
                    $mPolozka->obrazky[0]->zdroj,
                    [
                        'class' => 'fancybox',
                        'rel' => 'group',
                        'title' => $mPolozka->obrazky[0]->popisek
                    ]
                ); ?>
            </div>
            <div class="col-md-6">
                <h2><?= $mPolozka->titulek ?></h2>
                <h3><?= $mPolozka->subtitulek ?></h3>
                <ul>
                    <li>Kategorie: <?= $mPolozka->mKategorie->nazev ?></li>
                    <li>Skladem: <?= $mPolozka->mVarianta->skladem ?> ks</li>
                    <li>
                        Cena: <strike><?= Yii::$app->getFormatter()->asCurrency($mPolozka->mEshopCena->cena_doporucena, 'CZK') ?></strike>
                        / <?= Yii::$app->getFormatter()->asCurrency($mPolozka->mEshopCena->cena_aktualni, 'CZK') ?>
                    </li>
                </ul>
                <div class="detail-tlacitka">
                    <?= Html::a(
                        Html::icon("shopping-cart") . " Přidat do košíku",
                        ['/eshop/kosik/pridat', 'id' => $mPolozka->polozka_id],
                        [
                            'data-polozka' => $mPolozka->polozka_id,
                            'class' => 'btn btn-primary'
                        ]
                    ) ?>
                </div>
            </div>
            <div class="col-md-3">
                <h4>Další v kategorii</h4>
                <ul>
                    <li>
                        <?= Html::a($mPolozka->mKategorie->nazev, ['/eshop/katalog/index', 'id' => $mPolozka->kategorie_id]) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            $mVyrobce->nazev . ' v ' . $mPolozka->mKategorie->nazev,
                            ['/eshop/katalog/index', 'id' => $mPolozka->kategorie_id, 'vyrobce' => $mVyrobce->vyrobce_id]
                        ) ?>
                    </li>
                </ul>
                <hr>
                <div class="vyrobce-logo">
                <?php
                if ($mLogo != null) {
                    $text = Html::img(
                        Url::to(['/files/image', 'hash' => $mLogo->hash]),
                        [
                            'alt' => $mLogo->zdroj
                        ]
                    );
                } else {
                    $text = $mVyrobce->nazev;
                }

                echo $link = Html::a($text, ['/eshop/katalog/vyrobce', 'id' => $mVyrobce->vyrobce_id]);
                ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" id="detail-popis">
            <?= $mPolozka->popis ?>
        </div>
    </div>

    <?php
    if (count($mPolozka->obrazky) > 1) {
    ?>
    <div class="row">
        <div class="col-md-12" id="detail-galerie">
            <h4>Galerie</h4>
            <?php
            for ($i = 1; $i < count($mPolozka->obrazky); $i++) {

                echo Html::a(
                    Html::img($mPolozka->obrazky[$i]->nahled, ['class' => 'img-thumbnail']),
                    $mPolozka->obrazky[$i]->zdroj,
                    [
                        'class' => 'fancybox',
                        'rel' => 'group',
                        'title' => $mPolozka->obrazky[$i]->popisek
                    ]
                );
            }
            ?>
        </div>
    </div>
    <?php } ?>
</div>