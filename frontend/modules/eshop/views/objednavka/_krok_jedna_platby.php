<?php
use frontend\modules\eshop\forms\DopravaForm;
use yii\bootstrap\Html;

/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.11.2015
 * Time: 21:07
 *
 * @var $mDopravaForm DopravaForm
 * @var $aZpusobyPlatby array
 */

$classes = ['form-group', 'field-dopravaform-zpusob_platby_pk', 'required'];

if ($mDopravaForm->hasErrors('zpusob_platby_pk')) {
    $classes[] = 'has-error';
}
?>

<div class="<?= implode(' ', $classes) ?>">
    <?= Html::error($mDopravaForm, 'zpusob_platby_pk', [
        'class' => 'help-block help-block-error'
    ]) ?>
    <?php
    echo Html::activeRadioList($mDopravaForm, 'zpusob_platby_pk', $aZpusobyPlatby, [
        'item' => function ($index, $label, $name, $checked, $value) {
            return strtr('<div class="{class}"><label>{input} {nazev}</label></div>', [
                '{class}' => implode(' ', ['radio']),
                '{input}' => Html::input('radio', $name, $value, [
                    'checked' => $checked
                ]),
                '{nazev}' => $label
            ]);
        }
    ]);
    ?>
</div>