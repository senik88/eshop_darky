<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.11.2015
 * Time: 20:39
 *
 * @var $this View
 * @var $krok integer
 * @var $mKosik Kosik
 * @var $mObjednavka Objednavka
 * @var $mFakturacniUdaje FakturacniUdaje
 * @var $mDodaciUdaje DodaciUdaje
 *
 * @var $aFakturacniUdaje array pole fakturacnich udaju aktualne prihlaseneho uzivatele
 * @var $aDodaciUdaje array pole dodacich udaju aktualne prihlaseneho uzivatele
 */

use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\Objednavka;
use frontend\modules\eshop\models\Kosik;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;


echo $this->render('_kroky', ['krok' => $krok]);
?>

<div id="objednavka-krok-dva-form-wrapper">
<?php
$form = ActiveForm::begin([
    'method' => 'POST',
    'id' => 'objednavka-krok-dva-form'
]);
?>

<div class="row">
    <div class="col-md-5 col-md-offset-1">
        <h3>Fakturační údaje</h3>
        <?= $form->field($mObjednavka, 'fakturacni_udaje_pk')->dropDownList(['#' => 'Přidat fakturační údaje'] + $aFakturacniUdaje)->label(false) ?>

        <div id="fakturacni-udaje-form-wrapper">
            <?= $form->field($mFakturacniUdaje, 'obchodni_jmeno') ?>
            <?= $form->field($mFakturacniUdaje, 'ic') ?>
            <?= $form->field($mFakturacniUdaje, 'dic') ?>
            <?= $form->field($mFakturacniUdaje, 'jmeno') ?>
            <?= $form->field($mFakturacniUdaje, 'prijmeni') ?>
            <?= $form->field($mFakturacniUdaje, 'ulice') ?>
            <?= $form->field($mFakturacniUdaje, 'mesto') ?>
            <?= $form->field($mFakturacniUdaje, 'psc') ?>
            <?= $form->field($mFakturacniUdaje, 'zeme') ?>
        </div>
    </div>

    <div class="col-md-5">
        <div id="dodaci-stejne-wrapper">
            <h3>Dodací údaje</h3>
            <?= $form->field($mObjednavka, 'udaje_stejne')->checkbox()->label('Stejné jako fakturační') ?>
        </div>

        <?= $form->field($mObjednavka, 'dodaci_udaje_pk')->dropDownList(['#' => 'Přidat dodací údaje'] + $aDodaciUdaje)->label(false) ?>

        <div class="collapse" id="dodaci-udaje-form-wrapper">
            <?= $form->field($mDodaciUdaje, 'jmeno') ?>
            <?= $form->field($mDodaciUdaje, 'prijmeni') ?>
            <?= $form->field($mDodaciUdaje, 'ulice') ?>
            <?= $form->field($mDodaciUdaje, 'mesto') ?>
            <?= $form->field($mDodaciUdaje, 'psc') ?>
            <?= $form->field($mDodaciUdaje, 'zeme') ?>
            <?= $form->field($mDodaciUdaje, 'telefon') ?>
            <?= $form->field($mDodaciUdaje, 'poznamka')->textarea(['placeholder' => 'Firma, poschodí a podobně...']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <?= Html::submitButton(Html::icon('chevron-left') . ' ' . 'Krok zpět', ['class' => 'btn btn-warning', 'name' => 'akce', 'value' => 'zpet']) ?>
            <?= Html::submitButton('Další krok' . ' ' . Html::icon('chevron-right'), ['class' => 'btn btn-success', 'name' => 'akce', 'value' => 'potvrdit']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>

</div>

<?php

// fakturacni modal
// dodaci modal


$this->registerJs("
$(window).load(Senovo.handleObjednavkaKrokDvaForm);
");