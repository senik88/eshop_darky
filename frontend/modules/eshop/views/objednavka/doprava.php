<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.11.2015
 * Time: 20:39
 *
 * @var $this View
 * @var $krok integer
 * @var $mKosik Kosik
 * @var $mDopravaForm DopravaForm
 */

use frontend\modules\eshop\forms\DopravaForm;
use frontend\modules\eshop\models\Kosik;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\web\View;


echo $this->render('_kroky', ['krok' => $krok]);

?>
<div class="row" id="objednavka-kosik-grid-wrapper">
<?php
echo GridView::widget([
    'id' => 'objednavka-kosik-grid',
    'layout' => "{items}",
    'tableOptions' => ['class' => 'table'],
    'dataProvider' => $mKosik->nacti(),
    'columns' => [
        'nazev' => [
            'value' => function($model) {
                return Html::a(
                    sprintf('%s %s', $model['titulek'], $model['subtitulek']),
                    ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                );
            },
            'format' => 'raw'
        ],
        'kusu' => [
            'label' => 'Kusů',
            'format' => 'raw',
            'value' => function($model) {
                return sprintf('<span class="polozka-kusu">%s ks</span>',
                    $model['mnozstvi']
                );
            }
        ],
        'skladem' => [
            'label' => 'Skladem',
            'value' => function($model) {
                return sprintf('%s ks', $model['skladem']);
            }
        ],
        'cena_ks' => [
            'attribute' => 'cena_aktualni',
            'label' => 'Cena / ks',
            'format' => 'currency',
            'footer' => 'Celková cena:'
        ],
        'cena_celkem' => [
            'attribute' => 'cena_celkem',
            'label' => 'Cena celkem',
            'value' => function ($model) {
                return Yii::$app->getFormatter()->asCurrency($model['cena_celkem'],'CZK');
            },
        ]
    ]
]);

?>
</div>
<?php
$form = ActiveForm::begin([
    'method' => 'POST',
    'id' => 'objednavka-krok-jedna-form'
]);
?>

<div class="row" id="objednavka-moznosti-dopravy">
    <div class="col-md-6">
        <h3>Možnosti dopravy</h3>
        <div id="objednavka-moznosti-dopravy">
        <?php
            echo $form->field($mDopravaForm, 'zpusob_dopravy_pk')->radioList($mDopravaForm->vratZpusobyDopravyProForm())->label(false);
        ?>
        </div>
    </div>
    <div class="col-md-6">
        <h3>Možnosti platby</h3>
        <div id="objednavka-moznosti-platby">
        <?php
        if ($mDopravaForm->zpusob_dopravy_pk != null) {
            echo $this->render('_krok_jedna_platby', [
                'mDopravaForm' => $mDopravaForm,
                'aZpusobyPlatby' => $mDopravaForm->vratZpusobyPlatbyProForm()
            ]);
        } else {
            echo 'Vyberte způsob dopravy';
        }
        ?>
        </div>
    </div>
</div>

<div class="row cena-wrapper">
    <div class="col-md-12">
        <div class="pull-right">
            Cena dopravy: <span id="objednavka-cena-dopravy"><?= Yii::$app->formatter->asCurrency($mKosik->vratCastkuDopravy()) ?></span>
        </div>
    </div>
</div>

<div class="row cena-wrapper">
    <div class="col-md-12">
        <div class="pull-right">
            Celková cena objednávky: <span id="objednavka-cena-celkem"><?= Yii::$app->formatter->asCurrency($mKosik->vratCastkuCelkem()) ?></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <?= Html::submitButton('Další krok' . ' ' . Html::icon('chevron-right'), ['class' => 'btn btn-success', 'name' => 'akce', 'value' => 'potvrdit']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>