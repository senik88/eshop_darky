<?php
use yii\bootstrap\Html;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.11.2015
 * Time: 20:39
 *
 * @var $krok integer
 * @var $this View
 */

?>

<div class="row" id="objednavka-kroky">
    <div class="col-md-4 <?= $krok == 1 ? 'active' : '' ?>">
        <?php
        $text = "Doprava a platba";
        if ($krok > 1) {
            echo Html::a($text, ['/eshop/objednavka/krok-jedna']);
        } else {
            echo Html::tag('h2', $text);
        }
        ?>
    </div>

    <div class="col-md-4 <?= $krok == 2 ? 'active' : '' ?>">
        <?php
        $text = "Dodací a fakturační údaje";
        if ($krok > 2) {
            echo Html::a($text, ['/eshop/objednavka/krok-dva']);
        } else {
            echo Html::tag('h2', $text);
        }
        ?>
    </div>

    <div class="col-md-4 <?= $krok == 3 ? 'active' : '' ?>">
        <?php
        ?>
        <h2>Rekapitulace</h2>
    </div>
</div>