<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.11.2015
 * Time: 20:14
 */

namespace frontend\modules\eshop\controllers;


use common\components\Application;
use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\Objednavka;
use common\models\PlatbaDopravy;
use common\models\ZpusobDopravy;
use common\models\ZpusobPlatby;
use frontend\modules\eshop\forms\DopravaForm;
use frontend\modules\eshop\models\Kosik;
use yii\base\InlineAction;
use yii\bootstrap\Html;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ObjednavkaController
 * @package frontend\modules\eshop\controllers
 */
class ObjednavkaController extends Controller
{
    /**
     * @param InlineAction $action
     * @return bool|void
     */
    public function beforeAction($action)
    {
        $before = parent::beforeAction($action);

        if (\Yii::$app->user->isGuest) {
            Application::setFlashInfo('Pro dokonční objednávky se musíte přihlásit.');
            return $this->redirect(['/uzivatel/default/prihlaseni']);
        }

        return $before;
    }

    /**
     * doprava a platba
     */
    public function actionKrokJedna()
    {
        $mKosik = new Kosik();

        $post = \Yii::$app->request->post();

        $mObjednavka = $this->_nactiObjednavku();

        /** @var DopravaForm $mDoprava */
        $mDoprava = (new DopravaForm())->objednavkaNacti($mObjednavka);

        if ($mObjednavka == null) {
            Application::setFlashInfo("V košíku se nenachází žádné zboží.");
            return $this->redirect(['/site/index']);
        }

        if (!empty($post)) {
            $mDoprava->load($post);

            switch ($post['akce']) {
                case 'potvrdit':
                    if (!$mDoprava->validate()) {
                        break;
                    }

                    if ($mObjednavka->nastavDopravuPlatbu($mDoprava->zpusob_dopravy_pk, $mDoprava->zpusob_platby_pk)) {
                        return $this->redirect(['/eshop/objednavka/krok-dva']);
                    } else {
                        ddd('eh?');
                        return $this->redirect(['/eshop/objednavka/krok-jedna']);
                    }
                    break;
            }
        }

        return $this->render('doprava', [
            'krok' => 1,
            'mKosik' => $mKosik,
            'mDopravaForm' => $mDoprava
        ]);
    }

    /**
     * dodaci a fakturacni udaje
     */
    public function actionKrokDva()
    {
        $post = \Yii::$app->request->post();

        $mObjednavka = $this->_nactiObjednavku(Objednavka::SCENARIO_UDAJE);

        if ($mObjednavka == null) {
            Application::setFlashInfo("V košíku se nenachází žádné zboží.");
            return $this->redirect(['/site/index']);
        }

        if ($mObjednavka->platba_dopravy_id == null) {
            Application::setFlashInfo("Nejprve musíte vybrat způsob dopravy a platby.");
            return $this->redirect(['/eshop/objednavka/krok-jedna']);
        }

        $mFakturacniUdaje = new FakturacniUdaje(['scenario' => FakturacniUdaje::SCENARIO_OBJEDNAVKA]);
        $mDodaciUdaje = new DodaciUdaje(['scenario' => DodaciUdaje::SCENARIO_OBJEDNAVKA]);

        $aFakturacniUdaje = $mFakturacniUdaje->vratUzivatelovyProDropdown(\Yii::$app->user->id);
        $aDodaciUdaje = $mDodaciUdaje->vratUzivatelovyProDropdown(\Yii::$app->user->id);

        if (!empty($post)) {
            switch ($post['akce']) {
                case 'zpet':
                    return $this->redirect(['/eshop/objednavka/krok-jedna']);
                    break;
                case 'potvrdit':
                    $mObjednavka->load($post);

                    if ($mObjednavka->fakturacni_udaje_pk != null) {
                        $mFakturacniUdaje->nactiPodlePk($mObjednavka->fakturacni_udaje_pk);
                    }

                    if ($mObjednavka->udaje_stejne == 1) {
                        $mDodaciUdaje->load($mFakturacniUdaje->attributes, '');
                    } else if ($mObjednavka->dodaci_udaje_pk != null) {
                        $mDodaciUdaje->nactiPodlePk($mObjednavka->dodaci_udaje_pk);
                    }

                    $objednavkaValid = $mObjednavka->validate();

                    $mFakturacniUdaje->objednavka_pk = $mObjednavka->objednavka_pk;
                    $fakturacniValid = $mFakturacniUdaje->validate();
                    if ($fakturacniValid) {
                        $mFakturacniUdaje->ulozObjednavce();
                    }

                    $mDodaciUdaje->objednavka_pk = $mObjednavka->objednavka_pk;
                    $dodaciValid = $mDodaciUdaje->validate();
                    if ($dodaciValid) {
                        $mDodaciUdaje->ulozObjednavce();
                    }

                    // pokud je vse ok, tak redirect... mozna obalit try/catch cely blok a pokud nezvaliduji, tak chytit vyjimku
                    if ($objednavkaValid && $dodaciValid && $fakturacniValid) {
                        return $this->redirect(['/eshop/objednavka/krok-tri']);
                    }

                    break;
                default:
                    Application::setFlashWarning("Neplatná akce, pokud chcete odeslat objednávku, použijte tlačítko \"Další krok\".");
                    return $this->redirect(['/eshop/objednavka/krok-dva']);
            }
        }

        return $this->render('udaje', [
            'krok' => 2,
            'mObjednavka' => $mObjednavka,
            'mFakturacniUdaje' => $mFakturacniUdaje,
            'aFakturacniUdaje' => $aFakturacniUdaje,
            'mDodaciUdaje' => $mDodaciUdaje,
            'aDodaciUdaje' => $aDodaciUdaje
        ]);
    }

    /**
     * kontrola a potvrzeni
     */
    public function actionKrokTri()
    {
        // nacitat spis polozky objednavky... ale vyjde to nastejno
        $mKosik = new Kosik();
        $mObjednavka = $this->_nactiObjednavku(Objednavka::SCENARIO_POTVRZENI);

        // nejprve zkontroluju, ze uz je vybrana doprava
        if ($mObjednavka->platba_dopravy_id == null) {
            Application::setFlashInfo("Nejprve musíte vybrat způsob dopravy a platby.");
            return $this->redirect(['/eshop/objednavka/krok-jedna']);
        }
        // a taky dodaci a fakturacni udaje
        else if ($mObjednavka->fakturacni_udaje_pk == null || $mObjednavka->dodaci_udaje_pk == null) {
            Application::setFlashInfo("Nejprve musíte vybrat dodací a fakturační údaje.");
            return $this->redirect(['/eshop/objednavka/krok-dva']);
        }

        $mFakturacniUdaje = (new FakturacniUdaje())->nactiPodlePk($mObjednavka->fakturacni_udaje_pk);
        $mDodaciUdaje = (new DodaciUdaje())->nactiPodlePk($mObjednavka->dodaci_udaje_pk);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            switch ($post['akce']) {
                case 'zpet':
                    return $this->redirect(['/eshop/objednavka/krok-dva']);
                    break;
                case 'potvrdit':
                    $mObjednavka->load($post);

                    if ($mObjednavka->validate()) {
                        $mObjednavka->dokoncit();

                        $link = Html::a($mObjednavka->cislo, ['/eshop/objednavka/detail', 'id' => $mObjednavka->cislo]);
                        Application::setFlashSuccess("Objednávka byla uložena pod číslem {$link}.");
                        return $this->redirect(['/eshop/objednavka/dekujeme']);
                    }
                    break;
            }
        }

        return $this->render('zaver', [
            'krok' => 3,
            'mKosik' => $mKosik,
            'mObjednavka' => $mObjednavka,
            'mDodaciUdaje' => $mDodaciUdaje,
            'mFakturacniUdaje' => $mFakturacniUdaje
        ]);
    }

    /**
     * zobrazení, že objednávka byla přijata ke zpracování
     */
    public function actionDekujeme()
    {
        return $this->render('dekujeme');
    }

    /**
     * @param integer $id cislo objednavky
     * @return string
     */
    public function actionDetail($id)
    {
        $mObjednavka = (new Objednavka())->nactiPodleCisla($id);

        // neexistujici objednavka
        if ($mObjednavka == false) {
            ddd($id, 'neexistuje');
        }
        // objednavka mi nepatri
        else if ($mObjednavka->uzivatel_pk != \Yii::$app->user->id) {
            ddd($id, $mObjednavka->attributes, 'nepatri mi');
        }

        $mFakturacniUdaje = (new FakturacniUdaje())->nactiPodlePk($mObjednavka->fakturacni_udaje_pk);
        $mDodaciUdaje = (new DodaciUdaje())->nactiPodlePk($mObjednavka->dodaci_udaje_pk);
        $mPlatbaDopravy = (new PlatbaDopravy())->findOne(['platba_dopravy_id' => $mObjednavka->platba_dopravy_id]);
        $mZpusobPlatby = (new ZpusobPlatby())->findOne($mPlatbaDopravy->zpusob_platby_pk);
        $mZpusobDopravy = (new ZpusobDopravy())->findOne($mPlatbaDopravy->zpusob_dopravy_pk);

        return $this->render('detail', [
            'mObjednavka' => $mObjednavka,
            'mFakturacniUdaje' => $mFakturacniUdaje,
            'mDodaciUdaje' => $mDodaciUdaje,
            'mZpusobPlatby' => $mZpusobPlatby,
            'mZpusobDopravy' => $mZpusobDopravy
        ]);
    }

    /**
     * @return Response
     */
    public function actionUlozDodaci()
    {
        $model = new DodaciUdaje(['scenario' => DodaciUdaje::SCENARIO_OBJEDNAVKA]);
        return $this->ulozUdaje($model);
    }

    /**
     * @return Response
     */
    public function actionUlozFakturacni()
    {
        $model = new FakturacniUdaje(['scenario' => FakturacniUdaje::SCENARIO_OBJEDNAVKA]);
        return $this->ulozUdaje($model);
    }

    /**
     * todo nekdy casem refactorovat na samostatnou ajax action
     * @param FakturacniUdaje|DodaciUdaje $model
     * @return Response
     */
    protected function ulozUdaje($model)
    {
        $url = \Yii::$app->request->referrer;
        $post = \Yii::$app->request->post();
        $isAjax = \Yii::$app->request->isAjax;

        $response = [];
        if (!empty($post)) {
            $model->load($post);
            $model->uzivatel_pk = \Yii::$app->user->id;

            if ($model->validate()) {
                if (($pk = $model->ulozUzivateli()) !== false) {
                    if ($isAjax) {
                        $response['success'] = 1;
                        $response['data'] = [
                            'pk' => $pk,
                            'label' => $model->vratUzivatelovyProDropdown(\Yii::$app->user->id)[$pk]
                        ];
                    } else {
                        Application::setFlashSuccess("Údaje byly úspěšně uloženy.");
                    }
                } else {
                    $msg = "Nepodařilo se uložit údaje, opakujte akci později.";

                    if ($isAjax) {
                        $response['alert'] = $msg;
                    } else {
                        Application::setFlashError($msg);
                    }
                }
            } else {
                if ($isAjax) {
                    foreach ($model->getErrors() as $attr => $errors) {
                        $response['errors'][Html::getInputId($model, $attr)] = $errors;
                    }
                }
            }
        }

        if ($isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        } else {
            return $this->redirect($url);
        }
    }

    /* ---------- odtud jsou ajaxove metody ---------- */

    /**
     *
     */
    public function actionAjaxVratPlatby()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $mDopravaForm = new DopravaForm();

        $post = \Yii::$app->request->post();

        if (!isset($post['doprava'])) {
            throw new \Exception('Neplatny request');
        }

        $mDopravaForm->zpusob_dopravy_pk = $post['doprava'];
        $mKosik = new Kosik();

        $response = [
            'platby' => $this->renderFile('@app/modules/eshop/views/objednavka/_krok_jedna_platby.php', [
                'mDopravaForm' => $mDopravaForm,
                'aZpusobyPlatby' => $mDopravaForm->vratZpusobyPlatbyProForm()
            ]),
            'doprava' => '--- Kč', //\Yii::$app->formatter->asCurrency($mKosik->vratCastkuDopravy()),
            'celkem' => \Yii::$app->formatter->asCurrency($mKosik->vratCastkuObjednavky())
        ];

        return $response;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionAjaxNastavDopravu()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];

        try {
            $post = \Yii::$app->request->post();
            if (!isset($post['doprava']) || !isset($post['platba'])) {
                throw new \Exception('Neplatny request');
            }

            /** @var Objednavka $mObjednavka */
            $mObjednavka = (new Objednavka())->nactiObjednavkuKosiku();
            $mObjednavka->nastavDopravuPlatbu($post['doprava'], $post['platba']);

            $mKosik = new Kosik();

            $response['doprava'] = \Yii::$app->formatter->asCurrency($mKosik->vratCastkuDopravy());
            $response['celkem'] = \Yii::$app->formatter->asCurrency($mKosik->vratCastkuCelkem());
        } catch (\Exception $e) {
            \Yii::error("Chyba pri objednavka/ajax-nastav-dopravu, detail: {$e->getMessage()}");
            $response['error'] = "Neočekávaná chyba, opakujte akci později.";
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionAjaxNactiUdaje()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];

        try {
            $post = \Yii::$app->request->post();
            if (!isset($post['typ']) || !isset($post['pk'])) {
                throw new \Exception('Neplatny request');
            }
            // proc si to nevezme z use...
            $typ = 'common\models\\' . ucfirst($post['typ']) . 'Udaje';

            /** @var FakturacniUdaje|DodaciUdaje $model */
            $model = new $typ();
            $model->nactiPodlePk($post['pk']);

            $response['success'] = 1;
            $response['data'] = $model->attributes;
        } catch (\Exception $e) {
            \Yii::error("Chyba pri objednavka/ajax-nacti-udaje, detail: {$e->getMessage()}");
            $response['error'] = "Neočekávaná chyba, opakujte akci později.";
        }

        return $response;
    }

    /* ---------- a odtud protected fce v ramci tohoto controlleru ---------- */

    /**
     * @param string $scenario
     * @return Objednavka
     */
    protected function _nactiObjednavku($scenario = Objednavka::SCENARIO_DEFAULT)
    {
        return (new Objednavka([
            'scenario' => $scenario
        ]))->nactiObjednavkuKosiku();
    }
}