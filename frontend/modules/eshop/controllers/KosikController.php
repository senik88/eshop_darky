<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17. 10. 2015
 * Time: 21:51
 */

namespace frontend\modules\eshop\controllers;


use backend\modules\eshop\models\Polozka;
use common\components\Application;
use frontend\modules\eshop\models\Kosik;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Class KosikController
 * @package frontend\modules\eshop\controllers
 */
class KosikController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $id
     * @param null|int $kusu pocet kusu polozky pro pridani do kosiku
     * @return \yii\web\Response
     */
    public function actionPridat($id, $kusu = null)
    {
        $url = \Yii::$app->request->referrer;
        if ($url == null) {
            $url = Url::to(['/eshop/katalog/detail', 'id' => $id]);
        }

        if (\Yii::$app->user->isGuest) {
            Application::setFlashWarning('Pro přidání položky do košíku se musíte přihlásit.');
            return $this->redirect($url);
        }

        if ($kusu != null && intval($kusu) === 0) {
            Application::setFlashError("Neplatný počet kusů položky: $kusu!");
            return $this->redirect($url);
        }

        $ret = Kosik::pridejPolozku($id, $kusu);

        if ($ret) {
            Application::setFlashSuccess("Položka byla přidána do košíku.");
        } else {
            Application::setFlashError("Položku se nepodařilo přidat do košíku, opakujte akci později.");
        }

        return $this->redirect($url);
    }

    /**
     * Action ke smazani polozky z kosiku.
     *  Pokud je $kusu null, tak smaze jeden kus, pokud je int, tolik kusu, pokud "vse", tak je to jasne.
     *  Dodelat ty pocty kusu...
     *
     * @param $id
     * @param null|int|string $kusu bud null, pocet kusu ke smazani nebo "vse"
     * @return \yii\web\Response
     */
    public function actionOdebrat($id, $kusu = null)
    {
        $url = \Yii::$app->request->referrer;

        if (\Yii::$app->user->isGuest) {
            Application::setFlashWarning('Pro odebrání položky z košíku se musíte přihlásit.');
            return $this->redirect($url);
        }

        $ret = Kosik::odeberPolozku($id, $kusu == 'vse');

        if ($ret) {
            Application::setFlashSuccess("Položka byla odebrána z košíku.");
        } else {
            Application::setFlashError("Položku se nepodařilo odebrat z košíku, opakujte akci později.");
        }

        return $this->redirect($url);
    }

    /**
     * Nulty krok objednavky - vypis polozek z kosiku.
     *
     * @return string|\yii\web\Response
     */
    public function actionVypsat()
    {
        $mKosik = new Kosik();

        $post = \Yii::$app->request->post();

        if (isset($post['akce'])) {
            switch ($post['akce']) {
                case 'objednat':
                    return $this->redirect(['/eshop/objednavka/krok-jedna']);
                    break;
                case 'vymazat':
                    break;
                case 'domu':
                    return $this->redirect(['/site/index']);
                    break;
                default:
                    Application::setFlashWarning('Neznámá akce!');
                    return $this->redirect(['/eshop/kosik/vypsat']);
            }
        }

        return $this->render('vypsat', [
            'mKosik' => $mKosik
        ]);
    }

    /**
     * todo haaaa... co s tím...
     */
    public function actionVysypat()
    {
        $ret = Kosik::vysypat();
    }
}