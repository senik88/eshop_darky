<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.10.2015
 * Time: 20:10
 */

namespace frontend\modules\eshop\controllers;


use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use backend\modules\eshop\models\Vyrobce;
use common\models\Logo;
use common\models\ParametrPolozky;
use yii\debug\models\search\Log;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Class KatalogController
 * @package frontend\modules\eshop\controllers
 */
class KatalogController extends Controller
{
    /**
     * @var string Kategorie ID, v layoutu k ni muzu pristoupit pres $this->context->kategorie
     */
    public $kategorie;

    /**
     * @return string
     * @throws HttpException
     * @internal param null $id
     */
    public function actionIndex()
    {
        $id = \Yii::$app->request->get('id');

        if ($id == null) {
            throw new HttpException(500, 'Není vybrána kategorie');
        }

        /** @var Kategorie $mKategorie */
        $mKategorie = Kategorie::findOne(['kategorie_id' => $id]);
        if ($mKategorie == null) {
            throw new HttpException(404, 'Kategorie neexistuje');
        }

        $mNadrazena = null;
        if ($mKategorie->nadrazena != null) {
            $mNadrazena = Kategorie::findOne(['kategorie_pk' => $mKategorie->nadrazena]);
        }

        $this->kategorie = $mKategorie->kategorie_id;

        $mPolozka = new Polozka();
        $mPolozka->kategorie_id = $id;

        if ($mNadrazena != null) {
            $this->layout = '@app/views/layouts/_column2';

            return $this->render('index', [
                'mKategorie' => $mKategorie,
                'mNadrazena' => $mNadrazena,
                'mPolozka' => $mPolozka
            ]);
        } else {

            $podkategorie = $mKategorie->vratPodkategorie();

            $aPodkategorie = [];
            foreach ($podkategorie as $row) {
                $aPodkategorie[] = Kategorie::findOne($row['pk']);
            }

            return $this->render('index-nadrazena', [
                'mKategorie' => $mKategorie,
                'mNadrazena' => $mNadrazena,
                'mPolozka' => $mPolozka,
                'aPodkategorie' => $aPodkategorie
            ]);
        }
    }

    /**
     * Vypis zbozi vyrobce po kategoriich
     *
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionVyrobce($id)
    {
        /** @var Vyrobce $mVyrobce */
        $mVyrobce = Vyrobce::findOne(['vyrobce_id' => $id]);
        if ($mVyrobce == null) {
            throw new HttpException(404, 'Výrobce neexistuje');
        }

        $mLogo = Logo::findOne($mVyrobce->logo_pk);

        $aKategorie = $mVyrobce->vratPrehledProduktuKategorii();

        return $this->render('vyrobce', [
            'mVyrobce' => $mVyrobce,
            'mLogo' => $mLogo,
            'aKategorie' => $aKategorie
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionDetail($id)
    {
        $mPolozka = (new Polozka())->nactiEshop($id);
        if ($mPolozka == null) {
            throw new HttpException(404, 'Položka neexistuje');
        }

        /** @var Vyrobce $mVyrobce */
        $mVyrobce = Vyrobce::findOne($mPolozka->vyrobce_pk);
        /** @var Logo $mLogo */
        $mLogo = Logo::findOne($mVyrobce->logo_pk);
        /** @var ParametrPolozky[] $aParametry */
        $aParametry = $mPolozka->vratParametryPoSkupinach();

        $this->kategorie = $mPolozka->mKategorie->kategorie_id;

        return $this->render('detail', [
            'mPolozka' => $mPolozka,
            'mVyrobce' => $mVyrobce,
            'mLogo' => $mLogo,
            'aParametry' => $aParametry
        ]);
    }
}