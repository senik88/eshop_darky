<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17. 10. 2015
 * Time: 21:14
 */

namespace frontend\modules\eshop\components;

use Yii;
use yii\bootstrap\Widget;

/**
 * Class KosikWidget
 * @package frontend\modules\eshop\components
 */
class KosikWidget extends Widget
{
    /**
     * @var string
     */
    public $template = '@app/modules/eshop/components/views/kosik.php';

    /**
     *
     */
    public function run()
    {
        $app = Yii::$app;
        $db = $app->db;

        // potrebuju vsechny polozky kosiku
        $polozky_sql = "select * from kosik_uzivatele where uzivatel_pk = :uid";
        // a sumu polozek, udelam to nad DB, protoze je to rychlejis
        $celkem_sql = "select sum(mnozstvi) as polozek, sum(cena_aktualni * mnozstvi) as suma from kosik_uzivatele where uzivatel_pk = :uid";

        $params = [':uid' => $app->user->id];

        $polozky = $db->createCommand($polozky_sql)->bindValues($params)->queryAll();
        $celkem = $db->createCommand($celkem_sql)->bindValues($params)->queryOne();

        return $this->render($this->template, [
            'polozky' => $polozky,
            'celkem' => $celkem
        ]);
    }
}