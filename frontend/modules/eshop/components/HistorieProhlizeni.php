<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 22.02.2016
 * Time: 19:54
 */

namespace frontend\modules\eshop\components;


use common\components\helpers\String;
use common\components\SqlDataProvider;

/**
 * Class HistorieProhlizeni
 *
 * Wrapper pro zobrazeni historie prohlizenych produktu
 * todo dopracovat, ted se tim nezatezovat
 *
 * @package frontend\modules\eshop\components
 */
class HistorieProhlizeni
{
    /**
     * @return SqlDataProvider
     */
    public static function nacti()
    {
        // pro neprihlaseneho mam v cookie pk polozek
        if (\Yii::$app->user->isGuest) {
            $sql = "";
            $params = [];
        }
        // a u prihlaseneho to mam nasypane v DB - daly by se nad tim delat nejake chytrystiky
        else {
            $sql = "
                SELECT * FROM historie_prohlizeni h
                    JOIN eshop_aktualni e ON h.eshop_pk = e.eshop_pk
                WHERE h.uzivatel_pk = :pk
                ORDER BY h.OID DESC
            ";
            $params = [];
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params
        ]);
    }

    /**
     * @param $id
     * @return int|false
     */
    public static function uloz($id)
    {
        // pro neprihlaseneho budu ukladat do cookie seznam pk polozek
        if (\Yii::$app->user->isGuest) {

            return 1;
        }
        // a u prihlaseneho to zapisu do databaze
        else {
            $sql = "SELECT histori_prohlizeni_uloz(:upk, :epk)";
            $params = [
                ':upk' => \Yii::$app->user->id,
                ':epk' => $id
            ];

            return \Yii::$app->db->createCommand($sql, $params)->queryScalar();
        }
    }

    /**
     *
     */
    protected static function nactiCookieData()
    {
        $cookies = \Yii::$app->response->cookies;

        $name = self::vratCookieName();
        $value = $cookies->get($name);


    }

    /**
     * @return string
     */
    protected static function vratCookieName()
    {
        return String::webalize(\Yii::$app->name . ' eshop history');
    }
}