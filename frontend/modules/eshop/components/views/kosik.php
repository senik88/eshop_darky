<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17. 10. 2015
 * Time: 21:18
 *
 * @var $polozky array
 * @var $celkem array
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

$formatter = Yii::$app->getFormatter();
$formatter->nullDisplay = '<i>(Prázdný košík)</i>'
?>

<?php $form = ActiveForm::begin([
    'method' => 'POST',
    'action' => ['/eshop/kosik/vypsat']
]) ?>
<div id="kosik-widget">
    <a href="<?= Url::to(['/eshop/kosik/vypsat']) ?>">
        <span class="kosik">
            <?= Html::icon('shopping-cart') ?>
            <span class="pocet badge"><?= $celkem['polozek'] ?></span>
        </span>
        <br>
        <span class="cena"><?= $formatter->asCurrency($celkem['suma'], 'CZK') ?></span>
    </a>
    <?php if (!empty($polozky)) { ?>
        <div id="kosik-detail">
            <div id="kosik-polozky">
                <?php
                echo '<table>';
                echo '<tbody>';

                $controller = Yii::$app->controller->id;
                $action = Yii::$app->controller->action->id;

                $muzu_editovat = !($controller == 'objednavka' || ($controller == 'kosik' && $action == 'vypsat'));

                foreach ($polozky as $polozka) {
                    echo '<tr>';
                    {
                        echo Html::tag('td', Html::a($polozka['titulek'], Url::to(['/eshop/katalog/detail', 'id' => $polozka['polozka_id']])));
                        echo Html::tag('td', $polozka['mnozstvi'] . ' ks');
                        echo Html::tag('td', Yii::$app->getFormatter()->asCurrency($polozka['cena_celkem'], 'CZK'));
                        echo Html::tag('td', $muzu_editovat ? Html::a(Html::icon('remove'), Url::to(['/eshop/kosik/odebrat', 'id' => $polozka['polozka_id'], 'kusu' => 'vse'])) : '');
                    }
                    echo '</tr>';
                }
                echo '</tbody>';
                echo '<tfoot>';
                {
                    echo '<tr>';
                    {
                        echo Html::tag('td', null);
                        echo Html::tag('td', $celkem['polozek'] . ' ks');
                        echo Html::tag('td', Yii::$app->getFormatter()->asCurrency($celkem['suma'], 'CZK'));
                        echo Html::tag('td', null);
                    }
                    echo '<tr>';
                }
                echo '</tfoot>';
                echo '</table>';

            ?>
            </div>
            <div id="kosik-tlacitka">
                <?= Html::submitButton('Vymazat košík', ['class' => 'btn btn-info btn-sm', 'name' => 'akce', 'value' => 'vymazat', 'disabled' => !$muzu_editovat]) ?>
                <?= Html::submitButton('Objednat zboží', ['class' => 'btn btn-success btn-sm', 'name' => 'akce', 'value' => 'objednat', 'disabled' => !$muzu_editovat]) ?>
            </div>
        </div>
    <?php } ?>
</div>
<?php ActiveForm::end() ?>

<!--<a href="< ?//= Url::to(['/eshop/kosik/vypsat']) ?><!--">-->
<!--    <h3>-->
<!--        <div class="total">-->
<!--            <span class="simpleCart_total"></span></div>-->
<!--        <img src="< ?//= $theme->baseUrl ?><!--/images/cart.png" alt=""/></h3>-->
<!--</a>-->
<!--<p><a href="javascript:;" class="simpleCart_empty">Vysypat</a></p>-->
