
<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.09.2015
 * Time: 20:21
 *
 * @var $model RegistraceForm
 */
use frontend\modules\uzivatel\forms\RegistraceForm;
use frontend\modules\uzivatel\UzivatelModule;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->params['breadcrumbs'][] = 'Registrace';
?>

<div class="row" id="registrace-wrapper">
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin([
            'layout' => 'horizontal'
        ]);
        ?>

        <h2>Nová registrace</h2>

        <div class="form-fields">
            <h3>Kontaktní údaje</h3>

            <?= $form->field($model, 'jmeno') ?>
            <?= $form->field($model, 'prijmeni') ?>
            <?= $form->field($model, 'telefon') ?>

            <h3>Profilové údaje</h3>

            <?= $form->field($model, 'email')->hint(UzivatelModule::t('form', 'Slouží zároveň jako přihlašovací jméno')) ?>
            <?= $form->field($model, 'heslo') ?>
            <?= $form->field($model, 'heslo2') ?>

            <h3>Fakturační údaje</h3>

            <div id="udaje-firma">
                <?= $form->field($model, 'obchodni_jmeno') ?>
                <?= $form->field($model, 'ic') ?>
                <?= $form->field($model, 'dic') ?>
            </div>

            <?= $form->field($model, 'zeme')->dropDownList(RegistraceForm::itemAlias('zeme')) ?>
            <?= $form->field($model, 'ulice') ?>
            <?= $form->field($model, 'mesto') ?>
            <?= $form->field($model, 'psc') ?>
        </div>

        <div class="form-actions">
            <?= Html::submitButton('Registrovat', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

        </div>

        <div class="clearfix"></div>
        <?php ActiveForm::end(); ?>

    </div>
</div>