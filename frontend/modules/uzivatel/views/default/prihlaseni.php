<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.09.2015
 * Time: 23:12
 * @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model \common\models\LoginForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Přihlášení';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login row" id="prihlaseni-wrapper">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal'
        ]); ?>
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="form-fields row">
            <div class="col-md-8 col-md-offset-2">
                <p>Please fill out the following fields to login:</p>
                <?= $form->field($model, 'username') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    Pokud jste zapomněl(a) heslo, můžete si <?= Html::a('vyžádat nové', ['site/request-password-reset']) ?>.
                </div>
            </div>
        </div>

        <div class="form-actions">
            <?= Html::submitButton('Přihlásit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>