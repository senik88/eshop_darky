<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:20
 *
 * @var ObjednavkaSearchForm $model
 */
use frontend\modules\eshop\forms\ObjednavkaSearchForm;
use yii\bootstrap\Html;

?>

<h2>Moje objednávky</h2>

<?=
\yii\grid\GridView::widget([
    'dataProvider' => $model->search(),
    'columns' => [
        'cislo' => [
            'attribute' => 'cislo',
            'label' => 'Číslo',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(
                    $model['cislo'],
                    ['/eshop/objednavka/detail', 'id' => $model['cislo']]
                );
            }
        ],
        'cas_vytvoreni' => [
            'attribute' => 'cas_vytvoreni',
            'label' => 'Datum',
            'value' => function ($model) {
                return Yii::$app->formatter->asDate($model['cas_vytvoreni']);
            }
        ],
        'cena' => [
            'attribute' => 'cena'
        ],
        'stav' => [
            'attribute' => 'stav_objednavky_id',
            'label' => 'Stav'
        ]
    ]
])
?>