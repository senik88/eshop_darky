<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:20
 */
?>

<h2>Můj profil</h2>

<p>
    Na téhle stránce bude základní přehled:
</p>
<ul>
    <li>Posledních 5 objednávek</li>
    <li>Posledních 5 faktur</li>
    <li>Posledních 5 reklamací</li>
    <li>Registrační a poslední použité fakturační údaje</li>
</ul>
<p>
    Na levé straně pak bude menu s odkazy na jednotlivé sekce...
</p>