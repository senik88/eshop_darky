<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.09.2015
 * Time: 20:22
 */

namespace frontend\modules\uzivatel\forms;


use common\components\ItemAliasTrait;
use common\models\FakturacniUdaje;
use Exception;
use frontend\modules\uzivatel\models\UzivatelFrontend;
use frontend\modules\uzivatel\UzivatelModule;
use yii\base\Model;

/**
 * Class RegistraceForm
 * @package frontend\modules\uzivatel\forms
 */
class RegistraceForm extends Model
{
    use ItemAliasTrait;

    const ZEME_DEFAULT = 'CZ';

    /**
     * @var UzivatelFrontend
     */
    protected $_uzivatel;

    public $jmeno;

    public $prijmeni;

    public $email;

    public $heslo;

    public $heslo2;

    public $telefon;

    public $osobaTyp;

    public $ic;

    public $dic;

    public $obchodni_jmeno;

    public $ulice;

    public $mesto;

    public $psc;

    public $zeme = self::ZEME_DEFAULT;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jmeno' => UzivatelModule::t('form', 'Jméno'),
            'prijmeni' => UzivatelModule::t('form', 'Příjmení'),
            'email' => UzivatelModule::t('form', 'E-mail'),
            'heslo' => UzivatelModule::t('form', 'Heslo'),
            'heslo2' => UzivatelModule::t('form', 'Kontrola hesla'),
            'telefon' => UzivatelModule::t('form', 'Telefon'),
            'ic' => UzivatelModule::t('form', 'IČ'),
            'dic' => UzivatelModule::t('form', 'DIČ'),
            'obchodni_jmeno' => UzivatelModule::t('form', 'Obchodní jméno'),
            'ulice' => UzivatelModule::t('form', 'Ulice'),
            'mesto' => UzivatelModule::t('form', 'Město'),
            'psc' => UzivatelModule::t('form', 'PSČ'),
            'zeme' => UzivatelModule::t('form', 'Země'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jmeno', 'prijmeni', 'email', 'heslo', 'heslo2'/*, 'zeme', 'ulice', 'mesto', 'psc'*/], 'required'],
            [['telefon'], 'safe']
        ];
    }

    /**
     *
     */
    public function registruj()
    {
        $db = \Yii::$app->db;
        $trans = $db->beginTransaction();

        try {

            $mUzivatel = new UzivatelFrontend();

            $mUzivatel->load($this->attributes, '');
            $mUzivatel->stav = UzivatelFrontend::STAV_AKTIVNI;
            $mUzivatel->cas_registrace = date('Y-m-d H:i:s', time());
            $mUzivatel->ip_registrace = \Yii::$app->request->getUserIP();
            $mUzivatel->heslo = \Yii::$app->security->generatePasswordHash($this->heslo);
            $mUzivatel->validacni_klic = md5($mUzivatel->email.$mUzivatel->ip_registrace);

            if (!$mUzivatel->save()) {
                throw new Exception('nepodarilo se ulozit uzivatele');
            }

            $mUzivatel->refresh();

            /*$faUdaje = new FakturacniUdaje();
            $faUdaje->load($this->attributes, '');
            $faUdaje->uzivatel_pk = $mUzivatel->uzivatel_pk;

            $fa_pk = $faUdaje->ulozUzivateli();*/

            $trans->commit();

            return true;
        } catch (Exception $e) {
            \Yii::error("chyba pri registraci uzivatele: {$e->getMessage()}");
            $trans->rollBack();
            return false;
        }
    }

    /**
     * @return array
     */
    protected static function itemAliasData()
    {
        return [
            'forma' => [
                'FO' => UzivatelModule::t('form', 'Fyzická osoba'),
                'PO' => UzivatelModule::t('form', 'Firma')
            ],
            'zeme' => [
                'CZ' => UzivatelModule::t('form', 'Česká republika'),
                'SK' => UzivatelModule::t('form', 'Slovenská republika')
            ]
        ];
    }
}