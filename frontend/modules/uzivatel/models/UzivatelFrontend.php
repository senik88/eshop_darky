<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 14.09.2015
 * Time: 20:34
 */

namespace frontend\modules\uzivatel\models;


use backend\modules\uzivatel\UzivatelModule;
use common\components\uzivatel\Uzivatel;
use Exception;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/**
 * Class UzivatelFrontend
 * @package frontend\modules\uzivatel\models
 */
class UzivatelFrontend extends Uzivatel
{
    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        $this->rozhrani = self::ROZHRANI_FRONT;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PRIDAT] = [
            'email', 'rozhrani', 'stav', 'heslo', 'cas_registrace', 'ip_registrace', 'validacni_klic', 'jmeno', 'prijmeni'
        ];
        $scenarios[self::SCENARIO_UPRAVIT] = [
            'jmeno', 'prijmeni', 'email', 'heslo', 'stav', 'rozhrani'
        ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uzivatel_pk' => UzivatelModule::t('app', 'ID'),
            'cas_registrace' => UzivatelModule::t('app', 'Čas registrace'),
            'cas_prihlaseni' => UzivatelModule::t('app', 'Čas přihlášení'),
            'ip_prihlaseni' => UzivatelModule::t('app', 'IP přihlášení'),
            'stav' => UzivatelModule::t('app', 'Stav'),
            'email' => UzivatelModule::t('app', 'E-mail'),
        ];
    }

    /**
     * @param string $username
     * @return null|static
     */
    public static function findByUsername($username)
    {
        return parent::findByUsername($username, self::ROZHRANI_FRONT);
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $query = self::find()->where(['rozhrani' => $this->rozhrani]);

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'id' => [
                'attribute' => 'uzivatel_pk',
            ],
            'email',
            'stav' => [
                'attribute' => 'stav',
                'value' => function ($model) {
                    return Uzivatel::itemAlias('stavy', $model->stav);
                }
            ],
            'cas_registrace',
            'cas_prihlaseni',
            'ip_prihlaseni',
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/uzivatel/frontend/detail', 'id' => $model['uzivatel_pk']);
                            break;
                        case 'update':
                            $url = array('/uzivatel/frontend/upravit', 'id' => $model['uzivatel_pk']);
                            break;
                        case 'delete':
                            $url = array('/uzivatel/frontend/smazat', 'id' => $model['uzivatel_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil',
                            ]),
                            $url,
                            [
                                'title' => 'Upravit',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-trash',
                            ]),
                            $url,
                            [
                                'title' => 'Smazat',
                                'data-pjax' => 0,
                                'data-confirm' => "Opravdu chcete smazat uživatele?",
                                'data-method' => "post",
                            ]
                        );
                    }
                ),
            ]
        ];
    }
}