<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 20.07.2016
 * Time: 20:12
 */

namespace frontend\modules\uzivatel\models;


use common\components\helpers\DbUtils;
use common\components\SqlDataProvider;
use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\Objednavka;
use yii\base\Model;

/**
 * Class Dashboard
 * @package frontend\modules\uzivatel\models
 */
class Dashboard extends Model
{
    /**
     * @var
     */
    public $uzivatel_pk;

    /**
     * @var integer defaultni limit pro dataprovidery
     */
    public $limit;

    /**
     * @param $id
     * @return Dashboard
     */
    public static function factory($id)
    {
        $model = new self;
        $model->uzivatel_pk = $id;
        $model->limit = 5;

        return $model;
    }

    /**
     *
     */
    public static function vratPocetObjednavek()
    {
        $sql = "SELECT count(*) FROM objednavka WHERE uzivatel_pk = :pk AND NOT (stav_objednavky_id = ANY(:stavy))";
        $params = [
            ':pk' => \Yii::$app->user->id,
            ':stavy' => DbUtils::implodePgArray([Objednavka::STAV_STORNO, Objednavka::STAV_KOSIK])
        ];

        return \Yii::$app->db->createCommand($sql, $params)->queryScalar();
    }

    /**
     * @return SqlDataProvider
     */
    public function vratPosledniObjednavkyUzivatele()
    {
        $sql = "
            SELECT
                o.cislo,
                o.cas_vytvoreni,
                o.cas_zmeny,
                o.stav_objednavky_id,
                sum(ec.cena_aktualni * opmn.mnozstvi) + pd.cena AS cena
            FROM objednavky_view AS ov
                JOIN objednavka o ON ov.objednavka_pk = o.objednavka_pk
                JOIN objednavka_polozka_mn AS opmn ON o.objednavka_pk = OPMN.objednavka_pk
                JOIN eshop AS e ON opmn.eshop_pk = e.eshop_pk
                JOIN eshop_cena ec ON e.eshop_pk = ec.eshop_pk AND ec.platne_od <= o.cas_zmeny AND (ec.platne_do IS NULL OR ec.platne_do > o.cas_zmeny)
                JOIN platba_dopravy AS pd ON o.platba_dopravy_id = pd.platba_dopravy_id
        ";

        $where = [
            "o.uzivatel_pk = :upk",
            "NOT (o.stav_objednavky_id = ANY(:stavy))"
        ];

        $sql .= " WHERE " . implode(" AND ", $where);
        $sql .= " GROUP BY o.cislo, o.cas_vytvoreni, o.cas_zmeny, o.stav_objednavky_id, pd.cena";

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => [
                ':upk' => \Yii::$app->user->id,
                ':stavy' => DbUtils::implodePgArray([Objednavka::STAV_STORNO, Objednavka::STAV_KOSIK])
            ],
            'sort' => [
                'attributes' => ['cas_vytvoreni'],
                'defaultOrder' => ['cas_vytvoreni' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => $this->limit
            ]
        ]);
    }

    /**
     * @return SqlDataProvider
     */
    public function vratPosledniFakturyUzivatele()
    {

    }

    /**
     * @return SqlDataProvider
     */
    public function vratPosledniReklamaceUzivatele()
    {

    }

    /**
     * @return DodaciUdaje
     */
    public function vratPosledniDodaciUdajeUzivatele()
    {
        $sql = "SELECT dodaci_udaje_pk FROM uzivatel_dodaci_udaje WHERE uzivatel_pk = :upk ORDER BY dodaci_udaje_pk DESC LIMIT 1";
        $params = [
            ':upk' => $this->uzivatel_pk
        ];

        $pk = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryScalar();

        return (new DodaciUdaje())->nactiPodlePk($pk);
    }

    /**
     * @return FakturacniUdaje
     */
    public function vratPosledniFakturacniUdajeUzivatele()
    {
        $sql = "SELECT fakturacni_udaje_pk FROM uzivatel_fakturacni_udaje WHERE uzivatel_pk = :upk ORDER BY fakturacni_udaje_pk DESC LIMIT 1";
        $params = [
            ':upk' => $this->uzivatel_pk
        ];

        $pk = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryScalar();

        return (new FakturacniUdaje())->nactiPodlePk($pk);
    }
}