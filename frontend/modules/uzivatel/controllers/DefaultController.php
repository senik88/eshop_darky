<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.09.2015
 * Time: 22:32
 */

namespace frontend\modules\uzivatel\controllers;


use common\components\Application;
use common\models\LoginForm;
use frontend\modules\uzivatel\forms\RegistraceForm;
use frontend\modules\uzivatel\models\PrihlaseniForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package frontend\modules\uzivatel\controllers
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['prihlaseni', 'error', 'registrace'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['odhlaseni', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     *
     */
    public function actionRegistrace()
    {
        $model = new RegistraceForm();

        $post = Yii::$app->request->post();

        if (!empty($post)) {
            $model->load($post);

            if ($model->validate()) {
                if ($model->registruj()) {
                    Application::setFlashSuccess('Děkujeme za registraci');

                    // rovnou prihlasit
                    return $this->redirect(['/site/index']);
                } else {
                    Application::setFlashError("Chyba při registraci, opakujte akci později");
                }
            }
        }

        return $this->render('registrace', [
            'model' => $model
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionPrihlaseni()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $mLoginForm = new LoginForm();

        if ($mLoginForm->load(Yii::$app->request->post()) && $mLoginForm->login()) {
            return $this->goBack();
        } else {
            return $this->render('prihlaseni', [
                'model' => $mLoginForm
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionOdhlaseni()
    {
        return $this->redirect(['/uzivatel/default/prihlaseni']);
    }
}