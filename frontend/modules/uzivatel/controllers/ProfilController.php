<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:19
 */

namespace frontend\modules\uzivatel\controllers;


use common\components\Application;
use common\models\DodaciUdaje;
use frontend\modules\eshop\forms\ObjednavkaSearchForm;
use frontend\modules\uzivatel\models\Dashboard;
use frontend\modules\uzivatel\models\UzivatelFrontend;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ProfilController
 * @package frontend\modules\uzivatel\controllers
 */
class ProfilController extends Controller
{
    /**
     * @var string
     */
    public $layout = '@app/views/layouts/_profil';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index', 'objednavky', 'faktury', 'reklamace', 'hlidaci-pes', 'registracni-udaje', 'dodaci-udaje', 'wishlist',
                            'ajax-dodaci-udaje'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->view->params['short_title'] = 'Můj přehled';
        $this->view->params['breadcrumbs'][] = 'Můj přehled';

        $mDashboard = Dashboard::factory(\Yii::$app->user->id);

        $pObjednavky = $mDashboard->vratPosledniObjednavkyUzivatele();
        $countObjednavky = $pObjednavky->getTotalCount();

        $mDodaciUdaje = $mDashboard->vratPosledniDodaciUdajeUzivatele();
        $mFakturacniUdaje = $mDashboard->vratPosledniFakturacniUdajeUzivatele();
        $mUzivatel = UzivatelFrontend::findOne(\Yii::$app->user->id);

        return $this->render('index', [
            'mDashboard' => $mDashboard,
            'mDodaciUdaje' => $mDodaciUdaje,
            'mFakturacniUdaje' => $mFakturacniUdaje,
            'mUzivatel' => $mUzivatel,
            'pObjednavky' => $pObjednavky,
            'countObjednavky' => $countObjednavky
        ]);
    }

    /**
     * @return string
     */
    public function actionObjednavky()
    {
        $this->view->params['short_title'] = 'Moje objednávky';
        $this->view->params['breadcrumbs'][] = ['url' => ['/uzivatel/profil/index'], 'label' => 'Můj přehled'];
        $this->view->params['breadcrumbs'][] = 'Objednávky';
        $this->view->title = \Yii::$app->name . ' | ' . 'Moje objednávky';

        $model = new ObjednavkaSearchForm();

        $get = \Yii::$app->request->get();

        if (!empty($get)) {
            $akce = \Yii::$app->request->get('akce');
            switch ($akce) {
                case 'hledat':
                    $model->load($get);
                    break;
                case 'reset':
                    return $this->redirect(['/uzivatel/profil/objednavky']);
                    break;
                default:
                    Application::setFlashWarning("Neplatná akce.");
                    return $this->redirect(['/uzivatel/profil/objednavky']);
            }
        }

        return $this->render('objednavky', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     */
    public function actionFaktury()
    {
        $this->view->params['short_title'] = 'Moje faktury';
        $this->view->params['breadcrumbs'][] = ['url' => ['/uzivatel/profil/index'], 'label' => 'Můj přehled'];
        $this->view->params['breadcrumbs'][] = 'Faktury';

        return $this->render('faktury', []);
    }

    /**
     * @return string
     */
    public function actionReklamace()
    {
        $this->view->params['short_title'] = 'Moje reklamace';
        $this->view->params['breadcrumbs'][] = ['url' => ['/uzivatel/profil/index'], 'label' => 'Můj přehled'];
        $this->view->params['breadcrumbs'][] = 'Reklamace';

        return $this->render('reklamace', []);
    }

    /**
     * @return string
     */
    public function actionHlidaciPes()
    {
        $this->view->params['short_title'] = 'Hlídací pes';
        $this->view->params['breadcrumbs'][] = ['url' => ['/uzivatel/profil/index'], 'label' => 'Můj přehled'];
        $this->view->params['breadcrumbs'][] = 'Hlídací pes';

        return $this->render('hlidaci-pes', []);
    }

    /**
     * @return string
     */
    public function actionWishlist()
    {
        $this->view->params['short_title'] = 'Seznam přání';
        $this->view->params['breadcrumbs'][] = ['url' => ['/uzivatel/profil/index'], 'label' => 'Můj přehled'];
        $this->view->params['breadcrumbs'][] = 'Seznam přání';

        return $this->render('wishlist', []);
    }

    /**
     * @return string
     */
    public function actionRegistracniUdaje()
    {
        $this->view->params['short_title'] = 'Registrační údaje';
        $this->view->params['breadcrumbs'][] = ['url' => ['/uzivatel/profil/index'], 'label' => 'Můj přehled'];
        $this->view->params['breadcrumbs'][] = 'Registrační údaje';

        return $this->render('registracni', []);
    }

    /**
     * Prehled dodacich udaju
     * @return string
     */
    public function actionDodaciUdaje()
    {
        $this->view->params['short_title'] = 'Dodací údaje';
        $this->view->params['breadcrumbs'][] = ['url' => ['/uzivatel/profil/index'], 'label' => 'Můj přehled'];
        $this->view->params['breadcrumbs'][] = 'Dodací údaje';

        $mDodaciUdaje = new DodaciUdaje();
        $aDodaciUdaje = $mDodaciUdaje->vratUzivatelovyProDropdown(\Yii::$app->user->id);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            ddd($post);
        }

        return $this->render('dodaci', [
            'mDodaciUdaje' => $mDodaciUdaje,
            'aDodaciUdaje' => $aDodaciUdaje
        ]);
    }


    /**
     * @return string
     */
    public function actionAjaxDodaciUdaje()
    {
        $pk = \Yii::$app->request->get('dodaci_udaje_pk');

        $mDodaciUdaje = (new DodaciUdaje())->nactiPodlePk($pk);

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $mDodaciUdaje->attributes;
    }
}