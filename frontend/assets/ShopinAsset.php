<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.04.2016
 * Time: 15:05
 */

namespace frontend\assets;


use yii\web\AssetBundle;

/**
 * Class ShopinAsset
 * @package frontend\assets
 */
class ShopinAsset extends AssetBundle
{
    public $basePath = '@app/themes/shopin';
    public $baseUrl = '@web/themes/shopin';
    public $css = [
        'css/style.css',
        'css/style4.css',
        'css/jstarbox.css',
        'css/form.css',
        'css/popuo-box.css',
        'css/chocolat.css',
        'css/flexslider.css',
        'css/senovo.css?v=1.0.2',
//        'css/bootstrap-select.min.css',
//        'fancybox/jquery.fancybox.css?v=2.1.5',
    ];
    public $js = [
//        'js/bootstrap-select.min.js',
//        'fancybox/jquery.fancybox.pack.js?v=2.1.5',
        'js/jstarbox.js',
        'js/simpleCart.min.js',
        'js/jquery.chocolat.js',
        'js/jquery.flexslider.js',
        'js/senovo.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\SelectPickerAsset'
    ];
}