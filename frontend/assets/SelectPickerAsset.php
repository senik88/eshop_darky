<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class SelectPickerAsset
 * @package frontend\assets
 */
class SelectPickerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-select.min.css',
    ];
    public $js = [
        'js/bootstrap-select.min.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
