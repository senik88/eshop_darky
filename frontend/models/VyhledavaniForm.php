<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 27.02.2016
 * Time: 15:07
 */

namespace frontend\models;


use common\components\SqlDataProvider;
use yii\base\Model;

/**
 * Class VyhledavaniForm
 * @package frontend\models
 */
class VyhledavaniForm extends Model
{
    public $fraze;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fraze'], 'safe']
        ];
    }

    /**
     * @return SqlDataProvider
     */
    public function hledat()
    {
        $sql = "
            select * from polozka where titulek ~ :fraze or subtitulek ~ :fraze
        ";

        $params = [
            ':fraze' => $this->fraze
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params
        ]);
    }
}