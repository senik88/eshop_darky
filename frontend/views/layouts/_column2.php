<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.10.2015
 * Time: 20:35
 *
 * @var $this View
 * @var $content string
 */
use backend\modules\eshop\models\Kategorie;
use frontend\modules\eshop\controllers\KatalogController;
use yii\bootstrap\Html;
use yii\web\View;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <div id="leve-menu">
        <div id="podkategorie">
            <?php
            //$controller = Yii::$app->controller->id;
            $action = Yii::$app->controller->action->id;
            $podkategorie = [];
            if (Yii::$app->controller instanceof KatalogController) {
                if ($this->context->kategorie != null) {
                    /** @var Kategorie $mKategorie */
                    $mKategorie = Kategorie::findOne(['kategorie_id' => $this->context->kategorie]);

                    if ($mKategorie->nadrazena == null) {
                        $podkategorie = $mKategorie->vratPodkategorie();
                    } else {
                        /** @var Kategorie $mNadrazena */
                        $mNadrazena = Kategorie::findOne(['kategorie_pk' => $mKategorie->nadrazena]);
                        $podkategorie = $mNadrazena->vratPodkategorie();
                    }
                }
            }

            if (!empty($podkategorie)) {
                echo '<ul>';

                foreach ($podkategorie as $kat) {
                    $active = $this->context->kategorie == $kat['id'];
                    echo Html::tag(
                        'li',
                        Html::a(
                            $kat['na'],
                            ['/eshop/katalog/index', 'id' => $kat['id']]
                        ),
                        [
                            'class' => $active ? 'active' : ''
                        ]
                    );
                }

                echo '</ul>';
            }
            ?>
        </div>
        <div id="widgety">
            <div>
                Widgety? Filtry?
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div id="katalog-obsah">
        <?= $content ?>
    </div>

<?php $this->endContent(); ?>