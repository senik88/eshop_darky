<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.10.2015
 * Time: 20:58
 */

use backend\modules\eshop\models\Kategorie;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'options' => [
        'class' => 'navbar-inverse', // navbar-fixed-top
    ],
]);

$menuKategorie = Kategorie::findAll(['menu_zobrazit' => true]);

$menuItems = [];
foreach ($menuKategorie as $kategorie) {
    $menuItems[] = [
        'label' => $kategorie['nazev'],
        'url' => ['/eshop/katalog/index', 'id' => $kategorie['kategorie_id']]
    ];
}

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items' => $menuItems,
]);
NavBar::end();