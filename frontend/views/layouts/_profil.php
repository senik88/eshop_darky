<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:31
 *
 * @var $this View
 * @var $content string
 */
use yii\helpers\Url;
use yii\web\View;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <div id="leve-menu">
        <div id="profil-menu">
        <ul>
            <li><h4><a href="<?= Url::to(['/uzivatel/profil/index']) ?>">Můj přehled</a></h4></li>
            <li><h4>Moje nákupy</h4>
                <ul>
                    <li><a href="<?= Url::to(['/uzivatel/profil/objednavky']) ?>">Objednávky (X)</a></li>
                    <li><a href="<?= Url::to(['/uzivatel/profil/faktury']) ?>">Faktury (X)</a></li>
                    <li><a href="<?= Url::to(['/uzivatel/profil/reklamace']) ?>">Reklamace (X)</a></li>
                    <li><a href="<?= Url::to(['/uzivatel/profil/hlidaci-pes']) ?>">Hlídací pes</a></li>
                </ul>
            </li>
            <li><h4>Moje nastavení</h4>
                <ul>
                    <li><a href="<?= Url::to(['/uzivatel/profil/registracni-udaje']) ?>">Registrační údaje</a></li>
                    <li><a href="<?= Url::to(['/uzivatel/profil/dorucovaci-udaje']) ?>">Doručovací údaje</a></li>
                </ul>
            </li>
        </ul>
        </div>
    </div>

    <div id="profil-obsah">
        <?= $content ?>
    </div>

<?php $this->endContent(); ?>