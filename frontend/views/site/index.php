<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>TODO list - Seník</h2>
                <ul>
                    <li>Doplnit loga do DB schematu pro vyrobce</li>
                    <li>V DB schematu u polozka_obrazek zmenit polozka_pk na null / nebo si udelat extra tridu na prazdnej obrazek?</li>
                    <li>Upravit trochu schema - dat do eshop eshop_id které se bude skladat z id polozky a id varianty</li>
                </ul>

            </div>
            <div class="col-lg-4">
                <h2>TODO list - Myšák</h2>
                <ul>
                    <li>V administraci - tlačítko na front.</li>
                    <li>V-Garden import - nastavení importu - datum posledního importu, výběr dodavatele, log</li>
                    <li>V-Garden import - nahrání obrázku</li>
                    <li>Zobrazovat verzi eshopu v adminu</li>
                    <li>Editace položky</li>
                    <li>Dashboard - zobrazovat počet produktů v kategorii nezařazeno + produkt, které nemají výrobce (povinná pole)</li>
                    <li>Kategorie - přidat pole pro CATEGORYTEXT pro Heureku a přidat do exportu</li>
                </ul>

            </div>
            <div class="col-lg-4">
                <h2>Changelog</h2>

                <ul>
                    <li></li>
                </ul>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h2>Historie navštívených produktů</h2>
            </div>
        </div>

    </div>
</div>
