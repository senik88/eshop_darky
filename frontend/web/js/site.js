/**
 * Created by Lukáš on 21.10.2015.
 */
(function(Senovo, $, undefined) {
    /**
     * handler pro dopravu a platbu, prvni krok objednavky
     * @param event
     */
    Senovo.handleObjednavkaKrokJednaForm = function(event) {
        event.preventDefault();

        var request,
            $target = event.target,
            input_name = $target.getAttribute('name');

        if (input_name == 'DopravaForm[zpusob_dopravy_pk]') {
            request = $.ajax({
                method: 'POST',
                url:    'ajax-vrat-platby',
                data:   {doprava: $(this).val()}
            });

            request.done(function(response) {
                if (typeof response == 'object') {
                    $('#objednavka-moznosti-platby').html(response.platby);
                    $('#objednavka-cena-dopravy').html(response.doprava);
                    $('#objednavka-cena-celkem').html(response.celkem);
                } else {
                    window.console.log(response);
                }
            });
        } else if (input_name == 'DopravaForm[zpusob_platby_pk]') {
            var doprava = $('input[name="DopravaForm[zpusob_dopravy_pk]"]:checked').val();

            request = $.ajax({
                method: 'POST',
                url:    'ajax-nastav-dopravu',
                data:   {doprava: doprava, platba: $(this).val()}
            });

            request.done(function(response) {
                if (typeof response == 'object') {
                    if (response.error != undefined) {
                        window.console.log(response.error);
                    } else {
                        $('#objednavka-cena-dopravy').html(response.doprava);
                        $('#objednavka-cena-celkem').html(response.celkem);
                    }
                } else {
                    window.console.log(response);
                }
            });
        }
    };

    /**
     * Prepinani zobrazeni ruznych prvku objednavkoveho formulare v kroku 2
     */
    Senovo.handleObjednavkaKrokDvaForm = function(event) {
        // heh, tohle bude chtit neco lepsiho... mozna pres target u selectu, jinak to bude onload
        var target = $(event.target)[0].nodeName,
            $fa_udaje_select = $('#objednavka-fakturacni_udaje_pk'),
            $do_udaje_select = $('#objednavka-dodaci_udaje_pk'),
            $chk_stejne = $('#objednavka-udaje_stejne'),
            $do_wrapper = $('#dodaci-udaje-form-wrapper'),
            request = null,
            disable = true
        ;

        // tohle je odchyceni onload eventu, chtelo by to resit trochu lip...
        if (target == '#document') {
            if ($chk_stejne.prop('checked')) {
                $do_wrapper.collapse('hide');
                $do_udaje_select.prop('disabled', true);
            } else {
                $do_wrapper.collapse('show');
                $do_udaje_select.prop('disabled', false);
            }

            $do_udaje_select.selectpicker('refresh');

            if ($fa_udaje_select.val() != '#') {
                $.each($('input[name^="FakturacniUdaje"]'), function(i, input) {
                    $(input).attr('disabled', disable);
                });
            }

            if ($do_udaje_select.val() != '#') {
                $.each($('input[name^="DodaciUdaje"], textarea[name^="DodaciUdaje"]'), function(i, input) {
                    $(input).attr('disabled', disable);
                });
            }
        }
        // a tohle je odchyceni eventu na selectu
        else {
            var name = event.target.getAttribute('name');
            // vetev pro fakturacni udaje
            if (name == 'Objednavka[fakturacni_udaje_pk]') {
                // pokud neco vyberu, tak chci udaje zobrazit v polich
                if ($(this).val() != '#') {
                    // nejprve musim nacist data do jednotlivych poli
                    request = $.ajax({
                        url: 'ajax-nacti-udaje',
                        method: 'POST',
                        data: {typ: 'fakturacni', pk: $(this).val()}
                    });

                    request.done(function(response) {
                        if (response.success != undefined) {
                            $.each($('input[name^="FakturacniUdaje"]'), function(i, input) {

                                var name = $(input).attr('name').match('FakturacniUdaje\\[(\\w+)\\]')[1];
                                var value = response.data[name];

                                $(input).val(value).attr('disabled', true);
                            });
                        } else {

                        }
                    });

                }
                // pokud chci pridat nove, tak pole vycistim a enabluju
                else {
                    $.each($('input[name^="FakturacniUdaje"]'), function(i, input) {
                        $(input).val('').attr('disabled', false);
                    });
                }
            }
            // vetev pro dodaci udaje
            else if (name == 'Objednavka[dodaci_udaje_pk]') {
                // pokud neco vyberu, tak chci udaje zobrazit v polich
                if ($(this).val() != '#') {
                    // nejprve musim nacist data do jednotlivych poli
                    request = $.ajax({
                        url: 'ajax-nacti-udaje',
                        method: 'POST',
                        data: {typ: 'dodaci', pk: $(this).val()}
                    });

                    request.done(function(response) {
                        if (response.success != undefined) {
                            $.each($('input[name^="DodaciUdaje"], textarea[name^="DodaciUdaje"]'), function(i, input) {

                                var name = $(input).attr('name').match('DodaciUdaje\\[(\\w+)\\]')[1];
                                var value = response.data[name];

                                $(input).val(value).attr('disabled', true);
                            });
                        } else {

                        }
                    });

                }
                // pokud chci pridat nove, tak pole vycistim a enabluju
                else {
                    $.each($('input[name^="DodaciUdaje"], textarea[name^="DodaciUdaje"]'), function(i, input) {
                        $(input).val('').attr('disabled', false);
                    });
                }
            }
            // vetev pro prepinani checkboxu u dodacich udaju
            else if (name == 'Objednavka[udaje_stejne]') {
                if ($chk_stejne.prop('checked')) {
                    $do_wrapper.collapse('hide').on('hidden.bs.collapse', function() {
                        $.each($('input[name^="DodaciUdaje"], textarea[name^="DodaciUdaje"]'), function(i, input) {
                            $('#objednavka-krok-dva-form').yiiActiveForm('updateAttribute', $(input).attr('id'), '');
                        });
                    });

                    $do_udaje_select.prop('disabled', true);
                } else {
                    $do_wrapper.on('show.bs.collapse', function() {
                        $.each($('#dodaci-udaje-form-wrapper').find('.form-group'), function(i, div) {
                            if ($(div).hasClass('has-success')) {
                                $(div).removeClass('has-success');
                            }
                        });
                    }).collapse('show');

                    $do_udaje_select.prop('disabled', false);
                }

                $do_udaje_select.selectpicker('refresh');
            }
        }
    };

    /**
     * Updatuje KosikWidget, nutné volat pokaždé, když se provede něco s košíkem na frontu !!!
     */
    Senovo.refreshKosikWidget = function() {};

    /**
     *
     * @param meta
     * @param key
     */
    Senovo.addItemToBasket = function(meta, key) {
        window.console.log(
            'add',
            meta
        );
    };

    /**
     *
     * @param meta
     * @param key
     */
    Senovo.subtractItemFromBasket = function(meta, key) {
        window.console.log(
            'subtract',
            meta
        );
    };

    Senovo.removeItemFromBasket = function(meta, key) {
        window.console.log(
            'remove',
            meta
        );
    }

}(window.Senovo = window.Senovo || {}, jQuery));

$(function() {
    /**
     * Zobrazi/skryje detail nakupniho kosiku
     * @param show
     */
    var prepniKosikDetail = function(show) {
        var $detail = $('#kosik-detail');

        if (show) {
            $detail.show();
        } else {
            $detail.hide();
        }
    };

    var $kosikDetail = $('#kosik-detail');
    var $kosik = $('#kosik-widget');

    $kosik.mouseenter(function() {
        prepniKosikDetail(true);
    });

    $kosik.mouseleave(function() {
        prepniKosikDetail(false);
    });

    $kosikDetail.mouseenter(function() {
        prepniKosikDetail(true);
    });

    $kosikDetail.mouseleave(function() {
        prepniKosikDetail(false);
    });

    // handler pro dopravu a platbu, prvni krok objednavky
    $(document).on(
        'change',
        '#objednavka-krok-jedna-form input[type="radio"]',
        Senovo.handleObjednavkaKrokJednaForm
    );

    // handler pro zmenu dodacich a fakturacnich udaju
    $(document).on(
        'change',
        '#objednavka-krok-dva-form select, #objednavka-krok-dva-form input[type="checkbox"]',
        Senovo.handleObjednavkaKrokDvaForm
    );

    // handler pro praci s polozkami v kosiku
    /*$(document).on(
        'click',
        '.polozka-kusu .tlacitka a',
        function() {
            var meta = $(this).attr('data-polozka'),
                func = $(this).attr('data-func'),
                key = $(this).closest('tr').attr('data-key');

            // todo delete
            window.console.log(meta, func, key);

            if (func == 'plus') {
                Senovo.addItemToBasket(meta, key);
            } else if (func == 'minus') {
                Senovo.subtractItemFromBasket(meta, key);
            } else if (func == 'remove') {

            } else {
                alert('Neplatná funkce "' + func + '"!');
            }

            return false;
        }
    );*/
});