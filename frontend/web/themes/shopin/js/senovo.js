/**
 * Created by Senik on 24.04.2016.
 */

/**
 * PHP like ucfirst
 * @returns {string}
 */
String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/**
 * PHP like lcfirst
 * @returns {string}
 */
String.prototype.lcfirst = function() {
    return this.charAt(0).toLowerCase() + this.slice(1);
};

// vlastni namespace
(function(Senovo, $, undefined) {
    /**
     * handler pro dopravu a platbu, prvni krok objednavky
     * @param event
     */
    Senovo.handleObjednavkaKrokJednaForm = function(event) {
        event.preventDefault();

        var request,
            $target = event.target,
            input_name = $target.getAttribute('name');

        if (input_name == 'DopravaForm[zpusob_dopravy_pk]') {
            request = $.ajax({
                method: 'POST',
                url:    'ajax-vrat-platby',
                data:   {
                    doprava: $(this).val(),
                    _csrf: $('input[name="_csrf"]').val()
                }
            });

            request.done(function(response) {
                if (typeof response == 'object') {
                    $('#objednavka-moznosti-platby').html(response.platby);
                    $('#objednavka-cena-dopravy').html(response.doprava);
                    $('#objednavka-cena-celkem').html(response.celkem);
                } else {
                    window.console.log(response);
                }
            });
        } else if (input_name == 'DopravaForm[zpusob_platby_pk]') {
            var doprava = $('input[name="DopravaForm[zpusob_dopravy_pk]"]:checked').val();

            request = $.ajax({
                method: 'POST',
                url:    'ajax-nastav-dopravu',
                data:   {
                    doprava: doprava,
                    platba: $(this).val(),
                    _csrf: $('input[name="_csrf"]').val()
                }
            });

            request.done(function(response) {
                if (typeof response == 'object') {
                    if (response.error != undefined) {
                        window.console.log(response.error);
                    } else {
                        $('#objednavka-cena-dopravy').html(response.doprava);
                        $('#objednavka-cena-celkem').html(response.celkem);
                    }
                } else {
                    window.console.log(response);
                }
            });
        }
    };

    /**
     * Prepinani zobrazeni ruznych prvku objednavkoveho formulare v kroku 2
     */
    Senovo.handleObjednavkaKrokDvaForm = function(event) {
        // heh, tohle bude chtit neco lepsiho... mozna pres target u selectu, jinak to bude onload
        var target = $(event.target)[0].nodeName,
            $chk_stejne = $('#objednavka-udaje_stejne'),
            $do_wrapper = $('#dodaci-udaje-form-wrapper'),
            request = null
            ;

        // tohle je odchyceni onload eventu, chtelo by to resit trochu lip...
        if (target == '#document') {
            if ($chk_stejne.prop('checked')) {
                $do_wrapper.collapse('hide');
            } else {
                $do_wrapper.collapse('show');
            }
        }
        // a tohle je odchyceni eventu na tlacitkach
        else {
            var name = event.target.getAttribute('name'),
                $link = null;

            if (target == 'A') {
                $link = $($(event.target)[0]);
            } else if (target == 'SPAN') {
                $link = $($(event.target)[0]).closest('a');
            }

            if ($link != null) {
                var udaje_typ = $link.attr('data-typ');
            }

            if (udaje_typ != undefined) {
                var modal = '#objednavka-' + udaje_typ + '-modal',
                    udaje_pk = $link.attr('data-udaje'),
                    form_name = udaje_typ.ucfirst() + 'Udaje',
                    $modal = $(modal);

                // editace udaju
                if (udaje_pk != undefined) {
                    // nejprve musim nacist data do jednotlivych poli
                    request = $.ajax({
                        url: 'ajax-nacti-udaje',
                        method: 'POST',
                        data: {
                            typ: udaje_typ,
                            pk: udaje_pk,
                            _csrf: $('input[name="_csrf"]').val()
                        }
                    });

                    request.done(function(response) {
                        if (response.success != undefined) {
                            $.each($('input[name^="'+form_name+'"]', modal), function(i, input) {

                                var name = $(input).attr('name').match(form_name + '\\[(\\w+)\\]')[1];
                                var value = response.data[name];

                                $(input).val(value);
                            });
                        } else {

                        }
                    });
                }
                // pridavani udaju
                else {
                    $.each($('input[name^="'+form_name+'"], textarea[name^="'+form_name+'"]', modal), function(i, input) {
                        $(input).val('').attr('disabled', false);
                    });
                }

                $modal.modal('show');
            } else {
                // vetev pro prepinani checkboxu u dodacich udaju
                if (name == 'Objednavka[udaje_stejne]') {
                    if ($chk_stejne.prop('checked')) {
                        $do_wrapper.collapse('hide').on('hidden.bs.collapse', function() {
                            $.each($('input[name^="DodaciUdaje"], textarea[name^="DodaciUdaje"]'), function(i, input) {
                                $('#objednavka-krok-dva-form').yiiActiveForm('updateAttribute', $(input).attr('id'), '');
                            });
                        });

                        //$do_udaje_select.prop('disabled', true);
                    } else {
                        $do_wrapper.on('show.bs.collapse', function() {
                            $.each($('#dodaci-udaje-form-wrapper').find('.form-group'), function(i, div) {
                                if ($(div).hasClass('has-success')) {
                                    $(div).removeClass('has-success');
                                }
                            });
                        }).collapse('show');
                    }
                }
            }
        }
    };

    /**
     * Updatuje KosikWidget, nutné volat pokaždé, když se provede něco s košíkem na frontu !!!
     */
    Senovo.refreshKosikWidget = function() {};

    /**
     *
     * @param meta
     * @param key
     */
    Senovo.addItemToBasket = function(meta, key) {
        window.console.log(
            'add',
            meta
        );
    };

    /**
     *
     * @param meta
     * @param key
     */
    Senovo.subtractItemFromBasket = function(meta, key) {
        window.console.log(
            'subtract',
            meta
        );
    };

    Senovo.removeItemFromBasket = function(meta, key) {
        window.console.log(
            'remove',
            meta
        );
    };

    Senovo.handleUdajeForm = function(typ) {

    };

    Senovo.showModal = function(id) {
        $(id).modal('show');
    }

}(window.Senovo = window.Senovo || {}, jQuery));

// document.ready
$(function() {
    /**
     * Zobrazi/skryje detail nakupniho kosiku
     * @param show
     */
    var prepniKosikDetail = function(show) {
        var $detail = $('#kosik-detail');

        if (show) {
            $detail.show();
        } else {
            $detail.hide();
        }
    };

    var $kosikDetail = $('#kosik-detail');
    var $kosik = $('#kosik-widget');

    $kosik.mouseenter(function() {
        prepniKosikDetail(true);
    });

    $kosik.mouseleave(function() {
        prepniKosikDetail(false);
    });

    $kosikDetail.mouseenter(function() {
        prepniKosikDetail(true);
    });

    $kosikDetail.mouseleave(function() {
        prepniKosikDetail(false);
    });

    // handler pro dopravu a platbu, prvni krok objednavky
    $(document).on(
        'change',
        '#objednavka-krok-jedna-form input[type="radio"]',
        Senovo.handleObjednavkaKrokJednaForm
    );

    // handler pro zmenu dodacich a fakturacnich udaju
    $(document).on(
        'change',
        '#objednavka-krok-dva-form input[type="checkbox"]', // #objednavka-krok-dva-form select
        Senovo.handleObjednavkaKrokDvaForm
    );

    // handler pro pridani a editaci udaju
    $(document).on(
        'click',
        '#objednavka-krok-dva-form a[data-target$="modal"]',
        Senovo.handleObjednavkaKrokDvaForm
    );

    // handler pro praci s polozkami v kosiku
    /*$(document).on(
     'click',
     '.polozka-kusu .tlacitka a',
     function() {
     var meta = $(this).attr('data-polozka'),
     func = $(this).attr('data-func'),
     key = $(this).closest('tr').attr('data-key');

     // todo delete
     window.console.log(meta, func, key);

     if (func == 'plus') {
     Senovo.addItemToBasket(meta, key);
     } else if (func == 'minus') {
     Senovo.subtractItemFromBasket(meta, key);
     } else if (func == 'remove') {

     } else {
     alert('Neplatná funkce "' + func + '"!');
     }

     return false;
     }
     );*/

    $('#objednavka-fakturacni-form', '#objednavka-fakturacni-modal').on('beforeSubmit', function() {
        var form = $(this),
            $modal = $('#objednavka-fakturacni-modal'),
            request = $.ajax({
            url: 'uloz-fakturacni',
            method: 'POST',
            data: $(this).serialize()
        });

        request.done(function(response) {
            if (response.success != undefined) {

                // todo tohle by bylo dobre si vracet uz tim ajaxem, ze
                var html =  '<div class="radio-wrapper">' +
                                '<div class="form-group field-objednavka-fakturacni_udaje_pk required">' +
                                    '<div class="radio">' +
                                        '<label for="objednavka-fakturacni_udaje_pk-'+response.data.pk+'">' +
                                            '<input type="radio" id="objednavka-fakturacni_udaje_pk-'+response.data.pk+'" name="Objednavka[fakturacni_udaje_pk]" value="'+response.data.pk+'">' +
                                            response.data.label +
                                        '</label> '+
                                        '<p class="help-block help-block-error"></p>'+
                                    '</div>' +
                                '</div>' +
                                '<a class="icon-right" href="#" title="Upravit fakturační údaje" data-udaje="'+response.data.pk+'" data-target="#objednavka-fakturacni-modal" data-typ="fakturacni">' +
                                    '<span class="glyphicon glyphicon-pencil"></span>' +
                                '</a>' +
                            '</div>';

                var closestWrapper = $('#objednavka-fakturacni_udaje_pk-' + response.data.pk).closest('div.radio-wrapper');
                if (closestWrapper.length == 0) {
                    $('#fakturacni-udaje-form-wrapper').prepend(html);
                } else {
                    closestWrapper.replaceWith(html);
                }

                $modal.modal('hide');
            } else if (response.errors != undefined) {
                $('#objednavka-fakturacni-form').yiiActiveForm('updateMessages', response.errors, false);
            }
        });
    }).on('submit', function() {
        return false;
    });

    $('#objednavka-dodaci-form', '#objednavka-dodaci-modal').on('beforeSubmit', function() {
        var form = $(this),
            $modal = $('#objednavka-dodaci-modal'),
            request = $.ajax({
                url: 'uloz-dodaci',
                method: 'POST',
                data: $(this).serialize()
            });

        request.done(function(response) {
            if (response.success != undefined) {

                // todo tohle by bylo dobre si vracet uz tim ajaxem, ze
                var html =  '<div class="radio-wrapper">' +
                                '<div class="form-group field-objednavka-dodaci_udaje_pk required">' +
                                    '<div class="radio">' +
                                        '<label for="objednavka-dodaci_udaje_pk-'+response.data.pk+'">' +
                                            '<input type="radio" id="objednavka-dodaci_udaje_pk-'+response.data.pk+'" name="Objednavka[dodaci_udaje_pk]" value="'+response.data.pk+'">' +
                                            response.data.label +
                                        '</label> '+
                                        '<p class="help-block help-block-error"></p>'+
                                    '</div>' +
                                '</div>' +
                                '<a class="icon-right" href="#" title="Upravit dodací údaje" data-udaje="'+response.data.pk+'" data-target="#objednavka-dodaci-modal" data-typ="dodaci">' +
                                    '<span class="glyphicon glyphicon-pencil"></span>' +
                                '</a>' +
                            '</div>';

                var closestWrapper = $('#objednavka-dodaci_udaje_pk-' + response.data.pk).closest('div.radio-wrapper');
                if (closestWrapper.length == 0) {
                    $('#dodaci-udaje-form-wrapper').prepend(html);
                } else {
                    closestWrapper.replaceWith(html);
                }

                $modal.modal('hide');
            } else if (response.errors != undefined) {
                $('#objednavka-dodaci-form').yiiActiveForm('updateMessages', response.errors, false);
            }
        });
    }).on('submit', function() {
        return false;
    });
});
