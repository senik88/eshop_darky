<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\widgets\Breadcrumbs;

$code = 500;
if ($exception instanceof \yii\web\HttpException) {
    $code = $exception->statusCode;
}

$this->title = $name;
$this->params['breadcrumbs'][] = $code;
?>

<div class="banner-top">
    <div class="container">
        <h1><?= $code ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<div class="container">
    <div class="four">
        <h3><?= $code ?></h3>

        <p>
            The above error occurred while the Web server was processing your request.
        </p>
        <p>
            Please contact us if you think this is a server error. Thank you.
        </p>

        <a href="<?= \yii\helpers\Url::to(['/site/index']) ?>" class="hvr-skew-backward">Back To Home</a>
    </div>
</div>