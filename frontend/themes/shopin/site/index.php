<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.04.2016
 * Time: 17:08
 *
 * @var $this View
 */
use yii\web\View;

$theme = $this->theme;
$this->title = Yii::$app->name;

?>

<!--banner-->
<!--<div class="banner">
    <div class="container">
        <section class="rw-wrapper">
            <h1 class="rw-sentence">
                <span>Fashion &amp; Beauty</span>
                <div class="rw-words rw-words-1">
                    <span>Beautiful Designs</span>
                    <span>Sed ut perspiciatis</span>
                    <span>Totam rem aperiam</span>
                    <span>Nemo enim ipsam</span>
                    <span>Temporibus autem</span>
                    <span>intelligent systems</span>
                </div>
                <div class="rw-words rw-words-2">
                    <span>We denounce with right</span>
                    <span>But in certain circum</span>
                    <span>Sed ut perspiciatis unde</span>
                    <span>There are many variation</span>
                    <span>The generated Lorem Ipsum</span>
                    <span>Excepteur sint occaecat</span>
                </div>
            </h1>
        </section>
    </div>
</div>-->

<!--content-->
<div class="content">
    <div class="container">
        <!--products-->
        <div class="content-mid">
            <h3>Top produkty</h3>
            <label class="line"></label>
            <div class="mid-popular">
                <?php
                echo \yii\widgets\ListView::widget([
                    'id' => 'katalog-polozky',
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('@app/modules/eshop/views/katalog/_polozka', [
                            'model' => $model,
                            'wrapper' => 'col-md-3'
                        ]);
                    },
                    'dataProvider' => (new \backend\modules\eshop\models\Polozka())->vratTopProdukty(),
                    'layout' => "{items}",
                ]);
                ?>
                <div class="clearfix"></div>
            </div>
        </div>
        <!--//products-->
    </div>

    <div class="container">
        <div class="content-top">
            <div class="col-md-6 col-md">
                <div class="col-1">
                    <a href="single.html" class="b-link-stroke b-animate-go  thickbox">
                        <img src="<?= $theme->baseUrl ?>/images/pi.jpg" class="img-responsive" alt=""/><div class="b-wrapper1 long-img"><p class="b-animate b-from-right    b-delay03 ">Lorem ipsum</p><label class="b-animate b-from-right    b-delay03 "></label><h3 class="b-animate b-from-left    b-delay03 ">Trendy</h3></div></a>

                    <!---<a href="single.html"><img src="images/pi.jpg" class="img-responsive" alt=""></a>-->
                </div>
                <div class="col-2">
                    <span>Hot Deal</span>
                    <h2><a href="single.html">Luxurious &amp; Trendy</a></h2>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years</p>
                    <a href="single.html" class="buy-now">Buy Now</a>
                </div>
            </div>
            <div class="col-md-6 col-md1">
                <div class="col-3">
                    <a href="single.html"><img src="<?= $theme->baseUrl ?>/images/pi1.jpg" class="img-responsive" alt="">
                        <div class="col-pic">
                            <p>Lorem Ipsum</p>
                            <label></label>
                            <h5>For Men</h5>
                        </div></a>
                </div>
                <div class="col-3">
                    <a href="single.html"><img src="<?= $theme->baseUrl ?>/images/pi2.jpg" class="img-responsive" alt="">
                        <div class="col-pic">
                            <p>Lorem Ipsum</p>
                            <label></label>
                            <h5>For Kids</h5>
                        </div></a>
                </div>
                <div class="col-3">
                    <a href="single.html"><img src="<?= $theme->baseUrl ?>/images/pi3.jpg" class="img-responsive" alt="">
                        <div class="col-pic">
                            <p>Lorem Ipsum</p>
                            <label></label>
                            <h5>For Women</h5>
                        </div></a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>

</div>
<!--//content-->