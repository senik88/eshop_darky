<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.04.2016
 * Time: 17:15
 *
 * @var $this View
 */
use backend\modules\eshop\models\Kategorie;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\View;

$theme = $this->theme;
?>
<!-- brand -->
<div class="container">
    <div class="brand">
        <?php
        $vybrani = \backend\modules\eshop\models\Vyrobce::vratVybrane()->getModels();

        foreach ($vybrani as $vyrobce) {
            echo Html::tag(
                'div',
                Html::a(
                    Html::img(
                        Url::to(['/files/image', 'hash' => $vyrobce['hash']]),
                        [
                            'alt' => $vyrobce['zdroj'],
                            'class' => 'img-responsive',
                            'title' => $vyrobce['nazev']
                        ]
                    ),
                    ['/eshop/katalog/vyrobce', 'id' => $vyrobce['vyrobce_id']]
                ),
                [
                    'class' => 'col-md-3 brand-grid'
                ]
            );
        }
        ?>
        <div class="clearfix"></div>
    </div>
</div>
<!-- /brand -->

<div class="footer">
    <div class="footer-middle">
        <div class="container">
            <div class="col-md-3 footer-middle-in">
                <a href="<?= Url::to(['/site/index']) ?>"><img src="<?= $theme->baseUrl ?>/images/log.png" alt=""></a>
                <p>Suspendisse sed accumsan risus. Curabitur rhoncus, elit vel tincidunt elementum, nunc urna tristique nisi, in interdum libero magna tristique ante. adipiscing varius. Vestibulum dolor lorem.</p>
            </div>

            <div class="col-md-3 footer-middle-in">
                <h6>Informace</h6>
                <ul class=" in">
                    <li><a href="<?= Url::to(['/site/about']) ?>">Obchodní podmínky</a></li>
                    <li><a href="<?= Url::to(['/site/contact']) ?>">Kontakt</a></li>
                    <!--<li><a href="#">Returns</a></li>
                    <li><a href="contact.html">Site Map</a></li>-->
                </ul>
                <ul class="in in1">
                    <li><a href="<?= Url::to(['/uzivatel/profil/objednavky']) ?>">Moje objednávky</a></li>
                    <li><a href="<?= Url::to(['/uzivatel/profil/wishlist']) ?>">Seznam přání</a></li>
                    <li><a href="<?= Url::to(['/uzivatel/default/prihlaseni']) ?>">Přihlášení</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3 footer-middle-in">
                <h6>Kategorie</h6>
                <ul class="tag-in">
                    <?php
                    /** @var Kategorie $mKategorie */
                    foreach (Kategorie::findAll(['menu_zobrazit' => true]) as $mKategorie) {
                        echo Html::beginTag('li');
                        {
                            echo Html::a($mKategorie->nazev, ['/eshop/katalog/index', 'id' => $mKategorie->kategorie_id]);
                        }
                        echo Html::endTag('li');
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-3 footer-middle-in">
                <h6>Newsletter</h6>
                <span>Přihlášení k odběru novinek</span>
                <form>
                    <input type="text" value="Zadejte svůj e-mail" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Zadejte svůj e-mail';}">
                    <input type="submit" value="Přihlásit k odběru">
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <ul class="footer-bottom-top">
                <li><a href="#"><img src="<?= $theme->baseUrl ?>/images/f1.png" class="img-responsive" alt=""></a></li>
                <li><a href="#"><img src="<?= $theme->baseUrl ?>/images/f2.png" class="img-responsive" alt=""></a></li>
                <li><a href="#"><img src="<?= $theme->baseUrl ?>/images/f3.png" class="img-responsive" alt=""></a></li>
            </ul>
            <p class="footer-class">&copy; <?= date('Y') ?> Shopin. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> | Modified by <a href="http://senovo.cz">Senovo.cz</a></p>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>