<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\ShopinAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\widgets\Alert;

ShopinAsset::register($this);
$this->registerJs("
    $('select').selectpicker();
    //$('.fancybox').fancybox();
    $('a.picture').Chocolat();
", View::POS_END);

$theme = $this->theme;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="V-Garden, nářadí, zahrada" />
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <!-- header -->
    <div class="header">
        <div class="container">
            <div class="head">
                <div class=" logo">
                    <?= Html::a(
                        Html::img($theme->baseUrl . '/images/logo.png'),
                        ['/site/index']
                    ) ?>
                </div>
            </div>
        </div>

        <div class="header-top">
            <div class="container">
                <div class="col-sm-7 col-md-offset-2  header-login">
                    <?= $this->render('_header') ?>
                </div>

                <div class="col-sm-3 header-social">
                    <ul>
                        <li><a href="#"><i></i></a></li>
                        <li><a href="#"><i class="ic1"></i></a></li>
                        <li><a href="#"><i class="ic2"></i></a></li>
                        <li><a href="#"><i class="ic3"></i></a></li>
                        <li><a href="#"><i class="ic4"></i></a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>

        <div class="container">
            <div class="head-top">
                <div class="col-sm-8 col-md-offset-2 h_menu4">
                     <?= $this->render('_navbar', ['theme' => $theme]) ?>
                </div>
                <div class="col-sm-2 search-right">
                    <ul class="heart">
                        <li>
                            <a href="<?= Url::to(['/uzivatel/profil/wishlist']) ?>">
                                <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li>
                            <a class="play-icon popup-with-zoom-anim" href="#small-dialog">
                                <i class="glyphicon glyphicon-search"> </i>
                            </a>
                        </li>
                    </ul>
                    <div class="cart box_1">
                        <?= \frontend\modules\eshop\components\KosikWidget::widget() ?>
                    </div>
                    <div class="clearfix"></div>

                    <!---//pop-up-box---->
                    <div id="small-dialog" class="mfp-hide">
                        <div class="search-top">
                            <div class="login-search">
                                <input type="submit" value="">
                                <input type="text" value="Hledat" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Hledat';}">
                            </div>
                            <p>Shopin</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- /header -->

    <!-- content -->
    <div class="container">
    <?= Alert::widget() ?>
    </div>

    <?= $content ?>
    <!-- /content -->

    <!-- footer -->
    <?= $this->render('_footer'); ?>
    <!-- /footer -->

    <!-- other shit -->
    <?php $this->endBody() ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= $theme->baseUrl ?>/js/simpleCart.min.js"> </script>
    <!-- slide -->

    <!--light-box-files -->
    <script src="<?= $theme->baseUrl ?>/js/jquery.chocolat.js"></script>
    <script src="<?= $theme->baseUrl ?>/js/jquery.magnific-popup.js" type="text/javascript"></script>

    <link rel="stylesheet" href="<?= $theme->baseUrl ?>/css/chocolat.css" type="text/css" media="screen" charset="utf-8" />
    <!--light-box-files -->
    <script type="application/x-javascript">
        addEventListener("load", function() {
            var hideURLbar = function() {
                window.scrollTo(0,1);
            };

            setTimeout(hideURLbar, 0); }, false);
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('a.picture').Chocolat();

            $('.starbox').each(function() {
                var starbox = $(this);
                starbox.starbox({
                    average: starbox.attr('data-start-value'),
                    changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                    ghosting: starbox.hasClass('ghosting'),
                    autoUpdateAverage: starbox.hasClass('autoupdate'),
                    buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                    stars: starbox.attr('data-star-count') || 5
                }).bind('starbox-value-changed', function(event, value) {
                    if (starbox.hasClass('random')) {
                        var val = Math.random();
                        starbox.next().text(' '+val);
                        return val;
                    }
                })
            });

            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });
        });
    </script>
</body>
</html>
<?php $this->endPage() ?>