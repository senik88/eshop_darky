<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.10.2015
 * Time: 20:57
 */

use frontend\modules\eshop\components\KosikWidget;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$menuItems = [
    ['label' => 'Domů', 'url' => ['/site/index'], 'icon' => 'home'],
    ['label' => 'Obchodní podmínky', 'url' => ['/site/about'], 'icon' => 'book'],
    ['label' => 'Kontakt', 'url' => ['/site/contact'], 'icon' => 'phone-alt'],
];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Registrace', 'url' => ['/uzivatel/default/registrace'], 'icon' => 'cog'];
    $menuItems[] = ['label' => 'Přihlášení', 'url' => ['/uzivatel/default/prihlaseni'], 'icon' => 'play-circle'];
} else {
    $menuItems[] = [
        'label' => 'Můj účet',
        'url' => ['/uzivatel/profil/index'],
        'icon' => 'user'
    ];
    $menuItems[] = [
        'label' => 'Odhlásit',
        'url' => ['/site/logout'],
        'icon' => 'off',
        'linkOptions' => ['data-method' => 'post']
    ];
}

?>


    <ul>
    <?php
    foreach ($menuItems as $item) {
        echo Html::tag(
            'li',
            (isset($item['icon']) ? Html::icon($item['icon']) . ' ' : '' ) . Html::a(
                $item['label'],
                $item['url']
            )
        );
    }
    ?>
    </ul>

<!--<div class="row">
    <div class="col-md-3 text-left">
        logo
    </div>
    <div class="col-md-6 text-center">
        < ?php
        $fraze = isset($this->context->fraze) ? $fraze = $this->context->fraze : null;

        $model = new \frontend\models\VyhledavaniForm();
        $form = ActiveForm::begin([
            'action' => ['/site/hledat'],
            'method' => 'GET',
            'id' => 'search-form'
        ]);

        ?>

        <div class="input-wrapper">
            <div class="field-vyhledavaniform-fraze">
                <input type="text" id="vyhledavaniform-fraze" class="form-control" name="fraze" value="< ?= $fraze ?>">

                <span class="input-addon">
                    <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>

        < ?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-3 text-right">
        < ?= KosikWidget::widget() ?>
    </div>
</div>-->