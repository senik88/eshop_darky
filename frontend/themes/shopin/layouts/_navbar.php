<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.10.2015
 * Time: 20:58
 *
 * @var $theme Theme
 */

use backend\modules\eshop\models\Kategorie;
use frontend\themes\shopin\components\MenuRenderer;
use yii\base\Theme;
use yii\bootstrap\Html;

$renderer = new MenuRenderer();
?>

<nav class="navbar nav_bottom" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header nav_2">
        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
            <?php
            echo $renderer->renderMenu();
            ?>
        </ul>
    </div>
</nav>