<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:31
 *
 * @var $this View
 * @var $content string
 */
use frontend\modules\uzivatel\models\Dashboard;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <!--banner-->
    <div class="banner-top">
        <div class="container">
            <h1><?= isset($this->params['short_title']) ? $this->params['short_title'] : 'Můj profil' ?></h1>
            <em></em>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>

    <div class="container banner-padding">
        <div class="row">
            <div class="col-md-3">
                <div id="profil-menu">
                <ul>
                    <li><h4><a href="<?= Url::to(['/uzivatel/profil/index']) ?>">Můj přehled</a></h4></li>
                    <li><h4>Moje nákupy</h4>
                        <ul>
                            <li><a href="<?= Url::to(['/uzivatel/profil/objednavky']) ?>">Objednávky (<?= Dashboard::vratPocetObjednavek() ?>)</a></li>
                            <li><a href="<?= Url::to(['/uzivatel/profil/faktury']) ?>">Faktury (X)</a></li>
                            <li><a href="<?= Url::to(['/uzivatel/profil/reklamace']) ?>">Reklamace (X)</a></li>
                            <li><a href="<?= Url::to(['/uzivatel/profil/hlidaci-pes']) ?>">Hlídací pes</a></li>
                            <li><a href="<?= Url::to(['/uzivatel/profil/wishlist']) ?>">Seznam přání</a></li>
                        </ul>
                    </li>
                    <li><h4>Moje nastavení</h4>
                        <ul>
                            <li><a href="<?= Url::to(['/uzivatel/profil/registracni-udaje']) ?>">Registrační údaje</a></li>
                            <li><a href="<?= Url::to(['/uzivatel/profil/dodaci-udaje']) ?>">Dodací údaje</a></li>
                        </ul>
                    </li>
                    <li><h4>Můj obsah</h4>
                        <ul>
                            <li><a href="<?= Url::to('#') ?>">Hodnocení produktů</a></li>
                            <li><a href="<?= Url::to('#') ?>">Obrázky a videa</a></li>
                        </ul>
                    </li>
                </ul>
                </div>
            </div>

            <div class="col-md-9">
                <div id="profil-content">
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>