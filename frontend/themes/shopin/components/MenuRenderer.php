<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.06.2016
 * Time: 19:19
 */

namespace frontend\themes\shopin\components;

use backend\modules\eshop\models\Kategorie;
use frontend\components\MenuRenderer as Renderer;
use yii\bootstrap\Html;

/**
 * Class MenuRenderer
 * @package frontend\themes\shopin\components
 */
class MenuRenderer extends Renderer
{

    /**
     * @return string
     */
    public function renderMenu()
    {
        $menuItems = $this->getMenuItems();

        $menu = '';

        foreach ($menuItems as $item)
        {
            $classes = [];

            if (!empty($item['items'])) {
                $classes = array_merge($classes, [
                    'dropdown', 'mega-dropdown'
                ]);
            }

            echo Html::beginTag('li', ['class' => implode(' ', $classes)]);
            {
                $label = $item['label'];
                $url = $item['url'];
                $options = [
                    'class' => 'color'
                ];

                if (!empty($item['items'])) {
                    $label .= '<span class="caret"></span>';
                    $url = '#';
                    $options = array_merge($options, [
                        'data-toggle' => 'dropdown'
                    ]);
                }

                echo Html::a($label, $url, $options);

                if (!empty($item['items'])) {
                    echo Html::beginTag('div', ['class' => 'dropdown-menu']);
                    {
                        echo Html::beginTag('div', ['class' => 'menu-top']);
                        {
                            $maxCols = 4;
                            $maxRows = 4;
                            $maxCount = count($item['items']);

                            if ($maxCols * $maxRows < $maxCount) {
                                $maxRows = (int) ceil($maxCount / $maxCols);
                            }

                            $actCount = 0;
                            $actCols = 0;
                            $actRows = 0;

                            $items2 = [];
                            for ($i = 0; $i < $maxCols; $i++) {
                                for ($j = 0; $j < $maxRows; $j++) {
                                    if ($actCount == $maxCount) {
                                        break 2;
                                    }

                                    $items2[$i][$j] = $item['items'][$actCount];
                                    $actCount++;
                                }
                            }

                            // nastylovat natvrdo - max-width 850px a col1 pocitat sirku podle poctu rows
                            foreach ($items2 as $column) {
                                echo Html::beginTag('div', ['class' => 'col1']);
                                {
                                    echo Html::beginTag('div', ['class' => 'h_nav']);
                                    {
                                        echo Html::beginTag('ul');
                                        {
                                            foreach ($column as $row) {
                                                echo Html::tag(
                                                    'li',
                                                    Html::a($row['label'], $row['url'])
                                                );
                                            }
                                        }
                                        echo Html::endTag('ul');
                                    }
                                    echo Html::endTag('div');
                                }
                                echo Html::endTag('div');
                            }

                            /*echo Html::beginTag('div', ['class' => 'col1']);
                            {
                                echo Html::img(
                                    "{$theme->baseUrl}/images/me.png",
                                    [
                                        'class' => 'img-responsive',
                                        'alt' => 'category image'
                                    ]
                                );
                            }
                            echo Html::endTag('div');*/
                            echo Html::tag('div', null, ['class' => 'clearfix']);
                        }
                        echo Html::endTag('div');
                    }
                    echo Html::endTag('div');
                }
            }
            echo Html::endTag('li');
        }

        return $menu;
    }

    /**
     * @return array
     */
    protected function getMenuItems()
    {
        $menuItems = [];
        $aKategorie = Kategorie::findAll(['menu_zobrazit' => true]);
        foreach ($aKategorie as $kategorie) {

            $aSubKategorie = Kategorie::findAll(['nadrazena' => $kategorie->kategorie_pk]);

            $items = [];
            foreach ($aSubKategorie as $subkategorie) {
                $items[] = [
                    'label' => $subkategorie->nazev,
                    'url' => ['/eshop/katalog/index', 'id' => $subkategorie['kategorie_id']],
                ];
            }

            $menuItems[] = [
                'label' => $kategorie['nazev'],
                'url' => ['/eshop/katalog/index', 'id' => $kategorie['kategorie_id']],
                'items' => $items
            ];
        }

        return $menuItems;
    }

}