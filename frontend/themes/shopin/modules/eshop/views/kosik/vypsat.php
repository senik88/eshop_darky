<?php

/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 22.10.2015
 * Time: 17:38
 *
 * @var $this View
 * @var $mKosik Kosik
 */

use frontend\modules\eshop\models\Kosik;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$title = 'Výpis košíku';
$this->title = Yii::$app->name . ' | ' . $title;

$this->params['breadcrumbs'][] = $title;
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $title ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<div class="container banner-padding">
<?php
$form = ActiveForm::begin([]);

$dataprovider = $mKosik->nacti();
$data = $dataprovider->getModels();


if (!empty($data)) {

    echo '<div class="row">';

    echo GridView::widget([
        'id' => 'kosik-grid',
        'layout' => "{items}",
        'tableOptions' => ['class' => 'table'],
        'dataProvider' => $dataprovider,
        'showFooter' => true,
        'columns' => [
            'nahled' => [
                'value' => function ($model) {
                    return Html::a(
                        Html::img($model['nahled']),
                        ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                    );
                },
                'format' => 'raw'
            ],
            'nazev' => [
                'label' => 'Položka',
                'value' => function ($model) {
                    return Html::a(
                        sprintf('%s %s', $model['titulek'], $model['subtitulek']),
                        ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                    );
                },
                'format' => 'raw'
            ],
            'kusu' => [
                'label' => 'Kusů',
                'format' => 'raw',
                'value' => function ($model) {
                    // '<span class="polozka-kusu">%s ks<span class="tlacitka">%s&nbsp;%s</span></span>'
                    return sprintf('<span class="polozka-kusu">%s&nbsp;%s ks&nbsp;%s</span></span>',
                        Html::a(
                            Html::icon('minus-sign'),
                            ['/eshop/kosik/odebrat', 'id' => $model['polozka_id']],
                            ['data-polozka' => $model['polozka_id'], 'data-func' => 'minus']
                        ),
                        $model['mnozstvi'],
                        Html::a(
                            Html::icon('plus-sign'),
                            ['/eshop/kosik/pridat', 'id' => $model['polozka_id']],
                            ['data-polozka' => $model['polozka_id'], 'data-func' => 'plus']
                        )
                    );
                }
            ],
            'skladem' => [
                'label' => 'Skladem',
                'value' => function ($model) {
                    return sprintf('%s ks', $model['skladem']);
                }
            ],
            'cena_ks' => [
                'attribute' => 'cena_aktualni',
                'label' => 'Cena / ks',
                'format' => 'currency',
                'footer' => 'Celková cena:'
            ],
            'cena_celkem' => [
                'attribute' => 'cena_celkem',
                'label' => 'Cena celkem',
                'value' => function ($model) {
                    return Yii::$app->getFormatter()->asCurrency($model['cena_celkem'], 'CZK');
                },
                'footer' => Yii::$app->getFormatter()->asCurrency($mKosik->vratCastkuObjednavky(), 'CZK')
            ],
            'akce' => [
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(
                        Html::icon('remove'),
                        array('/eshop/kosik/odebrat', 'id' => $model['polozka_id'], 'kusu' => 'vse'),
                        [
                            'title' => 'Odebrat z košíku',
                        ]
                    );
                },
            ]
        ]
    ]);

    echo '</div>';

    echo '<div class="row">';
    {
        echo '<div class="col-md-12">';
        {
            echo '<div class="text-center">';
            {
                echo Html::submitButton('Pokračovat' . ' ' . Html::icon('chevron-right'), ['class' => 'btn btn-success', 'name' => 'akce', 'value' => 'objednat']);
            }
            echo '</div>';
        }
        echo '</div>';
    }
    echo '</div>';

} else {

    echo '<div class="row">';
    {
        echo '<div class="col-md-12">';
        {
            echo '<div id="prazdny-kosik" class="text-center">';
            {
                echo '<h2>V košíku nic není</h2>';

                echo Html::img(Yii::$app->request->baseUrl . '/images/essential/empty_cart.png', ['title' => 'Prázdný košík', 'alt' => 'Prázdný košík']);

                echo Html::submitButton('Zpět do obchodu', ['class' => 'btn btn-primary', 'name' => 'akce', 'value' => 'domu']);
            }
            echo '</div>';
        }
        echo '</div>';
    }
    echo '</div>';
}

ActiveForm::end();
?>
</div>

<?php
$this->registerJs("
    $(document).ready(function(c) {
        $('.close1').on('click', function(c){
            $('.cart-header').fadeOut('slow', function(c){
                $('.cart-header').remove();
            });
        });
    });
    $(document).ready(function(c) {
        $('.close2').on('click', function(c){
            $('.cart-header1').fadeOut('slow', function(c){
                $('.cart-header1').remove();
            });
        });
    });
    $(document).ready(function(c) {
        $('.close3').on('click', function(c){
            $('.cart-header2').fadeOut('slow', function(c){
                $('.cart-header2').remove();
            });
        });
    });
");