<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.05.2016
 * Time: 19:22
 *
 * @var Objednavka $mObjednavka
 * @var DodaciUdaje $mDodaciUdaje
 * @var FakturacniUdaje $mFakturacniUdaje
 * @var ZpusobPlatby $mZpusobPlatby
 * @var ZpusobDopravy $mZpusobDopravy
 */

use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\Objednavka;
use common\models\ZpusobDopravy;
use common\models\ZpusobPlatby;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

$title = 'Objednávka č. ' . $mObjednavka->cislo;
$this->title = Yii::$app->name . ' | ' . $title;

$this->params['breadcrumbs'][] = ['label' => 'Můj účet', 'url' => ['/uzivatel/profil/index']];
$this->params['breadcrumbs'][] = ['label' => 'Objednávky', 'url' => ['/uzivatel/profil/objednavky']];
$this->params['breadcrumbs'][] = $title;
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $title ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<div class="container banner-padding" id="objednavka-detail">
    <div class="row">
        <div class="col-md-12">
            <p><?= Yii::$app->formatter->asDate($mObjednavka->cas_zmeny) ?> | <?= Html::a("Tisk objednávky", "#") ?> | <?= Html::a("Stáhnout PDF", "#") ?></p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <?php
            echo GridView::widget([
                'id' => 'objednavka-detail-grid',
                'layout' => "{items}",
                'tableOptions' => ['class' => 'table'],
                'dataProvider' => $mObjednavka->nactiPolozky(),
                'columns' => [
                    'nahled' => [
                        'value' => function ($model) {
                            return Html::a(
                                Html::img($model['nahled']),
                                ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                            );
                        },
                        'format' => 'raw'
                    ],
                    'nazev' => [
                        'label' => 'Položka',
                        'value' => function ($model) {
                            return Html::a(
                                sprintf('%s %s', $model['titulek'], $model['subtitulek']),
                                ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                            );
                        },
                        'format' => 'raw'
                    ],
                    'kusu' => [
                        'label' => 'Kusů',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model['stav'] == Objednavka::STAV_ZALOZENA) {
                                $template = '<span class="polozka-kusu">{ks} ks<span class="tlacitka">{plus}&nbsp;{minus}</span></span>';
                            } else {
                                $template = '<span class="polozka-kusu">{ks} ks</span>';
                            }

                            return strtr($template, [
                                '{ks}' => $model['mnozstvi'],
                                '{plus}' => Html::a(Html::icon('plus-sign'), ['/eshop/kosik/pridat', 'id' => $model['polozka_id']], ['data-polozka' => $model['polozka_id'], 'data-func' => 'plus']),
                                '{minus}' => Html::a(Html::icon('minus-sign'), ['/eshop/kosik/odebrat', 'id' => $model['polozka_id']], ['data-polozka' => $model['polozka_id'], 'data-func' => 'minus'])
                            ]);
                        }
                    ],
                    /*'skladem' => [
                        'label' => 'Skladem',
                        'value' => function ($model) {
                            return sprintf('%s ks', $model['skladem']);
                        }
                    ],*/
                    'cena_ks' => [
                        'attribute' => 'cena_aktualni',
                        'label' => 'Cena / ks',
                        'format' => 'currency',
                    ],
                    'cena_celkem' => [
                        'attribute' => 'cena_celkem',
                        'label' => 'Cena celkem',
                        'value' => function ($model) {
                            return Yii::$app->getFormatter()->asCurrency($model['cena_celkem'], 'CZK');
                        },
                    ],
                    /*'akce' => [
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(
                                Html::icon('remove'),
                                array('/eshop/kosik/odebrat', 'id' => $model['polozka_id'], 'kusu' => 'vse'),
                                [
                                    'title' => 'Odebrat z košíku',
                                ]
                            );
                        },
                    ]*/
                ]
            ]);
            ?>

            <span>
                Celkem k úhradě
            </span>
        </div>
        <div class="col-md-3">
            <div id="objednavka-detail-doruceni" class="box-wrapper">
                <h4>Doručení a platba</h4>
                <div class="box-obsah">
                    <p><?= $mZpusobDopravy->nazev ?></p>
                    <p><?= $mZpusobPlatby->nazev ?></p>
                </div>
            </div>

            <div id="objednavka-detail-udaje" class="box-wrapper">
                <h4>Doručovací údaje</h4>
                <div class="box-obsah">
                    <?= $mDodaciUdaje->vypisBlokove() ?>
                </div>

                <h4>Fakturační údaje</h4>
                <div class="box-obsah">
                    <?= $mFakturacniUdaje->vypisBlokove() ?>
                </div>
            </div>

            <div id="objednavka-detail-doplnujici" class="box-wrapper">
                <h4>Dodatečné informace</h4>
                <div class="box-obsah">
                    <p><?= \yii\bootstrap\Html::textInput('oznaceni_uzivatel', $mObjednavka->oznaceni_uzivatel) ?></p>
                    <p><?= $mObjednavka->poznamka ?></p>
                </div>
            </div>
        </div>
    </div>
</div>