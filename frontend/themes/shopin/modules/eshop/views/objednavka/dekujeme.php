<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.11.2015
 * Time: 22:27
 */

use yii\widgets\Breadcrumbs;

$title = 'Děkujeme';
$this->title = Yii::$app->name . ' | ' . $title;

$this->params['breadcrumbs'][] = 'Objednávka';
$this->params['breadcrumbs'][] = $title;
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $title ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <h1>Děkujeme za objednávku</h1>
                <p>Na Váš email bylo zasláno potvrzení objednávky.</p>
                <p>Stav objednávky můžete sledovat v přehledu objednávek.</p>
            </div>
        </div>
    </div>
</div>