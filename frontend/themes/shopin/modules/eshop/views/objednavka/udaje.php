<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.11.2015
 * Time: 20:39
 *
 * @var $this View
 * @var $krok integer
 * @var $mKosik Kosik
 * @var $mObjednavka Objednavka
 * @var $mFakturacniUdaje FakturacniUdaje
 * @var $mDodaciUdaje DodaciUdaje
 *
 * @var $aFakturacniUdaje array pole fakturacnich udaju aktualne prihlaseneho uzivatele
 * @var $aDodaciUdaje array pole dodacich udaju aktualne prihlaseneho uzivatele
 */

use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\Objednavka;
use frontend\modules\eshop\models\Kosik;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$title = 'Dodací údaje';
$this->title = Yii::$app->name . ' | ' . $title;

$this->params['breadcrumbs'][] = 'Objednávka';
$this->params['breadcrumbs'][] = $title;
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $title ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<div class="container banner-padding" id="objednavka-krok-dva-form-wrapper">
    <?php
    $form = ActiveForm::begin([
        'method' => 'POST',
        'id' => 'objednavka-krok-dva-form',
        'enableClientValidation' => false
    ]);
    ?>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h3>Fakturační údaje</h3>
                <div id="fakturacni-udaje-form-wrapper">
                    <?php foreach ($aFakturacniUdaje as $pk => $faUdaje) { ?>
                        <div class="radio-wrapper">
                            <?= $form->field($mObjednavka, 'fakturacni_udaje_pk')->radio([
                                'value' => $pk,
                                'id' => Html::getInputId($mObjednavka, 'fakturacni_udaje_pk') . '-' . $pk,
                                'uncheck' => null
                            ])->label($faUdaje) ?>

                            <?= Html::a(Html::icon('pencil'), '#', [
                                'data-udaje' => $pk,
                                'data-target' => '#objednavka-fakturacni-modal',
                                'data-typ' => 'fakturacni',
                                'title' => 'Upravit fakturační údaje',
                                'class' => 'icon-right'
                            ]) ?>
                        </div>
                    <?php } ?>

                    <div class="radio-wrapper">
                        <div class="form-group field-objednavka-fakturacni_udaje_pk">
                            <div class="radio">
                                <?= Html::a('Přidat fakturační údaje', '#', [
                                    'data-target' => '#objednavka-fakturacni-modal',
                                    'data-typ' => 'fakturacni',
                                ]) ?>
                                <p class="help-block help-block-error"></p>
                            </div>

                            <?= Html::a(Html::icon('plus'), '#', [
                                'data-target' => '#objednavka-fakturacni-modal',
                                'data-typ' => 'fakturacni',
                                'title' => 'Přidat fakturační údaje',
                                'class' => 'icon-right'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="dodaci-stejne-wrapper">
                    <h3>Dodací údaje</h3>
                </div>
                <?= $form->field($mObjednavka, 'udaje_stejne')->checkbox()->label('Stejné jako fakturační') ?>
                <div class="collapse" id="dodaci-udaje-form-wrapper">
                    <?php foreach ($aDodaciUdaje as $pk => $doUdaje) { ?>
                        <div class="radio-wrapper">
                            <?= $form->field($mObjednavka, 'dodaci_udaje_pk')->radio([
                                'value' => $pk,
                                'id' => Html::getInputId($mObjednavka, 'dodaci_udaje_pk') . '-' . $pk,
                                'uncheck' => null
                            ])->label($doUdaje) ?>

                            <?= Html::a(Html::icon('pencil'), '#', [
                                'data-udaje' => $pk,
                                'data-target' => '#objednavka-dodaci-modal',
                                'data-typ' => 'dodaci',
                                'title' => 'Upravit dodací údaje',
                                'class' => 'icon-right'
                            ]) ?>
                        </div>
                    <?php } ?>

                    <div class="radio-wrapper">
                        <div class="form-group field-objednavka-dodaci_udaje_pk">
                            <div class="radio">
                                <?= Html::a('Přidat dodací údaje', '#', [
                                    'data-target' => '#objednavka-dodaci-modal',
                                    'data-typ' => 'dodaci',
                                    'title' => 'Přidat fakturační údaje'
                                ]) ?>
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>

                        <?= Html::a(Html::icon('plus'), '#', [
                            'data-target' => '#objednavka-dodaci-modal',
                            'data-typ' => 'dodaci',
                            'title' => 'Přidat dodací údaje',
                            'class' => 'icon-right'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <?= Html::submitButton(Html::icon('chevron-left') . ' ' . 'Krok zpět', ['class' => 'btn btn-warning', 'name' => 'akce', 'value' => 'zpet']) ?>
                    <?= Html::submitButton('Další krok' . ' ' . Html::icon('chevron-right'), ['class' => 'btn btn-success', 'name' => 'akce', 'value' => 'potvrdit']) ?>
                </div>
            </div>
        </div>

    <?php ActiveForm::end() ?>

    <?php Modal::begin([
        'id' => 'objednavka-fakturacni-modal',
        'header' => 'Fakturační údaje'
    ]); ?>

        <?php $fakturacniForm = ActiveForm::begin([
            'id' => 'objednavka-fakturacni-form',
            'action' => ['/eshop/objednavka/uloz-fakturacni'],
            'method' => 'POST',
            //'enableClientValidation' => false
        ]); ?>

            <div class="form-fields">
                <?= $fakturacniForm->field($mFakturacniUdaje, 'fakturacni_udaje_pk')->hiddenInput()->label(false) ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'obchodni_jmeno') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'ic') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'dic') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'jmeno') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'prijmeni') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'ulice') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'mesto') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'psc') ?>
                <?= $fakturacniForm->field($mFakturacniUdaje, 'zeme') ?>
            </div>

            <div class="form-actions">
                <?= Html::submitButton("Uložit", ['class' => 'btn btn-success', 'name' => 'akce', 'value' => 'fakturacni']) ?>
            </div>

        <?php ActiveForm::end() ?>

    <?php Modal::end(); ?>

    <?php Modal::begin([
        'id' => 'objednavka-dodaci-modal',
        'header' => 'Dodací údaje'
    ]); ?>

        <?php $dodaciForm = ActiveForm::begin([
            'id' => 'objednavka-dodaci-form',
            'action' => ['/eshop/objednavka/uloz-dodaci'],
            'method' => 'POST',
            'enableClientValidation' => false
        ]); ?>

            <div class="form-fields">
                <?= $dodaciForm->field($mDodaciUdaje, 'dodaci_udaje_pk')->hiddenInput()->label(false) ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'jmeno') ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'prijmeni') ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'ulice') ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'mesto') ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'psc') ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'zeme') ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'telefon') ?>
                <?= $dodaciForm->field($mDodaciUdaje, 'poznamka')->textarea(['placeholder' => 'Firma, poschodí a podobně...']) ?>
            </div>

            <div class="form-actions">
                <?= Html::submitButton("Uložit", ['class' => 'btn btn-success', 'name' => 'akce', 'value' => 'dodaci']) ?>
            </div>

        <?php ActiveForm::end() ?>

    <?php Modal::end(); ?>

</div>

<?php
$this->registerJs("$(window).load(Senovo.handleObjednavkaKrokDvaForm);");