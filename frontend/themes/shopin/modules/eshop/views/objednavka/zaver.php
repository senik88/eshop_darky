<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.11.2015
 * Time: 20:39
 *
 * @var $this View
 * @var $krok integer
 * @var $mKosik Kosik
 * @var $mObjednavka Objednavka
 * @var $mDodaciUdaje DodaciUdaje
 * @var $mFakturacniUdaje FakturacniUdaje
 */

use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use common\models\Objednavka;
use frontend\modules\eshop\models\Kosik;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$title = 'Shrnutí';
$this->title = Yii::$app->name . ' | ' . $title;

$this->params['breadcrumbs'][] = 'Objednávka';
$this->params['breadcrumbs'][] = $title;
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $title ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<div class="container banner-padding" id="objednavka-dokonceni-form-wrapper">

<?php
$form = ActiveForm::begin([
    'method' => 'POST',
    'id' => 'objednavka-dokonceni-form'
]);
?>

<div class="row">
    <div class="col-md-12">
        <?php
        echo GridView::widget([
            //'id' => 'kosik-grid',
            'layout' => "{items}",
            'tableOptions' => ['class' => 'table'],
            'dataProvider' => $mKosik->nacti($krok = 3),
            'showFooter' => true,
            'columns' => [
                'nazev' => [
                    'value' => function($model) {
                        $text = sprintf('%s %s', $model['titulek'], $model['subtitulek']);
                        if ($model['typ'] == 1) {
                            return Html::a(
                                $text,
                                ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                            );
                        }

                        return $text;
                    },
                    'format' => 'raw'
                ],
                'kusu' => [
                    'label' => 'Kusů',
                    'format' => 'raw',
                    'value' => function($model) {
                        return sprintf('<span class="polozka-kusu">%s ks</span>',
                            $model['mnozstvi']
                        );
                    }
                ],
                'skladem' => [
                    'label' => 'Skladem',
                    'value' => function($model) {
                        return sprintf('%s ks', $model['skladem']);
                    }
                ],
                'cena_ks' => [
                    'attribute' => 'cena_aktualni',
                    'label' => 'Cena / ks',
                    'format' => 'currency',
                    'footer' => 'Celková cena:'
                ],
                'cena_celkem' => [
                    'attribute' => 'cena_celkem',
                    'label' => 'Cena celkem',
                    'value' => function ($model) {
                        return Yii::$app->getFormatter()->asCurrency($model['cena_celkem'],'CZK');
                    },
                    'footer' => Yii::$app->getFormatter()->asCurrency($mKosik->vratCastkuObjednavky(), 'CZK')
                ]
            ]
        ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-1"></div>

    <div class="col-md-6" id="objednavka-poznamka">
        <?= $form->field($mObjednavka, 'oznaceni_uzivatel')->label('Vaše označení obj.') ?>
        <?= $form->field($mObjednavka, 'poznamka')->textarea()->label('Vaše poznámka pro nás') ?>
    </div>

    <div class="col-md-1"></div>

    <!--<div class="col-md-3">
        <ul>
            <li>Celková cena: <?/*= Yii::$app->formatter->asCurrency($mObjednavka->cena_celkem, 'CZK') */?></li>
        </ul>
    </div>-->

    <div class="col-md-1"></div>
</div>

<div class="row">
    <div class="col-md-1"></div>

    <div class="col-md-5">
        <h3>Fakturační údaje</h3>
        <?= \yii\widgets\DetailView::widget([
            'model' => $mFakturacniUdaje,
            'attributes' => [
                'obchodniJmeno' => [
                    'attribute' => 'obchodni_jmeno',
                    'visible' => $mFakturacniUdaje->obchodni_jmeno != null
                ],
                'ic' => [
                    'attribute' => 'ic',
                    'visible' => $mFakturacniUdaje->ic != null
                ],
                'dic' => [
                    'attribute' => 'dic',
                    'visible' => $mFakturacniUdaje->dic != null
                ],
                'jmeno' => [
                    'value' => sprintf('%s %s', $mFakturacniUdaje->prijmeni, $mFakturacniUdaje->jmeno),
                    'visible' => $mFakturacniUdaje->jmeno != null && $mFakturacniUdaje->prijmeni != null,
                    'label' => $mFakturacniUdaje->getAttributeLabel('jmeno')
                ],
                'ulice',
                'mesto' => [
                    'value' => sprintf('%s, %s', $mFakturacniUdaje->psc, $mFakturacniUdaje->mesto),
                    'label' => $mFakturacniUdaje->getAttributeLabel('mesto')
                ],
                'zeme'
            ],
            'options' => [
                'class' => 'table table-striped table-condensed detail-view'
            ]
        ]) ?>
    </div>

    <div class="col-md-5">
        <h3>Dodací údaje</h3>
        <?= \yii\widgets\DetailView::widget([
            'model' => $mDodaciUdaje,
            'attributes' => [
                'jmeno' => [
                    'value' => sprintf('%s %s', $mDodaciUdaje->prijmeni, $mDodaciUdaje->jmeno),
                    'visible' => $mDodaciUdaje->jmeno != null && $mDodaciUdaje->prijmeni != null,
                    'label' => $mDodaciUdaje->getAttributeLabel('jmeno')
                ],
                'ulice',
                'mesto' => [
                    'value' => sprintf('%s, %s', $mDodaciUdaje->psc, $mDodaciUdaje->mesto),
                    'label' => $mDodaciUdaje->getAttributeLabel('mesto')
                ],
                'zeme',
                'poznamka' => [
                    'attribute' => 'poznamka',
                    'visible' => $mDodaciUdaje->poznamka != null
                ],
                'telefon' => [
                    'attribute' => 'telefon',
                    'visible' => $mDodaciUdaje->telefon != null
                ]
            ],
            'options' => [
                'class' => 'table table-striped table-condensed detail-view'
            ]
        ]) ?>
    </div>

    <div class="col-md-1"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="text-center">
            <?= $form->field($mObjednavka, 'souhlas')->checkbox()->label('Souhlasím s obchodními podmínkami') ?>
            <?= Html::submitButton(Html::icon('chevron-left') . ' ' . 'Krok zpět', ['class' => 'btn btn-warning', 'name' => 'akce', 'value' => 'zpet']) ?>
            <?= Html::submitButton('Potvrdit objednávku' . ' ' . Html::icon('ok'), ['class' => 'btn btn-success', 'name' => 'akce', 'value' => 'potvrdit']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>

</div>