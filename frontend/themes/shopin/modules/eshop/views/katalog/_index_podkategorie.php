<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 19.06.2016
 * Time: 19:55
 *
 * @var $model Kategorie
 */

use backend\modules\eshop\models\Kategorie;
?>

<div class="col-md-3 podkategorie-wrapper">
    <div class="mid-pop">
        <div class="text-center">
            <?= \yii\bootstrap\Html::img($model->vratRandomObrazekProduktu()) ?>

            <?= \yii\bootstrap\Html::a($model->nazev, ['/eshop/katalog/index', 'id' => $model->kategorie_id], ['class' => 'btn btn-info']) ?>
        </div>
    </div>
</div>