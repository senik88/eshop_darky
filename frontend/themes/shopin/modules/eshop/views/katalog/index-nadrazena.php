<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.04.2016
 * Time: 17:25
 *
 * @var $this View
 * @var $mKategorie Kategorie
 * @var $mPolozka Polozka
 * @var $mNadrazena Kategorie
 * @var $aPodkategorie Kategorie[]
 */

use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$theme = $this->theme;

$this->title = Yii::$app->name . ' | ' . $mKategorie->nazev;
if ($mNadrazena != null) {
    $this->params['breadcrumbs'][] = [
        'label' => $mNadrazena->nazev,
        'url' => ['/eshop/katalog/index', 'id' => $mNadrazena->kategorie_id]
    ];
}
$this->params['breadcrumbs'][] = $mKategorie->nazev;

/*
 * todo
 * výpis okének se seznamem podkategorií
 * v každém okénku bude random obrázek jednoho z produktů podkategorie
 */

$dataProvider = new \yii\data\ArrayDataProvider([
    'allModels' => $aPodkategorie
])
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $mKategorie->nazev ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<!--content-->
<div class="product banner-padding">
    <div class="container">
        <?php
//        foreach ($aPodkategorie as $mPodkategorie) {
//            echo \yii\bootstrap\Html::img($mPodkategorie->vratRandomObrazekProduktu());
//        }

        echo \yii\widgets\ListView::widget([
            'id' => 'podkategorie-kategorie',
            'dataProvider' => $dataProvider,
            'itemView' => '_index_podkategorie',
            'layout' => "{items}",
            'emptyText' => "<div class='col-md-12'><h4>Kategorie zatím neobsahuje žádné podkategorie.</h4></div>"
        ]);
        ?>
        <div class="col-md-12">

        </div>
        <div class="clearfix"></div>
    </div>
</div>
