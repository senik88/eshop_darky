<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.02.2016
 * Time: 16:24
 *
 * @var View $this
 * @var Vyrobce $mVyrobce
 * @var Logo $mLogo
 * @var array $aKategorie
 */

use backend\modules\eshop\models\Vyrobce;
use common\models\Logo;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$this->title = Yii::$app->name . ' | ' . $mVyrobce->nazev;
$this->params['breadcrumbs'][] = $mVyrobce->nazev;

?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $mVyrobce->nazev ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<!--content-->
<div class="product">
    <div class="container">
        <div class="col-md-12">
            <div class="pull-left">
                <h1><?= $mVyrobce->nazev ?></h1>
            </div>
            <div class="pull-right">
                <div class="vyrobce-logo">
                <?php
                if ($mLogo != null) {
                    echo Html::img(
                        Url::to(['/files/image', 'hash' => $mLogo->hash]),
                        [
                            'alt' => $mLogo->zdroj
                        ]
                    );
                }
                ?>
                </div>
            </div>
        </div>

        <?php
        foreach ($aKategorie as $kategorie) {
            $json_data = $kategorie['data'];
            $produkty = json_decode($json_data, true);

            echo $this->render('_kategorie_vyrobce', [
                'kategorie' => $kategorie,
                'aProdukty' => $produkty
            ]);
        }
        ?>
    </div>
</div>