<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.04.2016
 * Time: 17:25
 *
 * @var $this View
 * @var $mKategorie Kategorie
 * @var $mPolozka Polozka
 * @var $mNadrazena Kategorie
 */

use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$theme = $this->theme;

$this->title = Yii::$app->name . ' | ' . $mKategorie->nazev;
if ($mNadrazena != null) {
    $this->params['breadcrumbs'][] = [
        'label' => $mNadrazena->nazev,
        'url' => ['/eshop/katalog/index', 'id' => $mNadrazena->kategorie_id]
    ];
}
$this->params['breadcrumbs'][] = $mKategorie->nazev;

$dataprovider = $mPolozka->vypis();
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $mKategorie->nazev ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<!--content-->
<div class="product">
    <div class="container">
        <div class="col-md-9">
            <div class="mid-popular">
                <?php
                echo \yii\widgets\ListView::widget([
                    'id' => 'katalog-polozky',
                    'itemView' => '_polozka',
                    'dataProvider' => $dataprovider,
                    'layout' => "{pager}\n{items}",
                    'emptyText' => "<div class='col-md-12'><h4>Kategorie zatím neobsahuje žádné zboží.</h4></div>"
                ]);
                ?>
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- Filtry -->
        <div class="col-md-3 product-bottom">
            <section  class="sky-form">
                <h4 class="cate">Discounts</h4>
                <div class="row row1 scroll-pane">
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Upto - 10% (20)</label>
                    </div>
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>40% - 50% (5)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>30% - 20% (7)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>10% - 5% (2)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Other(50)</label>
                    </div>
                </div>
            </section>

            <section  class="sky-form">
                <h4 class="cate">Type</h4>
                <div class="row row1 scroll-pane">
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Sofa Cum Beds (30)</label>
                    </div>
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Bags  (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Caps & Hats (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Jackets & Coats   (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Jeans  (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Shirts   (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Sunglasses  (30)</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Swimwear  (30)</label>
                    </div>
                </div>
            </section>

            <section  class="sky-form">
                <h4 class="cate">Brand</h4>
                <div class="row row1 scroll-pane">
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Roadstar</label>
                    </div>
                    <div class="col col-4">
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Levis</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Persol</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Nike</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Edwin</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>New Balance</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Paul Smith</label>
                        <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Ray-Ban</label>
                    </div>
                </div>
            </section>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
