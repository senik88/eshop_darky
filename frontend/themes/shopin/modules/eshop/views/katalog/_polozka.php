<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.10.2015
 * Time: 20:27
 *
 * jstarbox https://github.com/sabberworm/jStarbox
 *
 * @var $this View
 * @var $model array
 * @var $wrapper string
 */
use yii\bootstrap\Html;
use yii\web\View;

$theme = $this->theme;

// u top produktu to zobrazuju na cele siri, tak chci mit vic polozek vedle sebe
if (!isset($wrapper)) {
    $wrapper = 'col-md-4';
}
?>

<div class="<?= $wrapper ?> item-grid1 simpleCart_shelfItem" data-id="<?= $model['polozka_id'] ?>">
    <div class="mid-pop">
        <div class="pro-img">
            <?= Html::img($model['nahled'], ['class' => 'center-block']) ?>
            <div class="zoom-icon ">
                <a href="<?= $model['obrazek'] ?>" rel="title" class="picture b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
                <?= Html::a(
                    '<i class="glyphicon glyphicon-menu-right icon" title="' . $model['titulek'] . '"></i>',
                    ['/eshop/katalog/detail', 'id' => $model['polozka_id']]
                ) ?>
            </div>
        </div>
        <div class="mid-1">
            <div class="item-action">
                <div class="item-description">
                    <span><?= $model['kategorie'] ?></span>
                    <h6><?= Html::a(
                            $model['titulek'],
                            ['/eshop/katalog/detail', 'id' => $model['polozka_id']],
                            ['title' => $model['titulek']]
                        ) ?></h6>
                </div>
                <div class="img item_add">
                    <?= Html::a(
                        Html::img($theme->baseUrl . "/images/ca.png"),
                        ['/eshop/kosik/pridat', 'id' => $model['polozka_id']],
                        [
                            'data-polozka' => $model['polozka_id'],
                            //'class' => 'btn btn-primary btn-xs'
                        ]
                    ) ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="mid-2">
                <div class="item-available">
                    <p>
                        <span>Skladem <?= $model['skladem'] ?> ks</span>
                    </p>
                </div>
                <div class="clearfix"></div>

                <div class="item-price">
                    <p>
                        <label><?= Yii::$app->formatter->asCurrency($model['cena_doporucena'], 'CZK') ?></label>
                        <em class="item_price"><?= Yii::$app->formatter->asCurrency($model['cena_aktualni'], 'CZK') ?></em>
                    </p>
                </div>

                <br />

                <div class="item-stars block item-stars">
                    <?php
                    if ($model['pocet_hodnoceni'] > 0) {
                        echo Html::tag(
                            'div',
                            '',
                            [
                                'class' => 'starbox small ghosting',
                                'data-start-value' => $model['hodnoceni_float']
                            ]
                        );
                    } else {
                        echo Html::tag(
                            'span',
                            'Zatím nikdo nehodnotil',
                            [
                                'class' => 'no-stars'
                            ]
                        );
                    }
                    ?>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>