<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.02.2016
 * Time: 17:51
 *
 * @var array $kategorie
 * @var array $aProdukty
 */
use yii\bootstrap\Html;

?>

<div class="row">
    <div class="col-md-12">
        <table class="vypis-kategorie-vyrobce">
            <tr>
                <td>
                    <span><?= $kategorie['kategorie'] ?></span>
                </td>
                <?php
                for ($i = 0; $i < 4; $i++) {
                    echo '<td>';
                    if (array_key_exists($i, $aProdukty)) {
                        echo '<div class="polozka-obrazek">';
                        {
                            echo Html::a(
                                Html::img($aProdukty[$i]['nahled'], ['class' => 'img-thumbnail center-block']),
                                ['/eshop/katalog/detail', 'id' => $aProdukty[$i]['polozka_id']]
                            );
                        }
                        echo '</div>';

                        echo '<div class="polozka-popis">';
                        {
                            echo '<span>';
                            {
                                echo $aProdukty[$i]['titulek'];
                            }
                            echo '</span>';
                            echo '<br>';
                            echo '<span>';
                            {
                                echo Yii::$app->formatter->asCurrency($aProdukty[$i]['cena'], 'CZK');
                            }
                            echo '</span>';
                        }
                        echo '</div>';

                        echo '<div class="polozka-akce">';
                        {
                            echo Html::a('Detail', ['/eshop/katalog/detail', 'id' => $aProdukty[$i]['polozka_id']], ['class' => 'btn btn-info btn-xs']);
                        }
                        echo '</div>';
                    }
                    echo '</td>';
                }
                ?>
            </tr>
        </table>
    </div>
</div>