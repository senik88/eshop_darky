<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.10.2015
 * Time: 20:30
 *
 * @var $this View
 * @var $mPolozka Polozka
 * @var $mVyrobce Vyrobce
 * @var $mLogo Logo
 * @var $aParametry ParametrPolozky[]
 */

use backend\modules\eshop\models\Kategorie;
use backend\modules\eshop\models\Polozka;
use backend\modules\eshop\models\Vyrobce;
use common\models\Logo;
use common\models\ParametrPolozky;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$this->title = Yii::$app->name . ' | ' . $mPolozka->titulek;
if ($mPolozka->mKategorie->nadrazena) {
    /** @var Kategorie $mNadrazena */
    $mNadrazena = Kategorie::findOne($mPolozka->mKategorie->nadrazena);

    $this->params['breadcrumbs'][] = [
        'url' => ['/eshop/katalog/index', 'id' => $mNadrazena->kategorie_id],
        'label' => $mNadrazena->nazev
    ];
}
$this->params['breadcrumbs'][] = [
    'url' => ['/eshop/katalog/index', 'id' => $mPolozka->kategorie_id],
    'label' => $mPolozka->mKategorie->nazev
];
$this->params['breadcrumbs'][] = $mPolozka->titulek;

$theme = $this->theme;
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1><?= $mPolozka->titulek ?></h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<div class="single">
    <div class="container">
        <div class="col-md-12">
            <!-- galerie -->
            <div class="col-md-5 grid">
                <div class="flexslider">
                    <ul class="slides">
                        <?php
                        foreach ($mPolozka->obrazky as $mPolozkaObrazek) {
                            echo Html::beginTag('li', [
                                'data-thumb' => $mPolozkaObrazek->zdroj
                            ]);
                            {
                                echo Html::beginTag('div', [
                                    'class' => 'thumb-image'
                                ]);
                                {
                                    echo Html::img(
                                        $mPolozkaObrazek->zdroj,
                                        [
                                            'data-imagezoom' => "true",
                                            'class' => "img-responsive"
                                        ]
                                    );
                                }
                                echo Html::endTag('div');
                            }
                            echo Html::endTag('li');
                        }
                        ?>

                        <!--<li data-thumb="<?/*= $theme->baseUrl */?>/images/si.jpg">
                            <div class="thumb-image"> <img src="<?/*= $theme->baseUrl */?>/images/si.jpg" data-imagezoom="true" class="img-responsive"> </div>
                        </li>
                        <li data-thumb="<?/*= $theme->baseUrl */?>/images/si1.jpg">
                            <div class="thumb-image"> <img src="<?/*= $theme->baseUrl */?>/images/si1.jpg" data-imagezoom="true" class="img-responsive"> </div>
                        </li>
                        <li data-thumb="<?/*= $theme->baseUrl */?>/images/si2.jpg">
                            <div class="thumb-image"> <img src="<?/*= $theme->baseUrl */?>/images/si2.jpg" data-imagezoom="true" class="img-responsive"> </div>
                        </li>-->
                    </ul>
                </div>
            </div>
            <!-- galerie -->

            <!-- popis a buttony -->
            <div class="col-md-7 single-top-in">
                <div class="span_2_of_a1 simpleCart_shelfItem">
                    <h3><?= $mPolozka->titulek ?></h3>
                    <p class="in-para"><?= $mPolozka->subtitulek ?></p>
                    <div class="price_single">
                        <span class="reducedfrom item_price">
                            <?= Yii::$app->getFormatter()->asCurrency($mPolozka->mEshopCena->cena_aktualni, 'CZK') ?>
                        </span>
                        <!--<a href="#">click for offer</a>-->
                        <div class="clearfix"></div>
                    </div>
                    <h4 class="quick">Krátký popis:</h4>
                    <p class="quick_desc">
                        TBD
                    </p>
                    <div class="wish-list">
                        <ul>
                            <li class="wish">
                                <a href="<?= Url::to(['/eshop/default/prani', 'id' => $mPolozka->polozka_id]) ?>">
                                    <span class="glyphicon glyphicon-check" aria-hidden="true"></span>Do seznamu přání
                                </a>
                            </li>
                            <li class="compare">
                                <a href="<?= Url::to(['/eshop/default/porovnani', 'id' => $mPolozka->polozka_id]) ?>">
                                    <span class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span>Přidat k porovnání
                                </a>
                            </li>
                        </ul>
                    </div>

                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                        'method' => 'GET',
                        'action' => Url::to(['/eshop/kosik/pridat', 'id' => $mPolozka->polozka_id])
                    ]) ?>
                    <div class="quantity">
                        <div class="quantity-select">
                            <div class="entry value-minus">&nbsp;</div>
                            <div class="entry value"><span>1</span><?= Html::input('hidden', 'kusu', 1) ?></div>
                            <div class="entry value-plus active">&nbsp;</div>
                        </div>
                    </div>

                    <?= Html::submitButton(
                        Html::icon("shopping-cart") . " Přidat do košíku",
                        //['/eshop/kosik/pridat', 'id' => $mPolozka->polozka_id],
                        [
                            'data-polozka' => $mPolozka->polozka_id,
                            'class' => 'btn btn-primary add-to'
                        ]
                    ) ?>
                    <?php \yii\bootstrap\ActiveForm::end() ?>
                    <div class="clearfix"> </div>
                </div>

            </div>
            <div class="clearfix"></div>
            <!---->
            <div class="tab-head">
                <nav class="nav-sidebar">
                    <ul class="nav tabs">
                        <li class="active"><a href="#popis" data-toggle="tab">Popis produktu</a></li>
                        <li class=""><a href="#parametry" data-toggle="tab">Technické parametry</a></li>
                        <li class=""><a href="#recenze" data-toggle="tab">Hodnocení zákazníků</a></li>
                    </ul>
                </nav>
                <div class="tab-content one">
                    <div class="tab-pane active text-style" id="popis">
                        <div class="facts">
                            <?= $mPolozka->popis ?>
                        </div>

                    </div>
                    <div class="tab-pane text-style" id="parametry">

                        <div class="facts">
                            <?php
                            if (!empty($aParametry)) {
                                foreach ($aParametry as $skupina) {
                                    echo Html::tag('h4', $skupina['label']);

                                    echo Html::beginTag('ul');
                                    /** @var ParametrPolozky $mParametr */
                                    foreach ($skupina['items'] as $mParametr) {
                                        echo Html::tag('li', sprintf("<label>%s:</label> %s", $mParametr->formatujNazev(), $mParametr->hodnota));
                                    }

                                    echo Html::endTag('ul');
                                }
                            } else {
                                echo Html::tag('p', "Technické parametry jsou v přípravě");
                            }
                            ?>
                        </div>

                    </div>
                    <div class="tab-pane text-style" id="recenze">

                        <div class="facts">
                            <a href="<?= Url::to(['/eshop/katalog/recenze', 'id' => $mPolozka->polozka_id]) ?>">
                                <span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span>Přidat hodnocení
                            </a>
                            <p> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined </p>
                            <ul>
                                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Research</li>
                                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Design and Development</li>
                                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Porting and Optimization</li>
                                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>System integration</li>
                                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Verification, Validation and Testing</li>
                                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Maintenance and Support</li>
                            </ul>
                        </div>

                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <!---->
        </div>
        <!----->

        <div class="clearfix"> </div>
    </div>
</div>

<?php
$this->registerJs("
/*** quantity ***/
$('.value-plus').on('click', function(){
    var divUpd = $(this).parent().find('.value span'),
        input = $(this).parent().find('input[type=hidden]'),
        newVal = parseInt(divUpd.text(), 10)+1;

    divUpd.text(newVal);
    input.val(newVal);
});

$('.value-minus').on('click', function(){
    var divUpd = $(this).parent().find('.value span'),
        input = $(this).parent().find('input[type=hidden]'),
        newVal = parseInt(divUpd.text(), 10)-1;

    if(newVal>=1) {
        divUpd.text(newVal);
        input.val(newVal);
    }
});
/*** /quantity ***/

$(function() {
    var menu_ul = $('.menu-drop > li > ul'),
        menu_a  = $('.menu-drop > li > a');
    menu_ul.hide();
    menu_a.click(function(e) {
        e.preventDefault();
        if(!$(this).hasClass('active')) {
            menu_a.removeClass('active');
            menu_ul.filter(':visible').slideUp('normal');
            $(this).addClass('active').next().stop(true,true).slideDown('normal');
        } else {
            $(this).removeClass('active');
            $(this).next().stop(true,true).slideUp('normal');
        }
    });

});

$(window).load(function() {
    $('.flexslider').flexslider({
        animation: 'slide',
        controlNav: 'thumbnails'
    });
});
");