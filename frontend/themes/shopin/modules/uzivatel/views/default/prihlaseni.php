<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.09.2015
 * Time: 23:12
 * @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model \common\models\LoginForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = Yii::$app->name . ' | ' . 'Přihlášení';
$this->params['breadcrumbs'][] = 'Přihlášení';
?>

<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1>Přihlášení</h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>
<!--login-->
<div class="container">
    <div class="login">
        <?php
        $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'options' => []
        ]); ?>
            <div class="col-md-6 login-do">
                <div class="input-wrapper <?= $model->hasErrors('username') ? 'has-error' : '' ?>">
                    <div class="login-mail">
                        <?= Html::activeTextInput($model, 'username', ['placeholder' => 'Email']) ?>
                        <i  class="glyphicon glyphicon-envelope"></i>
                    </div>
                    <?php
                    if ($model->hasErrors('username')) {
                        echo Html::tag(
                            'p',
                            implode('; ', $model->getErrors('username')),
                            ['class' => 'error']
                        );
                    }
                    ?>
                </div>

                <div class="input-wrapper <?= $model->hasErrors('password') ? 'has-error' : '' ?>">
                    <div class="login-mail">
                        <?= Html::activePasswordInput($model, 'password', ['placeholder' => 'Heslo']) ?>
                        <i class="glyphicon glyphicon-lock"></i>
                    </div>
                    <?php
                    if ($model->hasErrors('password')) {
                        echo Html::tag(
                            'p',
                            implode('; ', $model->getErrors('password')),
                            ['class' => 'error']
                        );
                    }
                    ?>
                </div>

                <a class="news-letter " href="#">
                    <label class="checkbox1"><?= Html::checkbox(Html::getInputName($model, 'rememberMe')) ?><i> </i><?= $model->getAttributeLabel('rememberMe') ?></label>
                </a>

                <div style="color:#999;margin:1em 0">
                    Pokud jste zapomněl(a) heslo, můžete si <?= Html::a('vyžádat nové', ['site/request-password-reset']) ?>.
                </div>

                <label class="hvr-skew-backward">
                    <input type="submit" value="Přihlásit" name="login">
                </label>
            </div>
            <div class="col-md-6 login-right">
                <h3>Účet je zcela zdarma</h3>

                <p>Pellentesque neque leo, dictum sit amet accumsan non, dignissim ac mauris. Mauris rhoncus, lectus tincidunt tempus aliquam, odio
                    libero tincidunt metus, sed euismod elit enim ut mi. Nulla porttitor et dolor sed condimentum. Praesent porttitor lorem dui, in pulvinar enim rhoncus vitae. Curabitur tincidunt, turpis ac lobortis hendrerit, ex elit vestibulum est, at faucibus erat ligula non neque.</p>
                <a href="<?= Url::to(['/uzivatel/default/registrace']) ?>" class=" hvr-skew-backward">Registrovat</a>

            </div>

            <div class="clearfix"> </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>