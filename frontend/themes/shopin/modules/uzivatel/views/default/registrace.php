
<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.09.2015
 * Time: 20:21
 *
 * @var $model RegistraceForm
 */
use frontend\modules\uzivatel\forms\RegistraceForm;
use frontend\modules\uzivatel\UzivatelModule;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = Yii::$app->name . ' | ' . 'Registrace';
$this->params['breadcrumbs'][] = 'Registrace';
?>



<!--banner-->
<div class="banner-top">
    <div class="container">
        <h1>Registrace</h1>
        <em></em>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>
<!--login-->
<div class="container">
    <div class="login">
        <?php
        $form = ActiveForm::begin([
            'layout' => 'horizontal'
        ]);
        ?>
            <div class="col-md-6 login-do">
                <div class="login-mail">
                    <?= Html::activeTextInput($model, 'jmeno', ['placeholder' => 'Jméno']) ?>
                    <i  class="glyphicon glyphicon-user"></i>
                </div>
                <div class="login-mail">
                    <?= Html::activeTextInput($model, 'prijmeni', ['placeholder' => 'Příjmení']) ?>
                    <i  class="glyphicon glyphicon-user"></i>
                </div>
                <div class="login-mail">
                    <?= Html::activeTextInput($model, 'telefon', ['placeholder' => 'Telefon']) ?>
                    <i  class="glyphicon glyphicon-phone"></i>
                </div>
                <div class="login-mail">
                    <?= Html::activeTextInput($model, 'email', ['placeholder' => 'Email']) ?>
                    <i  class="glyphicon glyphicon-envelope"></i>
                </div>
                <div class="login-mail">
                    <?= Html::activePasswordInput($model, 'heslo', ['placeholder' => 'Heslo']) ?>
                    <i class="glyphicon glyphicon-lock"></i>
                </div>
                <div class="login-mail">
                    <?= Html::activePasswordInput($model, 'heslo2', ['placeholder' => 'Kontrola hesla']) ?>
                    <i class="glyphicon glyphicon-lock"></i>
                </div>
                <!--<a class="news-letter " href="#">
                    <label class="checkbox1"><input type="checkbox" name="checkbox" ><i> </i>Forget Password</label>
                </a>-->
                <label class="hvr-skew-backward">
                    <input type="submit" value="Registrovat">
                </label>

            </div>
            <div class="col-md-6 login-right">
                <h3>Účet je zcela zdarma</h3>

                <p>Pellentesque neque leo, dictum sit amet accumsan non, dignissim ac mauris. Mauris rhoncus, lectus tincidunt tempus aliquam, odio
                    libero tincidunt metus, sed euismod elit enim ut mi. Nulla porttitor et dolor sed condimentum. Praesent porttitor lorem dui, in pulvinar enim rhoncus vitae. Curabitur tincidunt, turpis ac lobortis hendrerit, ex elit vestibulum est, at faucibus erat ligula non neque.</p>
                <a href="<?= Url::to(['/uzivatel/default/prihlaseni']) ?>" class="hvr-skew-backward">Přihlášení</a>

            </div>

            <div class="clearfix"> </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>