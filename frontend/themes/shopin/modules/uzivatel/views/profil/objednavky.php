<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:20
 *
 * @var ObjednavkaSearchForm $model
 */
use frontend\modules\eshop\forms\ObjednavkaSearchForm;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;

?>

<div id="objednavky-vyhledavani" class="well">
    <?php
    echo Html::beginForm(['/uzivatel/profil/objednavky'], 'get', [
        'id' => 'objednavky-search-form',
        'class' => 'form-inline'
    ]);
    {
        echo Html::beginTag('div', ['class' => 'form-group']);
        {
            echo Html::label(
                'Vyhledávat můžete v čísle, Vašem označení objednávky nebo názvu položky',
                Html::getInputId($model, 'fraze')
            );

            echo Html::submitButton('Vymazat filtr', ['name' => 'akce', 'value' => 'reset', 'class' => 'btn btn-sm btn-warning']);
            echo Html::submitButton('Hledat', ['name' => 'akce', 'value' => 'hledat', 'class' => 'btn btn-sm btn-primary']);

            echo Html::beginTag('div', ['class' => 'form-inline-wrapper']);
            {
                echo Html::activeInput('text', $model, 'fraze', ['class' => 'form-control']);
            }
            echo Html::endTag('div');
        }
        echo Html::endTag('div');
    }
    echo Html::endForm();
    ?>
</div>

<?=
\yii\grid\GridView::widget([
    'dataProvider' => $model->search(),
    'columns' => [
        'cislo' => [
            'attribute' => 'cislo',
            'label' => 'Číslo',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(
                    $model['cislo'],
                    ['/eshop/objednavka/detail', 'id' => $model['cislo']]
                );
            }
        ],
        'cas_vytvoreni' => [
            'attribute' => 'cas_vytvoreni',
            'label' => 'Datum',
            'value' => function ($model) {
                return Yii::$app->formatter->asDate($model['cas_vytvoreni']);
            }
        ],
        'cena' => [
            'attribute' => 'cena'
        ],
        'stav' => [
            'attribute' => 'stav_objednavky_id',
            'label' => 'Stav',
            'value' => function ($model) {
                return \common\models\Objednavka::itemAlias('stav', $model['stav_objednavky_id']);
            }
        ]
    ]
])
?>