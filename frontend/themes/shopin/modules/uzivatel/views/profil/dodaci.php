<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:20
 *
 * @var View $this
 * @var array $aDodaciUdaje
 * @var DodaciUdaje $mDodaciUdaje
 */
use common\models\DodaciUdaje;
use yii\bootstrap\Html;
use yii\web\View;

?>

<div id="profil-dodaci-udaje">

    <h2>Dodací údaje</h2>

    <p class="napoveda well">
        V této sekci můžete přidávat, upravovat nebo mazat svoje dodací údaje.

        <?= Html::button('Přidat', [
            'data-target' => '#profil-dodaci-modal',
            'data-toggle' => 'modal',
            'data-typ' => 'dodaci',
            'title' => 'Přidat dodací údaje',
            'class' => 'btn btn-sm btn-primary dodaci-pridat'
        ]) ?>
    </p>

    <?php
    if (!empty($aDodaciUdaje)) {
        echo Html::beginTag('ul', ['id' => 'dodaci-udaje-list']);
        {
            foreach ($aDodaciUdaje as $pk => $text) {
                echo Html::tag(
                    'li',
                    Html::tag('span', $text, ['class' => 'dodaci-udaje-text'])
                    . Html::tag(
                        'span',
                        Html::a(Html::icon('pencil'), '#', [
                            'data-udaje' => $pk,
                            'data-target' => '#profil-dodaci-modal',
                            'title' => 'Upravit dodací údaje',
                            'class' => 'icon-right dodaci-upravit'
                        ])
                        . Html::a(Html::icon('remove'), '#', [
                            'data-udaje' => $pk,
                            'title' => 'Smazat dodací údaje',
                            'class' => 'icon-right dodaci-smazat'
                        ]),
                        [
                            'class' => 'dodaci-udaje-akce'
                        ]
                    ),
                    []
                );
            }
        }
        echo Html::endTag('ul');
    }
    ?>
</div>

<?php
\yii\bootstrap\Modal::begin([
    'id' => 'profil-dodaci-modal',
    'header' => 'Dodací údaje'
]);

$form = \yii\bootstrap\ActiveForm::begin([
    'id' => 'profil-dodaci-udaje-form',
    'method' => 'POST'
])
?>

    <div class="form-fields">

        <?= Html::hiddenInput(Html::getInputName($mDodaciUdaje, 'dodaci_udaje_pk')) ?>
        <?= $form->field($mDodaciUdaje, 'jmeno') ?>
        <?= $form->field($mDodaciUdaje, 'prijmeni') ?>
        <?= $form->field($mDodaciUdaje, 'ulice') ?>
        <?= $form->field($mDodaciUdaje, 'mesto') ?>
        <?= $form->field($mDodaciUdaje, 'psc') ?>
        <?= $form->field($mDodaciUdaje, 'zeme')->dropDownList(\common\models\Zeme::$seznam) ?>
        <?= $form->field($mDodaciUdaje, 'telefon') ?>
        <?= $form->field($mDodaciUdaje, 'poznamka')->textarea() ?>

    </div>

    <div class="form-actions">
        <?= Html::submitButton('Uložit', ['name' => 'ulozit', 'class' => 'btn btn-success']) ?>
    </div>

<?php
\yii\bootstrap\ActiveForm::end();

\yii\bootstrap\Modal::end();
?>

<?php
$this->registerJs("
$('.dodaci-upravit').on('click', function() {
    var modal = $(this).attr('data-target')
        request = $.ajax({
            url: 'ajax-dodaci-udaje',
            data: {dodaci_udaje_pk: $(this).attr('data-udaje')}
        });

    request.success(function(result) {
        $('div[class*=\"field-dodaciudaje\"] input, textarea').each(function() {
            var id = $(this).attr('id').match(/dodaciudaje-(\w+)/)[1];

            if (result[id] != undefined) {
                $(this).val(result[id]);
            }
        });
    });

    Senovo.showModal(modal);
});

$('.dodaci-pridat').on('click', function() {
    $('div[class*=\"field-dodaciudaje\"] input, textarea').each(function() {
        $(this).val('');
    });

    Senovo.showModal(modal);
});
");