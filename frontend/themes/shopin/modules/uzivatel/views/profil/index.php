<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.02.2016
 * Time: 8:20
 *
 * @var View $this
 * @var Dashboard $mDashboard
 * @var DodaciUdaje $mDodaciUdaje
 * @var FakturacniUdaje $mFakturacniUdaje
 * @var SqlDataprovider $pObjednavky
 * @var UzivatelFrontend $mUzivatel
 * @var integer $countObjednavky
 */

use common\components\SqlDataProvider;
use common\models\DodaciUdaje;
use common\models\FakturacniUdaje;
use frontend\modules\uzivatel\models\Dashboard;
use frontend\modules\uzivatel\models\UzivatelFrontend;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\web\View;

?>

<!--<div class="row dashboard-sekce">
    <div class="col-md-12">
        <h2>Můj profil</h2>
    </div>
</div>-->

<div class="row dashboard-sekce">
    <div class="col-md-12">
        <div class="header">
            <h3 class="pull-left">Objednávky</h3>
            <span class="pull-right">celkem máte <?= $countObjednavky ?> objednávek (<?= Html::a('zobrazit', ['/uzivatel/profil/objednavky']) ?>)</span>
            <div class="clearfix"></div>
        </div>

        <div class="obsah">
            <?= GridView::widget([
                'dataProvider' => $pObjednavky,
                'layout' => '{items}',
                'tableOptions' => [
                    'class' => 'table'
                ],
                'columns' => [
                    'cislo' => [
                        'attribute' => 'cislo',
                        'label' => 'Číslo',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(
                                $model['cislo'],
                                ['/eshop/objednavka/detail', 'id' => $model['cislo']]
                            );
                        }
                    ],
                    'cas_vytvoreni' => [
                        'attribute' => 'cas_vytvoreni',
                        'header' => 'Datum', // header, protoze nechci sortovat...
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model['cas_vytvoreni']);
                        }
                    ],
                    'cena' => [
                        'attribute' => 'cena',
                        'label' => 'Cena',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asCurrency($model['cena']);
                        }
                    ],
                    'stav' => [
                        'attribute' => 'stav_objednavky_id',
                        'label' => 'Stav',
                        'value' => function ($model) {
                            return \common\models\Objednavka::itemAlias('stav', $model['stav_objednavky_id']);
                        }
                    ],
                ]
            ]) ?>
        </div>
    </div>
</div>

<div class="row dashboard-sekce">
    <div class="col-md-12">
        <div class="header">
            <h3 class="pull-left">Faktury</h3>
            <span class="pull-right">celkem máte X faktur (<?= Html::a('zobrazit', ['/uzivatel/profil/faktury']) ?>)</span>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row dashboard-sekce">
    <div class="col-md-12">
        <div class="header">
            <h3 class="pull-left">Reklamace</h3>
            <span class="pull-right">celkem máte X reklamací (<?= Html::a('zobrazit', ['/uzivatel/profil/reklamace']) ?>)</span>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row dashboard-sekce">
    <div class="col-md-12">
        <h3>Uživatelské údaje</h3>

        <div class="row">
            <div class="col-md-6">
                <h4>Poslední dodací údaje</h4>

                <?= \yii\widgets\DetailView::widget([
                    'model' => $mDodaciUdaje,
                    'attributes' => [
                        'jmeno' => [
                            'attribute' => 'jmeno',
                            'label' => $mDodaciUdaje->getAttributeLabel('jmeno')
                        ],
                        'prijmeni' => [
                            'attribute' => 'prijmeni',
                            'label' => $mDodaciUdaje->getAttributeLabel('prijmeni')
                        ],
                        'ulice' => [
                            'attribute' => 'ulice',
                            'label' => $mDodaciUdaje->getAttributeLabel('ulice')
                        ],
                        'mesto' => [
                            'attribute' => 'mesto',
                            'label' => $mDodaciUdaje->getAttributeLabel('mesto')
                        ],
                        'psc' => [
                            'attribute' => 'psc',
                            'label' => $mDodaciUdaje->getAttributeLabel('psc')
                        ],
                        'zeme' => [
                            'attribute' => 'zeme',
                            'label' => $mDodaciUdaje->getAttributeLabel('zeme')
                        ],
                        'telefon' => [
                            'attribute' => 'telefon',
                            'label' => $mDodaciUdaje->getAttributeLabel('telefon')
                        ],
                        'poznamka' => [
                            'attribute' => 'poznamka',
                            'label' => $mDodaciUdaje->getAttributeLabel('poznamka')
                        ]
                    ]
                ]) ?>

                <h4>Registrační údaje</h4>

                <?= \yii\widgets\DetailView::widget([
                    'model' => $mUzivatel,
                    'attributes' => [
                        'email' => [
                            'attribute' => 'email'
                        ],
                        'uzivatel_pk' => [
                            'attribute' => 'uzivatel_pk'
                        ],
                        'cas_registrace' => [
                            'attribute' => 'cas_registrace',
                            'value' => Yii::$app->formatter->asDatetime($mUzivatel->cas_registrace)
                        ],
                    ]
                ]) ?>
            </div>

            <div class="col-md-6">
                <h4>Poslední fakturační údaje</h4>

                <?= \yii\widgets\DetailView::widget([
                    'model' => $mFakturacniUdaje,
                    'attributes' => [
                        'obchodni_jmeno' => [
                            'attribute' => 'obchodni_jmeno',
                            'label' => $mFakturacniUdaje->getAttributeLabel('obchodni_jmeno'),
                            'visible' => $mFakturacniUdaje->obchodni_jmeno != null
                        ],
                        'ic' => [
                            'attribute' => 'ic',
                            'label' => $mFakturacniUdaje->getAttributeLabel('ic'),
                            'visible' => $mFakturacniUdaje->ic != null
                        ],
                        'dic' => [
                            'attribute' => 'dic',
                            'label' => $mFakturacniUdaje->getAttributeLabel('dic'),
                            'visible' => $mFakturacniUdaje->dic != null
                        ],
                        'jmeno' => [
                            'attribute' => 'jmeno',
                            'label' => $mFakturacniUdaje->getAttributeLabel('jmeno'),
                            'visible' => $mFakturacniUdaje->jmeno != null
                        ],
                        'prijmeni' => [
                            'attribute' => 'prijmeni',
                            'label' => $mFakturacniUdaje->getAttributeLabel('prijmeni'),
                            'visible' => $mFakturacniUdaje->prijmeni != null
                        ],
                        'ulice' => [
                            'attribute' => 'ulice',
                            'label' => $mFakturacniUdaje->getAttributeLabel('ulice'),
                        ],
                        'mesto' => [
                            'attribute' => 'mesto',
                            'label' => $mFakturacniUdaje->getAttributeLabel('mesto'),
                        ],
                        'psc' => [
                            'attribute' => 'psc',
                            'label' => $mFakturacniUdaje->getAttributeLabel('psc'),
                        ],
                        'zeme' => [
                            'attribute' => 'zeme',
                            'label' => $mFakturacniUdaje->getAttributeLabel('zeme'),
                        ]
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>