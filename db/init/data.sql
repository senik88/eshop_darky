INSERT INTO uzivatel (email, rozhrani, stav, heslo, cas_registrace, cas_aktivace, cas_prihlaseni, ip_registrace, ip_prihlaseni, validacni_klic, jmeno, prijmeni) VALUES
  ('superadmin', 'BACK', 'AKTIVNI', '$2y$13$wNJopqS/muXpCWLz16yhd.MGiST5PZbUqhPb/etlV8bkoE0hDdnxO', now(), now(), null, '192.168.1.1'::inet, '192.168.1.1'::inet, md5('superadmin' || now()::text), 'Super', 'Admin'),
  ('admin@eshop.cz', 'BACK', 'AKTIVNI', '$2y$13$wNJopqS/muXpCWLz16yhd.MGiST5PZbUqhPb/etlV8bkoE0hDdnxO', now(), now(), null, '192.168.1.1'::inet, '192.168.1.1'::inet, md5('admin@eshop.cz' || now()::text), 'Eshop', 'Admin'),
  ('lukas.senohrabek@email.cz', 'FRONT', 'AKTIVNI', '$2y$13$wNJopqS/muXpCWLz16yhd.MGiST5PZbUqhPb/etlV8bkoE0hDdnxO', now(), now(), null, '192.168.1.1'::inet, '192.168.1.1'::inet, md5('lukas.senohrabek@email.cz' || now()::text), 'Lukáš', 'Senohrábek'),
  ('knotekl@seznam.cz', 'FRONT', 'AKTIVNI', '$2y$13$wNJopqS/muXpCWLz16yhd.MGiST5PZbUqhPb/etlV8bkoE0hDdnxO', now(), now(), null, '192.168.1.1'::inet, '192.168.1.1'::inet, md5('knotekl@seznam.cz' || now()::text), 'Lukáš', 'Knotek')
;

-- tohle je blbe, musim tam mit na to extra tridu
INSERT INTO polozka_obrazek (zdroj, nahled, popisek, hlavni, nahrano, polozka_pk, uzivatel_pk) VALUES
  (
      '/images/polozky/no-photo.gif'
    , '/images/polozky/no-photo.gif'
    , 'Obrázek není k dispozici'
    , FALSE
    , now()
    , NULL,
    1
  );

INSERT INTO kategorie (kategorie_id, nazev, menu_zobrazit, lze_zobrazit_zbozi, nadrazena) VALUES
    ('nezarazeno', 'Nezařazeno', FALSE, FALSE, NULL);

INSERT INTO vyrobce (vyrobce_id, nazev, web, datum_vlozeni, logo_pk) VALUES
    ('neznamy', 'Neznámý', NULL, now(), NULL);

INSERT INTO dodavatel (nazev, fakturacni_udaje_pk) VALUES
    ('Neznámý', NULL);

INSERT INTO stav_objednavky (stav_objednavky_id, popis, poradi) VALUES
    ('KOSIK', 'Zboží v košíku', 1),
    ('ZALOZENA', 'Založená', 2),
    ('POTVRZENA', 'Potvrzená objednávka', 3);

INSERT INTO skupina_parametru (skupina_id, nazev) VALUES
    ('parametry', 'Parametry'),
    ('technologie', 'Technologie'),
    ('rozmery-vaha', 'Rozměry a váha'),
    ('dalsi-informace', 'Další informace');