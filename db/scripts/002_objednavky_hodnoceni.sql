BEGIN;

-- -- Table hodnoceni_produktu
-- CREATE TABLE "hodnoceni_produktu"(
--     "hodnoceni_produktu_pk" Serial NOT NULL,
--     "procenta" Integer NOT NULL,
--     "popis" Text,
--     "klady" Json,
--     "zapory" Json,
--     "polozka_pk" Integer,
--     "uzivatel_pk" Integer
-- );
--
-- -- Create indexes for table hodnoceni_produktu
-- CREATE INDEX "ix_polozka_hodnoceni_produktu" ON "hodnoceni_produktu" ("polozka_pk");
-- CREATE INDEX "ix_uzivatel_hodnoceni_produktu" ON "hodnoceni_produktu" ("uzivatel_pk");
--
-- -- Add keys for table hodnoceni_produktu
-- ALTER TABLE "hodnoceni_produktu" ADD CONSTRAINT "Key24" PRIMARY KEY ("hodnoceni_produktu_pk");
-- ALTER TABLE "hodnoceni_produktu" ADD CONSTRAINT "produkt_hodnoceni_produktu" FOREIGN KEY ("polozka_pk") REFERENCES "polozka" ("polozka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ALTER TABLE "hodnoceni_produktu" ADD CONSTRAINT "hodnotil_uzivatel_pk" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;
--
-- -- ALter table objednavka
-- ALTER TABLE objednavka ADD COLUMN zpracovava_uzivatel_pk INTEGER;
-- ALTER TABLE "objednavka" ADD CONSTRAINT "uzivatel_zpracovava_objednavku" FOREIGN KEY ("zpracovava_uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- INSERT INTO stav_objednavky (stav_objednavky_id, popis, poradi) VALUES
--     ('ZPRACOVAVA_SE', 'Zpracovává se', 4),
--     ('ODESLANA', 'Odeslaná', 5),
--     ('STORNO', 'Stornovaná', 90);

-- ALTER TABLE zpusob_dopravy ADD COLUMN ma_tracking BOOLEAN;
-- UPDATE zpusob_dopravy SET ma_tracking = FALSE WHERE ma_tracking IS NULL;
-- ALTER TABLE zpusob_dopravy ALTER COLUMN ma_tracking SET NOT NULL;
--
-- ALTER TABLE zpusob_platby ADD COLUMN je_platba_predem BOOLEAN;
-- UPDATE zpusob_platby SET je_platba_predem = FALSE WHERE je_platba_predem IS NULL;
-- ALTER TABLE zpusob_platby ALTER COLUMN je_platba_predem SET NOT NULL;

\i ../procs/f_objednavka.sql