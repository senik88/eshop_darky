BEGIN;

DROP TABLE IF EXISTS filtr_kategorie;
DROP TABLE IF EXISTS filtr;
DROP TABLE IF EXISTS parametr_produktu;
DROP TABLE IF EXISTS parametr;
DROP TABLE IF EXISTS skupina_parametru;


CREATE TABLE "parametr"(
    "parametr_pk" Serial NOT NULL,
    "parametr_id" Text NOT NULL,
    "nazev" Text NOT NULL,
    "hodnota_typ" Text NOT NULL
        CONSTRAINT "check_hodnota_typ" CHECK (hodnota_typ IN ('NUMERIC', 'INTEGER', 'TEXT')),
    "jednotka" Text,
    "skupina_pk" Integer
)
;

-- Create indexes for table parametr

CREATE INDEX "IX_Relationship38" ON "parametr" ("skupina_pk")
;

-- Add keys for table parametr

ALTER TABLE "parametr" ADD CONSTRAINT "Key19" PRIMARY KEY ("parametr_pk")
;

ALTER TABLE "parametr" ADD CONSTRAINT "parametr_id" UNIQUE ("parametr_id")
;

-- Table parametr_produktu

CREATE TABLE "parametr_produktu"(
    "polozka_pk" Integer NOT NULL,
    "parametr_pk" Integer NOT NULL,
    "hodnota" Text NOT NULL
)
;

-- Create indexes for table parametr_produktu

CREATE INDEX "IX_Relationship36" ON "parametr_produktu" ("polozka_pk")
;

CREATE INDEX "IX_Relationship37" ON "parametr_produktu" ("parametr_pk")
;

-- Add keys for table parametr_produktu

ALTER TABLE "parametr_produktu" ADD CONSTRAINT "Key20" PRIMARY KEY ("polozka_pk","parametr_pk")
;

-- Table skupina_parametru

CREATE TABLE "skupina_parametru"(
    "skupina_pk" Serial NOT NULL,
    "skupina_id" Text NOT NULL,
    "nazev" Text NOT NULL
)
;

-- Add keys for table skupina_parametru

ALTER TABLE "skupina_parametru" ADD CONSTRAINT "Key21" PRIMARY KEY ("skupina_pk")
;

ALTER TABLE "skupina_parametru" ADD CONSTRAINT "skupina_id" UNIQUE ("skupina_id")
;

-- Table filtr

CREATE TABLE "filtr"(
    "filtr_pk" Serial NOT NULL,
    "typ" Text NOT NULL
        CONSTRAINT "check_filtr_typ" CHECK (typ IN ('ROZSAH', 'BODOVY')),
    "hodnota_od" Text NOT NULL,
    "hodnota_do" Text,
    "parametr_pk" Integer
)
;

-- Create indexes for table filtr

CREATE INDEX "IX_Relationship39" ON "filtr" ("parametr_pk")
;

-- Add keys for table filtr

ALTER TABLE "filtr" ADD CONSTRAINT "Key22" PRIMARY KEY ("filtr_pk")
;

-- Table filtr_kategorie

CREATE TABLE "filtr_kategorie"(
    "kategorie_pk" Integer NOT NULL,
    "filtr_pk" Integer NOT NULL
)
;

-- Create indexes for table filtr_kategorie

CREATE INDEX "IX_Relationship40" ON "filtr_kategorie" ("kategorie_pk")
;

CREATE INDEX "IX_Relationship41" ON "filtr_kategorie" ("filtr_pk")
;

-- Add keys for table filtr_kategorie

ALTER TABLE "filtr_kategorie" ADD CONSTRAINT "key_filtr_kategorie" PRIMARY KEY ("kategorie_pk","filtr_pk")
;

-- Table parametr_kategorie

CREATE TABLE "parametr_kategorie"(
    "kategorie_pk" Integer NOT NULL,
    "parametr_pk" Integer NOT NULL
)
;

-- Create indexes for table parametr_kategorie

CREATE INDEX "IX_Relationship42" ON "parametr_kategorie" ("kategorie_pk")
;

CREATE INDEX "IX_Relationship43" ON "parametr_kategorie" ("parametr_pk")
;

-- Add keys for table parametr_kategorie

ALTER TABLE "parametr_kategorie" ADD CONSTRAINT "Key23" PRIMARY KEY ("kategorie_pk","parametr_pk")
;

ALTER TABLE "parametr_produktu" ADD CONSTRAINT "Relationship36" FOREIGN KEY ("polozka_pk") REFERENCES "polozka" ("polozka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr_produktu" ADD CONSTRAINT "Relationship37" FOREIGN KEY ("parametr_pk") REFERENCES "parametr" ("parametr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr" ADD CONSTRAINT "Relationship38" FOREIGN KEY ("skupina_pk") REFERENCES "skupina_parametru" ("skupina_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "filtr" ADD CONSTRAINT "Relationship39" FOREIGN KEY ("parametr_pk") REFERENCES "parametr" ("parametr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "filtr_kategorie" ADD CONSTRAINT "Relationship40" FOREIGN KEY ("kategorie_pk") REFERENCES "kategorie" ("kategorie_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "filtr_kategorie" ADD CONSTRAINT "Relationship41" FOREIGN KEY ("filtr_pk") REFERENCES "filtr" ("filtr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr_kategorie" ADD CONSTRAINT "Relationship42" FOREIGN KEY ("kategorie_pk") REFERENCES "kategorie" ("kategorie_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr_kategorie" ADD CONSTRAINT "Relationship43" FOREIGN KEY ("parametr_pk") REFERENCES "parametr" ("parametr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;