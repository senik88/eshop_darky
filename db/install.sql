begin;

-- schema databaze
\i ./create/create.sql
\i ./create/views.sql

-- iniciacni data
\i ./init/data.sql

-- procedury
\i ./procs/f_utils.sql
\i ./procs/f_udaje.sql
\i ./procs/f_uzivatel.sql
\i ./procs/f_eshop.sql
\i ./procs/f_objednavka.sql
\i ./procs/f_vyrobci.sql
\i ./procs/f_parametry.sql