CREATE OR REPLACE FUNCTION objednavka_zmena(
    a_objednavka_pk   objednavka.objednavka_pk%TYPE
  , a_uzivatel_pk     objednavka.uzivatel_pk%TYPE
  , a_stav            objednavka.stav_objednavky_id%TYPE
  , a_platba_dopravy  objednavka.platba_dopravy_id%TYPE
  , a_fa_udaje        objednavka.fakturacni_udaje_pk%TYPE
  , a_do_udaje        objednavka.dodaci_udaje_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_objednavka_pk objednavka.objednavka_pk%TYPE;
BEGIN

  IF a_objednavka_pk IS NULL THEN

    INSERT INTO objednavka (uzivatel_pk, cas_vytvoreni, cas_zmeny, stav_objednavky_id, platba_dopravy_id, fakturacni_udaje_pk, dodaci_udaje_pk) VALUES
      (a_uzivatel_pk, now(), null, a_stav, a_platba_dopravy, a_fa_udaje, a_do_udaje)
    RETURNING objednavka_pk INTO l_objednavka_pk;

  ELSE

    IF NOT EXISTS(SELECT 1 FROM objednavka WHERE objednavka_pk = a_objednavka_pk) THEN
      RETURN -1;
    END IF;

    UPDATE objednavka SET
        uzivatel_pk = a_uzivatel_pk
      , cas_zmeny = now()
      , stav_objednavky_id = a_stav
      , platba_dopravy_id = a_platba_dopravy
      , fakturacni_udaje_pk = a_fa_udaje
      , dodaci_udaje_pk = a_do_udaje
    WHERE objednavka_pk = a_objednavka_pk
    RETURNING objednavka_pk INTO l_objednavka_pk;

  END IF;

  RETURN l_objednavka_pk;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION objednavka_pridej_polozku(
    a_uzivatel_pk       uzivatel.uzivatel_pk%TYPE
  , a_polozka_id        polozka.polozka_id%TYPE
  , a_kusu              INTEGER
  , OUT r_objednavka_pk objednavka.objednavka_pk%TYPE
  , OUT r_vysledek      TEXT
) RETURNS RECORD AS $func$
DECLARE
  l_objednavka RECORD;
  l_eshop_cena RECORD;
  lr_eshop      eshop_aktualni%ROWTYPE;
  lr_polozka    objednavka_polozka_mn%ROWTYPE;
BEGIN

  SELECT min(objednavka_pk) as objednavka_pk, count(*) as pocet INTO l_objednavka FROM objednavka WHERE uzivatel_pk = a_uzivatel_pk AND stav_objednavky_id = 'KOSIK';

  IF l_objednavka.objednavka_pk IS NULL THEN
    r_objednavka_pk := objednavka_zmena(NULL, a_uzivatel_pk, 'KOSIK', NULL, NULL, NULL);
  ELSIF l_objednavka.pocet > 1 THEN
    r_objednavka_pk := -2;
    r_vysledek := 'Uzivatel (' || a_uzivatel_pk || ') ma vice otevrenych kosiku, nesmi se stat!';
  ELSE
    r_objednavka_pk := l_objednavka.objednavka_pk;
  END IF;

  IF r_objednavka_pk < 0 THEN
    RETURN;
  END IF;

  SELECT * INTO lr_eshop FROM eshop_aktualni WHERE polozka_id = a_polozka_id;

  IF NOT FOUND THEN
    r_objednavka_pk := -3;
    r_vysledek := 'Polozka (' || a_polozka_id || ') neexistuje v aktualnim ceniku!';

    RETURN;
  END IF;

  SELECT * INTO lr_polozka FROM objednavka_polozka_mn WHERE eshop_pk = lr_eshop.eshop_pk AND objednavka_pk = r_objednavka_pk;

  IF NOT FOUND THEN
    INSERT INTO objednavka_polozka_mn (objednavka_pk, eshop_pk, mnozstvi) VALUES
      (r_objednavka_pk, lr_eshop.eshop_cena_pk, coalesce(a_kusu, 1));
  ELSE

    UPDATE objednavka_polozka_mn opm SET
      mnozstvi = mnozstvi + coalesce(a_kusu, 1)
    WHERE opm.objednavka_pk = r_objednavka_pk AND opm.eshop_pk = lr_polozka.eshop_pk;

  END IF;

  r_vysledek := 'Polozka (' || a_polozka_id || ') pridana do kosiku.';

  RETURN;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION objednavka_odeber_polozku(
      a_uzivatel_pk       uzivatel.uzivatel_pk%TYPE
    , a_polozka_id        polozka.polozka_id%TYPE
    , a_vse               BOOLEAN
    , OUT r_objednavka_pk objednavka.objednavka_pk%TYPE
    , OUT r_vysledek      TEXT
) RETURNS RECORD AS $func$
DECLARE
    l_objednavka    RECORD;
    l_eshop_cena    RECORD;
    lr_eshop        eshop_aktualni%ROWTYPE;
    lr_polozka      objednavka_polozka_mn%ROWTYPE;
BEGIN

    SELECT min(objednavka_pk) as objednavka_pk, count(*) as pocet INTO l_objednavka FROM objednavka WHERE uzivatel_pk = a_uzivatel_pk AND stav_objednavky_id = 'KOSIK';

    IF l_objednavka.objednavka_pk IS NULL THEN
        r_objednavka_pk := -1;
        r_vysledek := 'Uzivatel (' || a_uzivatel_pk || ') nema zadny otevreny kosik!';
    ELSIF l_objednavka.pocet > 1 THEN
        r_objednavka_pk := -2;
        r_vysledek := 'Uzivatel (' || a_uzivatel_pk || ') ma vice otevrenych kosiku, nesmi se stat!';
    ELSE
        r_objednavka_pk := l_objednavka.objednavka_pk;
    END IF;

    IF r_objednavka_pk < 0 THEN
        RETURN;
    END IF;

    SELECT * INTO lr_eshop FROM eshop_aktualni WHERE polozka_id = a_polozka_id;

    IF NOT FOUND THEN
        r_objednavka_pk := -3;
        r_vysledek := 'Polozka (' || a_polozka_id || ') neexistuje v aktualnim ceniku!';

        RETURN;
    END IF;

    SELECT * INTO lr_polozka FROM objednavka_polozka_mn WHERE eshop_pk = lr_eshop.eshop_cena_pk AND objednavka_pk = r_objednavka_pk;

    IF NOT FOUND THEN
        r_objednavka_pk := -4;
        r_vysledek := 'Polozka (' || a_polozka_id || ') neexistuje v kosiku uzivatele!';

        RETURN;

    ELSE
        IF a_vse OR lr_polozka.mnozstvi = 1 THEN
            DELETE FROM objednavka_polozka_mn WHERE eshop_pk = lr_eshop.eshop_cena_pk AND objednavka_pk = r_objednavka_pk;

        ELSE
            UPDATE objednavka_polozka_mn opm SET
                mnozstvi = mnozstvi - 1
            WHERE opm.objednavka_pk = r_objednavka_pk AND opm.eshop_pk = lr_polozka.eshop_pk;

        END IF;

    END IF;

    r_vysledek := 'Polozka (' || a_polozka_id || ') odebrana z kosiku.';

    IF NOT EXISTS(SELECT 1 FROM objednavka_polozka_mn WHERE objednavka_pk = r_objednavka_pk) THEN
        DELETE FROM objednavka WHERE objednavka_pk = r_objednavka_pk;

        r_vysledek := r_vysledek || ' A prazdna objednavka byla smazana.';
    END IF;

    RETURN;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION vysypat_kosik(
    a_uzivatel_pk       uzivatel.uzivatel_pk%TYPE
) RETURNS RECORD AS $func$
DECLARE
    lr_eshop    RECORD;
BEGIN

    FOR lr_eshop IN SELECT * FROM kosik_uzivatele WHERE uzivatel_pk = a_uzivatel_pk LOOP

        PERFORM objednavka_odeber_polozku(a_uzivatel_pk, lr_eshop.polozka_id, TRUE);

    END LOOP;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION objednavka_dokoncit(
      a_objednavka_pk   objednavka.objednavka_pk%TYPE
    , a_oznaceni        objednavka.oznaceni_uzivatel%TYPE
    , a_poznamka        objednavka.poznamka%TYPE
    , OUT r_cislo       TEXT
    , OUT r_vysledek    TEXT
) RETURNS RECORD AS $func$
DECLARE
    lr_objednavka   RECORD;
    lr_polozka      RECORD;

    l_blokace       objednavka_polozka_mn.blokace%TYPE;
    l_skladem       eshop.skladem%TYPE;
BEGIN

    SELECT
        o.stav_objednavky_id
        , coalesce(zd.ma_tracking, FALSE) AS ma_tracking
        , coalesce(zp.je_platba_predem, FALSE) AS ma_platbu_predem
    INTO lr_objednavka FROM objednavka o
        JOIN platba_dopravy pd ON o.platba_dopravy_id = pd.platba_dopravy_id
        LEFT JOIN zpusob_dopravy zd ON pd.zpusob_dopravy_pk = zd.zpusob_dopravy_pk
        LEFT JOIN zpusob_platby zp ON pd.zpusob_platby_pk = zp.zpusob_platby_pk
    WHERE objednavka_pk = a_objednavka_pk;

    -- nejdriv si zjistim, jestli vubec muzu objednavku zpracovat
    IF NOT FOUND THEN
        r_cislo := -1;
        r_vysledek := 'Objednavka s pk = ' || a_objednavka_pk || ' nenalezena!';
        RETURN;
    ELSIF lr_objednavka.stav_objednavky_id <> 'KOSIK' THEN
        r_cislo := -2;
        r_vysledek := 'Objednavka je v neplatnem stavu (' || lr_objednavka.stav_objednavky_id || ')!';
        RETURN;
    END IF;

    -- projedu vsechny polozky, nastavim blokaci, mozna bych mel i ponizit skladove zasoby
    -- todo tohle jeste domyslet, nevim uplne, co jsem tim chtel rict...
    FOR lr_polozka IN
        SELECT
              opm.mnozstvi AS objednano
            , opm.blokace
            , ea.skladem
            , ea.eshop_pk AS eshop
            , ea.eshop_cena_pk AS eshop_cena
        FROM objednavka_polozka_mn opm
            JOIN eshop_aktualni ea ON opm.eshop_pk = ea.eshop_pk
            --JOIN eshop e ON opm.eshop_pk = e.eshop_pk
            --JOIN eshop_cena ec ON e.eshop_pk = ec.eshop_pk
        WHERE opm.objednavka_pk = a_objednavka_pk
    LOOP
        -- pokud mam dostatek polozek
        IF lr_polozka.skladem >= lr_polozka.objednano THEN
            -- tak blokace bude tolik, kolik jsem toho objednal
            l_blokace := lr_polozka.objednano;
        -- pokud mam skladem min polozek
        ELSE
            -- tak zablokuju tolik, kolik muzu
            l_blokace := lr_polozka.skladem;
        END IF;

        -- nastavim spoctenou blokaci
        UPDATE objednavka_polozka_mn SET
            blokace = l_blokace
        WHERE objednavka_pk = a_objednavka_pk
            AND eshop_pk = lr_polozka.eshop;

        -- skladove zasoby neponizuju, to az pri odeslani objednavky
        -- todo nad blokaci pak bude muset bezet kontrola, cron bude spoustet db fce treba co pul hodiny
        UPDATE eshop SET
            blokace = blokace + l_blokace
        WHERE eshop_pk = lr_polozka.eshop;

    END LOOP;

    -- vygeneruju cislo objednavky
    r_cislo := objednavka_generuj_cislo();

    -- nastavim spravny stav objedanvky
    -- pohnu s casem
    UPDATE objednavka SET
          stav_objednavky_id = 'POTVRZENA'
        , cas_zmeny = now()
        , cas_vytvoreni = now() -- todo spis cas potvrzeni?
        , oznaceni_uzivatel = a_oznaceni
        , poznamka = a_poznamka
        , cislo = r_cislo
    WHERE objednavka_pk = a_objednavka_pk;

    IF lr_objednavka.ma_platbu_predem THEN
        PERFORM generuj_proforma_fakturu(a_objednavka_pk);
    END IF;

    r_vysledek := 'OK';
    RETURN;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION objednavka_generuj_cislo() RETURNS TEXT AS $func$
DECLARE
    l_ident     TEXT;
    l_rok       TEXT;
    l_mesic     TEXT;
    l_poradi    TEXT;
BEGIN
    l_ident := '21'; -- takze to je objednavka
    l_rok := to_char(now(), 'YY');
    l_mesic := to_char(now(), 'MM');
    l_poradi := ((SELECT count(*) FROM objednavka WHERE stav_objednavky_id <> 'KOSIK' AND EXTRACT(YEAR FROM cas_vytvoreni) = EXTRACT(YEAR FROM now())) + 1)::TEXT;

    RETURN l_ident || l_rok || l_mesic || lpad(l_poradi, 4, '0');
END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION objednavka_prevzit_zpracovani(
      a_objednavka_pk objednavka.objednavka_pk%TYPE
    , a_uzivatel_pk uzivatel.uzivatel_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    lr_objednavka objednavka%ROWTYPE;
    lr_uzivatel uzivatel%ROWTYPE;

    l_stav_zpracovava TEXT := 'ZPRACOVAVA_SE';
    l_stav_potvrzena TEXT := 'POTVRZENA';
    l_uziv_rozhrani TEXT := 'BACK';
BEGIN

    SELECT * INTO lr_objednavka FROM objednavka WHERE objednavka_pk = a_objednavka_pk;

    IF NOT FOUND THEN
        RETURN -1;
    END IF;

    IF lr_objednavka.zpracovava_uzivatel_pk IS NOT NULL THEN
        RETURN -2;
    END IF;

    IF lr_objednavka.stav_objednavky_id <> l_stav_potvrzena THEN
        RETURN -3;
    END IF;

    SELECT * INTO lr_uzivatel FROM uzivatel WHERE uzivatel_pk = a_uzivatel_pk;

    IF NOT FOUND THEN
        RETURN -4;
    END IF;

    IF lr_uzivatel.rozhrani <> l_uziv_rozhrani THEN
        RETURN -5;
    END IF;

    UPDATE objednavka SET
        zpracovava_uzivatel_pk = a_uzivatel_pk
        , stav_objednavky_id = l_stav_zpracovava
    WHERE objednavka_pk = a_objednavka_pk;

    RETURN  0;

END;
$func$ LANGUAGE 'plpgsql';