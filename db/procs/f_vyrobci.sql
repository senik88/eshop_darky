CREATE OR REPLACE FUNCTION preved_produkty_vyrobce(
      a_novy    vyrobce.vyrobce_pk%TYPE
    , a_stary   vyrobce.vyrobce_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
BEGIN
END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION vyrobce_generuj_id(
    a_nazev         vyrobce.nazev%TYPE
) RETURNS vyrobce.vyrobce_id%TYPE AS $func$
DECLARE
    l_vyrobce_id    vyrobce.vyrobce_id%TYPE;
    l_origin_id     vyrobce.vyrobce_id%TYPE;
    l_pocet         INTEGER := 0;
    l_add           TEXT;
BEGIN

    l_vyrobce_id := generuj_meta_id(a_nazev);
    l_origin_id := l_vyrobce_id;

    WHILE exists(SELECT 1 FROM vyrobce WHERE vyrobce_id = l_vyrobce_id) LOOP
        IF (l_pocet = 0) THEN l_add = ''; ELSE l_add = '-' || l_pocet; END IF;

        l_vyrobce_id := l_origin_id || l_add;

        l_pocet := l_pocet + 1;
    END LOOP;

    RETURN l_vyrobce_id;

END;
$func$ LANGUAGE 'plpgsql';