CREATE OR REPLACE FUNCTION uzivatel_uloz_fakturacni(
    a_uzivatel_pk         uzivatel.uzivatel_pk%TYPE
  , a_fakturacni_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%TYPE
  , a_jmeno               fakturacni_udaje.jmeno%TYPE
  , a_prijmeni            fakturacni_udaje.prijmeni%TYPE
  , a_titul_pred          fakturacni_udaje.titul_pred%TYPE
  , a_titul_za            fakturacni_udaje.titul_za%TYPE
  , a_obchodni_jmeno      fakturacni_udaje.obchodni_jmeno%TYPE
  , a_ic                  fakturacni_udaje.ic%TYPE
  , a_dic                 fakturacni_udaje.dic%TYPE
  , a_ulice               fakturacni_udaje.ulice%TYPE
  , a_mesto               fakturacni_udaje.mesto%TYPE
  , a_psc                 fakturacni_udaje.psc%TYPE
  , a_zeme                fakturacni_udaje.zeme%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_fakturacni_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%TYPE;
BEGIN

  IF NOT EXISTS(SELECT 1 FROM uzivatel WHERE uzivatel_pk = a_uzivatel_pk) THEN
    RETURN -1;
  END IF;

  l_fakturacni_udaje_pk := fakturacni_udaje_zmena(
      a_fakturacni_udaje_pk
    , a_jmeno
    , a_prijmeni
    , a_titul_pred
    , a_titul_za
    , a_obchodni_jmeno
    , a_ic
    , a_dic
    , a_ulice
    , a_mesto
    , a_psc
    , a_zeme
  );

  IF l_fakturacni_udaje_pk < 0 THEN
    RETURN -2;
  END IF;

  IF NOT EXISTS(SELECT 1 FROM uzivatel_fakturacni_udaje WHERE uzivatel_pk = a_uzivatel_pk AND fakturacni_udaje_pk = l_fakturacni_udaje_pk) THEN
    INSERT INTO uzivatel_fakturacni_udaje (uzivatel_pk, fakturacni_udaje_pk) VALUES (a_uzivatel_pk, l_fakturacni_udaje_pk);
  END IF;

  RETURN l_fakturacni_udaje_pk;

END;
$func$ LANGUAGE 'plpgsql';



CREATE OR REPLACE FUNCTION uzivatel_uloz_dodaci(
      a_uzivatel_pk         uzivatel.uzivatel_pk%TYPE
    , a_dodaci_udaje_pk     dodaci_udaje.dodaci_udaje_pk%TYPE
    , a_jmeno               dodaci_udaje.jmeno%TYPE
    , a_prijmeni            dodaci_udaje.prijmeni%TYPE
    , a_titul_pred          dodaci_udaje.titul_pred%TYPE
    , a_titul_za            dodaci_udaje.titul_za%TYPE
    , a_ulice               dodaci_udaje.ulice%TYPE
    , a_mesto               dodaci_udaje.mesto%TYPE
    , a_psc                 dodaci_udaje.psc%TYPE
    , a_zeme                dodaci_udaje.zeme%TYPE
    , a_telefon             dodaci_udaje.telefon%TYPE
    , a_poznamka            dodaci_udaje.poznamka%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_dodaci_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%TYPE;
BEGIN

  IF NOT EXISTS(SELECT 1 FROM uzivatel WHERE uzivatel_pk = a_uzivatel_pk) THEN
    RETURN -1;
  END IF;

    l_dodaci_udaje_pk := dodaci_udaje_zmena(
        a_dodaci_udaje_pk
        , a_jmeno
        , a_prijmeni
        , a_titul_pred
        , a_titul_za
        , a_ulice
        , a_mesto
        , a_psc
        , a_zeme
        , a_telefon
        , a_poznamka
    );

  IF l_dodaci_udaje_pk < 0 THEN
    RETURN -2;
  END IF;

  IF NOT EXISTS(SELECT 1 FROM uzivatel_dodaci_udaje WHERE uzivatel_pk = a_uzivatel_pk AND dodaci_udaje_pk = l_dodaci_udaje_pk) THEN
    INSERT INTO uzivatel_dodaci_udaje (uzivatel_pk, dodaci_udaje_pk) VALUES (a_uzivatel_pk, l_dodaci_udaje_pk);
  END IF;

  RETURN l_dodaci_udaje_pk;

END;
$func$ LANGUAGE 'plpgsql';