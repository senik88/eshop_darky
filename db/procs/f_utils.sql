CREATE OR REPLACE FUNCTION generuj_meta_id(
  a_text TEXT
) RETURNS TEXT AS $func$
DECLARE
  l_meta_id TEXT;
BEGIN

  CREATE EXTENSION IF NOT EXISTS unaccent;

  l_meta_id := TRIM(BOTH ' ' FROM lower(unaccent(a_text)));
  l_meta_id := TRIM(BOTH '-' FROM regexp_replace(l_meta_id, '[^a-z0-9]', '-', 'g'));

  RETURN l_meta_id;

END;
$func$ LANGUAGE 'plpgsql';