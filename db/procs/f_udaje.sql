
---
CREATE OR REPLACE FUNCTION fakturacni_udaje_zmena(
    a_fakturacni_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%TYPE
  , a_jmeno               fakturacni_udaje.jmeno%TYPE
  , a_prijmeni            fakturacni_udaje.prijmeni%TYPE
  , a_titul_pred          fakturacni_udaje.titul_pred%TYPE
  , a_titul_za            fakturacni_udaje.titul_za%TYPE
  , a_obchodni_jmeno      fakturacni_udaje.obchodni_jmeno%TYPE
  , a_ic                  fakturacni_udaje.ic%TYPE
  , a_dic                 fakturacni_udaje.dic%TYPE
  , a_ulice               fakturacni_udaje.ulice%TYPE
  , a_mesto               fakturacni_udaje.mesto%TYPE
  , a_psc                 fakturacni_udaje.psc%TYPE
  , a_zeme                fakturacni_udaje.zeme%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_pk    fakturacni_udaje.fakturacni_udaje_pk%TYPE;
BEGIN

  IF a_fakturacni_udaje_pk IS NULL THEN

    INSERT INTO fakturacni_udaje (
        jmeno
      , prijmeni
      , titul_pred
      , titul_za
      , obchodni_jmeno
      , ic
      , dic
      , ulice
      , mesto
      , psc
      , zeme
    ) VALUES (
        a_jmeno
      , a_prijmeni
      , a_titul_pred
      , a_titul_za
      , a_obchodni_jmeno
      , a_ic
      , a_dic
      , a_ulice
      , a_mesto
      , a_psc
      , a_zeme
    ) RETURNING fakturacni_udaje_pk INTO l_pk;

  ELSE

    IF NOT EXISTS(SELECT 1 FROM fakturacni_udaje WHERE fakturacni_udaje_pk = a_fakturacni_udaje_pk) THEN
      RETURN -1;
    END IF;

    UPDATE fakturacni_udaje SET
      jmeno = a_jmeno
    , prijmeni = a_prijmeni
    , titul_pred = a_titul_pred
    , titul_za = a_titul_za
    , obchodni_jmeno = a_obchodni_jmeno
    , ic = a_ic
    , dic = a_dic
    , ulice = a_ulice
    , mesto = a_mesto
    , psc = a_psc
    , zeme = a_zeme
    WHERE fakturacni_udaje_pk = a_fakturacni_udaje_pk RETURNING fakturacni_udaje_pk INTO l_pk;

  END IF;

  RETURN l_pk;

END;
$func$ LANGUAGE 'plpgsql';
---


--- zmena dodacich udaju, pokud predam pk, udela update, jinak insert
CREATE OR REPLACE FUNCTION dodaci_udaje_zmena(
      a_dodaci_udaje_pk dodaci_udaje.dodaci_udaje_pk%TYPE
    , a_jmeno           dodaci_udaje.jmeno%TYPE
    , a_prijmeni        dodaci_udaje.prijmeni%TYPE
    , a_titul_pred      dodaci_udaje.titul_pred%TYPE
    , a_titul_za        dodaci_udaje.titul_za%TYPE
    , a_ulice           dodaci_udaje.ulice%TYPE
    , a_mesto           dodaci_udaje.mesto%TYPE
    , a_psc             dodaci_udaje.psc%TYPE
    , a_zeme            dodaci_udaje.zeme%TYPE
    , a_telefon         dodaci_udaje.telefon%TYPE
    , a_poznamka        dodaci_udaje.poznamka%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_pk    dodaci_udaje.dodaci_udaje_pk%TYPE;
BEGIN

    IF a_dodaci_udaje_pk IS NULL THEN

        INSERT INTO dodaci_udaje (
            jmeno
            , prijmeni
            , titul_pred
            , titul_za
            , ulice
            , mesto
            , psc
            , zeme
            , poznamka
            , telefon
        ) VALUES (
            a_jmeno
            , a_prijmeni
            , a_titul_pred
            , a_titul_za
            , a_ulice
            , a_mesto
            , a_psc
            , a_zeme
            , a_poznamka
            , a_telefon
        ) RETURNING dodaci_udaje_pk INTO l_pk;

    ELSE

        IF NOT EXISTS(SELECT 1 FROM dodaci_udaje WHERE dodaci_udaje_pk = a_dodaci_udaje_pk) THEN
            RETURN -1;
        END IF;

        UPDATE dodaci_udaje SET
            jmeno = a_jmeno
            , prijmeni = a_prijmeni
            , titul_pred = a_titul_pred
            , titul_za = a_titul_za
            , ulice = a_ulice
            , mesto = a_mesto
            , psc = a_psc
            , zeme = a_zeme
            , poznamka = a_poznamka
            , telefon = a_telefon
        WHERE dodaci_udaje_pk = a_dodaci_udaje_pk RETURNING dodaci_udaje_pk INTO l_pk;

    END IF;

    RETURN l_pk;

END;
$func$ LANGUAGE 'plpgsql';



CREATE OR REPLACE FUNCTION objednavka_uloz_fakturacni(
      a_objednavka_pk       objednavka.objednavka_pk%TYPE
    , a_fakturacni_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%TYPE
    , a_jmeno               fakturacni_udaje.jmeno%TYPE
    , a_prijmeni            fakturacni_udaje.prijmeni%TYPE
    , a_titul_pred          fakturacni_udaje.titul_pred%TYPE
    , a_titul_za            fakturacni_udaje.titul_za%TYPE
    , a_obchodni_jmeno      fakturacni_udaje.obchodni_jmeno%TYPE
    , a_ic                  fakturacni_udaje.ic%TYPE
    , a_dic                 fakturacni_udaje.dic%TYPE
    , a_ulice               fakturacni_udaje.ulice%TYPE
    , a_mesto               fakturacni_udaje.mesto%TYPE
    , a_psc                 fakturacni_udaje.psc%TYPE
    , a_zeme                fakturacni_udaje.zeme%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_udaje_pk_nove         fakturacni_udaje.fakturacni_udaje_pk%TYPE;
    l_udaje_pk_stare        fakturacni_udaje.fakturacni_udaje_pk%TYPE;
    lr_objednavka           objednavka%ROWTYPE;
BEGIN

    SELECT * INTO lr_objednavka FROM objednavka WHERE objednavka_pk = a_objednavka_pk;
    IF NOT FOUND THEN
        RETURN -1;
    ELSE
        l_udaje_pk_stare := lr_objednavka.fakturacni_udaje_pk;
    END IF;

    -- pokusim se smazat fakturacni udaje z objednavky
    -- pokud jsou na neco navazane, tak vyleti chyba, ale neresim to
    IF l_udaje_pk_stare IS NOT NULL THEN
        BEGIN
            DELETE FROM fakturacni_udaje WHERE fakturacni_udaje_pk = l_udaje_pk_stare;
        EXCEPTION
            WHEN OTHERS THEN -- tak nic
        END;

    END IF;

    l_udaje_pk_nove := fakturacni_udaje_zmena(
          NULL -- vzdycky chci ulozit nove udaje
        , a_jmeno
        , a_prijmeni
        , a_titul_pred
        , a_titul_za
        , a_obchodni_jmeno
        , a_ic
        , a_dic
        , a_ulice
        , a_mesto
        , a_psc
        , a_zeme
    );

    IF l_udaje_pk_nove < 0 THEN
        RETURN -2;
    END IF;

    UPDATE objednavka SET fakturacni_udaje_pk = l_udaje_pk_nove WHERE objednavka_pk = a_objednavka_pk;

    RETURN l_udaje_pk_nove;

END;
$func$ LANGUAGE 'plpgsql';



CREATE OR REPLACE FUNCTION objednavka_uloz_dodaci(
      a_objednavka_pk       objednavka.objednavka_pk%TYPE
    , a_dodaci_udaje_pk     dodaci_udaje.dodaci_udaje_pk%TYPE
    , a_jmeno               dodaci_udaje.jmeno%TYPE
    , a_prijmeni            dodaci_udaje.prijmeni%TYPE
    , a_titul_pred          dodaci_udaje.titul_pred%TYPE
    , a_titul_za            dodaci_udaje.titul_za%TYPE
    , a_ulice               dodaci_udaje.ulice%TYPE
    , a_mesto               dodaci_udaje.mesto%TYPE
    , a_psc                 dodaci_udaje.psc%TYPE
    , a_zeme                dodaci_udaje.zeme%TYPE
    , a_telefon             dodaci_udaje.telefon%TYPE
    , a_poznamka            dodaci_udaje.poznamka%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_udaje_pk_nove         fakturacni_udaje.fakturacni_udaje_pk%TYPE;
    l_udaje_pk_stare        fakturacni_udaje.fakturacni_udaje_pk%TYPE;
    lr_objednavka           objednavka%ROWTYPE;
BEGIN

    SELECT * INTO lr_objednavka FROM objednavka WHERE objednavka_pk = a_objednavka_pk;
    IF NOT FOUND THEN
        RETURN -1;
    ELSE
        l_udaje_pk_stare := lr_objednavka.dodaci_udaje_pk;
    END IF;

    -- pokusim se smazat fakturacni udaje z objednavky
    -- pokud jsou na neco navazane, tak vyleti chyba, ale neresim to
    IF l_udaje_pk_stare IS NOT NULL THEN
        BEGIN
            DELETE FROM dodaci_udaje WHERE dodaci_udaje_pk = l_udaje_pk_stare;
            EXCEPTION
                WHEN OTHERS THEN -- tak nic
        END;

    END IF;

    l_udaje_pk_nove := dodaci_udaje_zmena(
          NULL -- vzdycky chci ulozit nove udaje
        , a_jmeno
        , a_prijmeni
        , a_titul_pred
        , a_titul_za
        , a_ulice
        , a_mesto
        , a_psc
        , a_zeme
        , a_telefon
        , a_poznamka
    );

    IF l_udaje_pk_nove < 0 THEN
        RETURN -2;
    END IF;

    UPDATE objednavka SET dodaci_udaje_pk = l_udaje_pk_nove WHERE objednavka_pk = a_objednavka_pk;

    RETURN l_udaje_pk_nove;

END;
$func$ LANGUAGE 'plpgsql';