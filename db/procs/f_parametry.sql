CREATE OR REPLACE FUNCTION parametr_zmena(
      a_parametr_pk parametr.parametr_pk%TYPE
    , a_parametr_id parametr.parametr_id%TYPE
    , a_nazev parametr.nazev%TYPE
    , a_hodnota_typ parametr.hodnota_typ%TYPE
    , a_jednotka parametr.jednotka%TYPE
    , a_skupina_pk parametr.skupina_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_pk parametr.parametr_pk%TYPE;
BEGIN

    IF NOT EXISTS(SELECT 1 FROM skupina_parametru WHERE skupina_pk = a_skupina_pk) THEN
        RETURN -1;
    END IF;

    IF a_parametr_pk IS NULL THEN
        INSERT INTO parametr (parametr_id, nazev, hodnota_typ, jednotka, skupina_pk) VALUES
            (a_parametr_id, a_nazev, a_hodnota_typ, nullif(a_jednotka, ''), a_skupina_pk)
        RETURNING parametr_pk INTO l_pk;

    ELSE
        UPDATE parametr SET
            parametr_id = a_parametr_id,
            nazev = a_nazev,
            hodnota_typ = a_hodnota_typ,
            jednotka = nullif(a_jednotka, ''),
            skupina_pk = a_skupina_pk
        WHERE parametr_pk = a_parametr_pk
        RETURNING parametr_pk INTO l_pk;

    END IF;

    RETURN l_pk;
END;
$func$ LANGUAGE 'plpgsql';