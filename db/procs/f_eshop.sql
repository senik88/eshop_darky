---


---
CREATE OR REPLACE FUNCTION eshop_zaloz(
    a_polozka_pk eshop.polozka_pk%TYPE
  , a_viditelne_od eshop.viditelne_od%TYPE
  , a_viditelne_do eshop.viditelne_do%TYPE
  , a_skladem eshop.skladem%TYPE
  , a_blokace eshop.blokace%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_eshop_pk eshop.eshop_pk%TYPE;
BEGIN

  -- todo tady to bude chtit kontroly a tak...
  INSERT INTO eshop (polozka_pk, viditelne_od, viditelne_do, skladem, blokace) VALUES
    (a_polozka_pk, a_viditelne_od, a_viditelne_do, a_skladem, coalesce(a_blokace, 0))
  RETURNING eshop_pk INTO l_eshop_pk;

  RETURN l_eshop_pk;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION eshop_cena_zaloz(
    a_eshop_pk eshop_cena.eshop_pk%TYPE
  , a_platne_od eshop_cena.platne_od%TYPE
  , a_platne_do eshop_cena.platne_do%TYPE
  , a_cena_dop eshop_cena.cena_doporucena%TYPE
  , a_cena_akt eshop_cena.cena_aktualni%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_cena_pk eshop_cena.eshop_cena_pk%TYPE;
BEGIN

  -- todo tady to bude chtit kontroly a tak...
  INSERT INTO eshop_cena (eshop_pk, platne_od, platne_do, cena_doporucena, cena_aktualni) VALUES
    (a_eshop_pk, a_platne_od, a_platne_do, a_cena_dop, a_cena_akt)
  RETURNING eshop_cena_pk INTO l_cena_pk;

  RETURN l_cena_pk;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION eshop_polozka_zaloz(
    a_titulek polozka.titulek%TYPE
  , a_subtitulek polozka.subtitulek%TYPE
  , a_popis polozka.popis%TYPE
  , a_dodavatel_pk polozka.dodavatel_pk%TYPE
  , a_vyrobce_pk polozka.vyrobce_pk%TYPE
    , a_externi_id polozka.externi_id%TYPE
    , a_ean polozka.ean%TYPE
  , a_kategorie_id polozka.kategorie_id%TYPE
  , a_viditelne_od eshop.viditelne_od%TYPE
  , a_viditelne_do eshop.viditelne_do%TYPE
  , a_skladem eshop.skladem%TYPE
  , a_blokace eshop.blokace%TYPE
  , a_cena_dop eshop_cena.cena_doporucena%TYPE
  , a_cena_akt eshop_cena.cena_aktualni%TYPE
  , a_plati_od eshop_cena.platne_od%TYPE
  , a_plati_do eshop_cena.platne_do%TYPE
  , OUT r_polozka_pk polozka.polozka_pk%TYPE
  , OUT r_vysledek TEXT
) RETURNS RECORD AS $func$
DECLARE
  l_pocet INTEGER := 0;
  l_add TEXT;
  l_polozka_pk polozka.polozka_pk%TYPE;
  l_polozka_id polozka.polozka_id%TYPE;

  l_eshop_pk eshop.eshop_pk%TYPE;
  l_eshop_cena_pk eshop_cena.eshop_cena_pk%TYPE;
BEGIN

  IF NOT exists(SELECT 1 FROM dodavatel WHERE dodavatel_pk = a_dodavatel_pk) THEN
    r_polozka_pk := -1;
    r_vysledek := 'Dodavatel nenalezen';
    RETURN;
  END IF;

  IF NOT exists(SELECT 1 FROM vyrobce WHERE vyrobce_pk = a_vyrobce_pk) THEN
    r_polozka_pk := -2;
    r_vysledek := 'Vyrobce nenalezen';
    RETURN;
  END IF;

  IF NOT exists(SELECT 1 FROM kategorie WHERE kategorie_id = a_kategorie_id) THEN
    r_polozka_pk := -3;
    r_vysledek := 'Kategorie nenalezena';
    RETURN;
  END IF;

  l_polozka_id := generuj_meta_id(a_titulek);

  WHILE exists(SELECT 1 FROM polozka WHERE polozka_id = l_polozka_id) LOOP
    l_pocet := l_pocet + 1;
    IF (l_pocet = 0) THEN l_add = ''; ELSE l_add = '-' || l_pocet; END IF;

    l_polozka_id := l_polozka_id || l_add;
  END LOOP;

  INSERT INTO polozka (polozka_id, titulek, subtitulek, popis, externi_id, ean, dodavatel_pk, vyrobce_pk, kategorie_id) VALUES
    (l_polozka_id, a_titulek, a_subtitulek, a_popis, a_externi_id, a_ean, a_dodavatel_pk, a_vyrobce_pk, a_kategorie_id)
  RETURNING polozka_pk INTO l_polozka_pk;

  l_eshop_pk := eshop_zaloz(
      l_polozka_pk
    , a_viditelne_od
    , a_viditelne_do
    , a_skladem
    , a_blokace
  );

  IF l_eshop_pk < 0 THEN
    r_polozka_pk := -4;
    r_vysledek := 'Chyba pri nastaveni polozky do eshopu';
    RETURN;
  END IF;

  l_eshop_cena_pk := eshop_cena_zaloz(
      l_eshop_pk
    , a_plati_od
    , a_plati_do
    , a_cena_dop
    , a_cena_akt
  );

  IF l_eshop_cena_pk < 0 THEN
    r_polozka_pk := -5;
    r_vysledek := 'Chyba pri nastaveni ceny do eshopu';
    RETURN;
  END IF;

  r_polozka_pk := l_polozka_pk;
  r_vysledek := l_polozka_id;
  RETURN;

END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION eshop_polozka_zmena(
    a_polozka_pk polozka.polozka_pk%TYPE
    , a_titulek polozka.titulek%TYPE
    , a_subtitulek polozka.subtitulek%TYPE
    , a_popis polozka.popis%TYPE
    , a_vyrobce polozka.vyrobce_pk%TYPE
    , a_dodavatel polozka.dodavatel_pk%TYPE
    , a_kategorie polozka.kategorie_id%TYPE
    , a_viditelne_od eshop.viditelne_od%TYPE
    , a_viditelne_do eshop.viditelne_do%TYPE
    , a_skladem eshop.skladem%TYPE
    , OUT r_polozka_pk polozka.polozka_pk%TYPE
    , OUT r_vysledek TEXT
) RETURNS RECORD AS $func$
DECLARE

BEGIN
    -- definice uz tady nevadi, v pripade chyby si to preprasknu
    r_vysledek := 'OK';

    -- tohle by se dalo dat do samostatnych procedur... pripadne nejlip sjednotit eshop_polozka_zaloz a eshop_polozka_zmena
    IF NOT exists(SELECT 1 FROM dodavatel WHERE dodavatel_pk = a_dodavatel) THEN
        r_polozka_pk := -1;
        r_vysledek := 'Dodavatel nenalezen';
        RETURN;
    END IF;

    IF NOT exists(SELECT 1 FROM vyrobce WHERE vyrobce_pk = a_vyrobce) THEN
        r_polozka_pk := -2;
        r_vysledek := 'Vyrobce nenalezen';
        RETURN;
    END IF;

    IF NOT exists(SELECT 1 FROM kategorie WHERE kategorie_id = a_kategorie) THEN
        r_polozka_pk := -3;
        r_vysledek := 'Kategorie nenalezena';
        RETURN;
    END IF;
    --

    IF NOT EXISTS(SELECT 1 FROM polozka WHERE polozka_pk = a_polozka_pk) THEN
        r_polozka_pk := -4;
        r_vysledek := 'Pro editaci nenalezena polozka eshopu s pk = ' || a_polozka_pk;
    END IF;

    UPDATE polozka SET
        titulek = a_titulek
        , subtitulek = a_subtitulek
        , popis = a_popis
        , dodavatel_pk = a_dodavatel
        , vyrobce_pk = a_vyrobce
        , kategorie_id = a_kategorie
    WHERE polozka_pk = a_polozka_pk RETURNING polozka_pk INTO r_polozka_pk;

    UPDATE eshop SET
        viditelne_od = a_viditelne_od
        , viditelne_do = a_viditelne_do
        , skladem = a_skladem -- todo na pocet kusu skladem je potreba dat pozor kvuli blokacim... pujde to vubec editovat?
    WHERE polozka_pk = r_polozka_pk;

    RETURN;
END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION polozka_obrazek_hlavni_vrat_pk(
  a_polozka_pk polozka.polozka_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_pk polozka_obrazek.polozka_obrazek_pk%TYPE;
BEGIN
  IF NOT EXISTS(SELECT 1 FROM polozka WHERE polozka_pk = a_polozka_pk) THEN
    RETURN -1;
  END IF;

  SELECT polozka_obrazek_pk INTO l_pk FROM polozka_obrazek
  WHERE polozka_pk = a_polozka_pk
  ORDER BY (CASE WHEN hlavni THEN 1 ELSE 2 END) ASC, polozka_obrazek_pk ASC LIMIT 1;

  IF NOT FOUND THEN

      select polozka_obrazek_pk INTO l_pk from polozka_obrazek where polozka_pk is null order by polozka_obrazek_pk asc limit 1;

      IF NOT FOUND THEN
          l_pk := -2;
      END IF;
  END IF;

  RETURN l_pk;
END;
$func$ LANGUAGE 'plpgsql';
---


---
CREATE OR REPLACE FUNCTION eshop_vrat_sumu_blokaci(
    a_eshop_pk eshop.eshop_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE

BEGIN

END;
$func$ LANGUAGE 'plpgsql';