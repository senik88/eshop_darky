﻿/*
Created: 19.08.2015
Modified: 08.10.2016
Model: eshop_darky
Database: PostgreSQL 9.2
*/


-- Create tables section -------------------------------------------------

-- Table objednavka

CREATE TABLE "objednavka"(
 "objednavka_pk" Serial NOT NULL,
 "cislo" Text,
 "cas_vytvoreni" Timestamp with time zone NOT NULL,
 "cas_zmeny" Timestamp with time zone,
 "poznamka" Text,
 "oznaceni_uzivatel" Text,
 "uzivatel_pk" Integer,
 "stav_objednavky_id" Text NOT NULL,
 "platba_dopravy_id" Text,
 "fakturacni_udaje_pk" Integer,
 "dodaci_udaje_pk" Integer,
 "zpracovava_uzivatel_pk" Integer
)
;

-- Create indexes for table objednavka

CREATE INDEX "ix_objednavka_stav_objednavky" ON "objednavka" ("stav_objednavky_id")
;

CREATE INDEX "ix_objednavka_uzivatele" ON "objednavka" ("uzivatel_pk")
;

CREATE INDEX "ix_objednavka_platba_dopravy" ON "objednavka" ("platba_dopravy_id")
;

CREATE INDEX "ix_objednavka_fakturacni_udaje" ON "objednavka" ("fakturacni_udaje_pk")
;

CREATE INDEX "ix_objednavka_dodaci_udaje" ON "objednavka" ("dodaci_udaje_pk")
;

CREATE INDEX "ix_objednavka_zpracovava_uzivatel" ON "objednavka" ("zpracovava_uzivatel_pk")
;

-- Add keys for table objednavka

ALTER TABLE "objednavka" ADD CONSTRAINT "Key1" PRIMARY KEY ("objednavka_pk")
;

-- Table polozka

CREATE TABLE "polozka"(
 "polozka_pk" Serial NOT NULL,
 "polozka_id" Text NOT NULL,
 "externi_id" Text,
 "ean" Text,
 "titulek" Text NOT NULL,
 "subtitulek" Text,
 "popis" Text NOT NULL,
 "dodavatel_pk" Integer NOT NULL,
 "vyrobce_pk" Integer NOT NULL,
 "kategorie_id" Text
)
;

-- Create indexes for table polozka

CREATE INDEX "IX_Relationship5" ON "polozka" ("dodavatel_pk")
;

CREATE INDEX "IX_Relationship6" ON "polozka" ("vyrobce_pk")
;

CREATE INDEX "IX_Relationship27" ON "polozka" ("kategorie_id")
;

CREATE UNIQUE INDEX "uniq_externi_id_vyrobce" ON "polozka" ("externi_id","dodavatel_pk")
;

-- Add keys for table polozka

ALTER TABLE "polozka" ADD CONSTRAINT "Key2" PRIMARY KEY ("polozka_pk")
;

ALTER TABLE "polozka" ADD CONSTRAINT "polozka_id" UNIQUE ("polozka_id")
;

-- Table dodavatel

CREATE TABLE "dodavatel"(
 "dodavatel_pk" Serial NOT NULL,
 "nazev" Text NOT NULL,
 "fakturacni_udaje_pk" Integer
)
;

-- Create indexes for table dodavatel

CREATE INDEX "ix_dodavatel_fakturacni_udaje" ON "dodavatel" ("fakturacni_udaje_pk")
;

-- Add keys for table dodavatel

ALTER TABLE "dodavatel" ADD CONSTRAINT "Key3" PRIMARY KEY ("dodavatel_pk")
;

-- Table vyrobce

CREATE TABLE "vyrobce"(
 "vyrobce_pk" Serial NOT NULL,
 "vyrobce_id" Text NOT NULL,
 "nazev" Text NOT NULL,
 "web" Text,
 "datum_vlozeni" Timestamp with time zone NOT NULL,
 "logo_pk" Integer
)
;

-- Create indexes for table vyrobce

CREATE INDEX "ix_logo_vyrobce" ON "vyrobce" ("logo_pk")
;

-- Add keys for table vyrobce

ALTER TABLE "vyrobce" ADD CONSTRAINT "Key4" PRIMARY KEY ("vyrobce_pk")
;

-- Table stav_objednavky

CREATE TABLE "stav_objednavky"(
 "stav_objednavky_id" Text NOT NULL,
 "popis" Text NOT NULL,
 "poradi" Smallint NOT NULL
)
;

-- Add keys for table stav_objednavky

ALTER TABLE "stav_objednavky" ADD CONSTRAINT "Key5" PRIMARY KEY ("stav_objednavky_id")
;

-- Table eshop

CREATE TABLE "eshop"(
 "eshop_pk" Serial NOT NULL,
 "polozka_pk" Integer,
 "viditelne_od" Timestamp with time zone NOT NULL,
 "viditelne_do" Timestamp with time zone,
 "skladem" Smallint NOT NULL,
 "blokace" Smallint NOT NULL
)
;

-- Create indexes for table eshop

CREATE INDEX "IX_Relationship11" ON "eshop" ("polozka_pk")
;

-- Add keys for table eshop

ALTER TABLE "eshop" ADD CONSTRAINT "Key6" PRIMARY KEY ("eshop_pk")
;

-- Table eshop_cena

CREATE TABLE "eshop_cena"(
 "eshop_cena_pk" Serial NOT NULL,
 "eshop_pk" Integer NOT NULL,
 "platne_od" Timestamp with time zone NOT NULL,
 "platne_do" Timestamp with time zone,
 "cena_doporucena" Numeric(10,2) NOT NULL,
 "cena_aktualni" Numeric(10,2) NOT NULL
)
;

-- Create indexes for table eshop_cena

CREATE INDEX "IX_Relationship9" ON "eshop_cena" ("eshop_pk")
;

-- Add keys for table eshop_cena

ALTER TABLE "eshop_cena" ADD CONSTRAINT "Key7" PRIMARY KEY ("eshop_cena_pk")
;

-- Table objednavka_polozka_mn

CREATE TABLE "objednavka_polozka_mn"(
 "objednavka_pk" Integer NOT NULL,
 "eshop_pk" Integer NOT NULL,
 "mnozstvi" Smallint NOT NULL,
 "blokace" Smallint DEFAULT 0 NOT NULL
)
;

-- Create indexes for table objednavka_polozka_mn

CREATE INDEX "IX_Relationship7" ON "objednavka_polozka_mn" ("objednavka_pk")
;

CREATE INDEX "IX_Relationship33" ON "objednavka_polozka_mn" ("eshop_pk")
;

-- Add keys for table objednavka_polozka_mn

ALTER TABLE "objednavka_polozka_mn" ADD CONSTRAINT "pk_objednavka_polozka_mn" PRIMARY KEY ("objednavka_pk","eshop_pk")
;

-- Table zpusob_dopravy

CREATE TABLE "zpusob_dopravy"(
 "zpusob_dopravy_pk" Serial NOT NULL,
 "nazev" Text NOT NULL,
 "ma_tracking" Boolean NOT NULL,
 "logo_pk" Integer
)
;

-- Create indexes for table zpusob_dopravy

CREATE INDEX "ix_logo_zpusob_dopravy" ON "zpusob_dopravy" ("logo_pk")
;

-- Add keys for table zpusob_dopravy

ALTER TABLE "zpusob_dopravy" ADD CONSTRAINT "Key9" PRIMARY KEY ("zpusob_dopravy_pk")
;

-- Table polozka_obrazek

CREATE TABLE "polozka_obrazek"(
 "polozka_obrazek_pk" Serial NOT NULL,
 "zdroj" Text NOT NULL,
 "nahled" Text NOT NULL,
 "popisek" Text NOT NULL,
 "hlavni" Boolean DEFAULT false NOT NULL,
 "nahrano" Timestamp with time zone NOT NULL,
 "polozka_pk" Integer,
 "uzivatel_pk" Integer
)
;

-- Create indexes for table polozka_obrazek

CREATE INDEX "IX_Relationship13" ON "polozka_obrazek" ("polozka_pk")
;

CREATE INDEX "IX_Relationship17" ON "polozka_obrazek" ("uzivatel_pk")
;

-- Add keys for table polozka_obrazek

ALTER TABLE "polozka_obrazek" ADD CONSTRAINT "Key10" PRIMARY KEY ("polozka_obrazek_pk")
;

-- Table zpusob_platby

CREATE TABLE "zpusob_platby"(
 "zpusob_platby_pk" Serial NOT NULL,
 "nazev" Text NOT NULL,
 "je_platba_predem" Boolean NOT NULL,
 "logo_pk" Integer
)
;

-- Create indexes for table zpusob_platby

CREATE INDEX "ix_logo_zpusob_platby" ON "zpusob_platby" ("logo_pk")
;

-- Add keys for table zpusob_platby

ALTER TABLE "zpusob_platby" ADD CONSTRAINT "Key11" PRIMARY KEY ("zpusob_platby_pk")
;

-- Table uzivatel

CREATE TABLE "uzivatel"(
 "uzivatel_pk" Serial NOT NULL,
 "email" Text NOT NULL,
 "rozhrani" Text NOT NULL
        CONSTRAINT "check_rozhrani" CHECK (rozhrani in ('FRONT', 'BACK')),
 "stav" Text NOT NULL,
 "heslo" Text NOT NULL,
 "cas_registrace" Timestamp with time zone NOT NULL,
 "cas_aktivace" Timestamp with time zone,
 "cas_prihlaseni" Timestamp with time zone,
 "ip_registrace" Inet,
 "ip_prihlaseni" Inet,
 "validacni_klic" Text NOT NULL,
 "jmeno" Text NOT NULL,
 "prijmeni" Text NOT NULL
)
;

-- Add keys for table uzivatel

ALTER TABLE "uzivatel" ADD CONSTRAINT "Key12" PRIMARY KEY ("uzivatel_pk")
;

ALTER TABLE "uzivatel" ADD CONSTRAINT "email" UNIQUE ("email")
;

ALTER TABLE "uzivatel" ADD CONSTRAINT "validacni_klic" UNIQUE ("validacni_klic")
;

-- Table kategorie

CREATE TABLE "kategorie"(
 "kategorie_pk" Serial NOT NULL,
 "kategorie_id" Text NOT NULL,
 "nazev" Text NOT NULL,
 "menu_zobrazit" Boolean NOT NULL,
 "lze_zobrazit_zbozi" Boolean DEFAULT true NOT NULL,
 "nadrazena" Integer
)
;

-- Create indexes for table kategorie

CREATE INDEX "IX_Relationship15" ON "kategorie" ("nadrazena")
;

-- Add keys for table kategorie

ALTER TABLE "kategorie" ADD CONSTRAINT "Key13" PRIMARY KEY ("kategorie_pk")
;

ALTER TABLE "kategorie" ADD CONSTRAINT "kategorie_id" UNIQUE ("kategorie_id")
;

-- Table dodaci_udaje

CREATE TABLE "dodaci_udaje"(
 "dodaci_udaje_pk" Serial NOT NULL,
 "jmeno" Text NOT NULL,
 "prijmeni" Text NOT NULL,
 "titul_pred" Text,
 "titul_za" Text,
 "ulice" Text NOT NULL,
 "mesto" Text NOT NULL,
 "psc" Text NOT NULL,
 "zeme" Text NOT NULL,
 "telefon" Text,
 "poznamka" Text
)
;

-- Add keys for table dodaci_udaje

ALTER TABLE "dodaci_udaje" ADD CONSTRAINT "Key14" PRIMARY KEY ("dodaci_udaje_pk")
;

-- Table fakturacni_udaje

CREATE TABLE "fakturacni_udaje"(
 "fakturacni_udaje_pk" Serial NOT NULL,
 "jmeno" Text NOT NULL,
 "prijmeni" Text NOT NULL,
 "titul_pred" Text,
 "titul_za" Text,
 "obchodni_jmeno" Text,
 "ic" Text,
 "dic" Text,
 "ulice" Text NOT NULL,
 "mesto" Text NOT NULL,
 "psc" Text NOT NULL,
 "zeme" Text NOT NULL
)
;

-- Add keys for table fakturacni_udaje

ALTER TABLE "fakturacni_udaje" ADD CONSTRAINT "Key15" PRIMARY KEY ("fakturacni_udaje_pk")
;

-- Table platba_dopravy

CREATE TABLE "platba_dopravy"(
 "platba_dopravy_pk" Serial NOT NULL,
 "platba_dopravy_id" Text NOT NULL,
 "cena" Numeric(5,2) NOT NULL,
 "viditelne_od" Timestamp with time zone NOT NULL,
 "viditelne_do" Timestamp(0) with time zone,
 "zpusob_dopravy_pk" Integer,
 "zpusob_platby_pk" Integer
)
;

-- Create indexes for table platba_dopravy

CREATE INDEX "IX_Relationship18" ON "platba_dopravy" ("zpusob_dopravy_pk")
;

CREATE INDEX "IX_Relationship19" ON "platba_dopravy" ("zpusob_platby_pk")
;

-- Add keys for table platba_dopravy

ALTER TABLE "platba_dopravy" ADD CONSTRAINT "Key16" PRIMARY KEY ("platba_dopravy_pk")
;

ALTER TABLE "platba_dopravy" ADD CONSTRAINT "platba_dopravy_id" UNIQUE ("platba_dopravy_id")
;

-- Table uzivatel_fakturacni_udaje

CREATE TABLE "uzivatel_fakturacni_udaje"(
 "uzivatel_pk" Integer NOT NULL,
 "fakturacni_udaje_pk" Integer NOT NULL
)
;

-- Create indexes for table uzivatel_fakturacni_udaje

CREATE INDEX "IX_Relationship21" ON "uzivatel_fakturacni_udaje" ("uzivatel_pk")
;

CREATE INDEX "IX_Relationship23" ON "uzivatel_fakturacni_udaje" ("fakturacni_udaje_pk")
;

-- Add keys for table uzivatel_fakturacni_udaje

ALTER TABLE "uzivatel_fakturacni_udaje" ADD CONSTRAINT "pk_uzivatel_fa_udaje" PRIMARY KEY ("uzivatel_pk","fakturacni_udaje_pk")
;

-- Table uzivatel_dodaci_udaje

CREATE TABLE "uzivatel_dodaci_udaje"(
 "uzivatel_pk" Integer NOT NULL,
 "dodaci_udaje_pk" Integer NOT NULL
)
;

-- Create indexes for table uzivatel_dodaci_udaje

CREATE INDEX "IX_Relationship22" ON "uzivatel_dodaci_udaje" ("uzivatel_pk")
;

CREATE INDEX "IX_Relationship24" ON "uzivatel_dodaci_udaje" ("dodaci_udaje_pk")
;

-- Add keys for table uzivatel_dodaci_udaje

ALTER TABLE "uzivatel_dodaci_udaje" ADD CONSTRAINT "pk_uzivatel_dod_udaje" PRIMARY KEY ("uzivatel_pk","dodaci_udaje_pk")
;

-- Table logo

CREATE TABLE "logo"(
 "logo_pk" Serial NOT NULL,
 "zdroj" Text NOT NULL,
 "sirka" Smallint NOT NULL,
 "vyska" Smallint NOT NULL,
 "hash" Text NOT NULL
)
;

-- Add keys for table logo

ALTER TABLE "logo" ADD CONSTRAINT "Key17" PRIMARY KEY ("logo_pk")
;

ALTER TABLE "logo" ADD CONSTRAINT "hash" UNIQUE ("hash")
;

-- Table historie_prohlizeni

CREATE TABLE "historie_prohlizeni"(
 "eshop_pk" Integer NOT NULL,
 "uzivatel_pk" Integer NOT NULL
)
;

-- Create indexes for table historie_prohlizeni

CREATE INDEX "IX_Relationship34" ON "historie_prohlizeni" ("eshop_pk")
;

CREATE INDEX "IX_Relationship35" ON "historie_prohlizeni" ("uzivatel_pk")
;

-- Add keys for table historie_prohlizeni

ALTER TABLE "historie_prohlizeni" ADD CONSTRAINT "Key18" PRIMARY KEY ("uzivatel_pk","eshop_pk")
;

-- Table parametr

CREATE TABLE "parametr"(
 "parametr_pk" Serial NOT NULL,
 "parametr_id" Text NOT NULL,
 "nazev" Text NOT NULL,
 "hodnota_typ" Text NOT NULL
        CONSTRAINT "check_hodnota_typ" CHECK (hodnota_typ IN ('NUMERIC', 'INTEGER', 'TEXT')),
 "jednotka" Text,
 "skupina_pk" Integer
)
;

-- Create indexes for table parametr

CREATE INDEX "IX_Relationship38" ON "parametr" ("skupina_pk")
;

-- Add keys for table parametr

ALTER TABLE "parametr" ADD CONSTRAINT "Key19" PRIMARY KEY ("parametr_pk")
;

ALTER TABLE "parametr" ADD CONSTRAINT "parametr_id" UNIQUE ("parametr_id")
;

-- Table parametr_produktu

CREATE TABLE "parametr_produktu"(
 "polozka_pk" Integer NOT NULL,
 "parametr_pk" Integer NOT NULL,
 "hodnota" Text NOT NULL
)
;

-- Create indexes for table parametr_produktu

CREATE INDEX "IX_Relationship36" ON "parametr_produktu" ("polozka_pk")
;

CREATE INDEX "IX_Relationship37" ON "parametr_produktu" ("parametr_pk")
;

-- Add keys for table parametr_produktu

ALTER TABLE "parametr_produktu" ADD CONSTRAINT "Key20" PRIMARY KEY ("polozka_pk","parametr_pk")
;

-- Table skupina_parametru

CREATE TABLE "skupina_parametru"(
 "skupina_pk" Serial NOT NULL,
 "skupina_id" Text NOT NULL,
 "nazev" Text NOT NULL
)
;

-- Add keys for table skupina_parametru

ALTER TABLE "skupina_parametru" ADD CONSTRAINT "Key21" PRIMARY KEY ("skupina_pk")
;

ALTER TABLE "skupina_parametru" ADD CONSTRAINT "skupina_id" UNIQUE ("skupina_id")
;

-- Table filtr

CREATE TABLE "filtr"(
 "filtr_pk" Serial NOT NULL,
 "typ" Text NOT NULL
        CONSTRAINT "check_filtr_typ" CHECK (typ IN ('ROZSAH', 'BODOVY')),
 "hodnota_od" Text NOT NULL,
 "hodnota_do" Text,
 "parametr_pk" Integer
)
;

-- Create indexes for table filtr

CREATE INDEX "IX_Relationship39" ON "filtr" ("parametr_pk")
;

-- Add keys for table filtr

ALTER TABLE "filtr" ADD CONSTRAINT "Key22" PRIMARY KEY ("filtr_pk")
;

-- Table filtr_kategorie

CREATE TABLE "filtr_kategorie"(
 "kategorie_pk" Integer NOT NULL,
 "filtr_pk" Integer NOT NULL
)
;

-- Create indexes for table filtr_kategorie

CREATE INDEX "IX_Relationship40" ON "filtr_kategorie" ("kategorie_pk")
;

CREATE INDEX "IX_Relationship41" ON "filtr_kategorie" ("filtr_pk")
;

-- Add keys for table filtr_kategorie

ALTER TABLE "filtr_kategorie" ADD CONSTRAINT "key_filtr_kategorie" PRIMARY KEY ("kategorie_pk","filtr_pk")
;

-- Table parametr_kategorie

CREATE TABLE "parametr_kategorie"(
 "kategorie_pk" Integer NOT NULL,
 "parametr_pk" Integer NOT NULL
)
;

-- Create indexes for table parametr_kategorie

CREATE INDEX "IX_Relationship42" ON "parametr_kategorie" ("kategorie_pk")
;

CREATE INDEX "IX_Relationship43" ON "parametr_kategorie" ("parametr_pk")
;

-- Add keys for table parametr_kategorie

ALTER TABLE "parametr_kategorie" ADD CONSTRAINT "Key23" PRIMARY KEY ("kategorie_pk","parametr_pk")
;

-- Table hodnoceni_produktu

CREATE TABLE "hodnoceni_produktu"(
 "hodnoceni_produktu_pk" Serial NOT NULL,
 "procenta" Integer NOT NULL,
 "popis" Text,
 "klady" Json,
 "zapory" Json,
 "polozka_pk" Integer NOT NULL,
 "uzivatel_pk" Integer
)
;

-- Create indexes for table hodnoceni_produktu

CREATE INDEX "ix_polozka_hodnoceni_produktu" ON "hodnoceni_produktu" ("polozka_pk")
;

CREATE INDEX "ix_uzivatel_hodnoceni_produktu" ON "hodnoceni_produktu" ("uzivatel_pk")
;

-- Add keys for table hodnoceni_produktu

ALTER TABLE "hodnoceni_produktu" ADD CONSTRAINT "Key24" PRIMARY KEY ("hodnoceni_produktu_pk")
;

-- Create relationships section ------------------------------------------------- 

ALTER TABLE "objednavka" ADD CONSTRAINT "objednavka_stav_objednavky" FOREIGN KEY ("stav_objednavky_id") REFERENCES "stav_objednavky" ("stav_objednavky_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "polozka" ADD CONSTRAINT "Relationship5" FOREIGN KEY ("dodavatel_pk") REFERENCES "dodavatel" ("dodavatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "polozka" ADD CONSTRAINT "Relationship6" FOREIGN KEY ("vyrobce_pk") REFERENCES "vyrobce" ("vyrobce_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka_polozka_mn" ADD CONSTRAINT "Relationship7" FOREIGN KEY ("objednavka_pk") REFERENCES "objednavka" ("objednavka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "eshop_cena" ADD CONSTRAINT "Relationship9" FOREIGN KEY ("eshop_pk") REFERENCES "eshop" ("eshop_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "eshop" ADD CONSTRAINT "Relationship11" FOREIGN KEY ("polozka_pk") REFERENCES "polozka" ("polozka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "polozka_obrazek" ADD CONSTRAINT "Relationship13" FOREIGN KEY ("polozka_pk") REFERENCES "polozka" ("polozka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka" ADD CONSTRAINT "uzivatel_zadal_objednavku" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "kategorie" ADD CONSTRAINT "Relationship15" FOREIGN KEY ("nadrazena") REFERENCES "kategorie" ("kategorie_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "polozka_obrazek" ADD CONSTRAINT "Relationship17" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "platba_dopravy" ADD CONSTRAINT "Relationship18" FOREIGN KEY ("zpusob_dopravy_pk") REFERENCES "zpusob_dopravy" ("zpusob_dopravy_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "platba_dopravy" ADD CONSTRAINT "Relationship19" FOREIGN KEY ("zpusob_platby_pk") REFERENCES "zpusob_platby" ("zpusob_platby_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka" ADD CONSTRAINT "Relationship20" FOREIGN KEY ("platba_dopravy_id") REFERENCES "platba_dopravy" ("platba_dopravy_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "uzivatel_fakturacni_udaje" ADD CONSTRAINT "Relationship21" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "uzivatel_dodaci_udaje" ADD CONSTRAINT "Relationship22" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "uzivatel_fakturacni_udaje" ADD CONSTRAINT "Relationship23" FOREIGN KEY ("fakturacni_udaje_pk") REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "uzivatel_dodaci_udaje" ADD CONSTRAINT "Relationship24" FOREIGN KEY ("dodaci_udaje_pk") REFERENCES "dodaci_udaje" ("dodaci_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka" ADD CONSTRAINT "Relationship25" FOREIGN KEY ("fakturacni_udaje_pk") REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka" ADD CONSTRAINT "Relationship26" FOREIGN KEY ("dodaci_udaje_pk") REFERENCES "dodaci_udaje" ("dodaci_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "polozka" ADD CONSTRAINT "Relationship27" FOREIGN KEY ("kategorie_id") REFERENCES "kategorie" ("kategorie_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "dodavatel" ADD CONSTRAINT "Relationship28" FOREIGN KEY ("fakturacni_udaje_pk") REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "zpusob_dopravy" ADD CONSTRAINT "Relationship29" FOREIGN KEY ("logo_pk") REFERENCES "logo" ("logo_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "zpusob_platby" ADD CONSTRAINT "Relationship30" FOREIGN KEY ("logo_pk") REFERENCES "logo" ("logo_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "vyrobce" ADD CONSTRAINT "Relationship32" FOREIGN KEY ("logo_pk") REFERENCES "logo" ("logo_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka_polozka_mn" ADD CONSTRAINT "Relationship33" FOREIGN KEY ("eshop_pk") REFERENCES "eshop" ("eshop_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "historie_prohlizeni" ADD CONSTRAINT "Relationship34" FOREIGN KEY ("eshop_pk") REFERENCES "eshop" ("eshop_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "historie_prohlizeni" ADD CONSTRAINT "Relationship35" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr_produktu" ADD CONSTRAINT "Relationship36" FOREIGN KEY ("polozka_pk") REFERENCES "polozka" ("polozka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr_produktu" ADD CONSTRAINT "Relationship37" FOREIGN KEY ("parametr_pk") REFERENCES "parametr" ("parametr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr" ADD CONSTRAINT "Relationship38" FOREIGN KEY ("skupina_pk") REFERENCES "skupina_parametru" ("skupina_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "filtr" ADD CONSTRAINT "Relationship39" FOREIGN KEY ("parametr_pk") REFERENCES "parametr" ("parametr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "filtr_kategorie" ADD CONSTRAINT "Relationship40" FOREIGN KEY ("kategorie_pk") REFERENCES "kategorie" ("kategorie_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "filtr_kategorie" ADD CONSTRAINT "Relationship41" FOREIGN KEY ("filtr_pk") REFERENCES "filtr" ("filtr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr_kategorie" ADD CONSTRAINT "Relationship42" FOREIGN KEY ("kategorie_pk") REFERENCES "kategorie" ("kategorie_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "parametr_kategorie" ADD CONSTRAINT "Relationship43" FOREIGN KEY ("parametr_pk") REFERENCES "parametr" ("parametr_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "hodnoceni_produktu" ADD CONSTRAINT "produkt_hodnoceni_produktu" FOREIGN KEY ("polozka_pk") REFERENCES "polozka" ("polozka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "hodnoceni_produktu" ADD CONSTRAINT "hodnotil_uzivatel_pk" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka" ADD CONSTRAINT "uzivatel_zpracovava_objednavku" FOREIGN KEY ("zpracovava_uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;





