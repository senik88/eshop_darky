DROP VIEW IF EXISTS eshop_aktualni CASCADE;
CREATE OR REPLACE VIEW eshop_aktualni AS
    SELECT
          p.*
        , lpad(p.polozka_pk::TEXT, 6, '0') as kod
        , e.eshop_pk
        , e.skladem
        , e.blokace
        , e.viditelne_od
        , e.viditelne_do
        , ec.eshop_cena_pk
        , ec.cena_aktualni
        , ec.cena_doporucena
        , ec.platne_od
        , ec.platne_do
    FROM eshop e
        JOIN polozka p ON p.polozka_pk = e.polozka_pk
        JOIN kategorie k ON p.kategorie_id = k.kategorie_id
        LEFT JOIN eshop_cena ec ON ec.eshop_pk = e.eshop_pk
    WHERE e.viditelne_od <= now()
          AND (e.viditelne_do IS NULL OR e.viditelne_do > now())
          AND ec.platne_od <= now()
          AND (ec.platne_do IS NULL OR ec.platne_do > now())
          -- AND e.skladem - e.blokace > 0
          AND k.lze_zobrazit_zbozi IS TRUE
;


DROP VIEW IF EXISTS kosik_uzivatele CASCADE;
CREATE OR REPLACE VIEW kosik_uzivatele AS
    SELECT
        e.polozka_pk
        , e.polozka_id
        , e.titulek
        , e.cena_aktualni
        , (e.cena_aktualni * opm.mnozstvi) AS cena_celkem
        , e.skladem
        , opm.mnozstvi
        , opm.objednavka_pk
        , o.uzivatel_pk
    FROM objednavka_polozka_mn opm
        JOIN objednavka o ON opm.objednavka_pk = o.objednavka_pk AND o.stav_objednavky_id = 'KOSIK'
        JOIN eshop_aktualni e ON e.eshop_cena_pk = opm.eshop_pk
;


DROP VIEW IF EXISTS objednavky_view;
CREATE OR REPLACE VIEW objednavky_view AS
--     SELECT
--         o.*
--         , opmn.mnozstvi
--         , p.*
--         , ec.cena_aktualni
--         , ec.cena_doporucena
--         , ec.platne_od
--         , ec.platne_do
--         , pd.cena AS cena_dopravy
--     FROM objednavka AS o
--         JOIN objednavka_polozka_mn AS opmn ON o.objednavka_pk = opmn.objednavka_pk
--         JOIN eshop AS e ON opmn.eshop_pk = e.eshop_pk
--         JOIN polozka AS p ON e.polozka_pk = p.polozka_pk
--         LEFT JOIN eshop_cena AS ec ON e.eshop_pk = ec.eshop_pk
--         LEFt JOIN platba_dopravy pd ON o.platba_dopravy_id = pd.platba_dopravy_id
--     WHERE ec.platne_od <= now()
--           AND (ec.platne_do IS NULL OR ec.platne_do > now())

    SELECT
        o.objednavka_pk
        , o.cislo
        , o.cas_vytvoreni
        , o.uzivatel_pk
        , o.stav_objednavky_id
        , sum(opmn.mnozstvi) AS pocet_polozek
        , sum(coalesce(ec.cena_aktualni, 0.00)) AS cena_polozek
        , max(coalesce(pd.cena, 0.00)) AS cena_dopravy
    FROM objednavka AS o
        JOIN objednavka_polozka_mn AS opmn ON o.objednavka_pk = opmn.objednavka_pk
        JOIN eshop AS e ON opmn.eshop_pk = e.eshop_pk
        JOIN polozka AS p ON e.polozka_pk = p.polozka_pk
        LEFT JOIN eshop_cena AS ec ON e.eshop_pk = ec.eshop_pk
        LEFT JOIN platba_dopravy pd ON o.platba_dopravy_id = pd.platba_dopravy_id
    WHERE ec.platne_od <= now()
          AND (ec.platne_do IS NULL OR ec.platne_do > now())
    GROUP BY o.objednavka_pk
;