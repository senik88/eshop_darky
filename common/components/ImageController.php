<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.02.2016
 * Time: 21:28
 */

namespace common\components;


use common\models\Logo;
use yii\web\Controller;

/**
 * Class ImageController
 * @package common\components
 */
class ImageController extends Controller
{
    /**
     * @param $hash
     */
    public function actionLogo($hash)
    {
        $mLogo = (new Logo)->nactiPodleHashe($hash);
        $file = $mLogo->vratCestu(true);

        if (file_exists($file)) {
            $this->odesliSouborNaVystup($file);
        }
    }

    /**
     * @param $hash
     */
    public function actionImage($hash)
    {
        $mLogo = (new Logo)->nactiPodleHashe($hash);
        $file = $mLogo->vratCestu(true);

        if (file_exists($file)) {
            $this->odesliSouborNaVystup($file);
        }
    }

    /**
     * @param $file
     */
    protected function odesliSouborNaVystup($file)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }
}