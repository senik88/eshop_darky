<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 18. 2. 2015
 * Time: 21:21
 */

namespace common\components;

/**
 * Class ItemAliasTrait
 *
 * Třída pro tvorbu čísleníku, které není potřeba editovat.
 *
 * @package common\components
 */
trait ItemAliasTrait
{
    /**
     * metoda vraci preklad hodnoty konstanty ci seznam konstatnt a jejich svazanych nazvu,
     * vice viz ECgComponent::itemAliasData();
     * @param $typ
     * @param null $kod
     * @return bool|array
     */
    public static function itemAlias($typ, $kod = NULL)
    {
        // static zajisti ze oddedena trida bude volat tuto metodu sama nad sebou
        $aItems = static::itemAliasData();
        return static::zpracuj($aItems, $typ, $kod);

    }

    /**
     * funkce k přetížení v jejích potomcích, slouží jako seznam všech potřebných seznamů konstant atd.
     * ukazujících na jejich přeloženou reprezentaci
     * pro danou třídu a též jejího datového podkladu - tabulek
     * příklad:
     *
     * $aItems = array(
     *     'stav' => array(
     *         self::STAV_CEKA_UZAMCENO => IHera::t('hera', 'STAV CEKA UZAMCENO'),
     *         self::STAV_SCHVALENO => IHera::t('hera', 'STAV SCHVALENO'),
     *         self::STAV_ZAMITNUTO => IHera::t('hera', 'STAV ZAMITNUTO'),
     *         self::STAV_KE_ZRUSENI => IHera::t('hera', 'STAV KE ZRUSENI'),
     *         self::STAV_ZRUSENO => IHera::t('hera', 'STAV ZRUSENO'),
     *     ),
     *     'zdroj' => array(
     *         self::ZDROJ_POBOX => 'P.O.Box',
     *         self::ZDROJ_WEB => 'Web'
     *     )
     * );
     * return $aItems
     *
     * @return array
     */
    protected static function itemAliasData()
    {
        return array();
    }


    /**
     * metoda zpracovava obecne vsechny pozadavky na metodu itemAlias()
     * ktera slouzi v modelech na ziskani ciselniku a popisu konstant.
     * metody jsou presmerovany sem a aby byl shodny kod metody jen jednou a to zde,
     * zmena teto metody tedy aktualne ovlivni i chovani itemalias u ECgFormModel a ECgActoveRecord
     * @param $aItems
     * @param $typ
     * @param null $kod
     * @throws \Exception
     * @return bool|array
     */
    protected static function zpracuj($aItems, $typ, $kod = NULL)
    {
        // na zvazenou je pouzit vyjimku...
        if (!is_array($aItems)) Throw New \Exception('Trida potomka musi spravne implementovat statickou metodu itemAliasData() vracejici pole!');

        // pokud je kod pole, cekame ze v nem bude seznam vice pozadovanych klicu
        if (is_array($kod)) {

            $aVysledek = array();

            // nasel jsem typ v poli aliasu
            if (isset($aItems[$typ])) {
                $aTypy = $aItems[$typ];

                // snazim se dotahat pozadovane klice a k nim prelozene hodnoty
                foreach ($kod as $jedenKod) {
                    // pokud klic co chci jeho hodnotu v aliasech je, zaradiim ho
                    if (isset($aTypy[$jedenKod])) {
                        $aVysledek[$jedenKod] = $aTypy[$jedenKod];
                    }
                }
            }
            // vracim pole aliasu daneho typu pro dany seznam kodu v poli
            return $aVysledek;
        }

        // je zadan kod, tedy mam odeslat jednu konkretni polozku
        if (isset($kod)) {
            // pokud je polozka existujici, odeslu jeji nazev
            if (isset($aItems[$typ][$kod])) {
                return $aItems[$typ][$kod];
                // jinak vracim false
            } else {
                return false;
            }
            // neni zadan klic konkretni polozky takze je pozadovan cely seznam
        } else {
            // pokud existuje typ odeslu cely seznam polozek jako pole
            if (isset($aItems[$typ])) {
                return $aItems[$typ];
            } else {
                // jinak vracim false
                return false;
            }
        }
    }
}