<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.09.2015
 * Time: 13:00
 */

namespace common\components\uzivatel;


use common\components\ItemAliasTrait;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class Uzivatel
 * @package common\components\uzivatel
 *
 * @property integer uzivatel_pk
 * @property string heslo
 * @property string email
 * @property string stav
 * @property string rozhrani
 * @property string cas_registrace
 * @property string cas_prihlaseni
 * @property string cas_aktivace
 * @property string ip_registrace
 * @property string ip_prihlaseni
 * @property string validacni_klic
 *
 */
class Uzivatel extends ActiveRecord implements IdentityInterface
{
    use ItemAliasTrait;

    /* -- konstanty -- */
    const ROZHRANI_BACK     = 'BACK';
    const ROZHRANI_FRONT    = 'FRONT';

    const STAV_AKTIVNI      = 'AKTIVNI';
    const STAV_NEAKTIVNI    = 'NEAKTIVNI';
    const STAV_SMAZANY      = 'SMAZANY';
    const STAV_REGISTRACE   = 'REGISTRACE';

    const SCENARIO_PRIDAT   = 'pridat';
    const SCENARIO_UPRAVIT  = 'upravit';
    const SCENARIO_HLEDAT   = 'hledat';
    /* --------------- */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uzivatel';
    }

    /**
     * @return array
     */
    public static function primaryKey()
    {
        return ['uzivatel_pk'];
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @param string $rozhrani
     * @return null|static
     */
    public static function findByUsername($username, $rozhrani)
    {
        return static::findOne(['email' => $username, 'rozhrani' => $rozhrani, 'stav' => self::STAV_AKTIVNI]);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->uzivatel_pk;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->heslo);
    }

    /**
     * @inheritdoc
     */
    protected static function itemAliasData()
    {
        return [
            'stavy' => [
                Uzivatel::STAV_AKTIVNI => 'Aktivní',
                Uzivatel::STAV_REGISTRACE => 'Registrace',
                Uzivatel::STAV_NEAKTIVNI => 'Neaktivní',
                Uzivatel::STAV_SMAZANY => 'Smazaný'
            ]
        ];
    }
}