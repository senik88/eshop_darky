<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 10.10.2015
 * Time: 16:38
 */

namespace common\components;


use yii\data\SqlDataProvider as YiiSqlDataProvider;
use yii\db\Connection;

/**
 * Class SqlDataProvider
 * @package common\components
 */
class SqlDataProvider extends YiiSqlDataProvider
{
    /**
     * @inheritdoc
     */
    protected function prepareTotalCount()
    {
        $yii = \Yii::$app;

        /** @var $connection Connection */
        $connection = (($this->db !== null) ? $this->db : $yii->db);

        $command = $connection->createCommand("SELECT count(*) FROM ({$this->sql}) AS subselect");
        $command->bindValues($this->params);
        $res = $command->queryScalar();

        return (int) $res;
    }
}