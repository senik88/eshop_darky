<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.10.2015
 * Time: 14:36
 */

namespace common\components\helpers;

use Imagine\Image\Box;
use Imagine\Image\Color;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

/**
 * Class Image
 * @package common\components\helpers
 */
class Image
{

    const LARGE_IMAGE = '1000x1000';
    const SMALL_IMAGE = '180x180';

    public static function saveTemporaryImage($path, $filename, $name, $size)
    {
        list($width, $height) = explode('x', $size);

        $name = $path . DIRECTORY_SEPARATOR . $name;

        $file = (new Imagine())->open($filename);

        $i_width = $file->getSize()->getWidth();
        $i_height = $file->getSize()->getHeight();

        $orgSize = $file->getSize();

        // velky obrazek zmensim na pozadovynou velikost
        // ale zachovam pomer stran
        if ($size == self::LARGE_IMAGE) {
            if ($i_width > $i_height) {
                if ($file->getSize()->getWidth() > $width) {
                    $file->resize($file->getSize()->widen($width));
                }
            } else {
                if ($file->getSize()->getHeight() > $height) {
                    $file->resize($file->getSize()->heighten($height));
                }
            }

            $file->thumbnail(new Box($width, $height), ManipulatorInterface::THUMBNAIL_OUTBOUND)
                ->save($name);
        }
        // ale nahled chci mit ctvercovy, takze to musim cele prepocitat
        else {
            $target = new Box($width, $height);

            if ($i_width > $i_height) {
                $h = $orgSize->getHeight() * ($target->getWidth() / $orgSize->getWidth());
                $cropBy = new Point( 0, ( max($target->getHeight() - $h , 0 ) ) / 2);
            } else {
                $w = $orgSize->getWidth() * ( $target->getHeight() / $orgSize->getHeight() );
                $cropBy = new Point( ( max ($target->getWidth() - $w, 0 ) ) / 2, 0);
            }

            $tempBox = new Box($width, $height);

            $color = new Color('#FFF', 100);
            $image = (new Imagine)->create($tempBox, $color);

            if ($i_width > $i_height) {
                if ($file->getSize()->getWidth() > $width) {
                    $file->resize($file->getSize()->widen($width));
                }
            } else {
                if ($file->getSize()->getHeight() > $height) {
                    $file->resize($file->getSize()->heighten($height));
                }
            }

            $image->paste($file, $cropBy)
                ->save($name);
        }

        return $name;
    }
}