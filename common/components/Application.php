<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.09.2015
 * Time: 20:37
 */

namespace common\components;


use Yii;

class Application {

    /**
     * todo bude to chtit brat nekde z konfigu, produkcni URL a tak
     */
    public static function jeBackend()
    {
        if (YII_ENV_PROD) {
            return $_SERVER['SERVER_NAME'] == "eshop-admin.senovo.cz";
        } else {
            return strpos($_SERVER['SCRIPT_NAME'], '/backend/') !== false;
        }
    }

    /**
     * doimplementovat
     */
    public static function log($message, $level)
    {

    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashError($message, $remove = true)
    {
        $type = 'danger';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashSuccess($message, $remove = true)
    {
        $type = 'success';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashInfo($message, $remove = true)
    {
        $type = 'info';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashWarning($message, $remove = true)
    {
        $type = 'warning';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $type
     * @param string $message
     * @param bool $remove
     */
    protected static function setFlash($type, $message, $remove)
    {
        Yii::$app->session->addFlash($type, $message, $remove);
    }

}