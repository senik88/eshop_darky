<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 28.02.2016
 * Time: 10:10
 */

namespace common\models;


use common\components\ItemAliasTrait;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class Parametr extends Model
{
    use ItemAliasTrait;

    const TYP_INTEGER   = 'INTEGER';
    const TYP_NUMERIC   = 'NUMERIC';
    const TYP_TEXT      = 'TEXT';
    const TYP_ARRAY     = 'ARRAY';

    /**
     * @var
     */
    public $parametr_pk;

    /**
     * @var
     */
    public $parametr_id;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $hodnota_typ;

    /**
     * @var
     */
    public $skupina_pk;

    /**
     * @var
     */
    public $jednotka;

    /**
     * @var
     */
    public $poradi;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     *
     */
    public function uloz()
    {
        $sql = "select parametr_zmena(:pk, :id, :nv, :ht, :jk, :sp)";
        $data = [
            ':pk' => $this->parametr_pk == '' ? null : $this->parametr_pk,
            ':id' => $this->parametr_id,
            ':nv' => $this->nazev,
            ':ht' => $this->hodnota_typ,
            ':jk' => $this->jednotka,
            ':sp' => $this->skupina_pk
        ];

        $pk = \Yii::$app->db->createCommand($sql)->bindValues($data)->queryScalar();

        if ($pk !== false) {
            return $pk;
        } else {
            return false;
        }
    }

    /**
     * @param integer|null $pk
     * @return Parametr|null
     */
    public function nactiPodlePk($pk = null)
    {
        if ($pk == null) {
            $pk = $this->parametr_pk;
        }

        $sql = "SELECT * FROM parametr WHERE parametr_pk = :pk";
        $params = [
            ':pk' => $pk
        ];

        return $this->nacti($sql, $params);
    }

    /**
     * @param string|null $id
     * @return Parametr|null
     */
    public function nactiPodleId($id = null)
    {
        if ($id == null) {
            $id = $this->parametr_id;
        }

        $sql = "SELECT * FROM parametr WHERE parametr_id = :id";
        $params = [
            ':id' => $id
        ];

        return $this->nacti($sql, $params);
    }

    /**
     * @param $sql
     * @param $params
     * @return Parametr|null
     */
    protected function nacti($sql, $params)
    {
        $data = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryOne();

        if ($data !== false && $this->load($data, '')) {
            return $this;
        } else {
            return null;
        }
    }

    /**
     *
     */
    public function najdiPakUloz()
    {
        $sql = "SELECT parametr_pk FROM parametr WHERE parametr_id = :id";
        $pk = \Yii::$app->db->createCommand($sql)->bindValue(':id', $this->parametr_id)->queryScalar();

        if ($pk !== false) {
            $this->parametr_pk = $pk;
        }

        return $this->uloz();
    }

    /**
     * @return string|null
     */
    public function formatujNazev()
    {
        if ($this->nazev == null) {
            return null;
        }

        $output = $this->nazev;

        if ($this->jednotka != null) {
            $output .= " [" . $this->jednotka . "]";
        }

        return $output;
    }

    /**
     * @return array
     */
    public function vratProDropdown()
    {
        $sql = "
            SELECT
                *
                , CASE
                    WHEN jednotka IS NOT NULL THEN nazev || ' [' || jednotka || ']'
                    ELSE nazev
                END AS nazev_formatovany
            FROM parametr ORDER BY nazev ASC";

        $data = \Yii::$app->db->createCommand($sql)->queryAll();

        return ArrayHelper::map($data, 'parametr_pk', 'nazev_formatovany');
    }

    /**
     *
     */
    public static function itemAliasData()
    {
        return [
            'typy' => [
                self::TYP_INTEGER => 'Celé číslo',
                self::TYP_NUMERIC => 'Desetinné číslo',
                self::TYP_TEXT => 'Text',
                self::TYP_ARRAY => 'Pole [ ; ]'
            ]
        ];
    }
}