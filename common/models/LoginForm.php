<?php
namespace common\models;

use backend\modules\uzivatel\models\UzivatelBackend;
use common\components\Application;
use common\components\uzivatel\Uzivatel;
use frontend\modules\uzivatel\models\UzivatelFrontend;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Uživatelské jméno',
            'password' => 'Heslo',
            'rememberMe' => 'Pamatovat si mě příště'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return Uzivatel|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            if (Application::jeBackend()) {
                /** @var $class UzivatelBackend */
                $class = 'backend\modules\uzivatel\models\UzivatelBackend';
            } else {
                /** @var $class UzivatelFrontend */
                $class = 'frontend\modules\uzivatel\models\UzivatelFrontend';
            }

            $this->_user = $class::findByUsername($this->username);
        }

        return $this->_user;
    }
}
