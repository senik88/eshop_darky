<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 22.05.2016
 * Time: 17:48
 */

namespace common\models;


use yii\base\Model;

/**
 * Class AbstractUdaje
 * @package common\models
 */
abstract class AbstractUdaje extends Model
{
    /**
     * @var integer
     */
    public $objednavka_pk;

    /**
     * @var integer
     */
    public $uzivatel_pk;

    /**
     * Ulozi udaje k uzivateli, pk uzivatele musi byt predem nastavene do modelu
     * @return boolean
     */
    abstract public function ulozUzivateli();

    /**
     * Ulozi udaje k objednavce, pk objednavky musi byt predem nastavene do modelu
     * @return boolean
     */
    abstract public function ulozObjednavce();

    /**
     * @param integer $pk identifikace udaju (primarni klic)
     * @return mixed
     */
    abstract public function nactiPodlePk($pk);

    /**
     * @param integer $uzivatel ID uzivatele
     * @return array
     */
    abstract public function vratUzivatelovyProDropdown($uzivatel);

    /**
     * Na zaklade template vykresli udaje jako string - radkovany.
     * Vhodne do sablony jako adresa.
     * @return mixed
     */
    abstract public function vypisBlokove();

    /**
     * Na zaklade template vykresli udaje jako string - v jednom radku.
     * Vhodne do formularu jako label selectu nebo radio buttonu.
     * @return mixed
     */
    abstract public function vypisRadkove();
}