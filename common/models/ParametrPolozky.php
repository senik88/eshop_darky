<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 18.06.2016
 * Time: 12:06
 */

namespace common\models;

/**
 * Class ParametrPolozky
 * @package common\models
 */
class ParametrPolozky extends Parametr
{
    /**
     * @var integer
     */
    public $polozka_pk;

    /**
     * @var
     */
    public $parametr_pk;

    /**
     * @var string|integer|float
     */
    public $hodnota;

    /**
     * tady staci prachsprosty insert, protoze pred tim vsechny parametry mazu
     */
    public function ulozHodnotu()
    {
        $sql = "INSERT INTO parametr_produktu (polozka_pk, parametr_pk, hodnota) VALUES (:polozka, :parametr, :hodnota)";
        $params = [
            ':polozka' => $this->polozka_pk,
            ':parametr' => $this->parametr_pk,
            ':hodnota' => $this->hodnota
        ];

        return \Yii::$app->db->createCommand($sql)->bindValues($params)->execute();
    }
}