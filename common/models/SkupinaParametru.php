<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 28.02.2016
 * Time: 10:24
 */

namespace common\models;


use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class SkupinaParametru
 * @package common\models
 */
class SkupinaParametru extends Model
{
    /**
     * @var
     */
    public $skupina_pk;

    /**
     * @var
     */
    public $skupina_id;

    /**
     * @var
     */
    public $nazev;

    /**
     *
     */
    public function vratProDropdown()
    {
        $sql = "select * from skupina_parametru";

        $data = \Yii::$app->db->createCommand($sql)->queryAll();

        if (empty($data)) {
            return $data;
        } else {
            return ArrayHelper::map($data, 'skupina_pk', 'nazev');
        }
    }
}