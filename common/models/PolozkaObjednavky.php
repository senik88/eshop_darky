<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.11.2015
 * Time: 19:00
 */

namespace common\models;


use yii\base\Model;

/**
 * Class PolozkaObjednavky
 * @package common\models
 */
class PolozkaObjednavky extends Model
{
    public $objedanvka_pk;

    public $eshop_cena_pk;

    public $mnozstvi;

    public $blokace;

    public $mPolozka;
}