<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17. 10. 2015
 * Time: 21:17
 */

namespace common\models;


use common\components\ItemAliasTrait;
use common\components\SqlDataProvider;
use yii\base\Model;

/**
 * Class Objednavka
 * @package common\models
 */
class Objednavka extends Model
{
    use ItemAliasTrait;

    const SCENARIO_DOPRAVA = 'doprava';
    const SCENARIO_UDAJE = 'udaje';
    const SCENARIO_POTVRZENI = 'potvrzeni';

    const STAV_KOSIK = 'KOSIK';
    const STAV_ZALOZENA = 'ZALOZENA';
    const STAV_POTVRZENA = 'POTVRZENA';
    const STAV_ZPRACOVAVA = 'ZPRACOVAVA_SE';
    const STAV_ODESLANA = 'ODESLANA';
    const STAV_STORNO = 'STORNO';

    /**
     * @var
     */
    public $objednavka_pk;

    /**
     * @var string ve formatu 21YYMMAAAAA, kde A = poradi objednavky
     */
    public $cislo;

    /**
     * @var
     */
    public $uzivatel_pk;

    /**
     * @var
     */
    public $cas_vytvoreni;

    /**
     * @var
     */
    public $cas_zmeny;

    /**
     * @var
     */
    public $stav_objednavky_id;

    /**
     * @var
     */
    public $platba_dopravy_id;

    /**
     * @var
     */
    public $fakturacni_udaje_pk;

    /**
     * @var
     */
    public $dodaci_udaje_pk;

    /**
     * @var
     */
    public $mPlatbaDopravy;

    /**
     * @var
     */
    public $mFakturacniUdaje;

    /**
     * @var
     */
    public $mDodaciUdaje;

    /**
     * @var
     */
    public $udaje_stejne;

    /**
     * @var
     */
    public $poznamka;

    /**
     * @var
     */
    public $oznaceni_uzivatel;

    /**
     * @var
     */
    public $souhlas;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        //$scenarios[self::SCENARIO_DOPRAVA] = [''];
        $scenarios[self::SCENARIO_UDAJE] = [
            'objednavka_pk', 'udaje_stejne', 'dodaci_udaje_pk', 'fakturacni_udaje_pk', 'platba_dopravy_id', 'uzivatel_pk'
        ];
        $scenarios[self::SCENARIO_POTVRZENI] = [
            'objednavka_pk', 'poznamka', 'oznaceni_uzivatel',  'dodaci_udaje_pk', 'fakturacni_udaje_pk', 'platba_dopravy_id', 'souhlas'
        ];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        // objednavka krok dva
        if ($this->scenario == self::SCENARIO_UDAJE) {
            return [
                [['fakturacni_udaje_pk'], 'required'],
                [['dodaci_udaje_pk'], 'required', 'when' => function($model) {
                    return $model->udaje_stejne == 0;
                }, 'whenClient' => 'function(attribute, value) {
                              return $("#objednavka-udaje_stejne").val() != "";
                          }'],
                [['objednavka_pk', 'dodaci_udaje_pk', 'udaje_stejne', 'platba_dopravy_id', 'uzivatel_pk'], 'safe']
            ];
        }
        // objednavka krok tri
        else if ($this->scenario == self::SCENARIO_POTVRZENI) {
            return [
                [['objednavka_pk', 'poznamka', 'oznaceni_uzivatel', 'dodaci_udaje_pk', 'udaje_stejne', 'platba_dopravy_id'], 'safe'],
                ['souhlas', 'required', 'requiredValue' => 1, 'message' => 'Musíte zaškrtnout souhlas.']
            ];
        }
        // obecne validace
        else {
            return [
                [$this->attributes(), 'safe']
            ];
        }
    }

    /**
     * @param $id
     * @return Objednavka|null
     */
    public function nactiPodleCisla($id)
    {
        $sql = "
            select * from objednavka where cislo = :cislo
        ";
        $params = [
            ':cislo' => $id
        ];

        $data = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryOne();

        if ($data === false) {
            return null;
        }

        if (!$this->load($data, '')) {
            return null;
        } else {
            // $this->nactiSouhrn();
            return $this;
        }
    }

    /**
     * @param $id
     * @return Objednavka|null
     */
    public function nactiPodlePk($id)
    {
        $sql = "
            select * from objednavka where objednavka_pk = :cislo
        ";
        $params = [
            ':cislo' => $id
        ];

        $data = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryOne();

        if ($data === false) {
            return null;
        }

        if (!$this->load($data, '')) {
            return null;
        } else {
            // $this->nactiSouhrn();
            return $this;
        }
    }

    /**
     *
     */
    protected function nactiSouhrn()
    {
        $celkem_sql = "
            SELECT
                  sum(ku.pocet_polozek) AS polozek
                , sum(ku.cena_polozek) AS castka
                , coalesce(pd.cena, 0.00) as doprava
            FROM objednavky_view ku
                JOIN objednavka o ON o.objednavka_pk = ku.objednavka_pk
                LEFT JOIN platba_dopravy pd ON o.platba_dopravy_id = pd.platba_dopravy_id
            WHERE ku.uzivatel_pk = :uid
            GROUP BY pd.cena
        ";

        $params = [':uid' => \Yii::$app->user->id];

        $celkem = \Yii::$app->db->createCommand($celkem_sql)->bindValues($params)->queryOne();

        return $celkem;
    }

    /**
     * @return Objednavka|null
     */
    public function nactiObjednavkuKosiku()
    {
        $sql = "
            SELECT
                o.*
                , t.cena_zbozi
                , coalesce(p.cena, 0.00) AS cena_doprava
                , (t.cena_zbozi + coalesce(p.cena, 0.00)) AS cena_celkem
            FROM objednavka o
                JOIN (
                    SELECT
                        k.objednavka_pk
                        , sum(k.cena_celkem) AS cena_zbozi
                    FROM kosik_uzivatele k
                    WHERE k.uzivatel_pk = :pk
                    GROUP BY k.objednavka_pk
                ) AS t ON t.objednavka_pk = o.objednavka_pk
                LEFT JOIN platba_dopravy p ON p.platba_dopravy_id = o.platba_dopravy_id
        ";
        $data = \Yii::$app->db->createCommand($sql)->bindValue(':pk', \Yii::$app->user->id)->queryOne();

        // tady vyhodit chybu, protoze tohle je blbost, aby mi to z kosiku vratilo pk a pak to nenaslo objednavku
        if ($data == false) {
            return null;
        }

        if (!$this->load($data, '')) {
            \Yii::error("nepodarilo se do objednavky nacist data: " . var_export($data, true));
            return null;
        }

        return $this;
    }

    /**
     * @return SqlDataProvider
     */
    public function nactiPolozky()
    {
        $sql = "
            select
                p.polozka_pk
                , p.polozka_id
                , p.titulek
                , p.subtitulek
                , ec.cena_aktualni
                , (ec.cena_aktualni * (opm.mnozstvi)::numeric) AS cena_celkem
                , e.skladem
                , opm.mnozstvi
                , opm.objednavka_pk
                , o.uzivatel_pk
                , po.nahled
                , o.stav_objednavky_id AS stav
            from objednavka o
                join objednavka_polozka_mn opm on o.objednavka_pk = opm.objednavka_pk
                join eshop e on opm.eshop_pk = e.eshop_pk
                join polozka p on e.polozka_pk = p.polozka_pk
                join eshop_cena ec on e.eshop_pk = ec.eshop_pk
                left join polozka_obrazek po on po.polozka_pk = p.polozka_pk and po.hlavni
            where o.objednavka_pk = :opk
                and ec.platne_od <= o.cas_zmeny and (ec.platne_do > o.cas_zmeny or ec.platne_do is null)
        ";
        $params = [
            ':opk' => $this->objednavka_pk
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => false
        ]);
    }

    /**
     * Nastavi platbe kombinaci dopravy a platby
     *
     * @param $doprava
     * @param $platba
     *
     * @return bool
     */
    public function nastavDopravuPlatbu($doprava, $platba)
    {
        if ($this->objednavka_pk == null) {
            \Yii::error("Nemuzu nastavit objednavce platbu a dopravu, neznam objednavku!");
            return false;
        }

        $mPlatbaDopravy = PlatbaDopravy::findOne(['zpusob_dopravy_pk' => $doprava, 'zpusob_platby_pk' => $platba]);

        if ($mPlatbaDopravy == null) {
            \Yii::error("Nemuzu nastavit objednavce platbu a dopravu, neznama platba ({$platba}) a doprava ({$doprava})!");
            return false;
        }

        $trans = \Yii::$app->db->beginTransaction();

        $sql = "update objednavka set platba_dopravy_id = :id where objednavka_pk = :pk";

        $result = \Yii::$app->db->createCommand($sql)->bindValues([
            ':id' => $mPlatbaDopravy->platba_dopravy_id,
            ':pk' => $this->objednavka_pk
        ])->execute();

        if ($result != 1) {
            $trans->rollBack();
            return false;
        } else {
            $trans->commit();
            return true;
        }
    }

    /**
     * Delam jen update objednavky, ty udaje si budu drzet u uzivatele i objednavky.
     * V pripade, ze by je uzivatel editoval, tak neudelam update udaju, ale insert a prehodim propojovaci tabulku.
     * Pokud by je uzivatel chtel smazat, tak jen smazu propojovaci tabulku.
     *
     * @param $fapk
     * @return bool
     * @throws \Exception
     */
    public function nastavFakturacniUdaje($fapk)
    {
        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();

        $sql = "UPDATE objednavka SET fakturacni_udaje_pk = :fpk WHERE objednavka_pk = :opk";
        $params = [
            'fpk' => $fapk,
            'opk' => $this->objednavka_pk
        ];

        try {
            $result = \Yii::$app->db->createCommand($sql, $params)->execute();

            if ($result !== 1) {
                throw new \Exception("chyba pri prirazeni fakturacnich udaju ({$fapk}) objednavce ({$this->objednavka_pk}), pocet updatnutych zaznamu = ({$result})");
            } else {
                $transaction->commit();
                return true;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * Delam jen update objednavky, ty udaje si budu drzet u uzivatele i objednavky.
     * V pripade, ze by je uzivatel editoval, tak neudelam update udaju, ale insert a prehodim propojovaci tabulku.
     * Pokud by je uzivatel chtel smazat, tak jen smazu propojovaci tabulku.
     *
     * @param $dopk
     * @return bool
     * @throws \Exception
     */
    public function nastavDodaciUdaje($dopk)
    {
        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();

        $sql = "UPDATE objednavka SET dodaci_udaje_pk = :dpk WHERE objednavka_pk = :opk";
        $params = [
            'dpk' => $dopk,
            'opk' => $this->objednavka_pk
        ];

        try {
            $result = \Yii::$app->db->createCommand($sql, $params)->execute();

            if ($result !== 1) {
                throw new \Exception("chyba pri prirazeni dodacich udaju ({$dopk}) objednavce ({$this->objednavka_pk}), pocet updatnutych zaznamu = ({$result})");
            } else {
                $transaction->commit();
                return true;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     *
     */
    public function dokoncit()
    {
        // nastavit objednavku na stav POTVRZENA (nebo tak neco)
        // odecist skladove zasoby - jak vlastne resit s tema importama od mysaka
        // odeslat email s potvrzenim objednavky
        $sql = "select * from objednavka_dokoncit(:pk, :oznaceni, :poznamka)";
        $params = [
            'pk' => $this->objednavka_pk,
            'oznaceni' => $this->oznaceni_uzivatel,
            'poznamka' => $this->poznamka
        ];

        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {
            $result = \Yii::$app->db->createCommand($sql, $params)->queryOne();

            if ($result['r_vysledek'] == 'OK') {
                $transaction->commit();
                $this->cislo = $result['r_cislo'];
                return;
            } else {
                throw new \Exception($result['r_vysledek']);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @return array
     */
    protected static function itemAliasData()
    {
        return [
            'stav' => [
                self::STAV_KOSIK => 'Zboží v košíku',
                self::STAV_ZALOZENA => 'Založená',
                self::STAV_POTVRZENA => 'Potvrzená',
                self::STAV_ZPRACOVAVA => 'Zpracovává se',
                self::STAV_ODESLANA => 'Odeslaná',
                self::STAV_STORNO => 'Stornovaná'
            ]
        ];
    }
}