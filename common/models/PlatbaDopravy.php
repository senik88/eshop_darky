<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.11.2015
 * Time: 19:28
 */

namespace common\models;


use Exception;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/**
 * Class PlatbaDopravy
 * @package common\models
 */
class PlatbaDopravy extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $query = self::find();

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'id' => [
                'attribute' => 'platba_dopravy_pk',
                'label' => 'ID'
            ],
            'zpusob_dopravy_pk' => [
                'value' => function ($model) {
                    $mDoprava = ZpusobDopravy::findOne($model->zpusob_dopravy_pk);
                    return $mDoprava->nazev;
                },
                'label' => 'Doprava'
            ],
            'zpusob_platby_pk' => [
                'value' => function ($model) {
                    $mPlatba = ZpusobPlatby::findOne($model->zpusob_platby_pk);
                    return $mPlatba->nazev;
                },
                'label' => 'Platba'
            ],
            'cena' => [
                'attribute' => 'cena',
                'label' => 'Cena'
            ],
            'viditelne_od' => [
                'value' =>  function ($model) {
                    return \Yii::$app->getFormatter()->asDatetime($model->viditelne_od);
                },
                'label' => 'Viditelné od',
                'format' => 'raw'
            ],
            'viditelne_do' => [
                'value' => function ($model) {
                    return \Yii::$app->getFormatter()->asDatetime($model->viditelne_do);
                },
                'label' => 'Viditelné do',
                'format' => 'raw'
            ],
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/eshop/dopravy/detail', 'id' => $model['platba_dopravy_pk']);
                            break;
                        case 'update':
                            $url = array('/eshop/dopravy/upravit', 'id' => $model['platba_dopravy_pk']);
                            break;
                        case 'delete':
                            $url = array('/eshop/dopravy/smazat', 'id' => $model['platba_dopravy_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil',
                            ]),
                            $url,
                            [
                                'title' => 'Upravit',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-trash',
                            ]),
                            $url,
                            [
                                'title' => 'Smazat',
                                'data-pjax' => 0,
                                'data-confirm' => "Opravdu chcete smazat doprava a platbu?",
                                'data-method' => "post",
                            ]
                        );
                    }
                ),
            ]
        ];
    }

}