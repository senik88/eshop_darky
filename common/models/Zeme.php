<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 19.07.2016
 * Time: 21:33
 */

namespace common\models;

/**
 * Class Zeme
 * @package common\models
 */
class Zeme {

    public static $seznam = [
        'CZ' => 'Česká republika',
        'SK' => 'Slovenská republika'
    ];

    /**
     * @param $kod
     * @return null
     */
    public static function vratPodleKodu($kod)
    {
        if (isset(self::$seznam[$kod])) {
            return self::$seznam[$kod];
        } else {
            return $kod;
        }
    }

}