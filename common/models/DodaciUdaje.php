<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.11.2015
 * Time: 18:44
 */

namespace common\models;


use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DodaciUdaje
 * @package common\models
 */
class DodaciUdaje extends AbstractUdaje
{

    const SCENARIO_OBJEDNAVKA = 'objednavka';

    public $dodaci_udaje_pk;
    public $jmeno;
    public $prijmeni;
    public $titulPred;
    public $titulZa;
    public $ulice;
    public $mesto;
    public $psc;
    public $zeme;
    public $telefon;
    public $poznamka;

    public $stejne_jako_fakturacni = 0;

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_OBJEDNAVKA] = ['jmeno', 'prijmeni', 'ulice', 'mesto', 'psc', 'zeme', 'telefon', 'poznamka', 'dodaci_udaje_pk'];

        return $scenarios;
    }

    /**
     * @return array
     */
    public function rules()
    {
        if ($this->scenario == self::SCENARIO_OBJEDNAVKA) {
            return [
                [['jmeno', 'prijmeni', 'ulice', 'mesto', 'psc', 'zeme'], 'required',
//                    'when' => function($model) {
//                        return $model->stejne_jako_fakturacni === 0;
//                    },
                    'whenClient' => 'function(attribute, value) {
                        return $("#objednavka-udaje_stejne").prop("checked") == false;
                }'],
                [['dodaci_udaje_pk', 'telefon', 'poznamka'], 'safe']
            ];
        } else {
            return [
                [$this->attributes(), 'safe']
            ];
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'jmeno' => 'Jméno',
            'prijmeni' => 'Příjmení',
            'titulPred' => 'Titul před',
            'titulZa' => 'Titul za',
            'ulice' => 'Ulice, č.p.',
            'mesto' => 'Město',
            'psc' => 'PSČ',
            'zeme' => 'Země',
            'telefon' => 'Telefon',
            'poznamka' => 'Poznámka',
            'stejne_jako_fakturacni' => 'Stejné jako fakturační'
        ];
    }

    /**
     * @param $dodaci_udaje_pk
     * @return DodaciUdaje
     */
    public function nactiPodlePk($dodaci_udaje_pk)
    {
        $sql = "select * from dodaci_udaje where dodaci_udaje_pk = :pk";
        $params = [
            'pk' => $dodaci_udaje_pk
        ];

        return $this->nactiPodleSql($sql, $params);
    }

    /**
     * @param $sql
     * @param $params
     * @return DodaciUdaje
     */
    public function nactiPodleSql($sql, $params)
    {
        $data = \Yii::$app->db->createCommand($sql, $params)->queryOne();

        if ($data == false) {
            ddd('chyba', $sql, $params);
        }

        if (!$this->load($data, '')) {
            ddd('not load', $data);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function ulozObjednavce()
    {
        $sql = "select * from objednavka_uloz_dodaci(
              :objednavka_pk
            , :do_udaje_pk
            , :jmeno
            , :prijmeni
            , :titul_pred
            , :titul_za
            , :ulice
            , :mesto
            , :psc
            , :zeme
            , :telefon
            , :poznamka
        )";

        $params = [
              ':objednavka_pk' => $this->objednavka_pk
            , ':do_udaje_pk' => ($this->dodaci_udaje_pk == '' ? null : $this->dodaci_udaje_pk)
            , ':jmeno' => $this->jmeno
            , ':prijmeni' => $this->prijmeni
            , ':titul_pred' => $this->titulPred
            , ':titul_za' => $this->titulZa
            , ':ulice' => $this->ulice
            , ':mesto' => $this->mesto
            , ':psc' => $this->psc
            , ':zeme' => $this->zeme
            , ':poznamka' => $this->poznamka
            , ':telefon' => $this->telefon
        ];

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        $pk = $command->queryScalar();
        if ($pk < 0) {
            Yii::error("neocekavany vysledek procedury objednavka_uloz_dodaci = ({$pk}), data: " . var_export($params, true));
            return false;
        } else {
            $this->dodaci_udaje_pk = $pk;
            return $pk;
        }
    }

    /**
     *
     */
    public function ulozUzivateli()
    {
        $sql = "select * from uzivatel_uloz_dodaci(
              :uzivatel_pk
            , :do_udaje_pk
            , :jmeno
            , :prijmeni
            , :titul_pred
            , :titul_za
            , :ulice
            , :mesto
            , :psc
            , :zeme
            , :telefon
            , :poznamka
        )";

        $params = [
            ':uzivatel_pk' => $this->uzivatel_pk
            , ':do_udaje_pk' => ($this->dodaci_udaje_pk == '' ? null : $this->dodaci_udaje_pk)
            , ':jmeno' => $this->jmeno
            , ':prijmeni' => $this->prijmeni
            , ':titul_pred' => $this->titulPred
            , ':titul_za' => $this->titulZa
            , ':ulice' => $this->ulice
            , ':mesto' => $this->mesto
            , ':psc' => $this->psc
            , ':zeme' => $this->zeme
            , ':telefon' => $this->telefon
            , ':poznamka' => $this->poznamka
        ];

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        $pk = $command->queryScalar();
        if ($pk < 0) {
            Yii::error("neocekavany vysledek procedury uzivatel_uloz_fakturacni = ({$pk}), data: " . var_export($params, true));
            return false;
        } else {
            $this->dodaci_udaje_pk = $pk;
            return $pk;
        }
    }

    /**
     * @param $uzivatel
     * @return array
     */
    public function vratUzivatelovyProDropdown($uzivatel)
    {
        $sql = "
            SELECT
                du.dodaci_udaje_pk AS pk
                , format('%s %s, %s, %s %s, %s', du.prijmeni, du.jmeno, du.ulice, du.psc, du.mesto, du.zeme) udaje
            FROM dodaci_udaje du
                JOIN uzivatel_dodaci_udaje udu ON du.dodaci_udaje_pk = udu.dodaci_udaje_pk
            WHERE udu.uzivatel_pk = :pk
            ORDER BY udaje ASC
        ";

        $data = \Yii::$app->db->createCommand($sql)->bindValue(':pk', $uzivatel)->queryAll();

        return ArrayHelper::map($data, 'pk', 'udaje');
    }

    /**
     * @return string
     */
    public function vypisBlokove()
    {
        $template = '<ul>';
        {
            $template .= '<li>{jmeno} {prijmeni}</li>';
            $template .= '<li>{ulice}, {psc} {mesto}</li>';
            $template .= '<li>{zeme}</li>';
        }
        $template .= '</ul>';

        return strtr($template, [
            '{jmeno}' => $this->jmeno,
            '{prijmeni}' => $this->prijmeni,
            '{ulice}' => $this->ulice,
            '{mesto}' => $this->mesto,
            '{psc}' => $this->psc,
            '{zeme}' => $this->zeme,
        ]);
    }

    public function vypisRadkove()
    {

    }
}