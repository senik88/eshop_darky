<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.10.2015
 * Time: 14:34
 */

namespace common\models;


use yii\db\ActiveRecord;

/**
 * Class PolozkaObrazek
 *
 * @property integer polozka_obrazek_pk
 * @property string zdroj
 * @property string nahled
 * @property string popisek
 * @property boolean hlavni
 * @property string nahrano
 * @property integer polozka_pk
 * @property integer uzivatel_pk
 *
 * @package common\models
 */
class PolozkaObrazek extends ActiveRecord
{
    /**
     * @return array
     */
    public static function primaryKey()
    {
        return ['polozka_obrazek_pk'];
    }


}