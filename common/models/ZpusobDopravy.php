<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 30.09.2015
 * Time: 20:49
 */

namespace common\models;


use Exception;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/**
 * Class ZpusobDopravy
 * @package common\models
 *
 * @property integer $zpusob_dopravy_pk
 * @property string $nazev
 * @property integer $logo_pk
 * @property boolean $ma_tracking
 */
class ZpusobDopravy extends ActiveRecord
{

    public function rules()
    {
        return [
            [['nazev', 'ma_tracking'], 'safe']
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * @param int $pagination
     * @return ActiveDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $query = self::find();

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'id' => [
                'attribute' => 'zpusob_dopravy_pk',
                'label' => 'ID'
            ],
            'nazev',
            'akce' => [
                'class' => ActionColumn::className(),
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/eshop/dopravy/detail-dopravy', 'id' => $model['zpusob_dopravy_pk']);
                            break;
                        case 'update':
                            $url = array('/eshop/dopravy/upravit-dopravu', 'id' => $model['zpusob_dopravy_pk']);
                            break;
                        case 'delete':
                            $url = array('/eshop/dopravy/smazat-dopravu', 'id' => $model['zpusob_dopravy_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil',
                            ]),
                            $url,
                            [
                                'title' => 'Upravit',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-trash',
                            ]),
                            $url,
                            [
                                'title' => 'Smazat',
                                'data-pjax' => 0,
                                'data-confirm' => "Opravdu chcete smazat dodavatele?",
                                'data-method' => "post",
                            ]
                        );
                    }
                ),
            ]
        ];
    }



    public static function nactiProDropdownSlogy() {
        /*
         *
         * <?= $form->field($mUdalost, 'barva')->dropDownList(array(
        '#d06b64' => 'defaultní',
        '#5484ed' => 'modrá',
        '#a4bdfc' => 'světle modrá',
        '#46d6db' => 'tyrkysová',
        '#7ae7bf' => 'světle tyrkysová',
        '#51b749' => 'zelená',
        '#fbd75b' => 'žlutá',
        '#ffb878' => 'oranžová',
        '#ff887c' => 'lososová',
        '#dc2127' => 'červená',
        '#dbadff' => 'fialová',
        '#e1e1e1' => 'šedá',
    ), array(
        'class' => 'selectpicker',
        'options' => array(
            '#d06b64' => array('data-content' => '<span class="label" style="background-color: #d06b64;">defaultní</span>'),
            '#5484ed' => array('data-content' => '<span class="label" style="background-color: #5484ed;">modrá</span>'),
            '#a4bdfc' => array('data-content' => '<span class="label" style="background-color: #a4bdfc;">světle modrá</span>'),
            '#46d6db' => array('data-content' => '<span class="label" style="background-color: #46d6db;">tyrkysová</span>'),
            '#7ae7bf' => array('data-content' => '<span class="label" style="background-color: #7ae7bf;">světle tyrkysová</span>'),
            '#51b749' => array('data-content' => '<span class="label" style="background-color: #51b749;">zelená</span>'),
            '#fbd75b' => array('data-content' => '<span class="label" style="background-color: #fbd75b;">žlutá</span>'),
            '#ffb878' => array('data-content' => '<span class="label" style="background-color: #ffb878;">oranžová</span>'),
            '#ff887c' => array('data-content' => '<span class="label" style="background-color: #ff887c;">lososová</span>'),
            '#dc2127' => array('data-content' => '<span class="label" style="background-color: #dc2127;">červená</span>'),
            '#dbadff' => array('data-content' => '<span class="label" style="background-color: #dbadff;">fialová</span>'),
            '#e1e1e1' => array('data-content' => '<span class="label" style="background-color: #e1e1e1;">šedá</span>')
        )
    )) ?>
         *
         *
         */


        $data = self::find()->all();

        return $data;

    }

}