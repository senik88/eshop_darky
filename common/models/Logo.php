<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 12.02.2016
 * Time: 18:28
 */

namespace common\models;


use Exception;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Class Logo
 * @package common\models
 *
 * @property integer logo_pk
 * @property string zdroj
 * @property integer sirka
 * @property integer vyska
 * @property string hash
 *
 */
class Logo extends ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $soubor;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @param $pk
     * @return Logo|null
     */
    public function nactiPodlePk($pk)
    {
        $sql = "SELECT * FROM logo WHERE logo_pk = :pk";
        $params = [':pk' => $pk];

        return $this->_nacti($sql, $params);
    }

    /**
     * @param $hash
     * @return Logo|null
     */
    public function nactiPodleHashe($hash)
    {
        $sql = "SELECT * FROM logo WHERE hash = :hash";
        $params = [':hash' => $hash];

        return $this->_nacti($sql, $params);
    }

    /**
     * @param $sql
     * @param $params
     * @return Logo|null
     */
    protected function _nacti($sql, $params = [])
    {
        $data = \Yii::$app->db->createCommand($sql, $params)->queryOne();

        if ($this->load($data, '')) {
            return $this;
        } else {
            return null;
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function ulozZeSouboru()
    {
        $filename = $this->soubor->baseName . '.' . $this->soubor->extension;
        $cesta = $this->vratCestu() . DIRECTORY_SEPARATOR . $filename;
        $i = 0;

        while (file_exists($cesta)) {
            $i++;
            $filename = $this->soubor->baseName . '-' . $i . '.' . $this->soubor->extension;
            $cesta = $this->vratCestu() . DIRECTORY_SEPARATOR . $filename;
        }

        if ($this->soubor->saveAs($cesta)) {
            list($sirka, $vyska) = getimagesize($cesta);

            $this->sirka = $sirka;
            $this->vyska = $vyska;
            $this->hash = md5(time().$filename);
            $this->zdroj = $filename;

            if ($this->save()) {
                $this->refresh();

                return $this->logo_pk;
            } else {
                throw new Exception("nepodarilo se ulozit logo do databaze ({$cesta})");
            }
        } else {
            throw new Exception("nepodarilo se ulozit logo ({$cesta})");
        }
    }

    /**
     * root/common/runtime/obrazky/loga
     * @param bool $soubor
     * @return string
     * @throws LogoPathNotExistsException
     * @throws LogoPathNotWritableException
     */
    public function vratCestu($soubor = false)
    {
        $this->overCestu();

        $cesta = \Yii::getAlias('@common') . DIRECTORY_SEPARATOR
            . 'runtime' . DIRECTORY_SEPARATOR
            . 'obrazky' . DIRECTORY_SEPARATOR
            . 'loga';

        if ($soubor) {
            $cesta .= DIRECTORY_SEPARATOR . $this->zdroj;
        }

        return $cesta;
    }

    /**
     * @return bool
     * @throws LogoPathNotExistsException
     * @throws LogoPathNotWritableException
     */
    protected function overCestu()
    {
        $path = \Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'runtime';
        if (!file_exists($path)) {
            throw new LogoPathNotExistsException($path);
        } else if (!is_writable($path)) {
            throw new LogoPathNotWritableException($path);
        }

        $path .= DIRECTORY_SEPARATOR . 'obrazky';
        if (!file_exists($path)) {
            throw new LogoPathNotExistsException($path);
        } else if (!is_writable($path)) {
            throw new LogoPathNotWritableException($path);
        }

        $path .= DIRECTORY_SEPARATOR . 'loga';
        if (!file_exists($path)) {
            throw new LogoPathNotExistsException($path);
        } else if (!is_writable($path)) {
            throw new LogoPathNotWritableException($path);
        }

        return true;
    }
}

/**
 * Class LogoPathNotExistsException
 * @package common\models
 */
class LogoPathNotExistsException extends \Exception
{
    public function __construct($path, $code = 0, Exception $previous = null)
    {
        parent::__construct("Cesta k logum neexistuje: {$path}", $code, $previous);
    }
}

/**
 * Class LogoPathNotExistsException
 * @package common\models
 */
class LogoPathNotWritableException extends \Exception
{
    public function __construct($path, $code = 0, Exception $previous = null)
    {
        parent::__construct("Cesta k logum neni zapisovatelna: {$path}", $code, $previous);
    }
}