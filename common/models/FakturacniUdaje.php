<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 30.09.2015
 * Time: 20:49
 */

namespace common\models;


use yii\helpers\ArrayHelper;

/**
 * Class FakturacniUdaje
 * @package common\models
 */
class FakturacniUdaje extends AbstractUdaje
{

    const SCENARIO_OBJEDNAVKA = 'objednavka';

    public $fakturacni_udaje_pk;
    public $jmeno;
    public $prijmeni;
    public $titulPred;
    public $titulZa;
    public $obchodni_jmeno;
    public $ic;
    public $dic;
    public $ulice;
    public $mesto;
    public $psc;
    public $zeme;

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_OBJEDNAVKA] = ['jmeno', 'prijmeni', 'obchodni_jmeno', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'zeme', 'fakturacni_udaje_pk'];

        return $scenarios;
    }

    /**
     * @return array
     */
    public function rules()
    {
        if ($this->scenario == self::SCENARIO_OBJEDNAVKA) {
            return [
                [['jmeno', 'prijmeni', 'ulice', 'mesto', 'psc', 'zeme'], 'required'/*, 'whenClient' => 'function(attribute, value) {
                    return $("#objednavka-fakturacni_udaje_pk").val() == "#";
                }'*/],
                [['obchodni_jmeno', 'ic', 'dic', 'fakturacni_udaje_pk'], 'safe'],
                [['ic'], 'required', 'when' => function($model) {
                    return $model->obchodni_jmeno != null && $model->obchodni_jmeno != '';
                }, 'whenClient' => 'function(attribute, value) {
                              return $("#fakturacniudaje-obchodni_jmeno").val() != "";
                          }'],
                ['ic', 'icValidator']
            ];
        } else {
            return [
                [$this->attributes(), 'safe']
            ];
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'jmeno' => 'Jméno',
            'prijmeni' => 'Příjmení',
            'titulPred' => 'Titul před',
            'titulZa' => 'Titul za',
            'ulice' => 'Ulice, č.p.',
            'mesto' => 'Město',
            'psc' => 'PSČ',
            'zeme' => 'Země',
            'obchodni_jmeno' => 'Obchodní jméno',
            'ic' => 'IČ',
            'dic' => 'DIČ'
        ];
    }

    public function icValidator($attribute, $params)
    {
        if (!preg_match('#^[0-9]+$#', $this->$attribute)) {
            $this->addError($attribute, "Neplatné IČ");
        }
    }

    /**
     * @return bool|null|string
     * @throws \Exception
     */
    public function ulozUzivateli()
    {
        $sql = "select * from uzivatel_uloz_fakturacni(
              :uzivatel_pk
            , :fa_udaje_pk
            , :jmeno
            , :prijmeni
            , :titul_pred
            , :titul_za
            , :obchodni_jmeno
            , :ic
            , :dic
            , :ulice
            , :mesto
            , :psc
            , :zeme
        )";

        $params = [
              ':uzivatel_pk' => $this->uzivatel_pk
            , ':fa_udaje_pk' => ($this->fakturacni_udaje_pk == '' ? null : $this->fakturacni_udaje_pk)
            , ':jmeno' => $this->jmeno
            , ':prijmeni' => $this->prijmeni
            , ':titul_pred' => $this->titulPred
            , ':titul_za' => $this->titulZa
            , ':obchodni_jmeno' => $this->obchodni_jmeno
            , ':ic' => $this->ic
            , ':dic' => $this->dic
            , ':ulice' => $this->ulice
            , ':mesto' => $this->mesto
            , ':psc' => $this->psc
            , ':zeme' => $this->zeme
        ];

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        $pk = $command->queryScalar();
        if ($pk < 0) {
            \Yii::error("neocekavany vysledek procedury uzivatel_uloz_fakturacni = ({$pk}), data: " . var_export($params, true));
            return false;
        } else {
            $this->fakturacni_udaje_pk = $pk;
            return $pk;
        }
    }

    /**
     * @return mixed
     */
    public function ulozObjednavce()
    {
        $sql = "select * from objednavka_uloz_fakturacni(
              :objednavka_pk
            , :fa_udaje_pk
            , :jmeno
            , :prijmeni
            , :titul_pred
            , :titul_za
            , :obchodni_jmeno
            , :ic
            , :dic
            , :ulice
            , :mesto
            , :psc
            , :zeme
        )";

        $params = [
              ':objednavka_pk' => $this->objednavka_pk
            , ':fa_udaje_pk' => ($this->fakturacni_udaje_pk == '' ? null : $this->fakturacni_udaje_pk)
            , ':jmeno' => $this->jmeno
            , ':prijmeni' => $this->prijmeni
            , ':titul_pred' => $this->titulPred
            , ':titul_za' => $this->titulZa
            , ':obchodni_jmeno' => $this->obchodni_jmeno
            , ':ic' => $this->ic
            , ':dic' => $this->dic
            , ':ulice' => $this->ulice
            , ':mesto' => $this->mesto
            , ':psc' => $this->psc
            , ':zeme' => $this->zeme
        ];

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        $pk = $command->queryScalar();
        if ($pk < 0) {
            \Yii::error("neocekavany vysledek procedury objednavka_uloz_fakturacni = ({$pk}), data: " . var_export($params, true));
            return false;
        } else {
            $this->fakturacni_udaje_pk = $pk;
            return $pk;
        }
    }

    /**
     * @param $fakturacni_udaje_pk
     * @return FakturacniUdaje
     */
    public function nactiPodlePk($fakturacni_udaje_pk)
    {
        $sql = "select * from fakturacni_udaje where fakturacni_udaje_pk = :pk";
        $params = [
            'pk' => $fakturacni_udaje_pk
        ];

        return $this->nactiPodleSql($sql, $params);
    }

    /**
     * @param $sql
     * @param $params
     * @return FakturacniUdaje
     */
    public function nactiPodleSql($sql, $params)
    {
        $data = \Yii::$app->db->createCommand($sql, $params)->queryOne();

        if ($data == false) {
            ddd('chyba', $sql, $params);
        }

        if (!$this->load($data, '')) {
            ddd('not load', $data);
        }

        return $this;
    }

    /**
     * todo to formatovani mozna udelat v PHPku, mohlo by se to treba i radkovat
     * @param $uzivatel
     * @return array
     */
    public function vratUzivatelovyProDropdown($uzivatel)
    {
        $sql = "
            SELECT
                fu.*
                , CASE
                    WHEN nullif(fu.obchodni_jmeno, '') IS NOT NULL AND fu.prijmeni IS NOT NULL THEN
                        format('%s - %s, %s %s, %s, %s %s, %s', fu.obchodni_jmeno, fu.ic, fu.prijmeni, fu.jmeno, fu.ulice, fu.psc, fu.mesto, fu.zeme)
                    WHEN nullif(fu.obchodni_jmeno, '') IS NOT NULL THEN
                        format('%s - %s, %s, %s %s, %s', fu.obchodni_jmeno, fu.ic, fu.ulice, fu.psc, fu.mesto, fu.zeme)
                    ELSE
                        format('%s %s, %s, %s %s, %s', fu.prijmeni, fu.jmeno, fu.ulice, fu.psc, fu.mesto, fu.zeme)
                END AS udaje
                , CASE
                    WHEN fu.obchodni_jmeno IS NOT NULL AND fu.prijmeni IS NOT NULL THEN 1
                    WHEN fu.obchodni_jmeno IS NOT NULL THEN 2
                    ELSE 3
                END AS poradi
            FROM fakturacni_udaje fu
                JOIN uzivatel_fakturacni_udaje ufu ON fu.fakturacni_udaje_pk = ufu.fakturacni_udaje_pk
            WHERE ufu.uzivatel_pk = :pk
            ORDER BY poradi ASC, udaje ASC
        ";

        $data = \Yii::$app->db->createCommand($sql)->bindValue(':pk', $uzivatel)->queryAll();

        return ArrayHelper::map($data, 'fakturacni_udaje_pk', 'udaje');
    }

    /**
     * @return string
     */
    public function vypisBlokove()
    {
        $template = '<ul>';
        {
            if ($this->obchodni_jmeno != null) {
                $template .= '<li>{obchodni_jmeno}</li>';
                $template .= '<li>IČ: {ic}, DIČ: {dic}</li>';
            }

            if ($this->prijmeni != null) {
                $template .= '<li>{jmeno} {prijmeni}</li>';
            }

            $template .= '<li>{ulice}, {psc} {mesto}</li>';
            $template .= '<li>{zeme}</li>';
        }
        $template .= '</ul>';

        return strtr($template, [
            '{obchodni_jmeno}' => $this->obchodni_jmeno,
            '{ic}' => $this->ic,
            '{dic}' => $this->dic,
            '{jmeno}' => $this->jmeno,
            '{prijmeni}' => $this->prijmeni,
            '{ulice}' => $this->ulice,
            '{mesto}' => $this->mesto,
            '{psc}' => $this->psc,
            '{zeme}' => $this->zeme,
        ]);
    }

    /**
     *
     */
    public function vypisRadkove()
    {
        // TODO: Implement vypisRadkove() method.
    }
}