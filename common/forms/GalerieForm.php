<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11.10.2015
 * Time: 11:39
 */

namespace common\forms;


use common\components\helpers\Image;
use common\models\PolozkaObrazek;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class GalerieForm
 * @package common\components\forms
 *
 * @see https://github.com/blueimp/jQuery-File-Upload/wiki
 * @see https://github.com/2amigos/yii2-file-upload-widget
 */
class GalerieForm extends Model
{
    /**
     * @var
     */
    public $polozka_pk;

    /**
     * @var UploadedFile
     */
    public $soubor;

    /**
     *
     */
    public function uloz()
    {
        if ($this->soubor && $this->soubor instanceof UploadedFile && !empty($this->soubor->tempName)) {
            $name = md5($this->soubor->name) . '.' . $this->soubor->getExtension();

            $mPolozkaObrazek = new PolozkaObrazek();
            $mPolozkaObrazek->polozka_pk = $this->polozka_pk;
            $mPolozkaObrazek->uzivatel_pk = Yii::$app->user->id;
            $mPolozkaObrazek->hlavni = false;
            $mPolozkaObrazek->popisek = $name;
            $mPolozkaObrazek->nahrano = date('Y-m-d H:i:s');

            $large = Image::saveTemporaryImage($this->savePath(), $this->soubor->tempName, $name, Image::LARGE_IMAGE);
            $small = Image::saveTemporaryImage($this->savePath(true), $this->soubor->tempName, $name, Image::SMALL_IMAGE);

            $mPolozkaObrazek->zdroj = $this->getImageUrl() . DIRECTORY_SEPARATOR . $name;
            $mPolozkaObrazek->nahled = $this->getThumbnailUrl() . DIRECTORY_SEPARATOR . $name;

            if ($mPolozkaObrazek->save()) {
                return $name;
            } else {
                @unlink($large);
                @unlink($small);

                return false;
            }

        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        $web = Yii::getAlias('@web')
            . DIRECTORY_SEPARATOR
            . 'images'
            . DIRECTORY_SEPARATOR
            . 'polozky'
            . DIRECTORY_SEPARATOR
            . $this->polozka_pk
        ;

        return strtr($web, [
            'backend' => 'frontend'
        ]);
    }

    /**
     * @return string
     */
    public function getThumbnailUrl()
    {
        $web = Yii::getAlias('@web')
            . DIRECTORY_SEPARATOR
            . 'images'
            . DIRECTORY_SEPARATOR
            . 'polozky'
            . DIRECTORY_SEPARATOR
            . 'nahledy'
            . DIRECTORY_SEPARATOR
            . $this->polozka_pk
        ;

        return strtr($web, [
            'backend' => 'frontend'
        ]);
    }

    /**
     * @param bool $nahled
     * @return string
     */
    public function savePath($nahled = false)
    {
        $path = Yii::getAlias('@frontend')
            . DIRECTORY_SEPARATOR
            . 'web'
            . DIRECTORY_SEPARATOR
            . 'images'
            . DIRECTORY_SEPARATOR
            . 'polozky';

        if (!file_exists($path)) {
            mkdir($path);
        }

        if ($nahled) {
            $path .= DIRECTORY_SEPARATOR . 'nahledy';
            if (!file_exists($path)) {
                mkdir($path);
            }
        }

        $path .= DIRECTORY_SEPARATOR . $this->polozka_pk;
        if (!file_exists($path)) {
            mkdir($path);
        }

        return $path;
    }
}